import SwiftUI

private extension ButtonStyle {
    var foregroundColor: Color { .white }
    var padding: CGFloat { 15 }
    var cornerRadius: CGFloat { 15 }
    var pressedColorOpacity: Double { 0.5 }
}

struct PrimaryButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(foregroundColor)
            .font(Font.body.bold())
            .padding(padding)
            .padding(.horizontal, 20)
            .background(Color("btn_primary").opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
    }
}

struct PrimaryButtonMapStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(foregroundColor)
            .font(Font.body.bold())
            .padding(8)
            .padding(.horizontal, 4)
            .background(Color("btn_primary").opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
    }
}

struct SecondaryButtonMapStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(foregroundColor)
            .font(Font.body.bold())
            .padding(8)
            .padding(.horizontal, 4)
            .background(Color("jcor_gold").opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
    }
}


struct PrimaryButtonStyleJcoR: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(foregroundColor)
            .font(Font.body.bold())
            .padding(padding)
            .padding(.horizontal, 20)
            .background(Color("jcor_gold").opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
    }
}


struct SecondaryButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(Color("btn_primary"))
            .font(Font.body.bold())
            .padding(padding)
            .padding(.horizontal, 20)
            .background(Color.white.opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(Color("btn_primary"), lineWidth: 1))
    }
}

struct SecondaryButtonStyleJcoR: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(Color("jcor_gold"))
            .font(Font.body.bold())
            .padding(padding)
            .padding(.horizontal, 20)
            .background(Color.white.opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(Color("jcor_gold"), lineWidth: 1))
    }
}

struct RedButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundColor(Color.red)
            .font(Font.body.bold())
            .padding(padding)
            .padding(.horizontal, 20)
            .background(Color.white.opacity(
                configuration.isPressed ? pressedColorOpacity : 1
            ))
            .cornerRadius(cornerRadius)
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(Color.red, lineWidth: 1))
    }
}
