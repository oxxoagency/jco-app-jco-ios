//
//  Util.swift
//  Jco
//
//  Created by Ed on 10/01/21.
//

import Foundation
import CommonCrypto

class Util {
    func HttpRequest(url: String, token: String, body: Data) -> String {
        print("HTTP")
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        var returnString = ""
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(token, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = body

        URLSession.shared.dataTask(with: request) {
            (data, response, error) in

            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a simple String
            if let data = data, let dataString = String(data: data, encoding: .utf8) {
//                print("Response data string:\n \(dataString)")
                returnString = dataString
            }
        }.resume()
        
        return returnString
    }
    
    func md5Hash (str: String) -> String {
        if let strData = str.data(using: String.Encoding.utf8) {
            /// #define CC_MD5_DIGEST_LENGTH    16          /* digest length in bytes */
            /// Creates an array of unsigned 8 bit integers that contains 16 zeros
            var digest = [UInt8](repeating: 0, count:Int(CC_MD5_DIGEST_LENGTH))
     
            /// CC_MD5 performs digest calculation and places the result in the caller-supplied buffer for digest (md)
            /// Calls the given closure with a pointer to the underlying unsafe bytes of the strData’s contiguous storage.
            strData.withUnsafeBytes {
                // CommonCrypto
                // extern unsigned char *CC_MD5(const void *data, CC_LONG len, unsigned char *md) --|
                // OpenSSL                                                                          |
                // unsigned char *MD5(const unsigned char *d, size_t n, unsigned char *md)        <-|
                CC_MD5($0.baseAddress, UInt32(strData.count), &digest)
            }
     
     
            var md5String = ""
            /// Unpack each byte in the digest array and add them to the md5String
            for byte in digest {
                md5String += String(format:"%02x", UInt8(byte))
            }
     
            // MD5 hash check (This is just done for example)
            if md5String.uppercased() == "8D84E6C45CE9044CAE90C064997ACFF1" {
                print("Matching MD5 hash: 8D84E6C45CE9044CAE90C064997ACFF1")
            } else {
                print("MD5 hash does not match: \(md5String)")
            }
            return md5String
     
        }
        return ""
    }
    
    func addTS (str: String) -> String {
        let NSstr = NSMutableString(string: str)
        if str.count > 6 {
            NSstr.insert(".", at: str.count - 6)
            NSstr.insert(".", at: str.count - 2)
        } else if str.count > 3 {
            NSstr.insert(".", at: str.count - 3)
        }
        return String(NSstr)
    }
}
