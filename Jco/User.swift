//
//  User.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import Foundation

import Combine

class User: ObservableObject {
//    @Published var tokenIsActive = false
    @Published var tokenIsActive = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
}
