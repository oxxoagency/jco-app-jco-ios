//
//  FavoriteGetRes.swift
//  Jco
//
//  Created by Ed on 18/10/21.
//

import Foundation

struct FavoriteGetRes: Codable, Hashable {
    let status: Int
    let data: [FavoriteMenu]?
}

struct FavoriteMenu: Codable, Hashable {
    let menuCode: String
    let categoryName: String
    let categoryTitle: String
    let menuName: String
    let menuImage: String
    let isPromo: Int
    let isFreedelivery: Int
    let menuPrice: Int
    let isFavorite: Int
}

struct FavoriteSetRes: Codable, Hashable {
    let status: Int
}
