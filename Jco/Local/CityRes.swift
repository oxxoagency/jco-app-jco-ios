//
//  CityData.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct CityRes: Codable, Hashable {
    let id = UUID()
    let statusCode: Int
    let data: [City]
}
