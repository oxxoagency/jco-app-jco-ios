//
//  RefreshTokenRes.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct RefreshTokenRes: Codable, Hashable {
    var statusCode: Int
    var accessToken: String?
    var error: String?
}
