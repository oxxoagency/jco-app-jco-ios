//
//  LocationQueryRes.swift
//  Jco
//
//  Created by Ed on 12/06/21.
//

import Foundation

struct LocationQueryRes: Decodable {
    let results: [LocationQuery]
    let status: String
}
