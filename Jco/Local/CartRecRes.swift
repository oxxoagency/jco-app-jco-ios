//
//  CartRecRes.swift
//  Jco
//
//  Created by Ed on 27/09/21.
//

import Foundation

struct CartRecRes: Codable, Hashable {
    let statusCode: Int
    let data: [CartRecMenu]
}

struct CartRecMenu: Codable, Hashable {
    let menuCode: String
    let menuName: String
    let menuNameEn: String
    let menuImg: String
    let menuPrice: Int
    let isOrder: Int?
}
