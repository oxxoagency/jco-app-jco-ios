//
//  CartCheckOutRes.swift
//  Jco
//
//  Created by Ed on 15/06/21.
//

import Foundation

struct CartCheckOutRes: Codable, Hashable {
    var token: String?
    var redirectUrl: String?
}
