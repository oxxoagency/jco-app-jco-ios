//
//  OrderDetailRes.swift
//  Jco
//
//  Created by Ed on 03/07/21.
//

import Foundation

struct OrderDetailRes: Codable, Hashable {
    let id = UUID()
    let statusCode: Int
    let data: DetailResData?
}

struct DetailResData: Codable, Hashable {
    let id = UUID()
    let orderId: String
    let orderStatus: Int
    let deliveryType: Int
    let orderAddress: String?
    let orderAddressinfo: String?
    let orderCity: String
    let orderOutletName: String
    let orderOutletAddress: String
    let orderTime: String
    let orderDetail: [DetailResProduct]
    let memberName: String
    let orderSubtotal: Int
//    let deliveryFee: Int
    let orderFee: Int
    let orderPromo: Int
//    let freeDelivery: Int
//    let totalDeliveryFee: Int?
    let orderTotal: Int
    let paymentMethod: String
    let orderPayment: DetailResPayment
//    let trxDetail: TrxDetailData
    let jpointInfo: JpointData?
    let orderStatusName: String?
    let paymentName: String?
    let orderDelivery: [OrderDelivery]?
}

struct OrderDelivery: Codable, Hashable {
    let orderDeliveryId: String?
    let status: String?
    let courier: Courier?
    let statusName: String?
    let trackingURL: String?
}

struct Courier: Codable, Hashable {
    let name: String?
    let phone: String?
    let licensePlate: String?
}

struct DetailResProduct: Codable, Hashable {
    let menuQuantity: Int
    let menuName: String
    let menuCode: String
    let menuPrice: Int
    let menuDetail: String?
    let menuImg: String
}

struct DetailResPayment: Codable, Hashable {
    let gwTransactionTime: String
    let gwTransactionExpireTime: String
    let gwPaymentType: String
    let gwStatusCode: String
    let gwTransactionStatus: String
    let gwVaNumbers: String?
    let paymentName: String?
    let paymentIcon: String?
}

struct TrxDetail: Codable, Hashable {
    let statusCode: Int
    let data: TrxDetailData
}

struct TrxDetailData: Codable, Hashable {
    let trxGwVaNumbersVaNumber: String?
    let trxGwVaNumbersBank: String?
    let trxGwBillKey: String?
}

struct JpointData: Codable, Hashable {
    let jpointEarn: Int
    let jpointUsed: Int
    let description: String
}
