//
//  City.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct City: Codable, Hashable {
    let id = UUID()
    let city: String
    let province: String
    let deliveryFee: Int
}
