//
//  UserAddressNewRes.swift
//  Jco
//
//  Created by Ed on 13/06/21.
//

import Foundation

struct UserAddressNewRes: Codable {
    let status: Int
    let message: String
}
