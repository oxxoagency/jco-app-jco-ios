//
//  OrderRes.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct OrderRes: Codable, Hashable {
    let id = UUID()
    let status: Int?
    let statusCode: String?
    let message: String?
    let error: String?
    let data: [OrderNew]?
}

struct OrderNew: Codable, Hashable {
    let orderId: String
    let orderStatus: Int
    let orderPaymentStatus: Int
    let orderTime: String
    let orderTotal: Int
    let orderTotalItem: Int?
    let menuList: String?
    let menuImg: String?
    let outletName: String?
    let outletAddress: String?
    let outletCity: String?
    let orderStatusName: String?
}

struct Order: Codable, Hashable {
    let id = UUID()
    let orderId: String
    let orderStatus: String
    let orderTime: String
    let orderCity: String
    let orderOutletId: String
    let orderOutletCode: String
//    let orderDetail: [OrderDetail]
}

struct OrderDetail: Codable, Hashable {
    let id = UUID()
    let menuCode: String?
    let menuName: String?
    let menuQuantity: String?
    let menuUnitprice: String?
    let menuPrice: String?
    let menuDetail: String?
    let menuImage: [OrderDetailImg]?
}

struct OrderDetailImg: Codable, Hashable {
    let image: String
}

