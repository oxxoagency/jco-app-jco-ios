//
//  LoginAPIResponse.swift
//  Jco
//
//  Created by Ed on 23/01/21.
//

import Foundation
struct LoginAPIResponse: Codable {
    let accessToken: String?
    let refreshToken: String?
    let statusCode: Int?
    let error: String?
}

