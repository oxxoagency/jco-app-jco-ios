//
//  Response.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 26/07/22.
//

import Foundation

struct Response: Codable, Hashable {
    let statusCode: Int?
    let message: String?
    let error: String?
}
