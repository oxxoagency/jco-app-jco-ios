//
//  BannerDetailRes.swift
//  Jco
//
//  Created by Ed on 19/09/21.
//

import Foundation

struct BannerDetailRes: Codable, Hashable {
    let statusCode: Int
    let data: BannerDetailResData
}

struct BannerDetailResData: Codable, Hashable {
    let bannerId: String
    let bannerImg: String
    let bannerDescription: String
    let startDate: String
    let endDate: String
    let relatedMenu: [BannerDetailResMenu]
}

struct BannerDetailResMenu: Codable, Hashable {
    let idMenu: String
    let menuCode: String
    let menuName: String
    let menuNameEn: String
    let menuImage: String
    let isPromo: Int
    let menuPrice: Int
    let menuDetail: String?
}
