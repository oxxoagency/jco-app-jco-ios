//
//  DeleteAddressRes.swift
//  Jco
//
//  Created by Ed on 12/11/21.
//

import Foundation

struct DeleteAddressRes: Codable, Hashable {
    let status: Int
}
