//
//  PaymentListRes.swift
//  Jco
//
//  Created by Ed on 11/10/21.
//

import Foundation

struct PaymentListRes: Codable, Hashable {
    let statusCode: Int
    let data: [PaymentList]
}

struct PaymentList: Codable, Hashable {
    let paymentMethod: String
    let paymentName: String
    let paymentIcon: String
}
