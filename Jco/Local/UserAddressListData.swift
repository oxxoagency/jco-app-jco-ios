//
//  UserAddressListData.swift
//  Jco
//
//  Created by Ed on 03/04/21.
//

import Foundation

struct UserAddressListData: Codable, Hashable {
    let id = UUID()
    let memberAddressId: Int
    let addressLabel: String
    let recipientName: String
    let recipientPhone: String
    let cityordistrict: String
    let address: String
    let addressDetails: String
    let latitude: String
    let longitude: String
    let recipientPostcode: Int
}
