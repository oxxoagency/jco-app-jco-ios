//
//  MenuDetailAPIResponse.swift
//  Jco
//
//  Created by Ed on 11/01/21.
//

import Foundation
struct MenuDetailAPIResponse: Codable {
    let data: MenuDetailData
}

struct MenuDetailData: Codable, Identifiable {
    let id = UUID()
    let menuCode: String
    let categoryName: String
    let categoryTitle: String
    let menuName: String
    let menuAmount: Int
    let menuPrice: Int
    let menuImage: [MenuImg]
    let menuDesc: String?
    let variantNum: Int?
    let details: [MenuDetail]?
//    let variantGroup: [MenuDetailVariantGroup]
//    let url: String
}

struct MenuDetail: Codable, Identifiable, Hashable {
    let id = UUID()
    let size: String?
    let temp: String?
    let packageName: String?
    let packageImage: String?
    let packageDescription: String?
    let menuCode: String?
    let menuPrice:Int?
    let menuImage: String?
    let packageDonutList: [MenuDonutItem]?
}

struct MenuDonutItem: Codable, Identifiable, Hashable {
    let id = UUID()
    let menuName: String?
    let menuImg: String?
}

struct MenuImg: Codable, Identifiable, Hashable {
    let id = UUID()
    let image: String
}
