//
//  CartRowItem.swift
//  Jco
//
//  Created by Ed on 17/04/21.
//

import Foundation

struct CartRowItem {
    let menuName: String
    let menuCode: String
    let menuImg: String
}
