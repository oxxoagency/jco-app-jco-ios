//
//  MenuVariantSelected.swift
//  Jco
//
//  Created by Ed on 20/05/21.
//

import Foundation

struct MenuVariantSelected {
    var packageName: String
    var packageQty: Int
}
