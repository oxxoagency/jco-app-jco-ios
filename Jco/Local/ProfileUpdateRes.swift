//
//  ProfileUpdateRes.swift
//  Jco
//
//  Created by Ed on 18/08/21.
//

import Foundation

struct UserProfileRes: Codable {
    let statusCode: Int
    let data: UserProfileResData
}

struct UserProfileResData: Codable {
    let memberEmail: String
    let memberName: String
    let memberPhone: String
    let memberPhone2: String
    let memberGender: Int
    let memberDob: String
}

struct ProfileUpdateRes: Codable, Hashable {
    let statusCode: Int
    let data: ProfileUpdateResData
}

struct ProfileUpdateResData: Codable, Hashable {
    let memberName: String
    let memberPhone: String
}

struct PasswordUpdateRes: Codable, Hashable {
    let statusCode: Int
}

struct JpointRes: Codable, Hashable {
    let statusCode: Int
    let data: JpointResData
}

struct JpointResData: Codable, Hashable {
    let point: Int
}

struct JpointMutationRes: Codable, Hashable {
    let statusCode: Int
    let data: [JpointMutationResData]
}

struct JpointMutationResData: Codable, Hashable {
    let id: Int
    let pointStatus: String
    let pointUsed: Int
    let pointEarn: Int
    let description: String
    let createdAt: String
}
