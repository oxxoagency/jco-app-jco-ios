//
//  LocationLatLng.swift
//  Jco
//
//  Created by Ed on 13/06/21.
//

import Foundation

struct LocationLatLng: Codable, Hashable {
    var lat: Double
    var lng: Double
}
