//
//  UserCityRes.swift
//  Jco
//
//  Created by Ed on 15/07/21.
//

import Foundation

struct UserCityRes: Codable, Hashable {
    let statusCode: Int
    let data: [UserCityOutlet]
}

struct UserCityOutlet: Codable, Hashable {
    let id = UUID()
    let outletId: String
    let outletCode: String
    let outletCity: String
}
