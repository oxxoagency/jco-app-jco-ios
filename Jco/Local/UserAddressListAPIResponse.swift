//
//  UserAddressListAPIResponse.swift
//  Jco
//
//  Created by Ed on 03/04/21.
//

import Foundation

struct UserAddressListAPIResponse: Codable {
    let data: [UserAddressListData]
}
