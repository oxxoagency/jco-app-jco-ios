//
//  Outlet.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct Outlet: Codable, Hashable {
    let id = UUID()
    let outletId: String
    let outletCode: String
    let outletName: String
    let outletPhone: String
    let outletAddress: String
    let outletPostcode: String
    let outletCity: String
    let outletProvince: String
    let outletLongitude: String
    let outletLatitude: String
    let outletTimezone: String
}
