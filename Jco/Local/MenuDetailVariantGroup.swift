//
//  MenuDetailVariantGroup.swift
//  Jco
//
//  Created by Ed on 11/01/21.
//

import Foundation
struct MenuDetailVariantGroup: Codable, Identifiable, Hashable {
    let id = UUID()
    let variantGroupName: String
    let variant: [MenuDetailVariant]
    let limit: Int
//    let url: String
}
