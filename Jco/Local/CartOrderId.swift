//
//  CartOrderId.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct CartOrderId: Codable, Hashable {
    var orderId: Int
    var orderDetail: [CartOrderDetail]?
    var orderFee: Int?
    var orderPromo: Int?
    var oderLoyalty: Int?
    var orderTotal: Int
}

struct CartOrderDetail: Codable, Hashable {
    let id = UUID()
    var menuCode: String?
    var menuName: String?
    var menuQuantity: Int?
    var menuUnitprice: Int?
    var menuPrice: Int?
    var menuDetail: String?
}
