//
//  MenuDetailVariant.swift
//  Jco
//
//  Created by Ed on 12/01/21.
//

import Foundation
struct MenuDetailVariant: Codable, Identifiable, Hashable {
    let id = UUID()
    let variantName: String
//    let url: String
}
