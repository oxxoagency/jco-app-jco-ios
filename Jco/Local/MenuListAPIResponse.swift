//
//  MenuListAPIResponse.swift
//  Testlogin3
//
//  Created by Ed on 07/01/21.
//

import Foundation

struct MenuListAPIResponse: Codable, Identifiable {
//    let count: Int
//    let next: String
//    let previous: String?
    let id = UUID()
    let user: HomeUser
    let products: [MenuListItem]
    let category: [CategoryListItem]
    let promos: [HomePromo]
    let version: HomeVersion
    let jpoint: Jpoint
}

struct HomeUser: Codable, Hashable {
    var statusCode: Int?
    var message: String?
    var memberEmail: String?
    var memberName: String?
    var memberPhone: String?
    var memberPhone2: String?
}

struct MenuListItem: Codable, Hashable {
//    let id = UUID()
    let menuName: String
    let menuImage: String
    let menuCode: String
    let menuPrice: Int
    let menuNormalprice: Int
    let isPromo: Int
    let isFreedelivery: Int
    let isFavorite: Int?
//    let url: String
}

struct CategoryListItem: Codable, Hashable {
//    let id = UUID()
    let categoryTitle: String
    let categoryName: String
}

struct HomePromo: Codable, Identifiable, Hashable {
    let id = UUID()
    let bannerId: String
    let bannerImg: String
}

struct HomeVersion: Codable, Hashable {
    let versionCodeLatest: String
    let versionBuildLatest: Int
    let mustUpdate: Bool
    let updateImg: String?
    let updateText: String?
    let updateUrl: String?
}

struct Jpoint: Codable, Hashable {
    let point: Int?
    let pointTier: JpointTier?
}

struct JpointTier: Codable, Hashable {
    let name: String
}
