//
//  MenuListByCategoryRes.swift
//  Jco
//
//  Created by Ed on 07/04/21.
//

import Foundation

struct MenuListByCategoryRes: Codable {
    let data: [MenuListItem]
}
