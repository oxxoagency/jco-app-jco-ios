//
//  OutletNearestRes.swift
//  Jco
//
//  Created by Ed on 28/07/21.
//

import Foundation

struct OutletNearestRes: Codable, Hashable {
    let statusCode: Int
    let data: [OutletNearest]
}

struct OutletNearest: Codable, Hashable {
    let outletId: String
    let outletCode: String
    let outletName: String
    let outletAddress: String
    let outletPostcode: String
    let outletCity: String
    let outletProvince: String
    let outletLatitude: String
    let outletLongitude: String
    let distance: String
}
