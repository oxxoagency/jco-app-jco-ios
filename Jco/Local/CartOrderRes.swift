//
//  CartAddOrderRes.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation

struct CartOrderRes: Codable, Hashable {
    var statusCode: Int
    var data: CartOrderId?
    var error: ErrorOrder?
}

struct ErrorOrder: Codable, Hashable {
    var message: String?
}
