//
//  LocationQuery.swift
//  Jco
//
//  Created by Ed on 12/06/21.
//

import Foundation

struct LocationQuery: Codable, Hashable {
    var formattedAddress: String
    var name: String
    var geometry: LocationGeometry
}

struct LocationQueryNew: Codable, Hashable {
    var formattedAddress: String
    var name: String
    var id: String
}

