//
//  CartFilterRes.swift
//  Jco
//
//  Created by Ed on 22/07/21.
//

import Foundation


struct CartFilterRes: Codable, Hashable {
    let statusCode: Int
    let data: CartFilterData?
}

struct CartFilterData: Codable, Hashable {
    let cartOrders: [CartFilterOrder]
    let ecoBag: CartFilterEcobag
    let subtotal: Int
    let deliveryFee: Int
    let freeDelivery: Int
    let totalDeliveryFee: Int
    let grandtotal: Int
    let jpointInfo: JPointInfo?
    let promo: CartPromo?
    let error: CartError?
}

struct CartFilterOrder: Codable, Hashable {
    let menuCode: String
    let menuName: String
    let menuImage: String
    let menuQuantity: Int
    let menuPrice: Int
    let subTotal: Int
    let menuDetail: String
}

struct CartFilterEcobag: Codable, Hashable {
    let ecobagCode: String
    let ecobagName: String
    let ecobagQuantity: Int
    let ecobagSubtotal: Int
}

struct JPointInfo: Codable, Hashable {
  let jpointEarn: Int
  let jpointUsed: Int
}

struct CartPromo: Codable, Hashable {
    let promoName: String?
    let promoValue: Int?
    let promoMessage: String?
}

struct CartError: Codable, Hashable {
    let type: String?
    let message: String?
    let couponCode: String?
}
