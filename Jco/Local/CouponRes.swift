//
//  CouponRes.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 23/05/22.
//

import Foundation

struct CouponRes: Identifiable, Codable {
    let id = UUID()
    let statusCode: Int?
    let error: String?
    let data: [Coupon]?
}

struct Coupon: Codable, Identifiable, Hashable {
    let id = UUID()
    let couponId: Int?
    let code: String?
    let couponName: String?
    let description: String?
    let image: String?
    let couponValue: Int?
    let couponType: Int?
    let minCart: Int?
    let maxDiscount: Int?
    let couponLimitUser: Int?
    let couponLimitType: Int?
    let couponLimit: Int?
    let paymentType: String?
    let endTime: String?
    let startTime: String?
    let isClaim: Int?
    let isUsed: Int?
    let claimAt: String?
    let couponDescription: String?
}
