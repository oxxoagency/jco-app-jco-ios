//
//  PromoRes.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 14/10/21.
//

import Foundation

struct PromoRes: Identifiable, Codable {
    let id = UUID()
    let data: [Promo]?
}

struct Promo: Codable, Identifiable, Hashable {
    let id = UUID()
    let menuCode: String
    let menuImage: String
    let menuName: String
    let startDate: String?
    let endDate: String?
}
