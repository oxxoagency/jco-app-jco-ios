//
//  LocationGeometry.swift
//  Jco
//
//  Created by Ed on 13/06/21.
//

import Foundation

struct LocationGeometry: Codable, Hashable {
    var location: LocationLatLng
}
