//
//  RegisterAPIResponse.swift
//  Jco
//
//  Created by Ed on 25/02/21.
//

import Foundation
struct RegisterAPIResponse: Codable {
    let status: Int?
    let statusCode: Int?
    let data: RegisterResponseData?
    let token: RegisterResponseToken?
}

struct RegisterResponseData: Codable {
    let memberName: String
    let memberEmail: String
    let memberPhone: String
}

struct RegisterResponseToken: Codable {
    let accessToken: String
    let refreshToken: String
}

struct UserCheckRes: Codable {
    let statusCode: Int
}
