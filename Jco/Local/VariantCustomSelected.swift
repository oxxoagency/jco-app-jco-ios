//
//  VariantCustomSelected.swift
//  Jco
//
//  Created by Ed on 17/06/21.
//

import Foundation

struct VariantCustomSelected {
    var menuName: String
    var menuQty: Int
}
