//
//  MenuService.swift
//  Testlogin3
//
//  Created by Ed on 07/01/21.
//

import Foundation
import Combine

protocol MenuService {
    var apiSession: APIService {get}
    
    func getMenuList() -> AnyPublisher<MenuListAPIResponse, APIError>
}

extension MenuService {
    
//    func getMenuList() -> AnyPublisher<MenuListAPIResponse, APIError> {
//        return apiSession.request(with: APIEndpoint.menuList)
//            .eraseToAnyPublisher()
//    }
}
