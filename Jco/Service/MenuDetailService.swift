//
//  MenuDetailService.swift
//  Jco
//
//  Created by Ed on 11/01/21.
//

import Foundation
import Combine

protocol MenuDetailService {
    var apiSession: APIService {get}
    
    func getMenuDetail(menuCode: String, brand: Int) -> AnyPublisher<MenuDetailAPIResponse, APIError>
}

extension MenuDetailService {
    
    func getMenuDetail(menuCode: String, brand: Int) -> AnyPublisher<MenuDetailAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.menuDetail(menuCode: menuCode, brand: brand))
            .eraseToAnyPublisher()
    }
}
