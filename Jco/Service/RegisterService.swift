//
//  RegisterService.swift
//  Jco
//
//  Created by Ed on 25/02/21.
//

import Foundation
import Combine

protocol RegisterService {
    var apiSession: APIService {get}
    
    func registerUser(name: String, pass: String, repass: String, hp: String, email: String) -> AnyPublisher<RegisterAPIResponse, APIError>
}

extension RegisterService {
    
    func registerUser(name: String, pass: String, repass: String, hp: String, email: String) -> AnyPublisher<RegisterAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.userRegister(name: name, pass: pass, repass: repass, hp: hp, email: email))
            .eraseToAnyPublisher()
    }
}
