//
//  LoginService.swift
//  Jco
//
//  Created by Ed on 23/01/21.
//

import Foundation
import Combine

protocol LoginService {
    var apiSession: APIService {get}
    
    func loginUser(user: String, pass: String) -> AnyPublisher<LoginAPIResponse, APIError>
}

extension LoginService {
    
    func loginUser(user: String, pass: String) -> AnyPublisher<LoginAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.userLogin(user: user, pass: pass))
            .eraseToAnyPublisher()
    }
}
