//
//  JcoApp.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import Firebase
import MidtransKit
import GooglePlaces
import PartialSheet
import GoogleMaps
import FirebaseAuth

@main
struct JcoApp: App {
    @Environment(\.scenePhase) var scenePhase
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate

    let persistenceController = PersistenceController.shared
    var cartViewModel = CartViewModel()
    let sheetManager: PartialSheetManager = PartialSheetManager()
    @StateObject var langSettings = LanguageSettings()
    @StateObject var brandSettings = BrandSettings()
//    init() {
//        FirebaseApp.configure()
//    }
    
    let appearance: UITabBarAppearance = UITabBarAppearance()
    init() {
//        FirebaseApp.configure()
        if #available(iOS 15.0, *) {
            UITabBar.appearance().scrollEdgeAppearance = appearance
        }
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(cartViewModel)
                .environmentObject(langSettings)
                .environmentObject(brandSettings)
                .environmentObject(sheetManager)
                .environment(\.locale, .init(identifier: langSettings.lang))
                .onOpenURL { url in
                          print("Received URL: \(url)")
                          Auth.auth().canHandle(url) // <- just for information purposes
                        }
        }.onChange(of: scenePhase) { _ in
            persistenceController.save()
        }
    }
}

class LanguageSettings: ObservableObject {
    @Published var lang = UserDefaults.standard.object(forKey: "AppLanguage") as? String ?? "id"
}

class BrandSettings: ObservableObject {
    @Published var curBrand = 1
}

class AppDelegate: NSObject, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()
        MidtransConfig.shared().setClientKey(Constants.MIDTRANS_CLIENT_KEY, environment: .production, merchantServerURL: Constants.API_URL_PAY + "transaction/jco/token/")
        MidtransCreditCardConfig.shared().paymentType = .twoclick
        MidtransCreditCardConfig.shared()?.authenticationType = .type3DS
        
        GMSPlacesClient.provideAPIKey("AIzaSyAy5-18rw5HgX7qz8KM0RKKRzjGBW5Bvnc")
        GMSServices.provideAPIKey("AIzaSyAy5-18rw5HgX7qz8KM0RKKRzjGBW5Bvnc")
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { _, _ in }
          )
        } else {
          let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

        Messaging.messaging().delegate = self
        
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            UserDefaults.standard.set(token, forKey: "FCMToken")
//            self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }

        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(userInfo) {
              completionHandler(.noData)
              return
            }
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      print("\(#function)")
      if Auth.auth().canHandle(url) {
        return true
      }
      return false
    }
  }
    
    func application(
      _ application: UIApplication,
      didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
      Messaging.messaging().apnsToken = deviceToken
        print("FCM TOKEN \(deviceToken)")
    }
    
    func userNotificationCenter(
       _ center: UNUserNotificationCenter,
       willPresent notification: UNNotification,
       withCompletionHandler completionHandler:
       @escaping (UNNotificationPresentationOptions) -> Void
     ) {
       completionHandler([[.banner, .sound]])
     }

     func userNotificationCenter(
       _ center: UNUserNotificationCenter,
       didReceive response: UNNotificationResponse,
       withCompletionHandler completionHandler: @escaping () -> Void
     ) {
       completionHandler()
     }

    
    func messaging(
        _ messaging: Messaging,
        didReceiveRegistrationToken fcmToken: String
      ) {
        let tokenDict = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(
          name: Notification.Name("FCMToken"),
          object: nil,
          userInfo: tokenDict)
          
          print("FCM \(fcmToken)")
      }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR FCM \(error.localizedDescription)")
    }

