//
//  APIError.swift
//  Testlogin3
//
//  Created by Ed on 29/12/20.
//

import Foundation

enum APIError: Error {
    case decodingError
    case httpError(Int)
    case unknown
}
