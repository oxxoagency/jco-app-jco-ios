//
//  APIService.swift
//  Testlogin3
//
//  Created by Ed on 29/12/20.
//

import Foundation
import Combine

protocol APIService {
    func request<T: Decodable>(with builder: RequestBuilder) -> AnyPublisher<T, APIError>
}
