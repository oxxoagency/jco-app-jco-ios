//
//  RequestBuilder.swift
//  Testlogin3
//
//  Created by Ed on 29/12/20.
//

import Foundation

protocol RequestBuilder {
    var urlRequest: URLRequest {get}
}
