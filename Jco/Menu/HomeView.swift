//
//  HomeView.swift
//  Testlogin3
//
//  Created by Ed on 08/01/21.
//

import SwiftUI

struct HomeView: View {
    @State var search: String = ""
    let results = [Result(score: 8), Result(score: 5), Result(score: 10), Result(score: 10), Result(score: 10)]
    let cards = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"]
    var productRow: Int
    
    init() {
        self.productRow = Int(ceil(Double(results.count)/2))
    }
    
    var body: some View {
    
        ScrollView {
            VStack(alignment: .leading) {
                ZStack {
                    Image(uiImage: UIImage(named: "home_banner")!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: .infinity)
                    HStack() {
                        HStack() {
                            
                        }.frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(Color.white)
                        .cornerRadius(10)
                        .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.2), radius: 5, x: 0, y: 10)
                    }.frame(maxWidth: .infinity, maxHeight: 80)
                    .offset(y:80)
                    .padding()
                }
                Group {
                    VStack(alignment: .leading, spacing: 10) {
                        Text("Order From")
                        NavigationLink(destination: OutletView()) {
                            Text("J.Co Cihampelas Walk")
                        }
                        HStack {
                            Image(uiImage: UIImage(named: "icon_search")!)
                                .resizable()
                                .scaledToFit()
                                .frame(width: 20)
                            TextField("Find donuts, drinks or yogurt", text: $search)
                        }.padding()
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color("border_primary"), lineWidth: 2)
                        )
                    }.padding(.top, 30)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                }
                Group {
                    VStack(alignment: .leading) {
                        Text("What are you looking for?").padding()
                        ScrollView(.horizontal) {
                            HStack {
                                Button("All") {}.buttonStyle(PrimaryButtonStyle())
                                Button("Promo") {}.buttonStyle(PrimaryButtonStyle())
                                Button("Donuts") {}.buttonStyle(PrimaryButtonStyle())
                                Button("Donuts") {}.buttonStyle(PrimaryButtonStyle())
                            }.padding()
                        }
                    }
                }
                Group {
                    VStack {
                        
                        GridStack(rows: productRow, columns: 2) { row, col in
                            if self.results.indices.contains(row * 2 + col) {
                                NavigationLink(destination: ProductDetailView(productId: self.results[row * 2 + col].score)) {
                                    VStack {
        //                                Text(self.cards[row * 2 + col])
                                        Text("R \(self.results[row * 2 + col].score)")
                                    }.frame(maxWidth: .infinity)
                                    .background(Color.red)
                                }
                            } else {
                                VStack {
                                    EmptyView()
                                }.frame(maxWidth: .infinity)
                                .background(Color.blue)
                            }
                        }
                    }
                }
                Spacer()
            }.navigationBarHidden(true)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
