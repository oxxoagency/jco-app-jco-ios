//
//  Constants.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import Foundation

struct Constants {
    static let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        static let APP_BUILD = Bundle.main.infoDictionary?["CFBundleVersion"] as? String

    
    /*======================
     * DEVELOPMENT
     ======================*/
    
//    static let API_URL = "http://54.251.204.250:2121/"
//    static let API_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiYTlkMWU2YWUwMDg1MDM4YWMwODhhZmZjNmFjMmU4YzM4OGI3YWI2ZDQ5OWVmNTc1MDRiZWY3OTQ1YzUwYmI3MzE5OGMyYTZiZWNiZDAzMTMiLCJpYXQiOjE1OTMyNDU2NzYsIm5iZiI6MTU5MzI0NTY3NiwiZXhwIjoxNjI0NzgxNjc2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.XdlnGZBlyV8ChOpKxqKBLaxdjbymrABhrIpFHLOTksFxRSDwIHIVmVZ6_4Clh89nsy8_CEYeZZ__4KmEPDtG5CmWDU4OWH0xhD9eaROb36vuqJHNKWkAkp3vGu8ET2Q465SASEjV9gAy9B2QtusBkieWTuRrH3nFbtbgQhnC-9YZ3Tspef_sfEiKTrnr3W8vizNQVr3uRAhfc_Rj1-OL5E97iWxcAhc-Oi6sxl2Rmlam92PEU3TRZ9uG4yNZfN1qhQCxBDUiNRiK2IjEqLq-huN9FRB1cZWfW9XobSzHjR6ut6Md8_xeaAFdFwzgnbPr-2zmXX4jG-M64sH8pTCXoPVxuKkQJKgp1je9jKZkeyDz4G5TK5vfO4RXWu8GyU3XbVFBCcFc81csHpLW1YZlSx_m6AIhMNy6vcSNyx3vedyP2HRqkg5_3WfCCS-PMhGTOLbQxYM0UOz9hgl_JZO3r1Zd8umiq3kXE0ypvbrM_tFyU1TUPEoZj2lQm1SdxdPs-vKtwInjhSXWThT3A_W_O2KcZA2_foSGHV8f5zXKihc0tbjfHY2x43CzAzYc6iNS_nteVWClp-x574psQNZ2eZHitesDdME1AiwET8JSiQ2dyL0TNaZbdMyilzcNYzlO7i-9t1579YfrD6QkgqRKKyAzLw0TA7buTRwtlv-g4o8"
    
//    static let API_URL = "http://54.251.204.250:80/"
    
//    static let API_URL_USR = "https://dev-usr.app.jcodelivery.com/"
//    static let API_URL_GO  = "https://dev-go.app.jcodelivery.com/"
//    static let API_URL_ADM = "https://dev-adm.app.jcodelivery.com/"
//    static let API_URL_PAY = "https://dev-pay.app.jcodelivery.com/"
//    static let API_URL_JCO = "https://api.test.jcodelivery.com/v1/"
//    static let MIDTRANS_CLIENT_KEY = "SB-Mid-client-pwQyxkkp-TFhcskW"
//    static let API_LOYALTY = "https://test.jcodelivery.com/"
    
    static let API_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMTA3ZDNkNjZhYWM0NzNiZTlmMWQ3Mjk4ZWE0NTU2Y2NlNDNhNjRmOWFkNmIxYWYyYjgyMmJiNzdiOTFlZTE2OTkxZTgyYmE5YTIxYzRlNWEiLCJpYXQiOiIxNjA2OTc4MTc2Ljk1OTk4OSIsIm5iZiI6IjE2MDY5NzgxNzYuOTU5OTkzIiwiZXhwIjoiMTYzODUxNDE3Ni45NTAwOTEiLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Iwvs7OZ4DpYBCSqxY5rqjHxCwYRn9lHSG07puT0rRi_eu69qX4DFupC29U7rPQrhIJAcGAYfTJb5Y3glxsTpS5AtAv6FvR1ha-CqmKsSMfPuikvIcA3kcJMOFF8YYLcpR8prkMLQc5D7fQ_lJs9V-krc0C15LjuV820bc6UGtmkhC3Doc1uxVzfN-UxgDqd0WmurC736nLBgh74EGfLRMtRYd6mCDWW0R7IGXuKLxpvk3UKOfn5dDTwOmpD4Uy83_Tx6O7hGdpXcDIlY69waxRYVVernLl1co9AuIn8YtM4HMVxEiOtAO07mgVc1rabzP2DOrwXbZjoCB_PsDv_S9ivHD16ge074ngVTDFBJgq56_ru-_xHOmry8A4XyPJrNE24vPjlVyiMj3utvf5qiVbKpqOLAtxgLKnE1GWzpMRlEtltqYNoOi1UvDq7FmoWUTBEwQLgkV-6RDSQ1FEcl1bcwsinaBER-EHVfCCB1mQVtV10L835s55w9qBoiSuxonLD_1MyTg6G0xAdV6FqggowXahy8zBa223QAfevYuOjhlnBS_4VwXHY6g9NwkpNN9X6cG_zNV9O8_O9N5x6pmiFvxHpMUYcQGi_T4_K6NKV1a6xH3sNmJaVowENG8luz5eStuodsKWbdnLdCo6hGfttWBei09ztN1jcoVJ8jnvQ"
    
    static let API_TOKEN_NEW = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZDEwMjU4ZGNlMmFiODBhMmE3YWUwYWU2MDBjN2ZmZGM0NjlkNjU2N2Q0YmZmMjhhMWRiZDRkNGY1ZWQyZjY0OTdjODQyMDdmMzQyYzQ2N2YiLCJpYXQiOjE2MjMyMDcyMDkuNzAxMTc4MDczODgzMDU2NjQwNjI1LCJuYmYiOjE2MjMyMDcyMDkuNzAxMTgzMDgwNjczMjE3NzczNDM3NSwiZXhwIjo0Nzc4ODgwODA5LjY5NDQxNDEzODc5Mzk0NTMxMjUsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.MWrPg_m-haGYfFW76O6RELuoXZJUGCbYfDbSl3rc6prik7aFqjRzKEmdLArOshshcvOTyIhBfcNqDBRqc0oOJBfFPLE361qKmZ5rwDj9VVFi43HnfQQMnzRjeyS6hF7VcVSDGkR816jbVrK43xDWNaTMQsyGHBv5oV4U9oHy6HpOHN3-tE4Weds20XScWujwBYfXOh4S_RcOdjfSrNBDLWNjUGG2wTW8U5IP9shL5qAPmM3O5ttcvtTv9WzKsc4PF3P7h5uEgVu7vR6uRhmiiBLqfdVwNXjuuEpnorZCsiaPESzebH7MMlbI3CjSpyqUbNRi-ATQ9rbxgr9tiMxtyEt4OtIdZkWB6BrOVINLm6OgQzbvHSDSFdi5N_D3obz6tik18Bye8OXn5Oqir5MikrHbUB1L0IeRl-tGacgB6XVXrwqkOBFXoggL7pA274zxaW128vxwcWLDD_Lib8meL-xPw2khsW1MPG-YljNmx6cNUfFn3dsGiZPNV-GzcuBl5Wshc1pCnaDbqkGkp7r2J0lCe7VpVxq1rxlhTunGZY5yGZCRAduqtsnB7rOeqJCB9s7V9MWEV0Nnad6tzlwcSqNBejgFSM4x3C238xfW9oDgX_xJwf4m-B8jBTWqOvAnbb41xqursQLCWzOI5ralMtJHv6FZ8zPiHUox0Aq81To"
    
    // HTTP
    static let MAP_API = "AIzaSyC-SH7TWbInnUsAVP89nw0lPz1EFfHnhTU";
    // IOS
//    static let MAP_API = "AIzaSyDnKEkATxoZw6vhjvvxsFaKqNbhV6heJTQ";
    
    
    
    /*======================
     * PRODUCTION
     ======================*/
    
    
    static let API_URL_USR = "https://jcousr.app.jcodelivery.com/"
    static let API_URL_GO  = "https://jcogo.app.jcodelivery.com/"
    static let API_URL_ADM = "https://jcoadm.app.jcodelivery.com/"
    static let API_URL_PAY = "https://jcopay.app.jcodelivery.com/"
    static let API_URL_JCO = "https://api.app.jcodelivery.com/v1/"
    static let MIDTRANS_CLIENT_KEY = "Mid-client-lJcRUzFpmgBSUR8x"
    static let API_LOYALTY = "https://jco.id/"
    
}

