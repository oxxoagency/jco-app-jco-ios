//
//  ProductDetailView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import Combine
import URLImage

struct ProductDetailView: View {
    @ObservedObject var viewModel = MenuDetailViewModel()
//    @State var productId: Int
    @State var productImg: String = "https://jco-dev.s3-ap-southeast-1.amazonaws.com/product/img/donut.png"
    
    let url: URL
    let menuCode: String
    let menuId: String

    init(menuCode: String, menuId: String) {
        self.url = URL(string:"https://jco-dev.s3-ap-southeast-1.amazonaws.com/product/img/donut.png")!
        self.menuCode = menuCode
        self.menuId = menuId
        self.viewModel.menuCode = menuCode
    }

    var body: some View {
        VStack {
//            MenuDetailView(menuCode: self.menuCode)
        }
        
//        ZStack {
//            ScrollView {
//                VStack {
//                    MenuDetailView()
////                    Text("PId \(productId)")
////                    Text("MId \(menuId)")
//                    URLImage(url: url,
//                             content: { image in
//                                 image
//                                     .resizable()
//                                     .aspectRatio(contentMode: .fit)
//                             })
//                    VStack(alignment: .leading) {
//                        Text("1 Box J.co Donuts")
//                            .frame(maxWidth: .infinity, alignment: .leading)
//                            .font(.system(size: 22))
//                            .padding(.vertical, 10)
//                            .padding(.horizontal, 15)
//                        Text("Rp. 40.000")
//                            .foregroundColor(Color("font_brown"))
//                            .padding(.horizontal, 15)
//                        Text("new nitrogen-infused individual filter coffee with 6 coffee bean variants : Sumatra Passion, Mexico Velvet, Honduras Suave, Sumatra Zest, Colombia Vivo, & Ethiopia Fleur.")
//                            .font(.system(size: 12))
//                            .foregroundColor(Color.gray)
//                            .padding(.horizontal, 15)
//                            .padding(.top, 5)
//                            .padding(.bottom, 75)
//                    }.frame(maxWidth: .infinity)
//            //        List {
//            //            ForEach(Model.imageURLArray.identified(by: \.id)) { model in ImageRow(model: model)
//            //
//            //            }
//            //        }
//                }
//            }
//
//            VStack {
//                Spacer()
//
//                HStack {
//                    Button(action: {
//
//                    }) {
//                        Image(systemName: "heart.fill")
//                            .padding(10)
//                            .foregroundColor(Color.gray)
//                    }.overlay(
//                        RoundedRectangle(cornerRadius: 10)
//                            .stroke(Color("border_primary"), lineWidth: 2)
//                    ).background(Color.white)
//
//                    Button(action: {
//
//                    }) {
//                        HStack {
//                            Spacer()
//                            Text("Add to Cart")
//                            Spacer()
//                        }
//                    }.buttonStyle(PrimaryButtonStyle())
//                }.padding(10)
//            }
//        }.frame(maxWidth: .infinity, maxHeight: .infinity)
//        .navigationBarHidden(true)
    }
}

struct ImageRow: View {
    let model: Model
    var body: some View {
        VStack(alignment: .center) {
            ImageViewContainer(imageURL: model.imageURL)
        }
    }
}

struct ImageViewContainer: View {
    @ObservedObject var remoteImageURL: RemoteImageURL
    
    init(imageURL: String) {
        remoteImageURL = RemoteImageURL(imageURL: imageURL)
    }
    
    var body: some View {
        Image(uiImage: (remoteImageURL.data.isEmpty) ? UIImage(imageLiteralResourceName: "Swift") : UIImage(data: remoteImageURL.data)!)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 250, height: 250)
    }
}

class RemoteImageURL: ObservableObject {
    var didChange = PassthroughSubject<Data, Never>()
    var data = Data() {
        didSet {
            didChange.send(data)
        }
    }
    
    init(imageURL: String) {
        guard let url = URL(string: imageURL) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in guard let data = data else { return }
            DispatchQueue.main.async {
                self.data = data
            }
        }.resume()
    }
}

struct ProductDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetailView(menuCode: "1", menuId: "test")
    }
}

struct Model: Identifiable {
    var id = UUID()
    var imageURL: String
    
    static let imageURLArray: [Model] = [
        .init(imageURL: "https://jco-dev.s3-ap-southeast-1.amazonaws.com/product/img/donut.png"),
        .init(imageURL: "https://jco-dev.s3-ap-southeast-1.amazonaws.com/product/img/donut.png"),
    ]
}
