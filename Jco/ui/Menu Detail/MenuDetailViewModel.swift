//
//  MenuDetailViewModel.swift
//  Jco
//
//  Created by Ed on 11/01/21.
//

import SwiftUI
import Combine

class MenuDetailViewModel: ObservableObject, MenuDetailService {
    var apiSession: APIService
//    @Environment(\.managedObjectContext) var managedObjectContext
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var managedObjectContext = PersistenceController.shared.container.viewContext
//    @ObservedObject var homeVm = HomeViewModel()
    
    @Published var detaildata: MenuDetailData?
    @Published var variantGroup = [MenuDetailVariantGroup]()
    @Published var menuDetails = [MenuDetail]()
    @Published var arrVariantChosen: [[Int]] = [[]]
    @Published var arrMenuVariantSelected = [MenuVariantSelected]()
    @Published var arrVariantCustomSelected = [VariantCustomSelected]()
    @Published var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    var tempCartVariantGrouped = [MenuVariantSelected]()
    
    @Published var menuCode = ""
    @Published var menuCodeChosen = ""
    @Published var menuImgChosen = ""
    @Published var donutChosen = 0
    @Published var variantLimit = 0
    @Published var variantSelectedCount = 0
    @Published var customDonutLimit = 0
    @Published var customDonutSelectedCount = 0
    @Published var menuQty = 1
    @Published var isLoading = true
    @Published var showAlertSuccess = false
    @Published var showAlertError = false
    @Published var errMsg = ""
    @Published var homeTabSelected = 1
    @Published var beverageTempSelected = ""
    @Published var beverageSizeSelected = ""
    @Published var price: Int? = 0
    
    var variantNum = 0
    var menuAmt = 0
    
    // Add to cart count
    var cartAddVariant = ""
    var cartAddCount = 0
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
  func getMenuDetail(brand: Int) {
        let cancellable = self.getMenuDetail(menuCode: menuCode, brand: brand)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error menudetail: \(error)")
                case .finished:
                    break
                }
                
            }) { (detail) in
                self.detaildata = detail.data
                self.price = detail.data.menuPrice
                self.menuCodeChosen = detail.data.menuCode
                self.menuImgChosen = detail.data.menuImage[0].image
                
//                self.variantGroup = detail.data.variantGroup
//                self.arrVariantChoosen2 = self.variantGroup.map{$0.variantGroupName}
//                self.arrVariantChoosen2 = self.menuDetails.map{$0.menuName}
                if let menuDetailsArr = detail.data.details {
                    self.menuDetails = menuDetailsArr
                }
                
                // Variant Number
                if let variantNumber = detail.data.variantNum {
                    print("VARIANTNUM " + String(variantNumber))
                    self.variantNum = variantNumber
                    self.variantLimit = self.variantNum
                }
                
                // Menu Amount
                self.menuAmt = detail.data.menuAmount
                
                let enumerateSequence = self.menuDetails.enumerated() // type: EnumerateSequence<[Int]>
                let newArray = Array(enumerateSequence)
                
                // Create qty array
                newArray.map { (i, v) -> MenuDetail in
//                    print(v.packageDonutList)
                    print(v.packageName)
                    
                    self.arrVariantChosen.append([])
                    
                    let menuOpt = MenuVariantSelected(packageName: v.packageName ?? "", packageQty: 0)
                    self.arrMenuVariantSelected.append(menuOpt)
//                    menuOpt.packageName = v.packageName ?? ""
                    
                    if let donutList = v.packageDonutList {
                        donutList.map{ (d) -> MenuDonutItem in
//                            print(String(di) + d.menuName)
                            self.arrVariantChosen[i].append(0)
                            return d
                        }
                    }
                    
                    // Custom donut
                    if v.packageName == "Pilih Sendiri" {
                        if let donutList = v.packageDonutList {
                            donutList.map{ (d) -> MenuDonutItem in
                                let donutOpt = VariantCustomSelected(menuName: d.menuName ?? "", menuQty: 0)
                                self.arrVariantCustomSelected.append(donutOpt)
                                return d
                            }
                        }
//                        print(self.arrVariantCustomSelected)
                    }
                    
                    return v
                }
                
                // Beverage, set the first temp, code, price & img
                if self.detaildata?.categoryName == "beverage" {
                    if self.menuDetails.count > 0 {
                        self.beverageTempSelected = self.menuDetails[0].temp ?? ""
                        self.price = self.menuDetails[0].menuPrice ?? 0
                        self.menuCodeChosen = self.menuDetails[0].menuCode ?? detail.data.menuCode
                        self.menuImgChosen = self.menuDetails[0].menuImage ?? detail.data.menuImage[0].image
                        self.beverageSizeSelected = self.menuDetails[0].size ?? ""
                    }
                }
                
                print(self.arrMenuVariantSelected)
//                print(self.arrVariantChosen)
//                print(detail.data)
                self.isLoading = false
        }
        cancellables.insert(cancellable)
    }
    
    func addMenuQty() {
        menuQty += 1
        variantLimit += variantNum
    }
    
    func subMenuQty() {
        if menuQty > 0 {
            menuQty -= 1
            variantLimit -= variantNum
        }
    }
    
    func addDonut(groupIndex: Int, donutIndex: Int) {
        if donutChosen < 5 {
            arrVariantChosen[groupIndex][donutIndex] += 1
            donutChosen += 1
        }
    }
    
    func subtractDonut(groupIndex: Int, donutIndex: Int) {
        if arrVariantChosen[groupIndex][donutIndex] > 0 {
            arrVariantChosen[groupIndex][donutIndex] -= 1
            donutChosen -= 1
        }
    }
    
    func addDonutPackage(groupIndex: Int, type: String) {
        if variantSelectedCount == variantLimit {
            addMenuQty()
        }
        
        arrMenuVariantSelected[groupIndex].packageQty += 1
        variantSelectedCount += 1
        
        if type == "custom" {
            customDonutLimit += menuAmt
        }
    }
    
    func subtractDonutPackage(groupIndex: Int, type: String) {
        if (type == "mix" && variantSelectedCount > 0) || (type == "custom" && customDonutSelectedCount <= (customDonutLimit - menuAmt)) {
            if arrMenuVariantSelected[groupIndex].packageQty > 0 {
                arrMenuVariantSelected[groupIndex].packageQty -= 1
                variantSelectedCount -= 1
            }
            
            if type == "custom" {
                customDonutLimit -= menuAmt
            }
            
            if((variantSelectedCount + variantNum) == (menuQty * variantNum)) {
                self.menuQty -= 1
                self.variantLimit -= variantNum
            }
        } else {
            if type == "custom" {
                self.errMsg = "Donut Qty Melebihi Limit, mohon kurangi jumlah paket"
                self.showAlertError = true
            }
        }
    }
    
    func addDonutCustom(index: Int, groupIndex: Int) {
//        if self.customDonutSelectedCount < self.customDonutLimit {
//            self.arrVariantCustomSelected[index].menuQty += 1
//            self.customDonutSelectedCount += 1
//        }
        
        if self.customDonutSelectedCount == self.customDonutLimit {
            self.addDonutPackage(groupIndex: groupIndex, type: "custom")
        }
        self.arrVariantCustomSelected[index].menuQty += 1
        self.customDonutSelectedCount += 1
    }
    
    func subDonutCustom(index: Int) {
        if self.arrVariantCustomSelected[index].menuQty > 0 {
            self.arrVariantCustomSelected[index].menuQty -= 1
            self.customDonutSelectedCount -= 1
        }
    }
    
    func setBeverageTempSelected(temp: String, price: Int, code: String, img: String) {
        self.beverageTempSelected = temp
        self.price = price
        self.menuCodeChosen = code
        self.menuImgChosen = img
    }
    
    func setPriceSelected(price: Int) {
        self.price = price
    }
    
    func addToCart(menuCode: String, menuName: String, menuImg: String, menuPrice: String, brand: Int) {
        self.isLoading = true
        
//        let result = PersistenceController(inMemory: true)
//        let viewContext = result.container.viewContext
        
        if menuQty > 0 {
            // Donut Package
            if brand == 1 {
            if let varNum = self.detaildata?.variantNum {
                if varNum > 0 {
                    if self.variantSelectedCount == self.variantLimit {
                        if self.customDonutSelectedCount == self.customDonutLimit {
                            self.arrMenuVariantSelected.map{ (s) -> MenuVariantSelected in
                                if s.packageQty > 0 {
                                    var previousVariant = ""
                                    
                                    for n in 1...s.packageQty {
                                        // Add variant to cart
                                        if cartAddCount == 1 {
                                            cartAddVariant += ", "
                                        }
                                        
                                        if s.packageName == "Pilih Sendiri" {
                                            var customDesc = ""
                                            var customCount = 0
                                            
                                            self.arrVariantCustomSelected.map{ (c) -> VariantCustomSelected in
                                                if c.menuQty > 0 {
                                                    if customCount != 0 {
                                                        customDesc += ", "
                                                    }
                                                    customDesc += String(c.menuQty) + " " + c.menuName
                                                    customCount += 1
                                                }
                                                return c
                                            }
                                            cartAddVariant += s.packageName + ": " + customDesc
                                        } else {
                                            cartAddVariant += s.packageName
                                        }
                                        cartAddCount += 1
                                        
                                        // Insert into temp container grouped
                                        if cartAddCount == Int(varNum) {
                                            if cartAddVariant == previousVariant {
                                                // If same variant, add qty
                                                let arrSize = tempCartVariantGrouped.count
                                                tempCartVariantGrouped[arrSize-1].packageQty += 1
                                            } else {
                                                let tempGrouped = MenuVariantSelected(packageName: cartAddVariant, packageQty: 1)
                                                tempCartVariantGrouped.append(tempGrouped)
                                                previousVariant = cartAddVariant
                                            }
                                            
                                            // Reset
                                            cartAddCount = 0
                                            cartAddVariant = ""
                                        }
                                        print(String(n) + " | " + String(cartAddCount) + cartAddVariant)
                                    }
                                }
                                return s
                            }
                            
                            // After finish group, loop through container grouped
                            tempCartVariantGrouped.map{ (te) -> MenuVariantSelected in
                                let newCart = Cart(context: managedObjectContext)
                                newCart.menuCode = menuCode
                                newCart.menuName = menuName
                                newCart.menuImg = menuImg
                                newCart.menuQty = Int16(te.packageQty)
                                newCart.menuPrices = String(menuPrice)
                                newCart.menuVariant = te.packageName
                                
                                PersistenceController.shared.save()
                                
                                return te
                            }
                            
                            self.showAlertSuccess = true
                        } else {
                            self.errMsg = "Qty Donut Pilih Sendiri Masih Kurang"
                            self.showAlertError = true
                        }
                    } else {
                        self.errMsg = "Please choose more variant"
                        self.showAlertError = true
                    }
                } else {
                    var menuCartVariant = ""
                    // Beverage
                    if self.detaildata?.categoryName == "beverage" {
                        menuCartVariant = self.beverageSizeSelected + " - " + self.beverageTempSelected
                    }
                    
                    let newCart = Cart(context: managedObjectContext)
                    newCart.menuCode = menuCode
                    newCart.menuName = menuName
                    newCart.menuImg = menuImg
                    newCart.menuQty = Int16(menuQty)
                    newCart.menuPrices = String(self.price ?? 0)
                    newCart.menuVariant = menuCartVariant
                    
                    PersistenceController.shared.save()
                    print(newCart)
                    
                    self.showAlertSuccess = true
                }
            }
            } else {
                var menuCartVariant = ""

                let newCart = CartJcoR(context: managedObjectContext)
                newCart.menuCode = menuCode
                newCart.menuName = menuName
                newCart.menuImg = menuImg
                newCart.menuQty = Int16(menuQty)
                newCart.menuPrices = String(self.price ?? 0)
                newCart.menuVariant = menuCartVariant
                
                PersistenceController.shared.save()
                print(newCart)
                
                self.showAlertSuccess = true
            }
            UserDefaults.standard.set(2, forKey: "CartQtyCount")
        } else {
            self.errMsg = "Please enter quantity"
            self.showAlertError = true
        }
        
        self.isLoading = false
    }
    
    func hideAlertSuccess() {
        self.showAlertSuccess = false
        self.resetQty()
    }
    
    func resetQty() {
        self.menuQty = 1
        self.donutChosen = 0
        self.variantSelectedCount = 0
        self.customDonutSelectedCount = 0
    }
    
    func viewCart() {
//        self.presentationMode.wrappedValue.dismiss()
//        self.homeTabSelected = 2
//        self.homeVm.homeTabSelected = 2
        UserDefaults.standard.set(2, forKey: "HomeTabSelected")
    }
    
//    func saveContext() {
//      do {
//        try managedObjectContext.save()
//      } catch {
//        print("Error: \(error)")
//      }
//    }
}

