//
//  MenuDetailView.swift
//  Jco
//
//  Created by Ed on 11/01/21.
//

import SwiftUI
import URLImage

struct MenuDetailView: View {
    @ObservedObject var viewModel = MenuDetailViewModel()
    @ObservedObject var hvm: HomeViewModel
    @ObservedObject var cartViewModel = CartViewModel()
//    @ObservedObject var tabData = HomeTabData()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var managedObjectContext
    @EnvironmentObject var brandSettings: BrandSettings
    @State var isLoading = true
//    @Binding var tab: Int
    
    let menuCode: String
    var isFromPromo: Bool
//    let hvm: HomeViewModel
    init(hvmm: HomeViewModel, menuCode: String, isFromPromo: Bool) {
        self.hvm = hvmm
//        self.tabData = tabData
        self.menuCode = menuCode
        self.isFromPromo = isFromPromo
        self.viewModel.menuCode = menuCode
//        self._tab = tab
    }
    
    @State var menuDetailImgSelected = ""
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            
            ScrollView {
                VStack(alignment: .leading) {
                    // Menu Img
                    Group {
                        GeometryReader { geometry in
                            if let menuImages = self.viewModel.detaildata?.menuImage {
                                ImageCarouselView(numberOfImages: menuImages.count) {
                                    ForEach(Array(menuImages.enumerated()), id: \.1) { index, img in
                                        URLImage(url: URL(string: img.image)!,
                                        content: { image in
                                             image
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: geometry.size.width, height: geometry.size.height)
                                                .clipped()
                                        })
                                    }
                                }
                            }
                        }.frame(height: 300, alignment: .center)
                    }
                    // Menu Img
                    
                    // Category Name
                    Group {
                        Button (action: {
                            
                        }) {
                            Text(self.viewModel.detaildata?.categoryTitle ?? "Category")
                                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                .font(.custom("Poppins-Medium", size: 15))
                                .padding(.horizontal, 10)
                                .padding(.vertical, 6)
                        }
                        .padding(.horizontal, 15)
                        .cornerRadius(20)
//                        .overlay(
//                            RoundedRectangle(cornerRadius: 15)
//                                .stroke(Color.white)
//                        )
                        .background(brandSettings.curBrand == 1 ? Color("c_menu_category") : Color("jcor_gold"))
                        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                        
                    }
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    // Category Name
                    
                    //Menu Name
                    Group {
                        HStack {
                            VStack(alignment: .leading) {
                                if let menuName = self.viewModel.detaildata?.menuName {
                                    Text(menuName)
                                        .frame(maxWidth: .infinity, alignment: .leading)
//                                        .font(.system(size: 18, weight: .bold))
                                        .padding(.vertical, 10)
                                        .padding(.horizontal, 15)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        .font(.custom("Poppins-Bold", size: 20))
                                }
                                if let menuPrice = self.viewModel.price {
                                    Text("Rp. " + Util().addTS(str: String(menuPrice)))
                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                        .padding(.horizontal, 15)
                                        .font(.custom("Poppins-Regular", size: 15))
                                }
                                
                                if(self.viewModel.detaildata?.categoryName == "package" && self.viewModel.menuDetails.count > 0) {
                                    Group {
                                        Text("Choose Package: " + String(self.viewModel.variantSelectedCount) + " / ")
                                            .foregroundColor(Color("c_666666"))
                                        + Text(String(self.viewModel.variantLimit))
                                            .foregroundColor(Color("c_666666"))
                                            .font(.custom("Poppins-Regular", size: 15))
                                    }.padding(.horizontal, 15)
                                    .padding(.top, 5)
                                }
                            }.frame(maxWidth: .infinity)
                            .padding(.bottom, 15)
                            
                            HStack {
                                if(!(self.viewModel.detaildata?.categoryName == "package" && self.viewModel.menuDetails.count > 0)) {
                                    Button(action: {
                                        self.viewModel.subMenuQty()
                                    }) {
                                        Image(systemName: "minus.square")
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                            .font(.system(size: 25))
                                    }
                                
                                    Text(String(self.viewModel.menuQty))
                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                    .font(.system(size: 20))

                                    Button(action: {
                                        self.viewModel.addMenuQty()
                                    }) {
                                        Image(systemName: "plus.square.fill")
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                            .font(.system(size: 25))
                                    }
                                } else {
                                    Text(String(self.viewModel.menuQty) + " X")
                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                    .font(.system(size: 22))
                                        .fontWeight(.bold)
                                        .padding(.horizontal, 15)
                                }
                            }.padding(.horizontal, 10)
                        }
                    }
                    // Menu Name
                    
                    // Menu Desc
                    Group {
                        if let menuDescription = self.viewModel.detaildata?.menuDesc {
                            Text(menuDescription)
                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                .font(.custom("Poppins-Regular", size: 15))
                                .padding()
                        }
                    }
                    // Menu Desc
                    
                    // Variant
                    MenuDetailVariantList(viewModel: self.viewModel)
                    
                    // Beverage
                    Group {
                        if self.viewModel.detaildata?.categoryName == "beverage" {
                            if self.viewModel.menuDetails.count > 0 {
                                VStack(alignment: .leading) {
                                    Text("Choose Size")
                                        .font(.custom("Poppins-Regular", size: 15))
                                        .foregroundColor(Color("c_666666"))
                                        .padding(.horizontal, 15)
                                    
                                    Group {
                                        Button(action: {
                                            
                                        }) {
                                            Text(self.viewModel.menuDetails[0].size ?? "")
                                                .padding(.horizontal, 15)
                                                .padding(.vertical, 8)
                                                .foregroundColor(Color.white)
                                                .font(.custom("Poppins-Regular", size: 15))
                                        }.overlay(
                                            RoundedRectangle(cornerRadius: 5)
                                                .stroke(Color("btn_primary"))
                                        ).background(Color("btn_primary"))
                                        .clipShape(RoundedRectangle(cornerRadius: 5, style: .continuous))
                                    }.padding()
                                    
                                    if self.viewModel.menuDetails.count > 0 {
                                        Text("Select a Variant")
                                            .font(.custom("Poppins-Regular", size: 15))
                                            .foregroundColor(Color("c_666666"))
                                            .padding(.horizontal, 15)
                                            .padding(.top, 5)
                                    }
                                    
                                    HStack {
                                        ForEach(self.viewModel.menuDetails, id: \.self) { det in
                                            if let temp = det.temp {
                                                if let priceSelected = det.menuPrice, let codeSelected = det.menuCode {
                                                    
//                                                    if let menuImgSelected = det.menuImage {
//                                                        menuDetailImgSelected = menuImgSelected
//                                                    } else {
//                                                        menuDetailImgSelected = self.viewModel.menuImgChosen
//                                                    }
                                                    
                                                    Button(action: {
                                                        self.viewModel.setBeverageTempSelected(
                                                            temp: temp,
                                                            price: priceSelected,
                                                            code: codeSelected,
                                                            img: det.menuImage ?? self.viewModel.menuImgChosen
                                                        )
//                                                        self.viewModel.setPriceSelected(price: priceSelected)
                                                    }) {
                                                        Text(temp)
                                                            .padding(.horizontal, 15)
                                                            .padding(.vertical, 8)
                                                            .font(.custom("Poppins-Regular", size: 15))
                                                            .foregroundColor(self.viewModel.beverageTempSelected == temp ? Color.white : Color.black)
                                                    }.overlay(
                                                        RoundedRectangle(cornerRadius: 5)
                                                            .stroke(self.viewModel.beverageTempSelected == temp ? Color("btn_primary") : Color.gray)
                                                    ).background(self.viewModel.beverageTempSelected == temp ? Color("btn_primary") : Color.white)
                                                    .clipShape(RoundedRectangle(cornerRadius: 5, style: .continuous))
                                                }
                                            }
                                        }
                                    }.padding()
                                }
                            }
                        }
                    }
                }
            }.padding(.bottom, 75)
            .background(self.brandSettings.curBrand == 1 ? Color("c_fff").edgesIgnoringSafeArea(.all) : Color("theme_background_jcor").edgesIgnoringSafeArea(.all))
            
            // Add to cart btn
            Group {
                VStack {
                    Spacer()
                    
                    HStack {
                        Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
                            Image(systemName: "house.fill")
                                .padding(15)
                                .foregroundColor(Color.gray)
                                .font(.system(size: 20))
                        }.overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("border_primary"), lineWidth: 2)
                        ).background(Color.white)
                        
                        ZStack {
                            Button(action: {
                                self.hvm.viewCart()
                                self.presentationMode.wrappedValue.dismiss()
                            } ) {
                                Image(systemName: "bag.fill")
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .font(.system(size: 22))
                            }.overlay(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color("border_primary"), lineWidth: 2)
                            ).background(Color.white)
                            ZStack{
                                Circle()
                                    .foregroundColor(.red)
                                
                                Text(String(cartViewModel.totalQtyCart(brand: brandSettings.curBrand)))
                                    .foregroundColor(.white)
                                    .font(Font.system(size: 14))
                            }
                            .frame(width: 20, height: 20)
                            .offset(x: 22, y: -25)
                            .opacity(1)
                            .zIndex(5)
                        }
                      
                        if brandSettings.curBrand == 1 {
                          Button(action: {
                              if self.viewModel.userLogged {
                                  if let menuCode = self.viewModel.detaildata?.menuCode, let menuName = self.viewModel.detaildata?.menuName, let menuImg = self.viewModel.detaildata?.menuImage, let menuPrice = self.viewModel.detaildata?.menuPrice {
                                      self.viewModel.addToCart(
                                          menuCode: self.viewModel.menuCodeChosen,
                                          menuName: menuName,
                                          menuImg: self.viewModel.menuImgChosen,
                                          menuPrice: String(menuPrice),
                                          brand: self.brandSettings.curBrand
                                      )
                                  }
                              } else {
                                  self.hvm.viewLogin()
                                  self.presentationMode.wrappedValue.dismiss()
                              }
                          }) {
                              HStack {
                                  Spacer()
                                  Text(self.viewModel.userLogged ? "Add to Cart" : "Please Login")
                                      .font(.custom("Poppins-Medium", size: 18))
                                  Spacer()
                              }
                          }.buttonStyle(PrimaryButtonStyle())
                          .alert(isPresented: $viewModel.showAlertError) {
                              Alert(title: Text("Error"), message: Text(self.viewModel.errMsg), dismissButton: .default(Text("Okay")))
                          }
                        } else {
                          Button(action: {
                              if self.viewModel.userLogged {
                                  if let menuCode = self.viewModel.detaildata?.menuCode, let menuName = self.viewModel.detaildata?.menuName, let menuImg = self.viewModel.detaildata?.menuImage, let menuPrice = self.viewModel.detaildata?.menuPrice {
                                      self.viewModel.addToCart(
                                          menuCode: self.viewModel.menuCodeChosen,
                                          menuName: menuName,
                                          menuImg: self.viewModel.menuImgChosen,
                                          menuPrice: String(menuPrice),
                                          brand: self.brandSettings.curBrand
                                      )
                                  }
                              } else {
                                  self.hvm.viewLogin()
                                  self.presentationMode.wrappedValue.dismiss()
                              }
                          }) {
                              HStack {
                                  Spacer()
                                  Text(self.viewModel.userLogged ? "Add to Cart" : "Please Login")
                                      .font(.custom("Poppins-Medium", size: 18))
                                  Spacer()
                              }
                          }.buttonStyle(PrimaryButtonStyleJcoR())
                          .alert(isPresented: $viewModel.showAlertError) {
                              Alert(title: Text("Error"), message: Text(self.viewModel.errMsg), dismissButton: .default(Text("Okay")))
                          }
                        }
                       
                    }.padding(10)
                }
            }
            
            // Arrow Back
            Group {
                HStack(alignment: .center) {
                    Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
                        Image(uiImage: UIImage(named: "arrow_left")!)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30)
                    }
                    
                    Spacer()
                    
                    Button(action: {}) {
                        Image(systemName: "heart.circle.fill")
                            .foregroundColor(Color(red: 1, green: 1, blue: 1, opacity: 0.7))
                            .font(.system(size: 40))
                            .zIndex(5)
                    }
                }.offset(x:0, y:0)
                .frame(maxWidth: .infinity)
                .padding(.horizontal, 20)
                .padding(.top, 20)
            }
            
            // Alert
            Group {
                Loader(isLoading: $viewModel.isLoading).zIndex(2)
                
                if self.viewModel.showAlertSuccess {
                    AlertSuccess(viewModel: self.viewModel, homeVm: hvm, isFromoPromo: isFromPromo)
                } else {
                    EmptyView()
                }
            }
        }.onAppear {
          if brandSettings.curBrand == 1 {
            self.viewModel.getMenuDetail(brand: 1)
          } else {
            self.viewModel.getMenuDetail(brand: 2)
          }
            
        }.navigationBarHidden(true)
        .background(Color("c_fff"))
    }
}

struct MenuPackageMix: View {
    @ObservedObject var viewModel: MenuDetailViewModel
    var vg: MenuDetail
    var packageName: String
    var indexGroup: Int
    
    var body: some View {
        VStack(alignment: .leading) {
            if let packageImage = vg.packageImage {
                GeometryReader { geometry in
                    URLImage(url: URL(string: packageImage)!,
                        content: { image in
                             image
                                 .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: geometry.size.width)
                                .clipped()
                    })
                }.frame(height: 300, alignment: .center)
            }
            
            Group {
                HStack {
                    Text(packageName)
                        .multilineTextAlignment(.leading)
                        .font(.custom("Poppins-Bold", size: 23))
//                        .font(.system(size: 23, weight: .heavy))
                        .foregroundColor(Color.gray)
                        .padding(.horizontal, 10)
                    
                    Spacer()
                    
                    HStack {
                        Button(action: {
                            self.viewModel.subtractDonutPackage(groupIndex: indexGroup, type: "mix")
                        }) {
                            Image(systemName: "minus.square")
                                .foregroundColor(Color("btn_primary"))
                                .font(.system(size: 25))
                        }
                        
                        Text(String(self.viewModel.arrMenuVariantSelected[indexGroup].packageQty))
                            .foregroundColor(Color("theme_text"))
                            .font(.system(size: 20))
                        
                        Button(action: {
                            self.viewModel.addDonutPackage(groupIndex: indexGroup, type: "mix")
                        }) {
                            Image(systemName: "plus.square.fill")
                                .foregroundColor(Color("btn_primary"))
                                .font(.system(size: 25))
                        }
                    }.padding(.horizontal, 10)
                }
                
                Group {
                    if let packageDescription = vg.packageDescription {
                        Text(packageDescription)
                            .font(.custom("Poppins-Regular", size: 12))
                            .foregroundColor(Color.gray)
//                            .font(.system(size: 12))
                            .padding()
                    }
                    
                    Rectangle().frame(height: 2.5).foregroundColor(Color("btn_primary"))
                }
            }.padding(.bottom, 15)
            
        }
    }
}

struct MenuDetailVariantList: View {
    @ObservedObject var viewModel: MenuDetailViewModel
    
    var body: some View {
        if(self.viewModel.detaildata?.categoryName == "package") {
            Group {
                ForEach(Array(self.viewModel.menuDetails.enumerated()), id: \.1) { indexGroup, vg in
                    if let packageName = vg.packageName {
                        if packageName != "Pilih Sendiri" {
                            // Package Mix
                            MenuPackageMix(viewModel: viewModel, vg: vg, packageName: packageName, indexGroup: indexGroup)
                        } else {
                            // Package Custom
                            VStack(alignment: .leading) {
                                Group {
                                    HStack {
                                        Group {
                                            VStack {
                                                Group {
                                                    Text(packageName)
                                                        .multilineTextAlignment(.leading)
                                                        .font(.system(size: 23, weight: .heavy))
                                                        .foregroundColor(Color.gray)
                                                    Text("Choose Donut: ")
                                                        .foregroundColor(Color("c_666666"))
                                                    + Text(String(self.viewModel.customDonutSelectedCount) + " / ")
                                                    + Text(String(self.viewModel.customDonutLimit))
                                                }.padding(.horizontal, 10)
                                                .font(.custom("Poppins-Regular", size: 14))
                                                .foregroundColor(Color("theme_text"))
                                            }
                                            Spacer()
                                        }
                                        Group {
                                            HStack {
                                                Button(action: {
                                                    self.viewModel.subtractDonutPackage(groupIndex: indexGroup, type: "custom")
                                                }) {
                                                    Image(systemName: "minus.square")
                                                        .foregroundColor(Color("btn_primary"))
                                                        .font(.system(size: 25))
                                                }
                                                
                                                Text(String(self.viewModel.arrMenuVariantSelected[indexGroup].packageQty))
    //                                                .foregroundColor(Color("theme_text"))
    //                                                .font(.system(size: 20))
                                                    .font(.custom("Poppins-Regular", size: 18))
                                                    .foregroundColor(Color("theme_text"))
                                                
                                                Button(action: {
                                                    self.viewModel.addDonutPackage(groupIndex: indexGroup, type: "custom")
                                                }) {
                                                    Image(systemName: "plus.square.fill")
                                                        .foregroundColor(Color("btn_primary"))
                                                        .font(.system(size: 25))
                                                }
                                            }.padding()
                                        }
                                    }.frame(maxWidth: .infinity)
                                }
                                
                                // Custom Donut Grid
                                Group {
                                    if let packageDonutList = vg.packageDonutList {
                                        GridStack(rows: 10, columns: 4) { row, col in
                                            if packageDonutList.indices.contains(row * 4 + col) {
                                                VStack {
                                                    if let variantImg = packageDonutList[row * 4 + col].menuImg {
                                                        URLImage(url: URL(string: variantImg)!,
                                                            content: { image in
                                                                 image
                                                                     .resizable()
                                                                    .aspectRatio(contentMode: .fit)
                                                        })
                                                    }
                                                    
                                                    if let variantName = packageDonutList[row * 4 + col].menuName {
                                                        Text(variantName)
                                                            .font(.custom("Poppins-Regular", size: 12))
                                                            .foregroundColor(Color("theme_text"))
                                                            .frame(height: 25)
                                                    }
                                                    
                                                    HStack {
                                                        Button(action: {
                                                            self.viewModel.subDonutCustom(index: row * 4 + col)
                                                        }) {
                                                            Image(systemName: "minus.square")
                                                                .foregroundColor(Color("btn_primary"))
                                                                .font(.system(size: 20))
                                                        }

                                                        Text(String(viewModel.arrVariantCustomSelected[row * 4 + col].menuQty))
                                                            .foregroundColor(Color("theme_text"))
                                                            .font(.system(size: 14))

                                                        Button(action: {
                                                            self.viewModel.addDonutCustom(index: row * 4 + col, groupIndex: indexGroup)
                                                        }) {
                                                            Image(systemName: "plus.square.fill")
                                                                .foregroundColor(Color("btn_primary"))
                                                                .font(.system(size: 20))
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }.padding(.bottom, 25)
                        }
                    }
                }
                
            }
        }
    }
}

struct AlertSuccess: View {
//    @Binding var showAlertSuccess: Bool
//    @ObservedObject var viewModel = MenuDetailViewModel()
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var viewModel: MenuDetailViewModel
    @ObservedObject var homeVm: HomeViewModel
    @State var isFromoPromo: Bool
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack() {
            Spacer()
            HStack {
                Spacer()
                Group {
                    VStack(alignment: .center) {
                        Text("Item sucessfully added to cart")
                            .font(.custom("Poppins-Regular", size: 15))
                            .frame(alignment: .center)
                            .multilineTextAlignment(.center)
                            .padding(.bottom, 30)
                            .foregroundColor(Color("theme_text"))
                        
                        if brandSettings.curBrand == 1 {
                            Button(action: {
                                self.viewModel.hideAlertSuccess()
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Back to Menu")
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
                            
                            Button(action: {
                                if self.isFromoPromo {
                                    self.presentationMode.wrappedValue.dismiss()
                                    self.viewModel.hideAlertSuccess()
                                    self.homeVm.isActivePromo = false
                                    self.homeVm.viewCart()

                                } else {
                                    self.homeVm.viewCart()
                                    self.viewModel.hideAlertSuccess()
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                            }) {
                                HStack {
                                    Spacer()
                                    Text("View Cart")
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
                        } else {
                            Button(action: {
                                self.viewModel.hideAlertSuccess()
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Back to Menu")
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyleJcoR())
                            
                            Button(action: {
                                self.homeVm.viewCart()
                                self.viewModel.hideAlertSuccess()
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                HStack {
                                    Spacer()
                                    Text("View Cart")
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyleJcoR())
                        }
                    }.frame(width: 200, height: 200)
                    .padding(.all, 30)
                }.background(Color("theme_background"))
                .cornerRadius(15)
                .frame(width: 500, height: 500)
                .shadow(radius: 10)
                Spacer()
            }
            Spacer()
        }.background(Color(red: 0, green: 0, blue: 0, opacity: 0.1))
        .frame(minWidth: 0,
               maxWidth: .infinity,
               minHeight: 0,
               maxHeight: .infinity)
        .offset(x:0, y:0)
        .zIndex(5)
        .ignoresSafeArea()
    }
}

struct MenuDetailView_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
