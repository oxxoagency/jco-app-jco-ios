//
//  UserAddressListView.swift
//  Jco
//
//  Created by Ed on 03/04/21.
//

import SwiftUI

struct UserAddressListView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel = UserAddressListViewModel()
    @ObservedObject var viewModelNew = UserAddressViewModel()
    @ObservedObject var cartViewModel: CartViewModel
    @State private var isPresented = false
    @State var textValue: String = "Jakarta"
    
//    @ObservedObject var cartViewModel = CartViewModel()
    
    var body: some View {
        VStack {
            VStack {
                // Header
                HStack {
                    Group {
                        Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
                            Image(systemName: "arrow.left")
                                .foregroundColor(Color.white)
                                .font(.system(size: 20))
                                .offset(x:0, y:0)
                        }.padding(.horizontal, 10)
                    }
                    .frame(width: 50)
                    
                    Spacer()
                    Text("Choose Address")
                        .foregroundColor(Color.white)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .font(.custom("Poppins-Bold", size: 20))
                    Spacer()
                    
                    Rectangle()
                            .frame(width: 50, height: 10)
                }.padding(.vertical, 20)
                    .background(brandSettings.curBrand == 1 ? Image("header").resizable().edgesIgnoringSafeArea(.top) : Image("header_jcor").resizable().edgesIgnoringSafeArea(.top))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                .frame(
                    minWidth: 0,
                    maxWidth: .infinity
                )
                // Header
                
                ZStack(alignment: .topLeading) {
                    VStack {
                        ScrollView {
                            Group {
                                ForEach(Array(self.viewModelNew.detaildata.enumerated()), id: \.1) { index, vg in
                                    Button(action: {
                                        self.cartViewModel.chooseAddress(
                                            brand: self.brandSettings.curBrand,
                                            chosenLabel: vg.addressLabel,
                                            chosenName: vg.recipientName,
                                            chosenPhone: vg.recipientPhone,
                                            chosenAddress: vg.address,
                                            choosenDetailAddress: vg.addressDetails,
                                            chosenAddressID: vg.memberAddressId,
                                            chosenCity: vg.cityordistrict,
                                            chosenPostcode: String(vg.recipientPostcode),
                                            chosenLatitude: vg.latitude,
                                            chosenLongitude: vg.longitude,
                                            orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR"
                                        )
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        VStack(alignment: .leading) {
                                            VStack(alignment: .leading) {
                                                HStack(alignment: .center) {
                                                    VStack(alignment: .leading) {
                                                        Text(vg.addressLabel)
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                            .font(.custom("Poppins-Medium", size: 14))
                                                        
                                                        Text(vg.recipientName)
                                                            .font(.custom("Poppins-Bold", size: 15))
                //                                            .font(.system(size: 14))
                //                                            .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
        //                                                    .padding(.vertical, 1)
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Button(action: {
                                                        self.viewModelNew.deleteAddress(addressID: vg.memberAddressId)
                                                    }) {
                                                        Image(systemName: "trash.circle")
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                            .font(.system(size: 26))
                                                    }
                                                    
                                                    
                                                    Text("Pilih Alamat")
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                                        .font(.custom("Poppins-Medium", size: 15))
                                                        .padding(.horizontal, 5)
                                                        .padding(.vertical, 6)
                                                        .cornerRadius(20)
                                                        .padding(.horizontal, 15)
                                                    
                                                    //                        .overlay(
                                                    //                            RoundedRectangle(cornerRadius: 15)
                                                    //                                .stroke(Color.white)
                                                    //                        )
                                                        .background(brandSettings.curBrand == 1 ? Color("c_menu_category") : Color("jcor_gold"))
                                                        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                                                }
                                                
                                                Text(vg.recipientPhone)
                                                    .font(.custom("Poppins-Regular", size: 13))
        //                                            .font(.system(size: 13))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                Text(vg.address)
                                                    .lineLimit(5)
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                    .font(.custom("Poppins-Regular", size: 12))
                                                    .multilineTextAlignment(.leading)
                                                Text(vg.addressDetails)
                                                    .lineLimit(5)
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                    .font(.custom("Poppins-Regular", size: 12))
                                            }.padding(.horizontal, 15)
                                            Rectangle().frame(height: 1)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                        }.padding(.vertical, 10)
                                        .frame(
                                              minWidth: 0,
                                              maxWidth: .infinity,
                                              alignment: .topLeading
                                        )
                                    }
                                }
                            }
                            Spacer()
                        }
                    }
                    
                    Group {
                        Loader(isLoading: $viewModelNew.isLoading).zIndex(2)
                    }
                }
                
                // Button Add New Address
                VStack(alignment: .leading) {
                    if brandSettings.curBrand == 1 {
                        NavigationLink(destination:  UserAddressNewView(locLat: 0.0, locLng: 0.0, locName: "", locAddress: "", vm: self.viewModelNew)){
                            HStack {
                                Spacer()
                                Text("Tambah Alamat")
                                    .font(.custom("Poppins-Bold", size: 18))
                                    .foregroundColor(Color.white)
                                    .cornerRadius(10)
                                Spacer()
                            }
                        }
                        .buttonStyle(PrimaryButtonStyle())
                        .frame(maxWidth: .infinity)
                    } else {
                        NavigationLink(destination:  UserAddressNewView(locLat: 0.0, locLng: 0.0, locName: "", locAddress: "", vm: self.viewModelNew)){
                            HStack {
                                Spacer()
                                Text("Tambah Alamat")
                                    .font(.custom("Poppins-Bold", size: 18))
                                    .foregroundColor(Color.white)
                                    .cornerRadius(10)
                                Spacer()
                            }
                        }
                        .buttonStyle(PrimaryButtonStyleJcoR())
                        .frame(maxWidth: .infinity)
                    }
              
                    
//                    NavigationLink(
//                        destination: UserAddressNewLocationView(viewModel: viewModelNew, cartVm: cartViewModel),
//                        isActive: $viewModelNew.isDismiss
//                    ) {
//                        HStack {
//                            Spacer()
//                            Text("Tambah Alamat")
//                                .font(.custom("Poppins-Bold", size: 18))
//                            Spacer()
//                        }
//                    }.isDetailLink(false)
//                    .buttonStyle(PrimaryButtonStyle())
//                    .frame(maxWidth: .infinity)
                    
                }.padding(.vertical, 5)
                .padding(.horizontal, 10)
                .frame(
                    minWidth: 0,
                    maxWidth: .infinity,
                    alignment: .topLeading
                )
                .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
                
                // /Button Add New Address
            }.navigationBarHidden(true)
            .navigationBarTitle("")
            .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
        }.onAppear {
            self.viewModelNew.getUserAddressList()
        }.navigationBarHidden(true)
        .navigationBarTitle("")
        .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
    }
}

struct ChooseLocationModalView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode
    @State var locationQuery: String = ""
    @State private var shouldAnimate = false
    let timer = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
    @State var leftOffset: CGFloat = -100
    @State var rightOffset: CGFloat = 100
    @ObservedObject var viewModel: UserAddressViewModel
    @State private var isPresentedNew = false
    @State private var openMap = false
 
    var body: some View {
        VStack {
            Header(title: "New Address", back: true)
            
            ScrollView {
                HStack {
                    TextField("Find Location", text: $viewModel.addressLocation)
                        .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .padding(.horizontal, 10)
                        .padding(.vertical, 8)
                        .overlay(RoundedRectangle(cornerRadius: 5.0).strokeBorder(Color.gray, style: StrokeStyle(lineWidth: 2.0)))
                        .padding(.horizontal, 10)
    //                    .background(RoundedRectangle(cornerRadius: 10)
    //                                    .stroke(Color.gray, lineWidth: 3)
    //                    )
    //                    .textFieldStyle(RoundedBorderTextFieldStyle())
    //                    .frame(height: 30)
                        .onReceive(
                            viewModel.$addressLocation
                                .debounce(for: .seconds(2.2), scheduler: DispatchQueue.main)
                        ) {
                            guard !$0.isEmpty else { return }
                            print(">> searching for: \($0)")
                            self.viewModel.findLocationNew(location: $0)
                        }
                        .onChange(of: self.viewModel.addressLocation) { newValue in
    //                        print(newValue)
    //                        self.viewModel.queryLocation(location: newValue)
                            self.viewModel.setIsLoadingResult(status: true)
                            self.viewModel.emptyMessage = ""
                        }

                    if brandSettings.curBrand == 1 {
                        Button(action: {
                            openMap.toggle()
                        }) {
                            Text("Pilih lewat peta")
                                .font(.custom("Poppins-Regular", size: 10))
                            
                        }.buttonStyle(PrimaryButtonMapStyle())
                            .padding(.horizontal, 4)
                            .fullScreenCover(isPresented: $openMap) {
                                MapView(viewModel: viewModel)
                            }
                        
                    } else {
                        Button(action: {
                            openMap.toggle()
                        }) {
                            Text("Pilih lewat peta")
                                .font(.custom("Poppins-Regular", size: 10))
                            
                        }.buttonStyle(SecondaryButtonMapStyle())
                            .padding(.horizontal, 4)
                            .fullScreenCover(isPresented: $openMap) {
                                MapView(viewModel: viewModel)
                            }
                        
                    }
                    
                }
                
                if self.viewModel.isLoadingResult {
                    Section {
                        ZStack {
                            Circle()
                                .fill(Color.orange)
                                .frame(width: 20, height: 20)
                                .offset(x: leftOffset)
                                .opacity(0.7)
                                .animation(Animation.easeInOut(duration: 1))
                            Circle()
                                .fill(Color.orange)
                                .frame(width: 20, height: 20)
                                .offset(x: leftOffset)
                                .opacity(0.7)
                                .animation(Animation.easeInOut(duration: 1).delay(0.2))
                            Circle()
                                .fill(Color.orange)
                                .frame(width: 20, height: 20)
                                .offset(x: leftOffset)
                                .opacity(0.7)
                                .animation(Animation.easeInOut(duration: 1).delay(0.4))
                        }
                        .onReceive(timer) { (_) in
                            swap(&self.leftOffset, &self.rightOffset)
                        }
                    }.padding(.top, 60)
                } else {
                    VStack(alignment: .leading) {
                        ForEach(self.viewModel.locationQueryListNew, id: \.self) { l in
//                            NavigationLink(destination: UserAddressNewView(
//                                    locLat: l.geometry.location.lat,
//                                    locLng: l.geometry.location.lng,
//                                    locName: l.name,
//                                    locAddress: l.formattedAddress,
//                                    vm: self.viewModel
//                            ).onAppear {
////                                presentationMode.wrappedValue.dismiss()
//                            }) {
                            Button(action: {
//                                isPresentedNew.toggle()
                                self.viewModel.selectLocation(
                                    locLat: 0.0,
                                    locLng: 0.0,
                                    locName: l.name,
                                    locAddress: l.formattedAddress
                                )
                                self.viewModel.findLocationDetail(id: l.id)
                                self.presentationMode.wrappedValue.dismiss()

                            }) {
                                VStack(alignment: .leading) {
                                    VStack(alignment: .leading) {
                                        Text(l.name)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .padding(.bottom, 3)
                                            .multilineTextAlignment(.leading)
                                            .font(.custom("Poppins-Medium", size: 15))
                                        
                                        Text(l.formattedAddress)
    //                                        .font(.system(size: 11))
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .multilineTextAlignment(.leading)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                    }.padding()
    //                                .background(Color.red)
                                    .frame(
                                        minWidth: 0,
                                        maxWidth: .infinity,
                                        alignment: .topLeading
                                    )
                                    
                                    Rectangle().frame(height: 1)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                }
                                .frame(
                                    minWidth: 0,
                                    maxWidth: .infinity,
                                    alignment: .topLeading
                                )
//                            }
                            }
                        }
                    }.fullScreenCover(isPresented: $viewModel.isPresentedNew) {
                        AddNewAddressModalView(vm: self.viewModel)
                    }
                }
                
                if !self.viewModel.emptyMessage.isEmpty {
                    Text(self.viewModel.emptyMessage)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .padding(.top, 20)
                        .multilineTextAlignment(.leading)
                        .font(.custom("Poppins-Medium", size: 15))
                }
            }
            .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
        }
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

struct AddNewAddressModalView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var vm: UserAddressViewModel
    
    var body: some View {
        VStack {
            Header(title: "New Address", back: true)
            
            ZStack(alignment: .topLeading) {
                VStack {
                    // Form
                    ScrollView {
                        Group {
                            VStack(alignment: .leading) {
                                Group {
                                    VStack(alignment: .leading) {
                                        Text(self.vm.locName)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .font(.custom("Poppins-Medium", size: 15))
                                        Text(self.vm.locAddress)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                            .font(.custom("Poppins-Regular", size: 12))
                                    }.padding()
                                    .frame(
                                        minWidth: 0,
                                        maxWidth: .infinity,
                                        alignment: .topLeading
                                    )
                                }
                                
                                if self.vm.recipientCity != "" {
                                    Group {
                                        Text("City: " + self.vm.recipientCity)
        //                                Text("Postcode: " + self.vm.recipientPostcode)
                                    }.padding(.horizontal, 15)
                                    .font(.custom("Poppins-Regular", size: 15))
                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                    
                                    Group {
                                        CustomTextField(placeHolder: "Label Alamat", value: $vm.addressLabel, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                        Spacer().frame(height: 30)
                                        CustomTextField(placeHolder: "Nama Penerima", value: $vm.recipientName, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                        Spacer().frame(height: 30)
                                        CustomTextField(placeHolder: "Nomor Handphone", value: $vm.recipientPhone, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: true)
                                        Spacer().frame(height: 30)
                                        CustomTextField(placeHolder: "Detail Alamat", value: $vm.recipientAddress, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                        Spacer().frame(height: 30)
                                        CustomTextField(placeHolder: "Kode Pos", value: $vm.recipientPostcode, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: true)
                                        Spacer().frame(height: 30)
                                    }
                                } else {
                                    Text("Mohon Maaf lokasi ini diluar radius pengantaran")
                                        .font(.custom("Poppins-Regular", size: 15))
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        .padding()
                                }
                            }
                        }
                    }
                    // Form
                    
                    // Button save
                    VStack(alignment: .leading) {
                        if self.vm.recipientCity != "" {
                            if brandSettings.curBrand == 1 {
                                Button(action: {
                                    self.vm.saveNewAddress(locLat: String(self.vm.selectedLat), locLng: String(self.vm.selectedLng)) { (isSuccess) in
                                        if isSuccess {
        //                                    self.vm.setDismiss()
                                            presentationMode.wrappedValue.dismiss()
                                        } else {
                                            
                                        }
                                    }
                                }) {
                                    HStack {
                                        Spacer()
                                        Text("Save New Address")
                                            .font(.custom("Poppins-Bold", size: 16))
                                        Spacer()
                                    }
                                }.buttonStyle(PrimaryButtonStyle())
                                .frame(maxWidth: .infinity)
                                .alert(isPresented: $vm.showAlert) {
                                    Alert(title: Text("Error"), message: Text("Please complete the form"), dismissButton: .default(Text("Okay")))
                                }
                            } else {
                                Button(action: {
                                    self.vm.saveNewAddress(locLat: String(self.vm.selectedLat), locLng: String(self.vm.selectedLng)) { (isSuccess) in
                                        if isSuccess {
        //                                    self.vm.setDismiss()
                                            presentationMode.wrappedValue.dismiss()
                                        } else {
                                            
                                        }
                                    }
                                }) {
                                    HStack {
                                        Spacer()
                                        Text("Save New Address")
                                            .font(.custom("Poppins-Bold", size: 16))
                                        Spacer()
                                    }
                                }.buttonStyle(PrimaryButtonStyleJcoR())
                                .frame(maxWidth: .infinity)
                                .alert(isPresented: $vm.showAlert) {
                                    Alert(title: Text("Error"), message: Text("Please complete the form"), dismissButton: .default(Text("Okay")))
                                }
                            }
                           
                        }
                    }
                    .padding(.horizontal, 10)
                    .frame(
                        minWidth: 0,
                        maxWidth: .infinity,
                        alignment: .topLeading
                    )
                    .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
                }
                
                Group {
                    Loader(isLoading: $vm.isLoading).zIndex(2)
                }
            }.frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
        }
        .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
    }
}

//struct UserAddressListView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserAddressListView()
//    }
//}
