//
//  MapViewController.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 13/04/22.
//

import GoogleMaps
import UIKit

class MapViewController: UIViewController {

  let map =  GMSMapView(frame: .zero)
  var isAnimating: Bool = false

  override func loadView() {
    super.loadView()
    self.view = map
  }
}

