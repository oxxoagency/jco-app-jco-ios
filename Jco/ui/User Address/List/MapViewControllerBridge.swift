//
//  MapViewControllerBridge.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 13/04/22.
//

import GoogleMaps
import SwiftUI

struct MapViewControllerBridge: UIViewControllerRepresentable {
    
    var markers: GMSMarker = GMSMarker()
    var viewModel: UserAddressViewModel
    
    func makeUIViewController(context: Context) -> MapViewController {
        let uiViewController = MapViewController()
        uiViewController.map.delegate = context.coordinator
        markers.position = CLLocationCoordinate2D(latitude: Double(UserDefaults.standard.object(forKey: "userLatitude") as? String ?? "") ?? 0.0, longitude: Double(UserDefaults.standard.object(forKey: "userLongitude") as? String ?? "") ?? 0.0)
        let camera = GMSCameraPosition.camera(withLatitude: Double(UserDefaults.standard.object(forKey: "userLatitude") as? String ?? "") ?? 0.0, longitude: Double(UserDefaults.standard.object(forKey: "userLongitude") as? String ?? "") ?? 0.0, zoom: 15.0)
        markers.map = uiViewController.map
        markers.map?.camera = camera
        markers.isDraggable = true
        return uiViewController
    }
    
    func updateUIViewController(_ uiViewController: MapViewController, context: Context) {
        
    }
    
    func makeCoordinator() -> MapViewCoordinator {
        return MapViewCoordinator(self, viewModel: viewModel, markers: markers)
    }
    
    
    final class MapViewCoordinator: NSObject, GMSMapViewDelegate {
        var mapViewControllerBridge: MapViewControllerBridge
        var viewModel: UserAddressViewModel
        var markers: GMSMarker
        
        init(_ mapViewControllerBridge: MapViewControllerBridge, viewModel: UserAddressViewModel, markers: GMSMarker) {
            self.mapViewControllerBridge = mapViewControllerBridge
            self.viewModel = viewModel
            self.markers = markers
        }
        
        func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
            let gmsGeocoder = GMSGeocoder()
            gmsGeocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude), completionHandler: { response, error in
                self.viewModel.tempAddressFromMap = response?.firstResult()?.lines?.joined() ?? ""
                self.viewModel.tempLatitudeFromMap = marker.position.latitude
                self.viewModel.tempLongitudeFromMap = marker.position.longitude
                self.markers.snippet = self.viewModel.tempAddressFromMap
            })
        }
    }
}

