//
//  UserAddressListViewModel.swift
//  Jco
//
//  Created by Ed on 03/04/21.
//

import Foundation
import Combine

class UserAddressListViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    @Published var detaildata = [UserAddressListData]()
    @Published var memberHandphone = UserDefaults.standard.object(forKey: "MemberHandphone") as? String ?? "Not Logged"
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getUserAddressList() {
        let cancellable = self.getUserAddressList(hp: memberHandphone)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    break
                }
                
            }) { (detail) in
                self.detaildata = detail.data
        }
        cancellables.insert(cancellable)
    }
    
}
