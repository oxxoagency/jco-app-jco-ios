//
//  UserAddressNewLocationView.swift
//  Jco
//
//  Created by Ed on 12/06/21.
//

import SwiftUI
import GooglePlaces

struct GoogleMapWrapper: UIViewControllerRepresentable {
    typealias UIViewControllerType = GmapViewController
    
    var vc: GmapViewController
    
    public init() {
        self.vc = GmapViewController()
    }
    
    func makeUIViewController(context: Context) -> GmapViewController {
        return vc
    }

    func updateUIViewController(_ uiViewController: GmapViewController, context: Context) {
//        code
    }
}

struct PlacePicker: UIViewControllerRepresentable {
    @ObservedObject var viewModel: UserAddressViewModel
    func makeCoordinator() -> Coordinator {
        Coordinator(self, viewModel: viewModel)
    }
    @Environment(\.presentationMode) var presentationMode
    @Binding var address: String

    func makeUIViewController(context: UIViewControllerRepresentableContext<PlacePicker>) -> GMSAutocompleteViewController {

        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = context.coordinator


        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                                                  UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))
        autocompleteController.placeFields = fields

        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        filter.country = "ID"
        autocompleteController.autocompleteFilter = filter
        return autocompleteController
    }

    func updateUIViewController(_ uiViewController: GMSAutocompleteViewController, context: UIViewControllerRepresentableContext<PlacePicker>) {
    }

    class Coordinator: NSObject, UINavigationControllerDelegate, GMSAutocompleteViewControllerDelegate {
        @ObservedObject var viewModel: UserAddressViewModel
        var parent: PlacePicker

        init(_ parent: PlacePicker, viewModel: UserAddressViewModel) {
            self.parent = parent
            self.viewModel = viewModel
        }

        func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
            DispatchQueue.main.async {
                print(place.description.description as Any)
                self.parent.address =  place.name!
                self.parent.presentationMode.wrappedValue.dismiss()
                self.viewModel.locName = "\(String(describing: place.name!)) \(String(describing: place.formattedAddress!))"
                self.viewModel.latitude = String(place.coordinate.latitude)
                self.viewModel.longitude = String(place.coordinate.longitude)
                self.viewModel.getCity(lat: place.coordinate.latitude, lng: place.coordinate.longitude)
            }
        }

        func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
            print("Error: ", error.localizedDescription)
        }

        func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            self.parent.presentationMode.wrappedValue.dismiss()
        }

    }
}


struct UserAddressNewLocationView: View {
//    @ObservedObject var viewModel = UserAddressViewModel()
    @ObservedObject var viewModel: UserAddressViewModel
    @ObservedObject var cartVm: CartViewModel
    @EnvironmentObject var brandSettings: BrandSettings
    
    @State var locationQuery: String = ""
    @State private var shouldAnimate = false
    let timer = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
    @State var leftOffset: CGFloat = -100
    @State var rightOffset: CGFloat = 100
    
    @State var selectedLat = 0.0
    @State var selectedLng = 0.0
    @State var selectedName = ""
    @State var selectedAddress = ""
    @State var selectedActive = false
    
    var body: some View {
        VStack {
            Header(title: "New Address", back: true)
            
            ScrollView {
                TextField("Find Location", text: $viewModel.addressLocation)
                    .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("jcor_gold"))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    .padding(.horizontal, 10)
                    .padding(.vertical, 8)
                    .overlay(RoundedRectangle(cornerRadius: 5.0).strokeBorder(Color.gray, style: StrokeStyle(lineWidth: 2.0)))
                    .padding(.horizontal, 10)
//                    .background(RoundedRectangle(cornerRadius: 10)
//                                    .stroke(Color.gray, lineWidth: 3)
//                    )
//                    .textFieldStyle(RoundedBorderTextFieldStyle())
//                    .frame(height: 30)
                    .onReceive(
                        viewModel.$addressLocation
                            .debounce(for: .seconds(2.2), scheduler: DispatchQueue.main)
                    ) {
                        guard !$0.isEmpty else { return }
                        print(">> searching for: \($0)")
                        self.viewModel.queryLocation(location: $0)
                    }
                    .onChange(of: self.viewModel.addressLocation) { newValue in
//                        print(newValue)
//                        self.viewModel.queryLocation(location: newValue)
                        self.viewModel.setIsLoadingResult(status: true)
                    }
                
                if self.viewModel.isLoadingResult {
                    Section {
                        ZStack {
                            Circle()
                                .fill(brandSettings.curBrand == 1 ? Color.orange : Color("jcor_gold"))
                                .frame(width: 20, height: 20)
                                .offset(x: leftOffset)
                                .opacity(0.7)
                                .animation(Animation.easeInOut(duration: 1))
                            Circle()
                                .fill(brandSettings.curBrand == 1 ? Color.orange : Color("jcor_gold"))
                                .frame(width: 20, height: 20)
                                .offset(x: leftOffset)
                                .opacity(0.7)
                                .animation(Animation.easeInOut(duration: 1).delay(0.2))
                            Circle()
                                .fill(brandSettings.curBrand == 1 ? Color.orange : Color("jcor_gold"))
                                .frame(width: 20, height: 20)
                                .offset(x: leftOffset)
                                .opacity(0.7)
                                .animation(Animation.easeInOut(duration: 1).delay(0.4))
                        }
                        .onReceive(timer) { (_) in
                            swap(&self.leftOffset, &self.rightOffset)
                        }
                    }.padding(.top, 60)
                } else {
                    VStack(alignment: .leading) {
                        ForEach(self.viewModel.locationQueryList, id: \.self) { l in
//                            NavigationLink(destination: UserAddressNewView(
//                                    locLat: l.geometry.location.lat,
//                                    locLng: l.geometry.location.lng,
//                                    locName: l.name,
//                                    locAddress: l.formattedAddress,
//                                    vm: self.viewModel
//                            )) {
                            Button(action: {
                                selectedLat = l.geometry.location.lat
                                selectedLng = l.geometry.location.lng
                                selectedName = l.name
                                selectedAddress = l.formattedAddress
                                selectedActive = true
                            }) {
                                VStack(alignment: .leading) {
                                    VStack(alignment: .leading) {
                                        Text(l.name)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .padding(.bottom, 3)
                                            .font(.custom("Poppins-Medium", size: 15))
                                        
                                        Text(l.formattedAddress)
    //                                        .font(.system(size: 11))
                                            .font(.custom("Poppins-Regular", size: 12))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                    }.padding()
    //                                .background(Color.red)
                                    .frame(
                                        minWidth: 0,
                                        maxWidth: .infinity,
                                        alignment: .topLeading
                                    )
                                    
                                    Rectangle().frame(height: 1)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                }
                                .frame(
                                    minWidth: 0,
                                    maxWidth: .infinity,
                                    alignment: .topLeading
                                )
//                            }
                            }
                        }.background(
                            NavigationLink(destination: UserAddressNewView(
                                locLat: self.selectedLat,
                                locLng: self.selectedLng,
                                locName: self.selectedName,
                                locAddress: self.selectedAddress,
                                vm: self.viewModel
                            ), isActive: $selectedActive) {
                                EmptyView()
                            }
                        )
                    }
                }
            }
            .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) :  Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
            
//            VStack(alignment: .leading) {
//                Button(action: {
//                    self.viewModel.saveNewAddress()
//                }) {
//                    HStack {
//                        Spacer()
//                        Text("Select this Location")
//                        Spacer()
//                    }
//                }.buttonStyle(PrimaryButtonStyle())
//                .frame(maxWidth: .infinity)
//                .alert(isPresented: $viewModel.showAlert) {
//                    Alert(title: Text("Error"), message: Text("Please choose and address"), dismissButton: .default(Text("Okay")))
//                }
//            }
//            .padding(.horizontal, 10)
//            .frame(
//                minWidth: 0,
//                maxWidth: .infinity,
//                alignment: .topLeading
//            )
//            .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
        }.navigationBarHidden(true)
            .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) :  Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

//struct UserAddressNewLocationView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserAddressNewLocationView()
//    }
//}
