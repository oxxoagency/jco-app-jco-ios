//
//  MapView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 13/04/22.
//

import GoogleMaps
import SwiftUI
import CoreLocation

struct MapView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel: UserAddressViewModel
    @State var zoomInCenter: Bool = false
    @State var expandList: Bool = false
    @State var selectedMarker: GMSMarker?
    @State var yDragTranslation: CGFloat = 0
    
    var body: some View {
        
        let scrollViewHeight: CGFloat = 80
        
        GeometryReader { geometry in
            ZStack(alignment: .top) {
                // Map
                let diameter = zoomInCenter ? geometry.size.width : (geometry.size.height * 2)
                MapViewControllerBridge(viewModel: viewModel)
                    .clipShape(
                        Circle()
                            .size(
                                width: diameter,
                                height: diameter
                            )
                            .offset(
                                CGPoint(
                                    x: (geometry.size.width - diameter) / 2,
                                    y: (geometry.size.height - diameter) / 2
                                )
                            )
                    )
                    .animation(.easeIn)
                    .background(Color(red: 254.0/255.0, green: 1, blue: 220.0/255.0))
                
                HStack {
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("Cancel")
                            .font(.custom("Poppins-Bold", size: 14))
                            .frame(maxWidth: .infinity)
                    }.buttonStyle(SecondaryButtonStyle())
                        .padding(.horizontal, 2)
                        .frame(maxWidth: .infinity)
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                        
//                        if viewModel.locAddress.isEmpty {
//                            // Default Location
//                            let gmsGeocoder = GMSGeocoder()
//                            gmsGeocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D(latitude: Double(UserDefaults.standard.object(forKey: "userLatitude") as? String ?? "") ?? 0.0, longitude: Double(UserDefaults.standard.object(forKey: "userLongitude") as? String ?? "") ?? 0.0), completionHandler: { response, error in
//                                self.viewModel.tempAddressFromMap = response?.firstResult()?.lines?.joined() ?? ""
//                                self.viewModel.tempLatitudeFromMap = Double(UserDefaults.standard.object(forKey: "userLatitude") as? String ?? "") ?? 0.0
//                                self.viewModel.tempLongitudeFromMap = Double(UserDefaults.standard.object(forKey: "userLongitude") as? String ?? "") ?? 0.0
//                            })
//                        }
                        
                        self.viewModel.isPresented = false
                        self.viewModel.locAddress = self.viewModel.tempAddressFromMap
                        self.viewModel.latitude = String(self.viewModel.tempLatitudeFromMap)
                        self.viewModel.longitude = String(self.viewModel.tempLongitudeFromMap)
                        self.viewModel.getCity(lat: self.viewModel.tempLatitudeFromMap, lng: self.viewModel.tempLongitudeFromMap)
                        
                    }) {
                        Text("Save")
                            .font(.custom("Poppins-Bold", size: 14))
                            .frame(maxWidth: .infinity)
                    }.buttonStyle(PrimaryButtonStyle())
                        .padding(.horizontal, 2)
                        .frame(maxWidth: .infinity)
                }
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .offset(
                    x: 0,
                    y: geometry.size.height - (expandList ? scrollViewHeight + 150 : scrollViewHeight)
                )
                .offset(x: 0, y: self.yDragTranslation)
                .animation(.spring())
                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                
                
            }
        }
    }
}
