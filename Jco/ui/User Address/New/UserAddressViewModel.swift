//
//  UserAddressViewModel.swift
//  Jco
//
//  Created by Ed on 20/04/21.
//

import Foundation
import Combine
import UIKit
import MapKit
import Contacts
import GooglePlaces

class UserAddressViewModel: ObservableObject, ModelService {
    @Published var detaildata = [UserAddressListData]()
    
    @Published var addressLocation = ""
    @Published var addressLabel = ""
    @Published var recipientName = ""
    @Published var recipientPhone = ""
    @Published var recipientPostcode = ""
    @Published var recipientCity = ""
    @Published var recipientAddress = ""
    @Published var recipientAddressDetail = ""
    @Published var latitude = ""
    @Published var longitude = ""
    @Published var locName = ""
    @Published var locAddress = ""
    @Published var isDismiss = false
    @Published var locationQuery = ""
    @Published var emptyMessage = ""
    
    @Published var isLoading = false
    @Published var isLoadingResult = false
    @Published var showAlert = false
    @Published var navigateAddressList = false
    @Published var memberHandphone = UserDefaults.standard.object(forKey: "MemberHandphone") as? String ?? "Not Logged"
    
    @Published var locationQueryList = [LocationQuery]()
    @Published var locationQueryListNew = [LocationQueryNew]()
    @Published var userCityData = [UserCityOutlet]()
    
    @Published var isPresented = false
    @Published var isPresentedNew = false
    @Published var selectedLng = 0.0
    @Published var selectedLat = 0.0
    
    @Published var tempAddressFromMap = ""
    @Published var tempLatitudeFromMap = 0.0
    @Published var tempLongitudeFromMap = 0.0

    
    var apiSession: APIService
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func findLocationNew(location: String){
            let token = GMSAutocompleteSessionToken.init()
            
            // Create a type filter.
            let filter = GMSAutocompleteFilter()
            filter.type = .address
            filter.country = "ID"
            self.emptyMessage = ""
            
            let placesClient = GMSPlacesClient()
            
            placesClient.findAutocompletePredictions(fromQuery: location,
                                                     filter: filter,
                                                     sessionToken: token,
                                                     callback: { (results, error) in
                if let error = error {
                    print("Autocomplete error: \(error)")
                    self.setIsLoadingResult(status: false)
                    return
                }
                
                if let results = results {
                    self.locationQueryListNew.removeAll()
                    for result in results {
                        self.locationQueryListNew.append(LocationQueryNew(formattedAddress: result.attributedFullText.string, name: result.attributedPrimaryText.string, id: result.placeID))
                        self.setIsLoadingResult(status: false)
                    }
                    if (results.isEmpty) {
                        self.setIsLoadingResult(status: false)
                        self.emptyMessage = "Location not found"
                    }
                }
            })
        }
        
        func findLocationDetail(id: String) {
            let fields: GMSPlaceField =  GMSPlaceField(rawValue: UInt(GMSPlaceField.coordinate.rawValue))
            let placesClient = GMSPlacesClient()
            placesClient.fetchPlace(fromPlaceID: id, placeFields: fields, sessionToken: nil, callback: {
              (place: GMSPlace?, error: Error?) in
              if let error = error {
                print("An error occurred: \(error.localizedDescription)")
                return
              }
              if let place = place {
                  print("CHECK \(place.coordinate.latitude)")
                  self.getCity(lat: place.coordinate.latitude, lng: place.coordinate.longitude)
                  self.latitude = String(place.coordinate.latitude)
                  self.longitude = String(place.coordinate.longitude)
              }
            })
        }

    
    func selectLocation(locLat: Double, locLng: Double, locName: String, locAddress: String) {
        self.selectedLat = locLat
        self.selectedLng = locLng
        self.locName = locName
        self.locAddress = locAddress
        self.getCity(lat: locLat, lng: locLng)
//        self.isPresentedNew = true
    }
    
    func getCity(lat: Double, lng: Double) {
        let cancellable = self.getUserCity(lat: String(lat), lng: String(lng))
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    break
                }
                
            }) { (result) in
                self.userCityData = result.data
                self.recipientCity = result.data[0].outletCity
            }
        cancellables.insert(cancellable)
    }
    
    func getCity2(lat: Double, lng: Double) {
        print(self.latitude)
        print(self.longitude)
//        let location = CLLocation(latitude: Double(self.lat) ?? 6.2088, longitude: Double(self.lng) ?? 106.8456)
        let location = CLLocation(latitude: lat, longitude: lng)
        location.placemark { placemark, error in
            guard let placemark = placemark else {
                print("Error:", error ?? "nil")
                return
            }
            print(placemark.zipCode ?? "")
            print(placemark.city ?? "")
            self.recipientPostcode = placemark.zipCode ?? ""
            self.recipientCity = placemark.city ?? ""
        }
    }
    
    func saveNewAddress(locLat: String, locLng: String, _ completion: @escaping ((Bool)->Void)) {
        if addressLabel != "" && recipientName != "" && recipientPhone != "" && recipientPostcode != "" {
            self.isLoading = true
            
            let cancellable = self.addUserAddress(
                memberHandphone: memberHandphone,
                addressLabel: addressLabel,
                recipientName: recipientName,
                recipientPhone: recipientPhone,
                recipientPostcode: recipientPostcode,
                recipientCity: recipientCity,
                recipientAddress: locName + locAddress,
                recipientAddressDetail: recipientAddress,
                latitude: self.latitude,
                longitude: self.longitude

            )
                .sink(receiveCompletion: { result in
                    switch result {
                    case .failure(let error):
                        print("Handle error: \(error)")
                        completion(false)
                        self.isLoading = false
                    case .finished:
//                        self.isDismiss = false
                        self.navigateAddressList = true
                        break
                    }
                    
                }) { (result) in
                    self.addressLabel = ""
                    self.recipientName = ""
                    self.recipientPhone = ""
                    self.recipientPostcode = ""
                    self.recipientAddress = ""
//                    self.isDismiss = false
//                    self.navigateAddressList = true
                    self.getUserAddressList()
                    self.isPresented = false
                    completion(true)
                }
            cancellables.insert(cancellable)
        } else {
            completion(false)
            self.showAlert = true
        }
    }
    
    func getUserAddressList() {
        self.isLoading = true
        let cancellable = self.getUserAddressList(hp: memberHandphone)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                    break
                }
                
            }) { (detail) in
                self.detaildata = detail.data
                self.isLoading = false
        }
        cancellables.insert(cancellable)
    }
    
    func queryLocation(location: String) {
        let cancellable = self.getLocationQuery(location: location)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    break
                }
                
            }) { (result) in
                self.locationQueryList = result.results
                self.isLoadingResult = false
            }
        cancellables.insert(cancellable)
    }
    
    func setDismiss() {
        self.isDismiss = false
    }
    
    func setIsLoadingResult(status: Bool) {
        self.isLoadingResult = status
    }
    
    func deleteAddress(addressID: Int) {
        self.isLoading = true
        let cancellable = self.deleteUserAddress(addressID: addressID)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                    self.isLoading = false
                case .finished:
                    break
                    self.isLoading = false
                }
                
            }) { (res) in
                print(res)
                self.getUserAddressList()
//                self.detaildata = detail.data
        }
        cancellables.insert(cancellable)
    }
    
//    func queryGmap() {
//        // Set up the autocomplete filter.
//        let filter = GMSAutocompleteFilter()
//
//        // Create a new session token.
//        let token: GMSAutocompleteSessionToken = GMSAutocompleteSessionToken.init()
//
//        // Create the fetcher.
//        var fetcher = GMSAutocompleteFetcher(filter: filter)
//        fetcher?.delegate = self
//        fetcher?.provide(token)
//    }
}

extension CLPlacemark {
    /// street name, eg. Infinite Loop
    var streetName: String? { thoroughfare }
    /// // eg. 1
    var streetNumber: String? { subThoroughfare }
    /// city, eg. Cupertino
    var city: String? { locality }
    /// neighborhood, common name, eg. Mission District
    var neighborhood: String? { subLocality }
    /// state, eg. CA
    var state: String? { administrativeArea }
    /// county, eg. Santa Clara
    var county: String? { subAdministrativeArea }
    /// zip code, eg. 95014
    var zipCode: String? { postalCode }
    /// postal address formatted
    @available(iOS 11.0, *)
    var postalAddressFormatted: String? {
        guard let postalAddress = postalAddress else { return nil }
        return CNPostalAddressFormatter().string(from: postalAddress)
    }
}

extension CLLocation {
    func placemark(completion: @escaping (_ placemark: CLPlacemark?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first, $1) }
    }
}
