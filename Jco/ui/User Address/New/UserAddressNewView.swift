//
//  UserAddressNewView.swift
//  Jco
//
//  Created by Ed on 03/04/21.
//

import SwiftUI

struct UserAddressNewView: View {
    @Environment(\.presentationMode) var presentationMode
//    @ObservedObject var viewModel = UserAddressViewModel()
    @ObservedObject var vm: UserAddressViewModel
    @EnvironmentObject var brandSettings: BrandSettings
//    @ObservedObject var cartVm: CartViewModel
    @State var textValue: String = "Jakarta"
    
    let locName: String
    let locAddress: String
    let locLat: Double
    let locLng: Double
    
    init(locLat: Double, locLng: Double, locName: String, locAddress: String, vm: UserAddressViewModel) {
        self.locLat = locLat
        self.locLng = locLng
        self.locName = locName
        self.locAddress = locAddress
        self.vm = vm
    }
    
    @State var name: String = ""
    @State var handphone: String = ""
    @State var address: String = ""
    
    var body: some View {
        Header(title: "New Address", back: true)
        ZStack(alignment: .topLeading) {
            VStack {
                // Form
                ScrollView {
                    Group {
                        VStack(alignment: .leading) {
                            Group {
                                VStack(alignment: .leading) {
                                    Text("Tambahkan Tempat Tersimpan")
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color("theme_background"))
                                        .font(.custom("Poppins-Medium", size: 20))
                                    Text("Simpan tempat favorit Anda")
                                        .foregroundColor(Color.gray)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color("theme_background"))
                                        .font(.custom("Poppins-Regular", size: 16))

                                }.padding()
                                .frame(
                                    minWidth: 0,
                                    maxWidth: .infinity,
                                    alignment: .topLeading
                                )
                            }
                            
//                            if self.vm.recipientCity != "" {
                                Group {
                                    Button(action: {
                                        self.vm.isPresented.toggle()
                                    }) {
                                        VStack(alignment: .leading) {
                                            Text(self.vm.locAddress.isEmpty ? "Pilih Lokasi Kamu" : self.vm.locAddress)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color("theme_background"))
                                                .multilineTextAlignment(.leading)
                                            Rectangle().frame(height: 2)
                                                .foregroundColor(Color("border_primary"))
                                            Spacer().frame(height: 30)
                                        }
                                    }.fullScreenCover(isPresented: $vm.isPresented) {
                                        ChooseLocationModalView(viewModel: self.vm)
                                        
                                    }

                                }.padding(.horizontal, 15)
                                .foregroundColor(Color("theme_text"))
                                
                                Group {
                                    CustomTextField(placeHolder: "Label Alamat", value: $vm.addressLabel, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                    Spacer().frame(height: 30)
                                    CustomTextField(placeHolder: "Nama Penerima", value: $vm.recipientName, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                    Spacer().frame(height: 30)
                                    CustomTextField(placeHolder: "Nomor Handphone", value: $vm.recipientPhone, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                    Spacer().frame(height: 30)
                                    CustomTextField(placeHolder: "Detail Alamat", value: $vm.recipientAddress, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                    Spacer().frame(height: 30)
                                    CustomTextField(placeHolder: "Kode Pos", value: $vm.recipientPostcode, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                    Spacer().frame(height: 30)
                                }
//                            }
//                            else {
//                                Text("Mohon Maaf lokasi ini diluar radius pengantaran")
//                                    .font(.custom("Poppins-Regular", size: 15))
//                                    .foregroundColor(Color("theme_text"))
//                                    .padding()
//                            }
                        }
                    }
                }
                // Form
                
                // Button save
                VStack(alignment: .leading) {
                    if self.vm.recipientCity != "" {
                        if brandSettings.curBrand == 1 {
                            Button(action: {
                                self.vm.saveNewAddress(locLat: String(locLat), locLng: String(locLng)) { (isSuccess) in
                                    if isSuccess {
                                        self.vm.setDismiss()
                                        presentationMode.wrappedValue.dismiss()
                                    } else {
                                        
                                    }
                                }
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Save New Address")
                                        .font(.custom("Poppins-Bold", size: 16))
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
                            .frame(maxWidth: .infinity)
                            .alert(isPresented: $vm.showAlert) {
                                Alert(title: Text("Error"), message: Text("Please complete the form"), dismissButton: .default(Text("Okay")))
                            }
                        } else {
                            Button(action: {
                                self.vm.saveNewAddress(locLat: String(locLat), locLng: String(locLng)) { (isSuccess) in
                                    if isSuccess {
                                        self.vm.setDismiss()
                                        presentationMode.wrappedValue.dismiss()
                                    } else {
                                        
                                    }
                                }
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Save New Address")
                                        .font(.custom("Poppins-Bold", size: 16))
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyleJcoR())
                            .frame(maxWidth: .infinity)
                            .alert(isPresented: $vm.showAlert) {
                                Alert(title: Text("Error"), message: Text("Please complete the form"), dismissButton: .default(Text("Okay")))
                            }
                        }
                    }
                }
                .padding(.horizontal, 10)
                .frame(
                    minWidth: 0,
                    maxWidth: .infinity,
                    alignment: .topLeading
                )
                .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
            }
            
            Group {
                Loader(isLoading: $vm.isLoading).zIndex(2)
            }
        }.onAppear {
            self.vm.getCity(lat: self.locLat, lng: self.locLng)
        }.navigationBarHidden(true)
            .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
    }
}

//struct UserAddressNewView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserAddressNewView()
//    }
//}
