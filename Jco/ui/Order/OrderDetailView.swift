//
//  OrderDetailView.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import SwiftUI
import PartialSheet

struct OrderDetailView: View {
    @ObservedObject var vm = OrderDetailViewModel()
    @EnvironmentObject var brandSettings: BrandSettings
    @State var isPresented = false
    let orderId: String
    let from: String
    init(orderId: String, from: String) {
        self.orderId = orderId
        self.from = from
        self.vm.orderId = orderId
    }
    
    var body: some View {
        VStack {
            Header(title: "Order Detail", back: true)
            
            ZStack(alignment: .topLeading) {
                if self.vm.isLoading == false {
                    Group {
                        ScrollView {
                            VStack(alignment: .leading) {
                                if let data = self.vm.detaildata {
                                    VStack(alignment: .leading) {
                                        HStack {
                                            Text("payment-status")
                                                .font(.custom("Poppins-Regular", size: 15))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            Spacer()
                                            if data.orderStatus == 1 {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                                    .font(.system(size: 15))
                                            } else if data.orderStatus == 2 {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(Color.green)
                                                    .font(.system(size: 15))
                                            } else if data.orderStatus == 3 {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(Color.green)
                                                    .font(.system(size: 15))
                                            } else if data.orderStatus == 5 {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(Color.green)
                                                    .font(.system(size: 15))
                                            } else if data.orderStatus == 6 {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(Color.green)
                                                    .font(.system(size: 15))
                                            } else if data.orderStatus == 8 {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                                    .font(.system(size: 15))
                                            } else {
                                                Text(data.orderStatusName ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                                    .font(.system(size: 15))
                                            }
                                        }.padding(.bottom, 10)
                                        
                                        HStack {
                                            Text("delivery-information")
                                                .font(.custom("Poppins-Medium", size: 18))
                                                .fontWeight(.bold)
                                                .padding(.bottom, 8)
                                                .multilineTextAlignment(.leading)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            
                                            Spacer()
                                            
                                            if self.vm.detaildata?.deliveryType == 0 {
                                                Button(action: {
                                                    self.isPresented = true
                                                }) {
                                                    HStack {
                                                        Text("check-order-status")
                                                            .font(.custom("Poppins-Regular", size: 10))
                                                            .bold()
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                            .cornerRadius(10)
                                                            .padding(.horizontal, 8)
                                                            .padding(.vertical, 8)
                                                    }
                                                }.background(brandSettings.curBrand == 1 ? Color("btn_primary").opacity(0.1) : Color("jcor_gold").opacity(0.1))

                                            }
                                        }.partialSheet(isPresented: $isPresented) {
                                            OrderStatusView(viewModel: self.vm)
                                        }

                                        
                                        Text("\((self.vm.detaildata?.deliveryType ?? 0) == 0 ? "Delivery From" : "Pick Up At")")
                                            .font(.custom("Poppins-Regular", size: 14))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                            .padding(.bottom, 2)
                                            .padding(.leading, 3)
                                        
                                        Text(self.vm.detaildata?.orderOutletName ?? "")
                                            .font(.custom("Poppins-Regular", size: 15))
                                            .padding(.bottom, 1.5)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
                                        Text(self.vm.detaildata?.orderOutletAddress ?? "")
                                            .font(.custom("Poppins-Regular", size: 13))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                        
                                        Spacer().frame(height: 25)
                                        
                                        if (self.vm.detaildata?.deliveryType ?? 0 == 0) {
                                            Text("Delivery To")
                                                .font(.custom("Poppins-Regular", size: 14))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                                .padding(.bottom, 2)
                                                .padding(.leading, 3)
                                            
                                            Text(self.vm.detaildata?.memberName ?? "")
                                                .font(.custom("Poppins-Medium", size: 15))
                                                .padding(.bottom, 1.5)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            
                                            Text(self.vm.detaildata?.orderAddress ?? "")
                                                .font(.custom("Poppins-Regular", size: 15))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            
                                            Text(self.vm.detaildata?.orderAddressinfo ?? "")
                                                .font(.custom("Poppins-Regular", size: 14))
                                                .padding(.bottom, 1.5)
                                                .foregroundColor(Color("theme_text"))

                                            
                                            Text(self.vm.detaildata?.orderCity ?? "")
                                                .font(.custom("Poppins-Regular", size: 14))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                        }
                                    }.padding(.vertical, 10)
                                
                                    Group {
                                        Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                    }
                                
                                    VStack(alignment: .leading) {
                                        Text("Ringkasan Pesanan")
                                            .font(.custom("Poppins-Medium", size: 18))
                                            .fontWeight(.bold)
                                            .padding(.bottom, 8)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
                                        Text("Order")
                                            .font(.custom("Poppins-Regular", size: 14))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                            .padding(.bottom, 1)
                                            .padding(.top, 3)
                                        
                                        Text(self.vm.detaildata?.orderId ?? "")
                                            .font(.custom("Poppins-Regular", size: 15))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
                                        if data.orderStatus == 1 && (self.vm.detaildata?.paymentMethod == "bca_va" ||  self.vm.detaildata?.paymentMethod == "mandiri_va"  || self.vm.detaildata?.paymentMethod == "bca"  || self.vm.detaildata?.paymentMethod == "mandiri" ||
                                            self.vm.detaildata?.paymentMethod == "echannel") {
                                            Text("Virtual Account Number")
                                                .font(.custom("Poppins-Regular", size: 14))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                .padding(.bottom, 1)
                                                .padding(.top, 3)
                                            
                                            if self.vm.detaildata?.paymentMethod == "bca"  || self.vm.detaildata?.paymentMethod == "mandiri" || self.vm.detaildata?.paymentMethod == "bca_va" {
                                                Text(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            } else if self.vm.detaildata?.paymentMethod == "echannel" {
                                                Text(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            } else {
                                                Text(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "")
                                                    .font(.custom("Poppins-Regular", size: 15))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            }
                                        }
                                        
                                        if (self.vm.detaildata?.paymentMethod == "bca_va" || self.vm.detaildata?.paymentMethod == "bca") && data.orderStatus == 1 {
                                            Text("""
                                                Harap melakukan pembayaran dalam jangka waktu 1 jam setelah submit order
                                                
                                                CARA PEMBAYARAN VIRTUAL ACCOUNT BCA:

                                                Cara pembayaran via ATM BCA
                                                1. Pada menu utama, pilih "Transaksi Lainnya"
                                                2. Pilih "Transfer"
                                                3. Pilih ke Rek "BCA Virtual Account"
                                                4. Masukkan nomor \(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "") lalu tekan "Benar"
                                                5. Pada halaman konfirmasi transfer akan muncul detail pembayaran Anda. Jika informasi telah sesuai tekan "Ya"


                                                Cara pembayaran via Klik BCA
                                                1. Pilih menu "Transfer Dana"
                                                2. Pilih "Transfer ke BCA Virtual Account"
                                                3. Masukkan nomor BCA Virtual Account  \(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "")
                                                4. Jumlah yang akan ditransfer, nomor rekening dan nama merchant akan muncul di halaman konfirmasi pembayaran, jika informasi benar klik "Lanjutkan"
                                                5. Masukkan respon KEYBCA APPLI 1 yang muncul pada Token BCA Anda, lalu klik tombol "Kirim"
                                                6. Transaksi Anda selesai


                                                Cara pembayaran via m-BCA
                                                1. Pilih "m-Transfer"
                                                2. Pilih "Transfer"
                                                3. Pilih "BCA Virtual Account"
                                                4. Pilih nomor rekening yang akan digunakan untuk pembayaran
                                                5. Masukkan nomor BCA Virtual Account \(self.vm.detaildata?.orderPayment.gwVaNumbers ?? ""), lalu pilih "OK"
                                                6. Nomor BCA Virtual Account dan nomor Rekening Anda akan terlihat di halaman konfirmasi rekening
                                                7. Pilih "OK" pada halaman konfirmasi pembayaran
                                                8. Masukkan PIN BCA untuk mengotorisasi pembayaran
                                                9. Transaksi Anda selesai
                                                """)
                                                .font(.custom("Poppins-Regular", size: 13))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                .multilineTextAlignment(.leading)
                                                .padding(.vertical, 5)
                                        }
                                        
                                        if (self.vm.detaildata?.paymentMethod == "echannel" || self.vm.detaildata?.paymentMethod == "echannel") && data.orderStatus == 1 {
                                            Text("""
                                                Harap melakukan pembayaran dalam jangka waktu 1 jam setelah order di submit

                                                CARA PEMBAYARAN MANDIRI BILL PAYMENT

                                                Pembayaran melalui ATM Mandiri:
                                                1. Masukkan PIN Anda
                                                2. Pada menu utama pilih menu "Pembayaran" kemudian pilih menu "Multi Payment"
                                                3. Masukan "Kode Perusahaan" dengan angka 70012
                                                4. Masukan Kode Pembayaran \(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "")
                                                5. Konfirmasi pembayaran Anda


                                                Cara membayar melalui Internet Banking Mandiri:
                                                1. Login ke Mandiri Internet Banking
                                                2. Di Menu Utama silakan pilih "Bayar" kemudian pilih "Multi Payment"
                                                3. Pilih akun anda di "Dari Rekening", kemudian di "Penyedia Jasa" pilih midtrans
                                                4. Masukkan Kode Pembayaran \(self.vm.detaildata?.orderPayment.gwVaNumbers ?? "") dan klik "Lanjutkan"
                                                5. Konfirmasi pembayaran anda menggunakan Mandiri Token
                                                """)
                                                .font(.custom("Poppins-Regular", size: 13))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                .multilineTextAlignment(.leading)
                                                .padding(.vertical, 5)
                                        }
                                        
                                        Text("Tanggal Transaksi")
                                            .font(.custom("Poppins-Regular", size: 14))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                            .padding(.bottom, 1)
                                            .padding(.top, 3)
                                        
                                        Text(self.vm.detaildata?.orderTime ?? "")
                                            .font(.custom("Poppins-Regular", size: 15))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
                                        Text("Produk")
                                            .font(.custom("Poppins-Regular", size: 14))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                            .padding(.bottom, 1)
                                            .padding(.top, 3)
                                        
                                        if let orderDetail = self.vm.detaildata?.orderDetail {
                                            ForEach(orderDetail, id: \.self) { d in
                                                HStack(alignment: .center) {
                                                    VStack(alignment: .leading) {
                                                        Text(String(d.menuQuantity) + " X " + d.menuName)
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                            .font(.custom("Poppins-Medium", size: 14))
                                                        Text(d.menuDetail ?? "")
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                            .font(.custom("Poppins-Regular", size: 12))
                                                    }
                                                    
                                                    Spacer()
                                                    
                                                    Text(Util().addTS(str: String(d.menuPrice)))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                        .font(.custom("Poppins-Medium", size: 14))
                                                }
                                            }
                                        }
                                    }.padding(.vertical, 10)
                                    
                                    Group {
                                        Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                    }
                                
                                    Group {
                                        HStack {
                                            VStack(alignment: .leading) {
                                                Text("Sub Total")
                                                
                                                Spacer().frame(height: 5)
//                                                Text("Eco Bag Besar")
//                                                Spacer().frame(height: 5)
                                                Text("delivery-service")
                                                
                                              if data.jpointInfo?.jpointUsed ?? 0 > 0 {
                                                    Spacer().frame(height: 5)
                                                    Text("jpoint-used")
                                                }
                                                
                                                if data.orderPromo < 0 {
                                                    Spacer().frame(height: 5)
                                                    Text("Promo")
                                                }
                                                
                                                Spacer().frame(height: 5)
                                                Text("Total")
                                                
                                              if data.jpointInfo?.jpointEarn ?? 0 > 0 {
                                                    Spacer().frame(height: 15)
                                                    Text("jpoint-earned")
                                                }
                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            Spacer()
                                            VStack(alignment: .leading) {
                                                Text("Rp. ")
                                                
                                                Spacer().frame(height: 5)
//                                                Text("Rp. ")
//                                                Spacer().frame(height: 5)
                                                Text("Rp. ")
                                                
                                                if data.jpointInfo?.jpointUsed ?? 0 > 0 {
                                                    Spacer().frame(height: 5)
                                                    Text(" ")
                                                }
                                                
                                                if data.orderPromo < 0 {
                                                    Spacer().frame(height: 5)
                                                    Text("Rp. ")
                                                }
                                                
                                                Spacer().frame(height: 5)
                                                Text("Rp. ")
                                                
                                                if data.jpointInfo?.jpointEarn ?? 0 > 0 {
                                                    Spacer().frame(height: 15)
                                                    Text(" ")
                                                }
                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                            VStack(alignment: .trailing) {
                                                Text(Util().addTS(str: String(data.orderSubtotal)))
                                                
                                                Spacer().frame(height: 5)
//                                                Text("0")
//                                                Spacer().frame(height: 5)
                                                Text(Util().addTS(str: String(data.orderFee)))
                                                
                                                if data.jpointInfo?.jpointUsed ?? 0 > 0 {
                                                    Spacer().frame(height: 5)
                                                  Text(Util().addTS(str: String(data.jpointInfo?.jpointUsed ?? 0)))
                                                }
                                                
                                                if data.orderPromo < 0 {
                                                    Text(Util().addTS(str: String(data.orderPromo)))
                                                }
                                                
                                                Spacer().frame(height: 5)
                                                Text(Util().addTS(str: String(data.orderTotal)))
                                                
                                                if data.jpointInfo?.jpointEarn ?? 0 > 0 {
                                                    Spacer().frame(height: 15)
                                                  Text(Util().addTS(str: String(data.jpointInfo?.jpointEarn ?? 0)) + " JPoint")
                                                }
                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                        }
                                        .font(.custom("Poppins-Regular", size: 14))
                                        Spacer().frame(height: 35)
                                        
                                        Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                          .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
                                        Link(destination: URL(string: "https://wa.me/6281288008990")!) {
                                            VStack(alignment: .center) {
                                                Image(systemName: "person.crop.circle.badge.questionmark")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                    .font(.system(size: 50))
                                                    .padding(.leading, 15)
                                                
                                                Text("Need Help?")
                                                    .font(.custom("Poppins-Regular", size: 14))
                                                    .foregroundColor(brandSettings.curBrand == 1 ?  Color("theme_text") : Color("jcor_gold"))
                                            }.padding(.vertical, 15)
                                            .frame(maxWidth: .infinity)
                                        }
                                        
                                    }
                                }
                            }.frame(
                                minWidth: 0,
                                maxWidth: .infinity,
                                alignment: .topLeading
                            ).padding(.horizontal, 8)
                            
                            Spacer()
                        }
                    }
                }
                
                Group {
                    Loader(isLoading: $vm.isLoading).zIndex(2)
                }
            }
        }.onAppear {
            self.vm.getOrderDetail(brand: self.brandSettings.curBrand)
        }.navigationBarHidden(true)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
        .addPartialSheet()
        
    }
}

//struct OrderDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        OrderDetailView()
//    }
//}
