//
//  OrderView.swift
//  Jco
//
//  Created by Ed on 04/05/21.
//

import SwiftUI
import URLImage

struct OrderView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @StateObject var vm = OrderViewModel()
//    @State private var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    var back: Bool
    @ObservedObject var hvm: HomeViewModel
    
    var body: some View {
        VStack {
            ZStack(alignment: .topLeading) {
                Header(title: "my-order", back: self.back)
                
                if self.vm.userLogged {
                    VStack(alignment: .leading) {
                        ScrollView {
                            if self.vm.detaildata.count == 0 {
                                Spacer().frame(height: 40)
                                VStack(alignment: .center) {
                                    Text(self.vm.message)
                                        .multilineTextAlignment(.center)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        .font(.custom("Poppins-Medium", size: 15))
                                }.frame(maxWidth: .infinity)
                            } else {
                                Group {
                                    ForEach(self.vm.detaildata, id: \.self) { d in
                                        NavigationLink(destination: OrderDetailView(orderId: d.orderId, from: "order")) {
                                            VStack(alignment: .leading) {
                                                HStack {
                                                    HStack {
                                                        if d.menuImg != nil {
                                                            URLImage(url: URL(string: d.menuImg ?? "")!,
                                                            content: { image in
                                                                 image
                                                                    .resizable()
                                                                    .aspectRatio(contentMode: .fill)
                                                                    .frame(width: 100, height: 100)
                                                                    .clipped()
                                                            })
                                                        }
                                                    }.frame(minWidth: 100, minHeight: 100)
                                                    .background(Color("c_e6e6e6"))
                                                    .cornerRadius(10)
                                                    
                                                    VStack(alignment: .leading) {
                                                        Text("Order " + d.orderId)
                                                            .font(.custom("Poppins-Medium", size: 15))
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                                            .fontWeight(.bold)
                                                            .padding(.bottom, 3)
                                                        
                                                        Text(d.orderTime)
                                                            .font(.custom("Poppins-Medium", size: 11))
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                                            .padding(.bottom, 1)
                                                        
                                                        if d.orderStatus == 1 {
                                                            Text(d.orderStatusName ?? "")
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        } else if d.orderStatus == 2 {
                                                            Text(d.orderStatusName ?? "")
                                                                .foregroundColor(Color.green)
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        } else if d.orderStatus == 3 {
                                                            Text(d.orderStatusName ?? "")
                                                                .foregroundColor(Color.green)
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        } else if d.orderStatus == 5 {
                                                            Text(d.orderStatusName ?? "")
                                                                .foregroundColor(Color.green)
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        } else if d.orderStatus == 6 {
                                                            Text(d.orderStatusName ?? "")
                                                                .foregroundColor(Color.green)
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        } else if d.orderStatus == 8 {
                                                            Text(d.orderStatusName ?? "")
                                                                .foregroundColor(Color("theme_text_primary"))
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        } else {
                                                            Text(d.orderStatusName ?? "")
                                                                .foregroundColor(Color("theme_text_primary"))
                                                                .font(.custom("Poppins-Medium", size: 13))
                                                        }
                                                        
                                                        
                                                        Text(d.menuList ?? "")
                                                            .font(.custom("Poppins-Regular", size: 13))
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                            .padding(.top, 2)
                                                            .multilineTextAlignment(.leading)
                                                    }.padding(.horizontal, 10)
                                                }
                                                
                                                HStack {
                                                    Text(String(d.orderTotalItem ?? 0) + " Items • ")
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                        .font(.custom("Poppins-Medium", size: 15))
                                                    + Text("Rp. " + Util().addTS(str: String(d.orderTotal)))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                        .font(.custom("Poppins-Medium", size: 15))
                                                }
                                                
                                                Rectangle().frame(height: 1).foregroundColor(Color.gray)
                                            }.frame(
                                                minWidth: 0,
                                                maxWidth: .infinity,
                                                alignment: .topLeading
                                            ).padding(.vertical, 10)
                                            .padding(.horizontal, 15)
                                        }
                                    }
                                }
                            }
                            
                            Spacer()
                                .frame(height: 100)
                        }
                    }.offset(x:0, y: 75)
                } else {
                    VStack(alignment: .center) {
                        Spacer()
                        Text("Please Log In first").multilineTextAlignment(.center)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                            .font(.custom("Poppins-Medium", size: 15))
                        
                        Spacer()
                    }.frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity
                    )
                }
                
                Group {
                    Loader(isLoading: $vm.isLoading).zIndex(2)
                }
            }
            
        }.onAppear {
            self.vm.getOrder(brand: self.brandSettings.curBrand)
        }
        .navigationBarTitle("") //this must be empty
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .background(self.brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.top) : Color("theme_background_jcor").edgesIgnoringSafeArea(.top))
    }
}

//struct OrderView_Previews: PreviewProvider {
//    static var previews: some View {
//        OrderView()
//    }
//}
