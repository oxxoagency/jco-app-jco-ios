//
//  LiveTrackView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 21/07/22.
//

import SwiftUI

struct LiveTrackView: View {
    var url: String?
    var body: some View {
        VStack {
            Header(title: "track-delivery", back: true)
            
            WebView(request: URLRequest(url: URL(string: url ?? "")!))
            
            Spacer()
        }.navigationBarHidden(true)
            .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}
