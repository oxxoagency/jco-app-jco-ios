//
//  OrderViewModel.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation
import Combine

class OrderViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    @Published var detaildata = [OrderNew]()
    @Published var isLoading = true
    @Published var message = "You have no order yet."
    @Published var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getOrder(brand: Int) {
        if userLogged {
            let cancellable = self.getOrder(brand: brand)
                .sink(receiveCompletion: { result in
                    switch result {
                    case .failure(let error):
                        print("Handle error order: \(error)")
                        self.isLoading = false
                    case .finished:
                        break
                    }

                }) { (detail) in
                    print("FINISH")
                    if let orderData = detail.data {
                        self.detaildata = orderData
                    }
                    print(self.detaildata)
                    self.isLoading = false
            }
            cancellables.insert(cancellable)
        } else {
            self.isLoading = false
        }
    }
    
}
