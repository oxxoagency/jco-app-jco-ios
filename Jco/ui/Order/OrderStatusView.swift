//
//  OrderStatusView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 19/07/22.
//

import SwiftUI
import PartialSheet

struct OrderStatusView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var viewModel: OrderDetailViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            orderStatus
            firstPoint
            if viewModel.detaildata?.orderStatus == 2 || viewModel.detaildata?.orderStatus == 3 || viewModel.detaildata?.orderStatus == 4 || viewModel.detaildata?.orderStatus == 5 || viewModel.detaildata?.orderStatus == 6 {
                secondPoint
                thirdPoint
            }
            if viewModel.detaildata?.orderStatus == 6 {
                fourthPoint
            }
        }.frame(
            minWidth: 0,
            maxWidth: .infinity,
            alignment: .leading
        ).padding(.horizontal, 4)
    }
}

extension OrderStatusView {
    
    var orderStatus: some View {
        Text("order-status")
            .font(.custom("Poppins-Regular", size: 16))
            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
            .padding(.bottom, 8)
    }
    
    var firstPoint: some View {
        HStack {
            
            Text("Order sudah diterima dan menunggu konfirmasi pembayaran")
                .font(.custom("Poppins-Regular", size: 14))
                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                .padding(.leading, 24)
                .padding(.top, 8)
        }.overlay(
            GeometryReader { proxy in
                VStack {
                    Image(systemName: "circle.fill")
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .font(.system(size: 20))
                    Rectangle().frame(width: 2, height: proxy.size.height).foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .padding(.top, -8)
                }
                
            }
        )
    }
    
    var secondPoint: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Order sudah dikonfirmasi dan mencari outlet terdekat")
                    .font(.custom("Poppins-Regular", size: 14))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    .padding(.leading, 24)
                    .padding(.top, 8)
                
                Text("Pembayaran Lunas dengan \(viewModel.detaildata?.orderPayment.paymentName ?? "")")
                    .font(.custom("Poppins-Regular", size: 12))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    .padding(.leading, 24)
                
                Text("Waktu Transaksi: \(viewModel.detaildata?.orderTime ?? "")")
                    .font(.custom("Poppins-Regular", size: 12))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    .padding(.leading, 24)
                
            }
        }.overlay(
            GeometryReader { proxy in
                VStack {
                    Image(systemName: "circle.fill")
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .font(.system(size: 20))
                    Rectangle().frame(width: 2, height: proxy.size.height).foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .padding(.top, -8)
                }
                
            }
        )
    }
    
    var thirdPoint: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(viewModel.detaildata?.orderStatusName ?? "")
                    .font(.custom("Poppins-Regular", size: 14))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    .padding(.leading, 24)
                    .padding(.top, 8)
                
                if self.viewModel.detaildata?.orderDelivery != nil {
                    ForEach((self.viewModel.detaildata?.orderDelivery)!, id: \.self) { orderDelivery in
                        if orderDelivery.courier?.name?.isEmpty ?? false {
                            Text(orderDelivery.statusName ?? "")
                                .font(.custom("Poppins-Regular", size: 12))
                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                .padding(.leading, 24)
                        } else {
                            Text("\(orderDelivery.statusName ?? "") Diantar oleh: \(orderDelivery.courier?.name ?? "") (\(orderDelivery.courier?.licensePlate ?? ""))")
                                .fixedSize(horizontal: false, vertical: true)
                                .font(.custom("Poppins-Regular", size: 12))
                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                .padding(.leading, 24)
                            
                            Text("No. HP: \(orderDelivery.courier?.phone ?? "")")
                                .font(.custom("Poppins-Regular", size: 12))
                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                .padding(.leading, 24)
                            
                            if !(orderDelivery.trackingURL?.isEmpty ?? false) {
                                NavigationLink(destination: {
                                    LiveTrackView(url: orderDelivery.trackingURL)
                                }()) {
                                    HStack {
                                        Text("track-delivery")
                                            .font(.custom("Poppins-Regular", size: 10))
                                            .bold()
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                            .cornerRadius(10)
                                            .padding(.horizontal, 8)
                                            .padding(.vertical, 8)
                                    }
                                }.background(brandSettings.curBrand == 1 ? Color("btn_primary").opacity(0.1) : Color("jcor_gold").opacity(0.1))
                                    .padding(.leading, 24)
                            }
                        }
                    }
                }
            }
        }.overlay(
            GeometryReader { proxy in
                VStack {
                    Image(systemName: "circle.fill")
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .font(.system(size: 20))
                    Rectangle().frame(width: 2, height: proxy.size.height).foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .padding(.top, -8)
                }
                
            }
        )
    }
    
    var fourthPoint: some View {
        HStack {
            Text("Order Selesai")
                .font(.custom("Poppins-Regular", size: 14))
                .foregroundColor(Color.green)
                .padding(.leading, 24)
                .padding(.top, 8)
        }.overlay(
            GeometryReader { proxy in
                VStack {
                    Image(systemName: "circle.fill")
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .font(.system(size: 20))
                    Rectangle().frame(width: 2, height: proxy.size.height).foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                        .padding(.top, -8)
                }
                
            }
        )
    }
}
