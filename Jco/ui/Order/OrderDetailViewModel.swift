//
//  OrderDetailViewModel.swift
//  Jco
//
//  Created by Ed on 03/07/21.
//

import Foundation
import Combine

class OrderDetailViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    @Published var detaildata: DetailResData?
    @Published var isLoading = true
    @Published var orderId = ""
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getOrderDetail(brand: Int) {
        let cancellable = self.getOrderDetail(brand: brand,orderId: self.orderId)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error orderdetail: \(error)")
                case .finished:
                    break
                }

            }) { (detail) in
                if let orderData = detail.data {
                    self.detaildata = orderData
                }
                self.isLoading = false
        }
        cancellables.insert(cancellable)
    }

}
