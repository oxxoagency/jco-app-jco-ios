//
//  UserOrder.swift
//  Jco
//
//  Created by Ed on 04/05/21.
//

import SwiftUI

struct UserOrder: View {
    var body: some View {
        VStack {
            Header(title: "my-order", back: true)
            
            Spacer()
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}

struct UserOrder_Previews: PreviewProvider {
    static var previews: some View {
        UserOrder()
    }
}
