//
//  ProfileView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import PartialSheet

struct ProfileView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var user: User
    @ObservedObject var vm: MenuListViewModel
    @ObservedObject var hvm: HomeViewModel
    @State private var accessToken = UserDefaults.standard.object(forKey: "AccessToken") as? String ?? "false"
    @State private var refreshToken = UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? "false"
    @State private var showAlert = false
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    let appBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
    
    let btnNav = [
        Navigation(txt: "change-password", image: UIImage(named: "change_password")!, pages: "password"),
        Navigation(txt: "my-order",  image: UIImage(named: "order")!, pages: "order"),
        Navigation(txt: "my-language", image: UIImage(named: "language")!, pages: "language"),
        Navigation(txt: "contact-us", image: UIImage(named: "contact_us")!, pages: "contact")
    ]
    
    let btnNavJcoR = [
      Navigation(txt: "change-password", image: UIImage(named: "change_password_jcor")!, pages: "password"),
      Navigation(txt: "my-order",  image: UIImage(named: "order_jcor")!, pages: "order"),
      Navigation(txt: "my-language", image: UIImage(named: "language_jcor")!, pages: "language"),
      Navigation(txt: "contact-us", image: UIImage(named: "contact_us_jcor")!, pages: "contact")
  ]
  
    let btnNavGeneral = [
        Navigation(txt: "terms-and-conditions", image: UIImage(named: "term_condition")!, pages: "tnc"),
        Navigation(txt: "privacy-policy", image: UIImage(named: "privacy_policy")!, pages: "pp"),
    ]
    
    let btnNavGeneralJcoR = [
        Navigation(txt: "terms-and-conditions", image: UIImage(named: "term_condition_jcor")!, pages: "tnc"),
        Navigation(txt: "privacy-policy", image: UIImage(named: "privacy_policy_jcor")!, pages: "pp"),
    ]
    
    var body: some View {
        VStack {
            Header(title: "Profile", back: false)
            
            ScrollView {
                VStack{
                    
        //            Text("User")
        //            Text("$\(accessToken)")
        //            Text(refreshToken)
                    
                    Image(uiImage: UIImage(named: "profile")!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 112, height: 112)
                    
                    Text(vm.userFullname)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                        .font(.custom("Poppins-Medium", size: 24))
                    
                    NavigationLink(destination: {
                        ProfileEditView(ivm: vm)
                    }()) {
                        Text("edit-profile")
                            .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                            .font(.custom("Poppins", size: 14))
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Account")
                            .font(.custom("Poppins-Medium", size: 18))
                            .padding(.vertical, 10)
                            .padding(.horizontal, 15)
                            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                        
                      ForEach(brandSettings.curBrand == 1 ? btnNav : btnNavJcoR, id: \.id) { nav in
                            NavigationLink(destination: {
                                VStack{
                                    if nav.pages.contains("password") {
                                        UserChangePassword()
                                    } else if nav.pages.contains("language") {
                                        UserLanguage()
                                    } else if nav.pages.contains("order") {
                                        OrderView(back: true, hvm: self.hvm)
                                    } else if nav.pages.contains("contact") {
                                        ContactUsWebView()
                                    }
                                }
                            }()) {
                                VStack(alignment: .leading) {
                                    HStack {
                                        Image(uiImage: nav.image)
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 46, height: 46)
                                        Text(LocalizedStringKey(nav.txt))
                                            .font(.custom("Poppins-Regular", size: 16))
                                        Spacer()
                                        Image(systemName: "chevron.forward")
                                    }.foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                    .padding(.horizontal, 5)
                                    .padding(.vertical, 10)
                                    
                                    Rectangle()
                                        .fill(Color.gray)
                                        .frame(width: .infinity, height: 1)
                                }.padding(.leading)
                                .padding(.trailing)
                            }
                    
            //                NavigationLink(destination: ForgotPasswordView()) {
            //                    VStack(alignment: .leading) {
            //                        HStack {
            //                            Text("\(nav.txt)")
            //                        }
            //                        Rectangle()
            //                            .fill(Color.red)
            //                            .frame(width: .infinity, height: 1)
            //                    }.padding(.leading)
            //                    .padding(.trailing)
            //
            //                }.frame(maxWidth: .infinity)
                        }
                    }
                    
                    VStack(alignment: .leading) {
                        Text("General")
                            .font(.custom("Poppins-Medium", size: 18))
                            .padding(.vertical, 10)
                            .padding(.horizontal, 15)
                            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                        
                        ForEach(brandSettings.curBrand == 1 ? btnNavGeneral : btnNavGeneralJcoR, id: \.id) { nav in
                            NavigationLink(destination: {
                                VStack{
                                    if nav.pages.contains("tnc") {
                                       UserTnc()
                                    } else if nav.pages.contains("pp") {
                                       UserPrivacy()
                                    }
                                }
                            }()) {
                                VStack(alignment: .leading) {
                                    HStack {
                                        Image(uiImage: nav.image)
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 46, height: 46)
                                        Text(LocalizedStringKey(nav.txt))
                                            .font(.custom("Poppins-Regular", size: 16))
                                        Spacer()
                                        Image(systemName: "chevron.forward")
                                    }.foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                    .padding(.horizontal, 5)
                                    .padding(.vertical, 10)
                                    
                                    
                                    Rectangle()
                                        .fill(Color.gray)
                                        .frame(width: .infinity, height: 1)
                                }.padding(.leading)
                                .padding(.trailing)
                            }
                        }
                    }
                    
                    Spacer().frame(height: 20)
                    
                  if brandSettings.curBrand == 1 {
                    Button(action: {
                        self.removeLocalJco()
                    }) {
                        HStack {
                            Spacer()
                            Text("Sign Out")
                            Spacer()
                        }
                    }.buttonStyle(PrimaryButtonStyle())
                    .font(.custom("Poppins-Medium", size: 16))
                    .padding(.horizontal, 16)
                  } else {
                    Button(action: {
                        self.user.tokenIsActive = false
                        UserDefaults.standard.set(false, forKey: "userIsLogged")
                        UserDefaults.standard.set("", forKey: "AccessToken")
                        UserDefaults.standard.set("", forKey: "RefreshToken")
                        UserDefaults.standard.set("", forKey: "MemberEmail")
                        UserDefaults.standard.set("", forKey: "MemberName")
                        UserDefaults.standard.set("0", forKey: "MemberPhone")
                        UserDefaults.standard.set("", forKey: "MemberPhone2")
                        
                        // Address
                        UserDefaults.standard.set("", forKey: "AddressLabel")
                        UserDefaults.standard.set("", forKey: "AddressName")
                        UserDefaults.standard.set("", forKey: "AddressPhone")
                        UserDefaults.standard.set("", forKey: "AddressUser")
                        UserDefaults.standard.set(0, forKey: "AddressID")
                        UserDefaults.standard.set("Jakarta Barat", forKey: "AddressCity")
                        UserDefaults.standard.set("", forKey: "AddressPostcode")
                        UserDefaults.standard.set("", forKey: "AddressLatitude")
                        UserDefaults.standard.set("", forKey: "AddressLongitude")
                        
                        // Outlet
                        UserDefaults.standard.set("", forKey: "OutletID")
                        UserDefaults.standard.set("", forKey: "OutletName")
                        UserDefaults.standard.set("", forKey: "OutletCity")
                        UserDefaults.standard.set("", forKey: "OutletCode")
                        
                        // Ecobag
                        UserDefaults.standard.set("", forKey: "EcobagName")
                        UserDefaults.standard.set(0, forKey: "EcobagSubtotal")
                        UserDefaults.standard.set(1, forKey: "EcobagQuantity")
                        UserDefaults.standard.set(0, forKey: "DeliveryFee")
                        UserDefaults.standard.set(0, forKey: "FreeDelivery")
                        
                        // Payment
                        UserDefaults.standard.set("", forKey: "PaymentMethod")
                        UserDefaults.standard.set("", forKey: "PaymentMethodName")
                        UserDefaults.standard.set("", forKey: "PaymentMethodIcon")
                        
                        self.vm.logout()
                    }) {
                        HStack {
                            Spacer()
                            Text("Sign Out")
                            Spacer()
                        }
                    }.buttonStyle(PrimaryButtonStyleJcoR())
                    .font(.custom("Poppins-Medium", size: 16))
                    .padding(.horizontal, 16)
                  }
                    
                    Button(action: {
                        self.showAlert = true
                    }) {
                        HStack {
                            Spacer()
                            Text("delete-account")
                            Spacer()
                        }
                    }
                    .buttonStyle(RedButtonStyle())
                    .font(.custom("Poppins-Medium", size: 16))
                    .padding(.horizontal, 16)
                    .padding(.vertical, 8)
                    
                    Text("\(appVersion ?? "")(\(appBuild ?? ""))")
                        .font(.custom("Poppins-Regular", size: 14))
                        .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                }.padding(.bottom, 25)
            }
        }
        .partialSheet(isPresented: $showAlert) {
            DeleteAccountView(isPresented: $showAlert, user: user, vm: self.vm)
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.inline)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.top) : Color("theme_background_jcor").edgesIgnoringSafeArea(.top))
        .background(Color("theme_background").edgesIgnoringSafeArea(.top))
        .alert(isPresented: $vm.isAlertProfile) {
            if vm.isDelete {
                return Alert(title: Text(vm.titleErrorMessageProfile), message: Text(vm.errorMessageProfile), dismissButton: .default(Text("Okay"),action: self.removeLocalJco))
            } else {
                return Alert(title: Text(vm.titleErrorMessageProfile), message: Text(vm.errorMessageProfile), dismissButton: .default(Text("Okay")))
            }
        }
        .addPartialSheet()
        
    }
    
    func removeLocalJco(){
        self.user.tokenIsActive = false
        UserDefaults.standard.set(false, forKey: "userIsLogged")
        UserDefaults.standard.set("", forKey: "AccessToken")
        UserDefaults.standard.set("", forKey: "RefreshToken")
        UserDefaults.standard.set("", forKey: "MemberEmail")
        UserDefaults.standard.set("", forKey: "MemberName")
        UserDefaults.standard.set("0", forKey: "MemberPhone")
        UserDefaults.standard.set("", forKey: "MemberPhone2")
        
        // Address
        UserDefaults.standard.set("", forKey: "AddressLabel")
        UserDefaults.standard.set("", forKey: "AddressName")
        UserDefaults.standard.set("", forKey: "AddressPhone")
        UserDefaults.standard.set("", forKey: "AddressUser")
        UserDefaults.standard.set(0, forKey: "AddressID")
        UserDefaults.standard.set("Jakarta Barat", forKey: "AddressCity")
        UserDefaults.standard.set("", forKey: "AddressPostcode")
        UserDefaults.standard.set("", forKey: "AddressLatitude")
        UserDefaults.standard.set("", forKey: "AddressLongitude")
        
        // Outlet
        UserDefaults.standard.set("", forKey: "OutletID")
        UserDefaults.standard.set("", forKey: "OutletName")
        UserDefaults.standard.set("", forKey: "OutletCity")
        UserDefaults.standard.set("", forKey: "OutletCode")
        
        // Ecobag
        UserDefaults.standard.set("", forKey: "EcobagName")
        UserDefaults.standard.set(0, forKey: "EcobagSubtotal")
        UserDefaults.standard.set(1, forKey: "EcobagQuantity")
        UserDefaults.standard.set(0, forKey: "DeliveryFee")
        UserDefaults.standard.set(0, forKey: "FreeDelivery")
        
        // Payment
        UserDefaults.standard.set("", forKey: "PaymentMethod")
        UserDefaults.standard.set("", forKey: "PaymentMethodName")
        UserDefaults.standard.set("", forKey: "PaymentMethodIcon")
        
        self.vm.logout()
    }
}

//struct ProfileView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileView(user: User())
//    }
//}

struct Navigation {
    var id = UUID()
    var txt: String
    var image: UIImage
    var pages: String
}
