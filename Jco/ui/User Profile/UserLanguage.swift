//
//  UserLanguage.swift
//  Jco
//
//  Created by Ed on 04/05/21.
//

import SwiftUI

struct UserLanguage: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @EnvironmentObject var langSettings: LanguageSettings
    @Environment(\.presentationMode) var presentationMode
    
    let btnNav = [
        Navigation(txt: "Indonesia", image: UIImage(named: "indonesia")!, pages: "tnc"),
        Navigation(txt: "English", image: UIImage(named: "english")!, pages: "pp"),
    ]
    
    var body: some View {
        VStack {
            Header(title: "my-language", back: true)
            
            VStack(alignment: .leading) {
                HStack {
                    RadioButtonGroups { selected in
                        print("LANG: " + selected)
                        langSettings.lang = selected
                        // change language here
                        UserDefaults.standard.set(selected, forKey: "AppLanguage")
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }
                
            }.padding(.leading)
                .padding(.trailing)
            
            Spacer()
        }.navigationBarHidden(true)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

struct RadioButtonField: View {
    @EnvironmentObject var brandSettings: BrandSettings
    let id: String
    let label: String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    let isMarked:Bool
    let callback: (String)->()
    let image: UIImage
    
    init(
        id: String,
        label:String,
        size: CGFloat = 20,
        color: Color = Color.black,
        textSize: CGFloat = 14,
        isMarked: Bool = false,
        callback: @escaping (String)->(),
        image: UIImage
    ) {
        self.id = id
        self.label = label
        self.size = size
        self.color = color
        self.textSize = textSize
        self.isMarked = isMarked
        self.callback = callback
        self.image = image
    }
    
    var body: some View {
        Button(action:{
            self.callback(self.id)
        }) {
            VStack{
                HStack(alignment: .center, spacing: 10) {
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 46, height: 46)
                    Text(label)
                        .font(.custom("Poppins-Regular", size: 16))
                    
                    Spacer()
                    Image(systemName: self.isMarked ? "largecircle.fill.circle" : "circle")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: self.size, height: self.size)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                }.foregroundColor(brandSettings.curBrand == 1 ? self.color : Color.white)
                Rectangle()
                    .fill(Color.gray)
                    .frame(width: .infinity, height: 1)
            }
        }
        .foregroundColor(Color.white)
    }
}

enum Language: String {
    case indonesia = "Indonesia"
    case english = "English"
}

struct RadioButtonGroups: View {
    let callback: (String) -> ()
    
    @State var selectedId: String = UserDefaults.standard.object(forKey: "AppLanguage") as? String ?? "id"
    
    var body: some View {
        VStack {
            radioIndonesiaMajority
            radioEnglishMajority
        }
    }
    
    var radioIndonesiaMajority: some View {
        RadioButtonField(
            id: "id",
            label: Language.indonesia.rawValue,
            isMarked: selectedId == "id" ? true : false,
            callback: radioGroupCallback,
            image: UIImage(named: "indonesia")!
        )
    }
    
    var radioEnglishMajority: some View {
        RadioButtonField(
            id: "en",
            label: Language.english.rawValue,
            isMarked: selectedId == "en" ? true : false,
            callback: radioGroupCallback,
            image: UIImage(named: "english")!
        )
    }
    
    func radioGroupCallback(id: String) {
        selectedId = id
        callback(id)
    }
}

struct UserLanguage_Previews: PreviewProvider {
    static var previews: some View {
        UserLanguage()
    }
}
