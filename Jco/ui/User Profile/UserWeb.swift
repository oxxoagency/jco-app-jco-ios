//
//  UserWeb.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 09/06/22.
//

import SwiftUI

struct UserWeb: View {
    var body: some View {
        VStack {
            Header(title: "JCO", back: true)
            
            WebView(request: URLRequest(url: URL(string: "https://jcodonuts.com")!))
            
            Spacer()
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}

