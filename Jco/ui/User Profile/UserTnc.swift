//
//  UserTnc.swift
//  Jco
//
//  Created by Ed on 04/05/21.
//

import SwiftUI

struct UserTnc: View {
    var body: some View {
        VStack {
            Header(title: "terms-and-conditions", back: true)
            
            WebView(request: URLRequest(url: URL(string: "https://order.jcodelivery.com/policies/privacy")!))
            
            Spacer()
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}

struct UserTnc_Previews: PreviewProvider {
    static var previews: some View {
        UserTnc()
    }
}
