//
//  DeleteAccountView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 26/07/22.
//

import SwiftUI
import PartialSheet

struct DeleteAccountView: View {
    @Binding var isPresented: Bool
    @ObservedObject var user: User
    @ObservedObject var vm: MenuListViewModel
    @EnvironmentObject var partialSheetManager: PartialSheetManager
    @State private var isSecured: Bool = true
    
    var body: some View {
        VStack {
            Text("delete-account")
                .padding(.horizontal, 16)
                .font(.custom("Poppins-Regular", size: 16))
                .foregroundColor(Color.black)
                
            Text("confirmation-delete-account")
                .padding(.horizontal, 16)
                .font(.custom("Poppins-Regular", size: 14))
                .foregroundColor(Color.black)
                
            ZStack(alignment: .trailing) {
                if isSecured {
                    CustomTextField(placeHolder: "Password", value: $vm.password, lineColor: Color("border_primary"), width: 2, secure: true, is_disabled: false, is_number: false)
                        .foregroundColor(Color("theme_text"))
                }else {
                    CustomTextField(placeHolder: "Password", value: $vm.password, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                        .foregroundColor(Color("theme_text"))
                }
                Button(action: {
                    isSecured.toggle()
                }) {
                    Image(systemName: self.isSecured ? "eye.slash" : "eye")
                        .accentColor(.gray)
                }.padding(.horizontal,10)
            }

            
            HStack {
                Button(action: {
                    self.partialSheetManager.closePartialSheet()
                }) {
                    Text("Cancel")
                        .font(.custom("Poppins-Bold", size: 14))
                        .frame(maxWidth: .infinity)
                }.buttonStyle(SecondaryButtonStyle())
                    .padding(.horizontal, 2)
                    .frame(maxWidth: .infinity)
                Button(action: {
                    self.partialSheetManager.closePartialSheet()
                    self.vm.deleteAccountVM({ (isDelete) in
                        vm.isDelete = isDelete
                    })
                  
                    
                }) {
                    Text("delete-account")
                        .font(.custom("Poppins-Bold", size: 14))
                        .frame(maxWidth: .infinity)
                }.buttonStyle(PrimaryButtonStyle())
                    .padding(.horizontal, 2)
                    .frame(maxWidth: .infinity)
            }.padding()
            
        }
    }
}
