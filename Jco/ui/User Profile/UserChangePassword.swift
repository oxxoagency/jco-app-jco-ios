//
//  UserChangePassword.swift
//  Jco
//
//  Created by Ed on 28/09/21.
//

import SwiftUI

struct UserChangePassword: View {
    @ObservedObject var vm = ChangePasswordViewModel()
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack {
            Header(title: "change-password", back: true)
            
            ZStack(alignment: .topLeading) {
                VStack {
                    Spacer().frame(height: 40)
                    
                    ZStack(alignment: .trailing) {
//                        if isSecuredOldPassword {
                        CustomTextField(placeHolder: "Old Password", value: $vm.oldPass, lineColor: Color("border_primary"), width: 2, secure: self.vm.isSecuredOldPassword, is_disabled: false, is_number: false)
                                .foregroundColor(Color("theme_text"))
                            
//                        } else {
//                            CustomTextField(placeHolder: "Old Password", value: $vm.oldPass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
//                                .foregroundColor(Color("theme_text"))
//                        }
                        Button(action: {
                            self.vm.isSecuredOldPassword.toggle()
                        }) {
                            Image(systemName: self.vm.isSecuredOldPassword ? "eye.slash" : "eye")
                                .accentColor(.gray)
                        }.padding(.horizontal,10)
                    }
                    Spacer().frame(height: 40)
                    
                    ZStack(alignment: .trailing) {
//                        if isSecuredNewPassword {
                        CustomTextField(placeHolder: "New Password", value: $vm.newPass, lineColor: Color("border_primary"), width: 2, secure: self.vm.isSecuredNewPassword, is_disabled: false, is_number: false)
                                .foregroundColor(Color("theme_text"))
//                        } else {
//                            CustomTextField(placeHolder: "New Password", value: $vm.newPass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
//                                .foregroundColor(Color("theme_text"))
//                        }
                        Button(action: {
                            self.vm.isSecuredNewPassword.toggle()
                        }) {
                            Image(systemName: self.vm.isSecuredNewPassword ? "eye.slash" : "eye")
                                .accentColor(.gray)
                        }.padding(.horizontal,10)
                    }
                    Spacer().frame(height: 40)
                    
                    ZStack(alignment: .trailing) {
//                        if isSecuredRepeatPassword {
                        CustomTextField(placeHolder: "Repeat Password", value: $vm.rePass, lineColor: Color("border_primary"), width: 2, secure: self.vm.isSecuredRepeatPassword, is_disabled: false, is_number: false)
                                .foregroundColor(Color("theme_text"))
//                        } else {
//                            CustomTextField(placeHolder: "Repeat Password", value: $vm.rePass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
//                                .foregroundColor(Color("theme_text"))
//                        }
                        Button(action: {
                            self.vm.isSecuredRepeatPassword.toggle()
                        }) {
                            Image(systemName: self.vm.isSecuredRepeatPassword ? "eye.slash" : "eye")
                                .accentColor(.gray)
                        }.padding(.horizontal,10)
                    }
                    Spacer()
                    
                    VStack {
                      if brandSettings.curBrand == 1 {
                        Button(action: {
                            self.vm.changePassword() { (isSuccess) in
                                if isSuccess {
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                            }
                        }) {
                            HStack {
                                Spacer()
                                Text("Save")
                                    .font(.custom("Poppins-Medium", size: 18))
                                Spacer()
                            }
                        }.buttonStyle(PrimaryButtonStyle())
                        .font(.custom("Poppins-Medium", size: 16))
                        .alert(isPresented: $vm.showAlert) {
                            Alert(title: Text("Error"), message: Text(self.vm.alertMessage), dismissButton: .default(Text("Okay")))
                        }
                      } else {
                        Button(action: {
                            self.vm.changePassword() { (isSuccess) in
                                if isSuccess {
                                    self.presentationMode.wrappedValue.dismiss()
                                }
                            }
                        }) {
                            HStack {
                                Spacer()
                                Text("Save")
                                    .font(.custom("Poppins-Medium", size: 18))
                                Spacer()
                            }
                        }.buttonStyle(PrimaryButtonStyleJcoR())
                        .font(.custom("Poppins-Medium", size: 16))
                        .alert(isPresented: $vm.showAlert) {
                            Alert(title: Text("Error"), message: Text(self.vm.alertMessage), dismissButton: .default(Text("Okay")))
                        }
                      }
                        
//                        Button(action: {
//                            self.presentationMode.wrappedValue.dismiss()
//                        }) {
//                            HStack {
//                                Spacer()
//                                Text("Cancel")
//                                    .font(.custom("Poppins-Medium", size: 18))
//                                Spacer()
//                            }
//                        }.buttonStyle(SecondaryButtonStyle())
//                        .font(.custom("Poppins-Medium", size: 16))
                    }.padding(.horizontal, 15)
                }
                
                Group {
                    Loader(isLoading: $vm.isLoading).zIndex(2)
                }
            }
            
            Spacer()
        }.navigationBarHidden(true)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

struct UserChangePassword_Previews: PreviewProvider {
    static var previews: some View {
        UserChangePassword()
    }
}
