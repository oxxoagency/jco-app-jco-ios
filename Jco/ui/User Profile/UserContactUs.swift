//
//  UserContactUs.swift
//  Jco
//
//  Created by Ed on 04/05/21.
//

import SwiftUI

struct UserContactUs: View {
    @State var isWeb = false
    @EnvironmentObject var brandSettings: BrandSettings
    let btnNav = [
        Navigation(txt: "www.jcodonuts.com", image: UIImage(named: "language")!, pages: "web"),
        Navigation(txt: "hello@jcodelivery.com", image: UIImage(named: "contact_us")!, pages: "mail"),
        Navigation(txt: "0812-8800-8990", image: UIImage(named: "contact_us")!, pages: "phone"),
    ]
    
    let btnNavJcoR = [
        Navigation(txt: "www.jcodonuts.com", image: UIImage(named: "language_jcor")!, pages: "web"),
        Navigation(txt: "hello@jcodelivery.com", image: UIImage(named: "contact_us_jcor")!, pages: "mail"),
        Navigation(txt: "0812-8800-8990", image: UIImage(named: "contact_us_jcor")!, pages: "phone"),
    ]
    
    let btnSocmed = [
        Navigation(txt: "iamjcolovers", image: UIImage(named: "facebook")!, pages: "fb"),
        Navigation(txt: "jcoindonesia", image: UIImage(named: "instagram")!, pages: "ig"),
        Navigation(txt: "J.CO Donuts & Coffee", image: UIImage(named: "youtube")!, pages: "language"),
    ]
    
    var body: some View {
//        VStack {
//            Header(title: "Contact Us", back: true)
//
//            WebView(request: URLRequest(url: URL(string: "https://order.jcodelivery.com/help/ios")!))
//
//            Spacer()
//        }.navigationBarHidden(true)
//        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
        
        VStack(alignment: .leading) {
            Header(title: "contact-us", back: true)

            ScrollView {
            VStack(alignment: .leading) {
                Text("Contact")
                    .font(.custom("Poppins-Medium", size: 18))
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)

                ForEach(brandSettings.curBrand == 1 ? btnNav : btnNavJcoR, id: \.id) { nav in
                    Button(action: {
                        switch nav.pages {
                        case "web":
                            self.isWeb = true
                        case "mail":
                            if let url = URL(string: "mailto:\(nav.txt)") {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                        case "phone":
                            if let url = URL(string: "tel://\("081288008990")") {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                            
                        default:
                            break
                        }

                    }) {
                        HStack {
                            Image(uiImage: nav.image)
                                .resizable()
                                .scaledToFit()
                                .frame(width: 46, height: 46)
                            Text("\(nav.txt)")
                                .font(.custom("Poppins-Regular", size: 16))
                            Spacer()
                            Image(systemName: "chevron.forward")
                        }.foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                        .padding(.horizontal, 5)
                        .padding(.vertical, 10)
                    }
           
                    Rectangle()
                        .fill(Color.gray)
                        .frame(width: .infinity, height: 1)
                }

                Text("Social Media")
                    .font(.custom("Poppins-Medium", size: 18))
                    .padding(.vertical, 10)
                    .padding(.horizontal, 15)
                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)

                ForEach(btnSocmed, id: \.id) { sm in
                    Button(action: {
                        switch sm.pages {
                        case "fb":
                            UIApplication.tryURL(urls: [
                                "fb://profile/iamjcolovers", // App
                                "http://www.facebook.com/iamjcolovers" // Website if app fails
                            ])
                        case "ig":
                            UIApplication.tryURL(urls: [
                                "instagram://user?username=jcoindonesia", // App
                                "https://www.instagram.com/jcoindonesia/" // Website if app fails
                            ])
                        default:
                            break
                        }

                    }) {
                        HStack {
                            Image(uiImage: sm.image)
                                .resizable()
                                .scaledToFit()
                                .frame(width: 46, height: 46)
                            Text("\(sm.txt)")
                                .font(.custom("Poppins-Regular", size: 16))
                            Spacer()
                            Image(systemName: "chevron.forward")
                        }.foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                        .padding(.horizontal, 5)
                        .padding(.vertical, 10)

                    }
                    Rectangle()
                        .fill(Color.gray)
                        .frame(width: .infinity, height: 1)
                }
                
                Group {
                    Text("Alamat")
                        .font(.custom("Poppins-Medium", size: 17))
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    
                    Text("""
                        Mal Kelapa Gading 1 & 2 Lantai Dasar Unit G-140 C & D
                        Jalan Boulevar Kelapa Gading
                        Kelapa Gading
                        Jakarta Utara
                        """)
                        .font(.custom("Poppins-Regular", size: 14))
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .lineLimit(nil)

                }.padding(.vertical, 10)
                    
                Rectangle()
                    .fill(Color.gray)
                    .frame(width: .infinity, height: 1)
                    
                Group {
                    Text("LAYANAN PENGADUAN KONSUMEN")
                        .font(.custom("Poppins-Medium", size: 17))
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                    
                    Text("""
                        DITJEN PERLINDUNGAN KONSUMEN & TERTIB NIAGA KEMENDAG RI
                        0853-1111-1010
                        """)
                        .font(.custom("Poppins-Regular", size: 14))
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .lineLimit(nil)
                }.padding(.vertical, 10)
            }.padding(.horizontal, 15)

            Spacer()
            }
            
            NavigationLink(destination: UserWeb(), isActive: $isWeb) {
                EmptyView()
            }

        }.navigationBarHidden(true)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)!) {
                if #available(iOS 10.0, *) {
                    application.open(URL(string: url)!)
                } else {
                    application.openURL(URL(string: url)!)
                }
                return
            }
        }
    }
}


struct UserContactUs_Previews: PreviewProvider {
    static var previews: some View {
        UserContactUs()
    }
}
