//
//  ChangePasswordViewModel.swift
//  Jco
//
//  Created by Ed on 28/09/21.
//

import Foundation
import Combine

class ChangePasswordViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    
    @Published var isLoading = false
    @Published var oldPass = ""
    @Published var newPass = ""
    @Published var rePass = ""
    @Published var showAlert = false
    @Published var alertMessage = ""
    
    @Published var isSecuredOldPassword = true
    @Published var isSecuredNewPassword = true
    @Published var isSecuredRepeatPassword = true
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func changePassword(_ completion: @escaping ((Bool)->Void)) {
        if self.oldPass != "" && self.newPass != "" && self.rePass != "" {
            if self.newPass.count >= 5 {
                if self.newPass == self.rePass {
                    self.isLoading = true
                    
                    let cancellable = self.updatePassword(pass: self.oldPass, newpass: self.newPass, confpass: self.rePass)
                        .sink(receiveCompletion: { result in
                            switch result {
                            case .failure(let error):
                                print("Handle error update: \(error)")
                                self.showAlert = true
                                self.alertMessage = "Update Failed. Wrong Password"
                                self.isLoading = false
                                completion(false)
                            case .finished:
                                break
                            }

                        }) { (result) in
                            print(result)
                            if result.statusCode == 200 {
                                self.showAlert = true
                                self.alertMessage = "Password Successfuly Updated"
                                self.newPass = ""
                                self.oldPass = ""
                                self.rePass  = ""
                                completion(true)
                            } else {
                                completion(false)
                            }
                            self.isLoading = false
                    }
                    cancellables.insert(cancellable)
                } else {
                    completion(false)
                    self.showAlert = true
                    self.alertMessage = "Password and Confirm Password does not match"
                }
            } else {
                completion(false)
                self.showAlert = true
                self.alertMessage = "Password length must be more than 5 characters"
            }
        } else {
            completion(false)
            self.showAlert = true
            self.alertMessage = "Please complete the form"
        }
    }
}
