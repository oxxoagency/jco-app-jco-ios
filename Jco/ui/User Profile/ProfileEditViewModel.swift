//
//  ProfileEditViewModel.swift
//  Jco
//
//  Created by Ed on 18/08/21.
//

import Foundation
import Combine

class ProfileEditViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    var cancellables = Set<AnyCancellable>()
    
    @Published var isLoading = false
    @Published var name = UserDefaults.standard.object(forKey: "MemberName") as? String ?? ""
    @Published var memberPhone = UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? ""
    @Published var gender = 0
    @Published var dateOfBirth = ""
    @Published var showingAlert = false
    @Published var alertTitle = ""
    @Published var errorMsg = ""
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func saveEdit(_ completion: @escaping ((Bool)->Void)) {
        if name != "" {
            self.isLoading = true
            
            let cancellable = self.updateProfile(phone: memberPhone, name: name, gender: gender)
                .sink(receiveCompletion: { result in
                    switch result {
                    case .failure(let error):
                        print("Handle error edit: \(error)")
                    case .finished:
                        break
                    }
                }) { (detail) in
                    if detail.statusCode == 200 {
                        self.showingAlert = true
                        self.alertTitle = "Success"
                        self.errorMsg = "Update Successful"
                        
                        UserDefaults.standard.set(detail.data.memberName, forKey: "MemberName")
                        completion(true)
                    } else {
                        self.showingAlert = true
                        self.alertTitle = "Error"
                        self.errorMsg = "Update Error"
                        completion(false)
                    }
                    self.isLoading = false
            }
            cancellables.insert(cancellable)
        } else {
            completion(false)
            self.errorMsg = "Name cannot be empty"
        }
    }
}
