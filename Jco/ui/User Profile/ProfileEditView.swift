//
//  ProfileEditView.swift
//  Jco
//
//  Created by Ed on 30/04/21.
//

import SwiftUI

struct ProfileEditView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var viewModel = ProfileEditViewModel()
    @ObservedObject var ivm: MenuListViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack {
            Header(title: "edit-profile", back: true)
            
            ZStack(alignment: .topLeading) {
                VStack(alignment: .leading) {
                    ScrollView {
                        Group {
                            Image(uiImage: UIImage(named: "profile")!)
                                .resizable()
                                .scaledToFit()
                                .frame(width: 112, height: 112)
                            
                            Text(self.viewModel.name)
                                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                .font(.custom("Poppins-Medium", size: 24))
                        
                            
                            CustomTextField(placeHolder: "Full Name", value: self.$viewModel.name, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                            
                            CustomTextField(placeHolder: "Phone Number", value: self.$viewModel.memberPhone, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: true, is_number: false)
                            
                            GenderRadioButtonGroups { selected in
                                self.viewModel.gender = selected
                            }
                            
//                            CustomTextField(placeHolder: "Gender", value: self.$viewModel.gender, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
                            
                            CustomTextField(placeHolder: "Date of birth", value: self.$viewModel.dateOfBirth, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
//                            CustomTextField(placeHolder: "", value: self.$viewModel.email, lineColor: Color("border_primary"), width: 2, secure: false)
//                            Text("Password")
//                            CustomTextField(placeHolder: "* * * * * *", value: self.$viewModel.pass, lineColor: Color("border_primary"), width: 2, secure: true)
//                            Text("Confirm Password")
//                            CustomTextField(placeHolder: "* * * * * *", value: self.$viewModel.repass, lineColor: Color("border_primary"), width: 2, secure: true)
                        }.padding(.horizontal, 10)
                        .padding(.vertical, 10)
                        
                        Spacer()
                        
                      if brandSettings.curBrand == 1 {
                        HStack {
                            Button(action: {
                                self.viewModel.saveEdit() { (isSuccess) in
                                    if isSuccess {
//                                        self.user.tokenIsActive = true
                                        self.presentationMode.wrappedValue.dismiss()
                                        self.ivm.userFullname = self.viewModel.name
                                    } else {
//                                        self.showingAlert = true
                                    }
                                }
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Save")
                                        .font(.custom("Poppins-Medium", size: 16))
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
                            .alert(isPresented: $viewModel.showingAlert) {
                                Alert(title: Text(self.viewModel.alertTitle), message: Text(self.viewModel.errorMsg), dismissButton: .default(Text("Okay")))
                            }
                        }.padding(.vertical)
                        .padding(.horizontal)
                        
                        HStack {
                            Button(action: {
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Cancel")
                                        .font(.custom("Poppins-Medium", size: 16))
                                    Spacer()
                                }
                            }.buttonStyle(SecondaryButtonStyle())
                        }.padding(.vertical)
                        .padding(.horizontal)
                        
                      } else {
                        HStack {
                            Button(action: {
                                self.viewModel.saveEdit() { (isSuccess) in
                                    if isSuccess {
//                                        self.user.tokenIsActive = true
                                        self.presentationMode.wrappedValue.dismiss()
                                        self.ivm.userFullname = self.viewModel.name
                                    } else {
//                                        self.showingAlert = true
                                    }
                                }
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Save")
                                        .font(.custom("Poppins-Medium", size: 16))
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyleJcoR())
                            .alert(isPresented: $viewModel.showingAlert) {
                                Alert(title: Text(self.viewModel.alertTitle), message: Text(self.viewModel.errorMsg), dismissButton: .default(Text("Okay")))
                            }
                        }.padding(.vertical)
                        .padding(.horizontal)
                        
                        HStack {
                            Button(action: {
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Cancel")
                                        .font(.custom("Poppins-Medium", size: 16))
                                    Spacer()
                                }
                            }.buttonStyle(SecondaryButtonStyleJcoR())
                        }.padding(.vertical)
                        .padding(.horizontal)
                      }
                        
                       
                    }
                }
                
                Group {
                    Loader(isLoading: $viewModel.isLoading).zIndex(2)
                }
            }
            
            Spacer()
        }.navigationBarHidden(true)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

struct GenderRadioButtonField: View {
    @EnvironmentObject var brandSettings: BrandSettings
    let id: Int
    let label: String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    let isMarked:Bool
    let callback: (Int)->()
    
    init(
        id: Int,
        label:String,
        size: CGFloat = 20,
        color: Color = Color.black,
        textSize: CGFloat = 14,
        isMarked: Bool = false,
        callback: @escaping (Int)->()
    ) {
        self.id = id
        self.label = label
        self.size = size
        self.color = color
        self.textSize = textSize
        self.isMarked = isMarked
        self.callback = callback
    }
    
    var body: some View {
        Button(action:{
            self.callback(self.id)
        }) {
            VStack{
                HStack(alignment: .center, spacing: 10) {
                    Image(systemName: self.isMarked ? "largecircle.fill.circle" : "circle")
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: self.size, height: self.size)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                    
                    Text(label)
                        .font(.custom("Poppins-Regular", size: 16))
                }.foregroundColor(brandSettings.curBrand == 1 ? self.color : Color.white)
            }
        }
        .foregroundColor(Color.white)
    }
}

enum EditGender: String {
    case male = "Male"
    case female = "Female"
}

struct GenderRadioButtonGroups: View {
    let callback: (Int) -> ()
    
    @State var selectedId: Int = 1
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                radioMale
                Spacer().frame(width: 25)
                radioFemale
            }
        }
        .padding(.horizontal, 25)
        .frame(maxWidth: .infinity, alignment: .leading)
    }
    
    var radioMale: some View {
        GenderRadioButtonField(
            id: 1,
            label: EditGender.male.rawValue,
            isMarked: selectedId == 1 ? true : false,
            callback: radioGroupCallback
        )
    }
    
    var radioFemale: some View {
        GenderRadioButtonField(
            id: 0,
            label: EditGender.female.rawValue,
            isMarked: selectedId == 0 ? true : false,
            callback: radioGroupCallback
        )
    }
    
    func radioGroupCallback(id: Int) {
        selectedId = id
        callback(id)
    }
}

//struct ProfileEditView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileEditView()
//    }
//}
