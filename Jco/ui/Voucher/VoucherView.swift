//
//  VoucherView.swift
//  Jco
//
//  Created by Ed on 23/09/21.
//

import SwiftUI
import URLImage


struct VoucherView: View {
    @ObservedObject var viewModel = VoucherViewModel()
    @ObservedObject var cartViewModel: CartViewModel
    @EnvironmentObject var brandSettings: BrandSettings
    
    
    var body: some View {
        VStack {
            Header(title: "Voucher Code", back: true)
            
            VStack(alignment: .leading) {
                ScrollView(.vertical, showsIndicators: false) {
                    ForEach(self.viewModel.data, id: \.self) { voucher in
                        VoucherRow(brandSettings: _brandSettings, cartViewModel: cartViewModel, voucher: voucher)
                    }
                }
            }
            
            Spacer()
        }
        .onAppear {
            self.viewModel.getVoucher(
                deliveryType: String(cartViewModel.deliveryType),
                orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR"

            )
        }
        .navigationBarHidden(true)
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor_80").edgesIgnoringSafeArea(.bottom))
    }
}
struct VoucherRow: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var cartViewModel: CartViewModel
    var voucher: Coupon
    
    
    var body: some View {
        VStack {
            imageVoucher
            nameVoucher
            period
            description
            buttonVoucher
            line
        }.frame(width: UIScreen.main.bounds.width - 24)
    }
}

extension VoucherRow {
    var imageVoucher: some View {
        URLImage(url: URL(string: voucher.image ?? "")!, content: { image in
            image
                .resizable()
                .aspectRatio(contentMode: .fill)
                .scaledToFit()
                .cornerRadius(16)
        })
    }
    
    var nameVoucher: some View {
        Text(voucher.couponName ?? "")
            .font(.custom("Poppins-Medium", size: 16))
            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top)
    }
    
    var period: some View {
        HStack {
            Image(systemName: "calendar")
                .foregroundColor(Color("btn_primary"))
                .font(.system(size: 20))
                .padding(.top, 2)
            VStack {
                Text("Promo Period")
                    .font(.custom("Poppins-Regular", size: 10))
                    .foregroundColor(Color.gray)
                    .frame(maxWidth: .infinity, alignment: .leading)
                if let startDate = voucher.startTime, let endDate = voucher.endTime {
                    Text(startDate + " - " + endDate)
                        .font(.custom("Poppins-Medium", size: 13))
                        .foregroundColor(Color("btn_primary"))
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
            }
        }
    }
    
    var description: some View {
        Text(voucher.description ?? "")
            .font(.custom("Poppins-Regular", size: 14))
            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top)
    }
    

        
    var line: some View {
        Rectangle()
            .fill(Color.gray)
            .frame(width: .infinity, height: 1)
            .padding(.vertical)
    }
    
    var buttonVoucher: some View {
        VStack {
            if brandSettings.curBrand == 1 {
                Button(action: {
                    self.cartViewModel.setDismissVoucher()
                    self.cartViewModel.voucherName = voucher.couponName ?? ""
                    self.cartViewModel.voucherCode = voucher.code ?? ""
                }) {
                    HStack {
                        Spacer()
                        Text("use-voucher")
                            .font(.custom("Poppins-Medium", size: 18))
                        Spacer()
                    }
                }.buttonStyle(PrimaryButtonStyle())
                    .font(.custom("Poppins-Medium", size: 16))
            } else {
                Button(action: {
                    self.cartViewModel.setDismissVoucher()
                    self.cartViewModel.voucherName = voucher.couponName ?? ""
                    self.cartViewModel.voucherCode = voucher.code ?? ""
                    presentationMode.wrappedValue.dismiss()
                }) {
                    HStack {
                        Spacer()
                        Text("use-voucher")
                            .font(.custom("Poppins-Medium", size: 18))
                        Spacer()
                    }
                }.buttonStyle(PrimaryButtonStyleJcoR())
                    .font(.custom("Poppins-Medium", size: 16))
            }
        }
    }

}
