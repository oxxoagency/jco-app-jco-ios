//
//  VoucherViewModel.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 23/05/22.
//

import Foundation
import Combine

class VoucherViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    @Published var data = [Coupon]()
    @Published var isLoading = true
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getVoucher(deliveryType: String, orderBrand: String){
        let cancellable = self.couponList(deliveryType: deliveryType, orderBrand: orderBrand)
            .sink(receiveCompletion: { result in
                switch result {
                case.failure(let error):
                    print("Handle error voucher \(error)")
                case.finished:
                    break
                }
            }) { (data) in
                self.data = data.data!
            }
        cancellables.insert(cancellable)
    }
}
