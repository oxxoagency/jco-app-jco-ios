//
//  FavoriteViewModel.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 18/10/21.
//

import Foundation
import Combine

class FavoriteViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    
    @Published var isLoading = true
    @Published var data = [FavoriteMenu]()
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getFavorites(brand: Int){
        let cancellable = self.getMenuFavorite(brand: brand).sink(receiveCompletion: { result in
            switch result {
            case .failure(let error):
                print("Handle error fav: \(error)")
            case .finished:
                break
            }
        }){ (result) in
            if let favorites = result.data {
                self.data = favorites
            }
            print(self.data)
            self.isLoading = false
        }
        cancellables.insert(cancellable)
    }
    
    func productFavorite(menuCode: String, isFavorite: Int) {
        let cancellable = self.setMenuFavorite(menuCode: menuCode, isFavorite: isFavorite).sink(receiveCompletion: { result in
            switch result {
            case .failure(let error):
                print("Handle error pfav: \(error)")
            case .finished:
                break
            }
        }){ (result) in
            
        }
        cancellables.insert(cancellable)

    }
    
    func setFavorite(isFavorite: Int) -> Int{
        var favorite = 0
        if isFavorite == 1 {
            favorite = 0
        } else if isFavorite == 0 {
            favorite = 1
        }
        print("\(favorite)")
        return favorite
    }
}

