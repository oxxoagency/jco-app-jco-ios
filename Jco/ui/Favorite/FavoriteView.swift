//
//  NotificationView.swift
//  Jco
//
//  Created by Ed on 10/01/21.
//

import SwiftUI
import URLImage


struct FavoriteView: View {
    @State private var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    @EnvironmentObject var brandSettings: BrandSettings
    @StateObject var vm = FavoriteViewModel()
    @ObservedObject var hvm: HomeViewModel
    
    var body: some View {
        VStack {
            Header(title: "Favorites", back: false)
            
            if userLogged == true {
                ScrollView { 
                    if vm.data.isEmpty {
                        Text("There is no product yet").multilineTextAlignment(.center)
                          .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        
                        Spacer()
                    } else {
                        GridStack(rows: 10, columns: 2) { row, col in
                            if self.vm.data.indices.contains(row * 2 + col) {
                                
                                NavigationLink(destination: MenuDetailView(hvmm: hvm, menuCode: self.vm.data[row * 2 + col].menuCode, isFromPromo: false
                                                                          )) {
                                    VStack(alignment: .leading) {
                                        VStack(alignment: .leading) {
                                            ZStack(alignment: .topTrailing) {
                                                Button(action: {
                                                    self.vm.productFavorite(menuCode: self.vm.data[row * 2 + col].menuCode, isFavorite: self.vm.setFavorite(isFavorite: self.vm.data[row * 2 + col].isFavorite))
                                                }) {
                                                    if  self.vm.data[row * 2 + col].isFavorite == 1 {
                                                        Image(systemName: "heart.fill")
                                                            .foregroundColor(Color(.red))
                                                            .font(.system(size: 30))
                                                    } else {
                                                        Image(systemName: "heart.fill")
                                                            .foregroundColor(Color(red: 0, green: 0, blue: 0, opacity: 0.4))
                                                            .font(.system(size: 30))
                                                    }
                                                }.offset(x:-5, y:7)
                                                    .zIndex(5)
                                                
                                                VStack(alignment: .leading) {
                                                    URLImage(url: URL(string: self.vm.data[row * 2 + col].menuImage)!,
                                                             content: { image in
                                                        image
                                                            .resizable()
                                                            .aspectRatio(contentMode: .fit)
                                                            .clipped()
                                                            .background(Color.white)
                                                    }).background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))
                                                    
                                                    VStack(alignment: .leading) {
                                                        Text("\(self.vm.data[row * 2 + col].menuName)")
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white).font(.system(size: 12))
                                                            .padding(.horizontal, 5)
                                                            .font(.custom("Poppins-Regular", size: 13))
                                                            .multilineTextAlignment(.leading)
  
                                                        HStack(alignment: .top) {
                                                            Text("\(Util().addTS(str: String(self.vm.data[row * 2 + col].menuPrice)))")
                                                                .font(.custom("Poppins-Regular", size: 14))
                                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                            //                                        .font(.system(size: 12))
                                                                .padding(.horizontal, 5)
                                                                .padding(.bottom, 5)
                                                                .lineLimit(1)
                                                            
                                                            Spacer()
                                                            
                                                            if self.vm.data[row * 2 + col].isPromo == 1 {
                                                                Button (action: {
                                                                    
                                                                }) {
                                                                    Text("Promo")
                                                                        .foregroundColor(Color.white)
                                                                        .font(.custom("Poppins-Medium", size: 13))
                                                                        .padding(.horizontal, 2)
                                                                        .lineLimit(1)
                                                                }
                                                                .padding(.horizontal, 5)
                                                                .cornerRadius(20)
                                                                .background(brandSettings.curBrand == 1 ? Color("c_menu_category") : Color.red)
                                                                .clipShape(RoundedRectangle(cornerRadius: 5, style: .continuous))
                                                            }
                                                            
                                                            if self.vm.data[row * 2 + col].isFreedelivery == 1 {
                                                                Button (action: {
                                                                    
                                                                }) {
                                                                    Text("Free Delivery")
                                                                        .foregroundColor(Color.white)
                                                                        .font(.custom("Poppins-Medium", size: 13))
                                                                        .padding(.horizontal, 2)
                                                                        .lineLimit(1)
                                                                }
                                                                .padding(.horizontal, 5)
                                                                .frame(maxWidth: 50)
                                                                .cornerRadius(20)
                                                                .background(Color.green)
                                                                .clipShape(RoundedRectangle(cornerRadius: 5, style: .continuous))
                                                            }
                                                        }.padding(.trailing, 6)
                                                    }.frame(minHeight: 70, alignment: .topLeading)
                                                }.frame(maxWidth: .infinity, alignment: .topLeading)
                                                .background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))
                                                    .cornerRadius(10)
                                            }
                                        }.frame(maxWidth: .infinity)
                                            .cornerRadius(10)
                                    }.cornerRadius(10)
                                    .background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))
                                        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                                        .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.15), radius: 5, x: 0, y: 0)
                                }.padding(.bottom, 5)
                                    .padding(.horizontal, 5)
                            } else {
                                VStack {
                                    EmptyView()
                                }.frame(maxWidth: .infinity)
                                .background(Color.blue)
                            }
                        }
                    }
                }
            } else {
                VStack(alignment: .center) {
                    Spacer()
                    Text("Please Log In first").multilineTextAlignment(.center)
                      .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .font(.custom("Poppins-Medium", size: 15))
                    
                    Spacer()
                }.frame(
                    maxWidth: .infinity,
                    maxHeight: .infinity
                )
            }
        }.onAppear{
            self.vm.getFavorites(brand: self.brandSettings.curBrand)
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.inline)
        .background(brandSettings.curBrand == 1 ? Color("c_fff").edgesIgnoringSafeArea(.top) : Color("theme_background_jcor").edgesIgnoringSafeArea(.top))
    }
}

//struct NotificationView_Previews: PreviewProvider {
//    static var previews: some View {
//        FavoriteView()
//    }
//}
