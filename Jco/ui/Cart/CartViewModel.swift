//
//  CartViewModel.swift
//  Jco
//
//  Created by Ed on 17/04/21.
//

import Foundation
import SwiftUI
import Combine
import MidtransKit
import CoreData

class CartViewModel: ObservableObject, ModelService {
//    @Environment(\.managedObjectContext) var managedObjectContext
    var managedObjectContext = PersistenceController.shared.container.viewContext
    
//    var managedObjectContext2 = PersistenceController.shared.container.viewContext
    var apiSession: APIService
    var cancellables = Set<AnyCancellable>()
    @Published var addressChosenLabel = UserDefaults.standard.object(forKey: "AddressLabel") as? String ?? ""
    @Published var addressChosenName = UserDefaults.standard.object(forKey: "AddressName") as? String ?? ""
    @Published var addressChosenPhone = UserDefaults.standard.object(forKey: "AddressPhone") as? String ?? ""
    @Published var addressChosenAddress = UserDefaults.standard.object(forKey: "AddressUser") as? String ?? ""
    @Published var addressChosenAddressID = UserDefaults.standard.object(forKey: "AddressID") as? Int ?? 0
    @Published var addressChosenCity = UserDefaults.standard.object(forKey: "AddressCity") as? String ?? ""
    @Published var addressChosenPostcode = UserDefaults.standard.object(forKey: "AddressPostcode") as? String ?? ""
    @Published var addressChosenLatitude = UserDefaults.standard.object(forKey: "AddressLatitude") as? String ?? ""
    @Published var addressChosenLongitude = UserDefaults.standard.object(forKey: "AddressLongitude") as? String ?? ""
    
    @Published var outletChosenID = UserDefaults.standard.object(forKey: "OutletID") as? String ?? ""
    @Published var outletChosenName = UserDefaults.standard.object(forKey: "OutletName") as? String ?? ""
    @Published var outletChosenCity = UserDefaults.standard.object(forKey: "OutletCity") as? String ?? ""
    @Published var outletChosenCode = UserDefaults.standard.object(forKey: "OutletCode") as? String ?? ""
    @Published var outletChosenLatitude = UserDefaults.standard.object(forKey: "OutletLatitude") as? String ?? ""
    @Published var outletChosenLongitude = UserDefaults.standard.object(forKey: "OutletLongitude") as? String ?? ""
    @Published var outletChosenAddress = UserDefaults.standard.object(forKey: "OutletAddress") as? String ?? ""
    @Published var outletChosenPostcode = UserDefaults.standard.object(forKey: "OutletPostcode") as? String ?? ""

    
    @Published var ecobagName = UserDefaults.standard.object(forKey: "EcobagName") as? String ?? "Eco Bag"
    @Published var ecobagSubtotal = UserDefaults.standard.object(forKey: "EcobagSubtotal") as? Int ?? 0
    @Published var ecobagQuantity = UserDefaults.standard.object(forKey: "EcobagQuantity") as? Int ?? 1
    @Published var deliveryFee = UserDefaults.standard.object(forKey: "DeliveryFee") as? Int ?? 0
    @Published var freeDelivery = UserDefaults.standard.object(forKey: "FreeDelivery") as? Int ?? 0
    
    @Published var memberEmail = UserDefaults.standard.object(forKey: "MemberEmail") as? String ?? ""
    @Published var memberName = UserDefaults.standard.object(forKey: "MemberName") as? String ?? ""
    @Published var memberPhone = UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? ""
    
    @Published var paymentMethod = UserDefaults.standard.object(forKey: "PaymentMethod") as? String ?? ""
    @Published var paymentMethodName = UserDefaults.standard.object(forKey: "PaymentMethodName") as? String ?? "Choose Payment"
    @Published var paymentMethodIcon = UserDefaults.standard.object(forKey: "PaymentMethodIcon") as? String ?? ""
    
    @Published var delivery = true
    @Published var deliveryType = 0
    @Published var isLoading = false
    @Published var navigatePayment = false
    @Published var paymentUrl = ""
    @Published var paymentUrl2 = "https://google.com"
    @Published var notes = ""
    @Published var showMT = false
    @Published var mtToken: MidtransTransactionTokenResponse?
    @Published var mtPaymentFeature: MidtransPaymentFeature?
    @Published var useJpoint = false
    
//    @Published var deliveryFee = 0
//    @Published var ecobagSubtotal = 0
//    @Published var ecobagName = "Eco Bag"
    
    @Published var cartz: [Cart2ViewModel] = []
    @Published var cartzJcoR: [Cart2JcoRViewModel] = []
    @Published var cartSubtotal = 0
    @Published var cartTotal = 0
    var cartOrderDetail: [[String: Any]] = []
    @Published var nearestOutlet = [OutletNearest]()
    @Published var cartRecommendation = [CartRecMenu]()
    @Published var isAnyRecommendation = false
    @Published var paymentList = [PaymentList]()
    @Published var showAlert = false
    @Published var alertMessage = ""
    @Published var orderFinishedID = 0
    @Published var paymentResult = "DEF"
    @Published var navigateOrderDetail = false
    @Published var MTMenuName = "Donuts"
    @Published var jPoint = 0
    @Published var jPointReceived = 0
    @Published var jPointUsed = 0
    @Published var voucherName = ""
    @Published var voucherCode = ""
    @Published var isUsedVoucher = false
    @Published var promoValue = 0
    @Published var isDisableButtonOrder = false
    
    var MTItemDetail = [MidtransItemDetail]()
    
    @Published var isDismiss = false
    @Published var isDismissVoucher = false
    
//    @FetchRequest(
//        entity: Cart.entity(),
//        sortDescriptors: [
//            NSSortDescriptor(keyPath: \Cart.menuName, ascending: true)
//        ]
//    ) var cartItems: FetchedResults<Cart>
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
//        cartz = PersistenceController.shared.getAllCarts().map(Cart2ViewModel.init)
        self.getAllcarts(brand: 1)
    }
    
//    func getCartItems() {
//        print(cartItems)
//        print(memberName + " | " + memberEmail + " | " + memberPhone)
//    }
    
    func chooseAddress(brand: Int, chosenLabel: String, chosenName: String, chosenPhone: String, chosenAddress: String, choosenDetailAddress: String, chosenAddressID: Int, chosenCity: String, chosenPostcode: String, chosenLatitude: String, chosenLongitude: String, orderBrand: String) {
        addressChosenLabel      = chosenLabel
        addressChosenName       = chosenName
        addressChosenPhone      = chosenPhone
        addressChosenAddress    = chosenAddress
        addressChosenAddressID  = chosenAddressID
        addressChosenCity       = chosenCity
        addressChosenPostcode   = chosenPostcode
        addressChosenLatitude   = chosenLatitude
        addressChosenLongitude  = chosenLongitude
        
        UserDefaults.standard.set(chosenLabel, forKey: "AddressLabel")
        UserDefaults.standard.set(chosenName, forKey: "AddressName")
        UserDefaults.standard.set(choosenDetailAddress, forKey: "AddressDetail")
        UserDefaults.standard.set(chosenPhone, forKey: "AddressPhone")
        UserDefaults.standard.set(chosenAddress, forKey: "AddressUser")
        UserDefaults.standard.set(chosenAddressID, forKey: "AddressID")
        UserDefaults.standard.set(chosenCity, forKey: "AddressCity")
        UserDefaults.standard.set(chosenPostcode, forKey: "AddressPostcode")
        UserDefaults.standard.set(chosenLatitude, forKey: "AddressLatitude")
        UserDefaults.standard.set(chosenLongitude, forKey: "AddressLongitude")
        
        // Get nearest outlet
        getNearestOutlet(brand: brand, lat: chosenLatitude, lng: chosenLongitude, orderBrand: orderBrand)
        
        // Filter Cart
        self.filterCart(brand: brand, city: self.addressChosenCity, orderBrand: orderBrand)
    }

    func filterCart(brand: Int, city: String, orderBrand: String) {
        var city2 = self.addressChosenCity
        if self.deliveryType == 1 {
            city2 = self.outletChosenCity
        }
        
        self.isLoading = true
        let cancellable = self.getCartFilter(brand: brand, city: city2, deliveryType: self.deliveryType,  cartOrderDetail: cartOrderDetail, useJpoint: self.useJpoint ? 1 : 0, orderAddress: addressChosenAddress, orderAddressInfo: addressChosenAddress, orderLatitude: addressChosenLatitude, orderLongitude: addressChosenLongitude, couponCode: voucherCode.isEmpty ? "" : voucherCode, orderBrand: orderBrand)
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                    print("Handle Error Filter: \(error)")
                        self.isLoading = false
                    print(result)
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                
//                self.cartSubtotal = result.data?.subtotal ?? 0
//                self.cartTotal = (result.data?.grandtotal ?? 0) + (result.data?.ecoBag.ecobagSubtotal ?? 0)
                
                var curEcobagName = result.data?.ecoBag.ecobagName ?? "Eco Bag"
                if curEcobagName == "" {
                    curEcobagName = "Eco Bag"
                }
                
                self.ecobagName  = curEcobagName
                self.ecobagSubtotal = result.data?.ecoBag.ecobagSubtotal ?? 0
                self.ecobagQuantity = result.data?.ecoBag.ecobagQuantity ?? 1
                self.deliveryFee = result.data?.deliveryFee ?? 0
                self.freeDelivery = result.data?.freeDelivery ?? 0
                self.jPointReceived = result.data?.jpointInfo?.jpointEarn ?? 0
                self.jPointUsed = result.data?.jpointInfo?.jpointUsed ?? 0
                self.isUsedVoucher = !(result.data?.promo?.promoName?.isEmpty ?? false)
                self.promoValue = result.data?.promo?.promoValue ?? 0
                
                if !(result.data?.error?.message?.isEmpty ?? false) {
                    self.showAlert = true
                    self.alertMessage = result.data?.error?.message ?? ""
                    self.voucherName = ""
                    self.voucherCode = ""
                }
                
                UserDefaults.standard.set(self.ecobagName, forKey: "EcobagName")
                UserDefaults.standard.set(self.ecobagSubtotal, forKey: "EcobagSubtotal")
                UserDefaults.standard.set(self.ecobagQuantity, forKey: "EcobagQuantity")
                UserDefaults.standard.set(self.deliveryFee, forKey: "DeliveryFee")
                UserDefaults.standard.set(self.freeDelivery, forKey: "FreeDelivery")
                
                // Clear carts
                self.clearCart(brand: brand)
                
                // Add cart entity
                if brand == 1 {
                    if let cartOrders = result.data?.cartOrders {
                        cartOrders.map{ (o) ->
                            CartFilterOrder in
                            let newCart = Cart(context: self.managedObjectContext)
                            newCart.menuCode = o.menuCode
                            newCart.menuName = o.menuName
                            newCart.menuImg = o.menuImage
                            newCart.menuQty = Int16(o.menuQuantity)
                            newCart.menuPrices = String(o.menuPrice)
                            newCart.menuVariant = o.menuDetail
                            PersistenceController.shared.save()
                            return o
                        }
                        print("CART ORDER \(cartOrders) \(result.data?.cartOrders)")
                        
                        // Get menu name for midtrans
                        if !cartOrders.isEmpty {
                            self.MTMenuName = cartOrders[0].menuName + " " + cartOrders[0].menuDetail
                        }
                    }
                } else {
                    if let cartOrders = result.data?.cartOrders {
                        cartOrders.map{ (o) ->
                            CartFilterOrder in
                            let newCart = CartJcoR(context: self.managedObjectContext)
                            newCart.menuCode = o.menuCode
                            newCart.menuName = o.menuName
                            newCart.menuImg = o.menuImage
                            newCart.menuQty = Int16(o.menuQuantity)
                            newCart.menuPrices = String(o.menuPrice)
                            newCart.menuVariant = o.menuDetail
                            PersistenceController.shared.save()
                            return o
                        }
                        print("CART ORDER \(cartOrders) \(result.data?.cartOrders)")
                        
                        // Get menu name for midtrans
                        if !cartOrders.isEmpty {
                            self.MTMenuName = cartOrders[0].menuName + " " + cartOrders[0].menuDetail
                        }
                    }
                }
             
                
                self.getAllcarts(brand: brand)
                self.isLoading = false
                self.cartTotal = result.data?.grandtotal ?? 0
            }
        cancellables.insert(cancellable)
    }
    
    func getNearestOutlet(brand: Int, lat: String, lng: String, orderBrand: String) {
        let cancellable = self.getOutletNearest(
            brand: brand, lat: lat,
            lng: lng
        )
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Nearest: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                self.nearestOutlet = result.data
                if result.data.count > 0 {
                    self.chooseOutlet(
                        brand: brand,
                        outletId: result.data[0].outletId,
                        outletName: result.data[0].outletName,
                        outletCity: result.data[0].outletCity,
                        outletCode: result.data[0].outletCode,
                        outletLatitude: result.data[0].outletLatitude,
                        outletLongitude: result.data[0].outletLongitude,
                        outletAddress: result.data[0].outletAddress,
                        outletPostcode: result.data[0].outletPostcode,
                        orderBrand: orderBrand
                    )
                }
            }
        cancellables.insert(cancellable)
    }
    
    func chooseOutlet(brand: Int, outletId: String, outletName: String, outletCity: String, outletCode: String, outletLatitude: String, outletLongitude: String, outletAddress: String, outletPostcode: String, orderBrand: String) {
        outletChosenID   = outletId
        outletChosenName = outletName
        outletChosenCity = outletCity
        outletChosenCode = outletCode
        outletChosenLatitude = outletLatitude
        outletChosenLongitude = outletLongitude
        outletChosenAddress = outletAddress
        outletChosenPostcode = outletPostcode
        
        UserDefaults.standard.set(outletId, forKey: "OutletID")
        UserDefaults.standard.set(outletName, forKey: "OutletName")
        UserDefaults.standard.set(outletCity, forKey: "OutletCity")
        UserDefaults.standard.set(outletCode, forKey: "OutletCode")
        UserDefaults.standard.set(outletLatitude, forKey: "OutletLatitude")
        UserDefaults.standard.set(outletLongitude, forKey: "OutletLongitude")
        UserDefaults.standard.set(outletAddress, forKey: "OutletAddress")
        UserDefaults.standard.set(outletPostcode, forKey: "OutletPostcode")
        
        // Filter Cart
        self.filterCart(brand: brand, city: self.outletChosenCity, orderBrand: orderBrand)
    }
    
    func deleteItem(at index: Int) {
        print(index)
//        let cart = cartItems[index]
//        managedObjectContext2.delete(cart)
//        PersistenceController.shared.save()
        
//        for index in offsets {
//            let cart = cartItems[index]
//            managedObjectContext2.delete(cart)
////        }
//        PersistenceController.shared.save()
    }
    
    func encodeURL(str: String) {
//        let unreservedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~"
//        let unreservedCharsSet: CharacterSet = CharacterSet(charactersIn: unreservedChars)
//        let encodedString = self.addingPercentEncoding(withAllowedCharacters: unreservedCharsSet)!
//        return encodedString
    }
    
    func cartOrder(brand: Int) {
        if self.deliveryType == 0 {
            self.consumeOrderCheckout(brand: brand, name: self.addressChosenName, city: self.addressChosenCity,latitude: self.addressChosenLatitude, longitude: self.addressChosenLongitude, address: self.addressChosenAddress, detailAddress: UserDefaults.standard.object(forKey: "AddressDetail") as? String ?? "", postcode: self.addressChosenPostcode)
        } else {
            self.consumeOrderCheckout(brand: brand, name: UserDefaults.standard.object(forKey: "MemberName") as? String ?? "", city: self.outletChosenCity, latitude: outletChosenLatitude, longitude: outletChosenLongitude, address: self.outletChosenAddress, detailAddress: self.outletChosenAddress, postcode: self.outletChosenPostcode)
        }
    }
    
    func consumeOrderCheckout(brand: Int, name: String, city: String, latitude: String, longitude: String, address: String, detailAddress: String, postcode: String){
        isDisableButtonOrder = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.isDisableButtonOrder = false
        }

        let time = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let strTime = timeFormatter.string(from: time)
        var jpoint_used = 0
        if useJpoint {
            jpoint_used = 1
        }
        
        if jPoint >= cartTotal {
            self.navigateOrderDetail = true
        }
        
//                print("JPOINT USED:" + String(jpoint_used))
        
        self.isLoading = true
        let cancellable = self.addCartOrder(
            brand: brand,
            name: name,
            email: self.memberEmail,
            phone: self.addressChosenPhone,
            trxTime: strTime,
            city: city,
            address: address,
            detailAddress: detailAddress,
            postcode: postcode,
            latitude: latitude,
            longitude: longitude,
            notes: self.notes,
            outletID: self.outletChosenID,
            outletCode: self.outletChosenCode,
            orderDetail: self.cartOrderDetail,
            deliveryType: self.deliveryType,
            orderPayment: self.paymentMethod,
            jpoint: jpoint_used,
            couponCode: self.isUsedVoucher ? voucherCode : ""
        )
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Order: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                
                if result.statusCode == 200 {
                    self.orderFinishedID = result.data?.orderId ?? 0
                    self.midtransPay(brand: brand, data: result.data!)
    //                self.clearCart()
    //                self.checkOut(orderId: result.data?.orderId ?? 0)
                    
                } else {
                    self.showAlert = true
                    self.alertMessage = result.error?.message ?? "Unknown Error. Please Re-login"
                }
                self.isLoading = false
            }
        cancellables.insert(cancellable)
    }
    
    func checkOut(orderId: Int) {
        self.isLoading = true
        let cancellable = self.addCartCheckOut(orderId: String(orderId))
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Checkout: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                self.paymentUrl = result.redirectUrl ?? ""
                self.navigatePayment = true
                self.isLoading = false
            }
        cancellables.insert(cancellable)
    }
    
    func cartCheckoutPayment() {
        let cancellable = self.addCartPayment(orderId: String(self.orderFinishedID))
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Checkout Payment: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
            }
        cancellables.insert(cancellable)
    }
    
    func getAllcarts(brand: Int) {
        //        cartz = CoreDataManager.shared.getAllCarts().map(Cart2ViewModel.init)
        if brand == 1 {
            cartz = PersistenceController.shared.getAllCarts().map(Cart2ViewModel.init)
            
            self.cartTotal = 0
            var tempTotal = 0
            cartOrderDetail = []
            cartz.forEach { (c) in
                let mPrice = Int(c.menuPrices ?? "0") ?? 0
                let mQty = Int(c.menuQty)
                let tempSubTotal = (mPrice * mQty)
                tempTotal += tempSubTotal
                
                let cartDetailRow: [String: Any] = [
                    "menu_code": c.menuCode,
                    "menu_name": c.menuName,
                    "menu_quantity": c.menuQty,
                    "material_unit_price": c.menuPrices,
                    "material_sub_total": String(tempSubTotal),
                    "menu_detail": c.menuVariant
                ]
                cartOrderDetail.append(cartDetailRow)
            }
            self.cartSubtotal = tempTotal
            self.cartTotal = tempTotal + self.deliveryFee + self.ecobagSubtotal
        } else {
            cartzJcoR = PersistenceController.shared.getAllCartsJcoR().map(Cart2JcoRViewModel.init)
            
            self.cartTotal = 0
            var tempTotal = 0
            cartOrderDetail = []
            cartzJcoR.forEach { (c) in
                let mPrice = Int(c.menuPrices ?? "0") ?? 0
                let mQty = Int(c.menuQty ?? 0)
                let tempSubTotal = (mPrice * mQty)
                tempTotal += tempSubTotal
                
                let cartDetailRow: [String: Any] = [
                    "menu_code": c.menuCode,
                    "menu_name": c.menuName,
                    "menu_quantity": c.menuQty,
                    "material_unit_price": c.menuPrices,
                    "material_sub_total": String(tempSubTotal),
                    "menu_detail": c.menuVariant
                ]
                cartOrderDetail.append(cartDetailRow)
            }
            self.cartSubtotal = tempTotal
            self.cartTotal = tempTotal + self.deliveryFee + self.ecobagSubtotal
        }
    }
    
    func delete(_ cart: Cart2ViewModel) {
        let existingCart = CoreDataManager.shared.getCartById(id: cart.id)
        if let existingCart = existingCart {
            CoreDataManager.shared.deleteCart(cart: existingCart)
        }
    }
    
    func deleteJcoR(_ cart: Cart2JcoRViewModel) {
        let existingCart = CoreDataManager.shared.getCartByIdJcoR(id: cart.id)
        if let existingCart = existingCart {
            CoreDataManager.shared.deleteCartJcoR(cart: existingCart)
        }
    }
    
    func save() {
        let newCart = Cart(context: CoreDataManager.shared.viewContext)
        newCart.menuCode = "12312312"
        newCart.menuName = "nama menu"
        newCart.menuImg = "menuimage"
        CoreDataManager.shared.save()
    }
    
    func subtractTotal(mPrice: String) {
//        self.cartTotal -= Int(mPrice) ?? 0
//        self.cartSubtotal -= Int(mPrice) ?? 0
    }

    func updateQty(_ cart: Cart2ViewModel, action: String) {
        let existingCart = PersistenceController.shared.getCartById(id: cart.id)
        if let existingCart = existingCart {
            PersistenceController.shared.updateCart(cart: existingCart, action: action)
        }
    }
    
    func clearCart(brand: Int) {
        if brand == 1 {
            self.cartz.map{ (c) ->
                Cart2ViewModel in
                self.delete(c)
                return c
            }
        } else {
            self.cartzJcoR.map{ (c) ->
                Cart2JcoRViewModel in
                self.deleteJcoR(c)
                return c
            }
        }
        
        getAllcarts(brand: brand)
    }
    
    func midtransPay(brand: Int,  data: CartOrderId) {
//        let itemDetail = MidtransItemDetail.init(itemID: "1231321", name: self.MTMenuName, price: self.cartTotal as NSNumber, quantity: 1)
        
        self.MTItemDetail = []
        // Create MTItemDetails array
        if brand == 1 {
            data.orderDetail?.forEach { order in
                MTItemDetail.append(MidtransItemDetail.init(itemID: order.menuCode, name: order.menuName, price: order.menuUnitprice as NSNumber?, quantity: order.menuQuantity as NSNumber?))
                
            }
        } else {
            cartzJcoR.forEach { (c) in
                if let itemDetailPrice = Int(c.menuPrices ?? "0") {
                    let priceNumber = NSNumber(value:itemDetailPrice)
                    
                    var mtDetailName = c.menuName
                    let nameCount = mtDetailName?.count
                    
                    if nameCount ?? 0 > 45 {
                        var arrMtDetailName = Array(mtDetailName ?? "")
                        mtDetailName = String(arrMtDetailName[0..<45])
                        mtDetailName = mtDetailName ?? "" + "..."
                    }
                    
                    let itemDetail = MidtransItemDetail.init(itemID: c.menuCode, name: mtDetailName, price: priceNumber, quantity: c.menuQty as NSNumber? ?? 0 as NSNumber)
                    MTItemDetail.append(itemDetail!)
                }
            }
        }
  
        
        // Insert delivery fee to item
        let itemDelivery = MidtransItemDetail.init(itemID: "1111111111-000-1", name: "Delivery Service", price: data.orderFee as NSNumber?, quantity: 1)
        let itemJpoint = MidtransItemDetail.init(itemID: "1111111111-000-3", name: "Potongan Jpoint", price: -(data.oderLoyalty ?? 0) as NSNumber, quantity: 1)
        var promoValue = 0
        if data.orderPromo ?? 0 > 0 {
            promoValue = data.orderPromo ?? 0 * -1
        } else {
            promoValue = data.orderPromo ?? 0
        }
        let promo = MidtransItemDetail.init(itemID: "1111111111-000-2", name: "Promo", price:promoValue as NSNumber, quantity: 1)
        MTItemDetail.append(itemDelivery!)
        if isUsedVoucher {
            MTItemDetail.append(promo!)
        }
        if useJpoint {
            MTItemDetail.append(itemJpoint!)
        }

        let shipAddr = MidtransAddress()
        let billAddr = MidtransAddress()
        let customerDetail = MidtransCustomerDetails.init(firstName: self.memberName, lastName: "", email: UserDefaults.standard.object(forKey: "MemberEmail") as? String ?? "", phone: self.memberPhone, shippingAddress: shipAddr, billingAddress: billAddr)

        let transactionDetail = MidtransTransactionDetails.init(orderID: String(data.orderId), andGrossAmount: self.cartTotal as NSNumber)
        let expireTime = MidtransTransactionExpire.init(expireTime: nil, expireDuration: 1, withUnitTime: MindtransTimeUnitType.hour)
        
//        CreditCard creditCardOptions = new CreditCard();
//        creditCardOptions.setAuthentication(Authentication.AUTH_3DS);
//        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);

        MidtransMerchantClient.shared().requestTransactionToken(with: transactionDetail!, itemDetails: MTItemDetail, customerDetails: customerDetail, customField: nil, binFilter: nil, blacklistBinFilter: nil, transactionExpireTime: expireTime) { (response, error) in
            print(response)
            if (response != nil) {
                //handle response
                self.mtToken = response
                self.showMT = true
                
                // MT Payment Feature
                if self.paymentMethod == "credit_cards" {
                    self.mtPaymentFeature = MidtransPaymentFeature.creditCard
                } else if self.paymentMethod == "bca_va" {
                    self.mtPaymentFeature = MidtransPaymentFeature.bankTransferBCAVA
                } else if self.paymentMethod == "echannel" {
                    self.mtPaymentFeature = MidtransPaymentFeature.bankTransferMandiriVA
                } else if self.paymentMethod == "cimb_clicks" {
                    self.mtPaymentFeature = MidtransPaymentFeature.cimbClicks
                } else if self.paymentMethod == "gopay" {
                    self.mtPaymentFeature = MidtransPaymentFeature.GOPAY
                }
            }
            else {
                print(error)
            }
        }
    }
    
    func getRecommendation() {
        let cancellable = self.getCartRecommendation(city: self.addressChosenCity, cartInfo: cartOrderDetail)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle Error Recommendation: \(error)")
                    self.isAnyRecommendation = false
                    self.cartRecommendation = []
                case .finished:
                    self.isAnyRecommendation = true
                }
            }) { (result) in
                self.cartRecommendation = result.data
            }
        cancellables.insert(cancellable)
    }
    
    func changeDeliveryType(brand: Int, type: Int, orderBrand: String) {
        if type == 2 {
            // Pick Up
            self.deliveryType = 1
            self.cartTotal -= self.deliveryFee
            self.deliveryFee = 0
            self.filterCart(brand: brand, city: self.outletChosenCity, orderBrand: orderBrand)
        } else {
            // Delivery
            self.deliveryType = 0
            self.deliveryFee = UserDefaults.standard.object(forKey: "DeliveryFee") as? Int ?? 0
            self.cartTotal += self.deliveryFee
            self.filterCart(brand: brand, city: self.addressChosenCity, orderBrand: orderBrand)
        }
    }
    
    func totalQtyCart(brand: Int) -> Int16 {
        if brand == 1 {
            var total : Int16 = 0
            cartz.map{cart in
                total += cart.menuQty
            }
            return total
        } else {
            var total : Int16 = 0
            cartzJcoR.map{cart in
                total += cart.menuQty ?? 0
            }
            return total
        }
        
    }
    
    func getPaymentList() {
        let cancellable = self.getPaymentList()
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Checkout: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                if result.statusCode == 200 {
                    self.paymentList = result.data
                }
            }
        cancellables.insert(cancellable)
    }
    
    func choosePaymentMethod(paymentName: String, paymentMethod: String, paymentIcon: String) {
        self.paymentMethod = paymentMethod
        self.paymentMethodName = paymentName
        self.paymentMethodIcon = paymentIcon
        UserDefaults.standard.set(paymentName, forKey: "PaymentMethodName")
        UserDefaults.standard.set(paymentMethod, forKey: "PaymentMethod")
        UserDefaults.standard.set(paymentIcon, forKey: "PaymentMethodIcon")
    }
    
    func paymentSuccess() {
//        self.cartCheckoutPayment()
        self.navigateOrderDetail = true
        self.freeDelivery = 0
        UserDefaults.standard.set(0, forKey: "FreeDelivery")
        self.clearCart(brand: 1)
        self.clearCart(brand: 2)
    }
    
    func paymentFailed() {
        self.showAlert = true
        self.alertMessage = "Payment Failed. Please retry or contact our support"
    }
    
    func setDismiss() {
        self.isDismiss = false
    }
    
    func setDismissVoucher() {
        self.isDismissVoucher = false
    }
    
    func getJPoint() {
        let cancellable = self.getJpoint()
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Jpoint: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                self.jPoint = result.data.point
            }
        cancellables.insert(cancellable)
    }
    
    func toggleJpoint(brand: Int, city: String, isOn: Bool, orderBrand: String) {
        if isOn {
            useJpoint = true
        
        } else {
            useJpoint = false
        }
        
        filterCart(brand: brand, city: city, orderBrand: orderBrand)
    }
}

struct Cart2ViewModel {
    let cart: Cart
    var id: NSManagedObjectID {
        return cart.objectID
    }
    
    var menuCode: String { return cart.menuCode ?? "code" }
    var menuName: String? { return cart.menuName ?? "name" }
    var menuPrices: String? { return cart.menuPrices ?? "0" }
    var menuImg: String? { return cart.menuImg ?? "img" }
    var menuQty: Int16 { return cart.menuQty }
    var menuVariant: String? { return cart.menuVariant ?? "" }
}


struct Cart2JcoRViewModel {
    let cart: CartJcoR
    var id: NSManagedObjectID {
        return cart.objectID
    }
    
    var menuCode: String { return cart.menuCode ?? "code" }
    var menuName: String? { return cart.menuName ?? "name" }
    var menuPrices: String? { return cart.menuPrices ?? "0" }
    var menuImg: String? { return cart.menuImg ?? "img" }
    var menuQty: Int16? { return cart.menuQty }
    var menuVariant: String? { return cart.menuVariant ?? "" }
}
