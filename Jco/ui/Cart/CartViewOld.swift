//
//  CartView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

//import SwiftUI
//import URLImage
//
//struct CartViewOld: View {
//    @ObservedObject var viewModel = CartViewModel()
//    @ObservedObject var cartListVM = Cart2ListViewModel()
//    @State private var delivery = true
//    
//    func deleteCart(at offsets: IndexSet) {
//        offsets.forEach { index in
//            let cart = cartListVM.cartz[index]
//            cartListVM.delete(cart)
//        }
//        cartListVM.getAllcarts()
//    }
//    
//    @Environment(\.managedObjectContext) var managedObjectContext
//    @FetchRequest(
//        entity: Cart.entity(),
//        sortDescriptors: [
//            NSSortDescriptor(keyPath: \Cart.menuName, ascending: true)
//        ]
//    ) var cartItems: FetchedResults<Cart>
//    
//    var body: some View {
//        VStack {
////            Header(title: "Cart", back: false)
//            
//            VStack(alignment: .leading) {
//                ScrollView {
//                    // Tab delivery / pick up
//                    HStack {
//                        
//                        Button(action: {
//                            withAnimation {
//                                self.delivery = true
//                            }
//                        }) {
//                            VStack {
//                                Text("Delivery")
//                                    .foregroundColor(self.delivery ? Color("btn_primary") : Color("c_666666"))
//                                    .padding(.vertical, 15)
//                                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 0)
//                                if self.delivery {
//                                    Rectangle().frame(height: 2.5).foregroundColor(Color("btn_primary"))
//                                }
//                            }.frame(minWidth: 0, maxWidth: .infinity)
//                            .padding(.horizontal, 3)
//                        }
//                        
//                        Button(action: {
//                            withAnimation {
//                                self.delivery = false
//                            }
//                        }) {
//                            VStack {
//                                Text("Pick Up")
//                                    .foregroundColor(!self.delivery ? Color("btn_primary") : Color("c_666666"))
//                                    .padding(.vertical, 15)
//                                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 6)
//                                if !self.delivery {
//                                    Rectangle().frame(height: 2.5).foregroundColor(Color("btn_primary"))
//                                }
//                            }.frame(minWidth: 0, maxWidth: .infinity)
//                            .padding(.horizontal, 3)
//                        }
//                        
//                    }
//                    .background(Color("theme_background"))
//                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.2), radius: 5, x: 0, y: 5)
//                    
////                    Rectangle().frame(height: 1).foregroundColor(Color.gray)
//                    // Tab delivery / pick up
//
//                    // Choose address
//                    if delivery {
//                        NavigationLink(destination: UserAddressListView(cartViewModel: viewModel)) {
//                            VStack(alignment: .leading) {
//                                Text("Delivery To")
//                                    .foregroundColor(Color.gray)
//                                    .font(.system(size: 12))
//                                    .padding(.horizontal, 5)
//                                    .padding(.bottom, 2)
//                                if self.viewModel.addressChosenLabel == "" {
//                                    Text("Choose Delivery Address")
//                                }
//                                Text(self.viewModel.addressChosenLabel)
//                                    .foregroundColor(Color.black)
//                                    .font(.system(size: 13))
//                                if self.viewModel.addressChosenName != "" {
//                                    Text(self.viewModel.addressChosenName + " (" + self.viewModel.addressChosenPhone + ")")
//                                        .foregroundColor(Color.black)
//                                        .font(.system(size: 13))
//                                }
//                                Text(self.viewModel.addressChosenAddress)
//                                    .foregroundColor(Color.gray)
//                                    .font(.system(size: 11))
//                            }.padding(.horizontal, 20)
//                            .padding(.top, 5)
//                            .padding(.bottom, 10)
//                            .frame(
//                                  minWidth: 0,
//                                  maxWidth: .infinity,
//                                  alignment: .topLeading
//                            )
//                        }
//        //                Text("Details go here.")
//        //                    .transition(.move(edge: .bottom))
//        //
//        //                // Moves in from leading out, out to trailing edge.
//        //                Text("Details go here.")
//        //                    .transition(.slide)
//        //
//        //                // Starts small and grows to full size.
//        //                Text("Details go here.")
//        //                    .transition(.scale)
//                    } else {
//                        VStack(alignment: .leading) {
//                            Text("Pick Up At")
//                                .foregroundColor(Color.gray)
//                                .font(.system(size: 12))
//                                .padding(.horizontal, 5)
//                                .padding(.bottom, 2)
//                        }.padding(.horizontal, 20)
//                        .padding(.top, 5)
//                        .padding(.bottom, 10)
//                        .frame(
//                              minWidth: 0,
//                              maxWidth: .infinity,
//                              alignment: .topLeading
//                        )
//                    }
//                    // Choose address
//                
//                    // List items in cart
//                    VStack(alignment: .leading) {
//                        ForEach(cartItems.indices, id: \.self) { cat in
//                            HStack(alignment: .top) {
//                                HStack {
//                                    if let mImg = cartItems[cat].menuImg {
//                                        if mImg != nil {
//                                            URLImage(url: URL(string: mImg.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")!,
//                                            content: { image in
//                                                 image
//                                                    .resizable()
//                                                    .aspectRatio(contentMode: .fill)
//                                                    .clipped()
//                                                    .frame(maxWidth: 100)
//                                            })
//                                        }
//                                    }
//                                }.frame(minWidth: 100, minHeight: 100)
//                                .background(Color("c_e6e6e6"))
//                                .cornerRadius(10)
//                                
//                                VStack(alignment: .leading) {
//    //                                Text(String(id))
//                                    HStack {
//                                        if let mName = cartItems[cat].menuName {
//                                            Text(mName)
//                                                .font(.system(size: 13))
//                                                .padding(.top, 5)
//                                                .foregroundColor(Color("theme_text"))
//                                        }
//                                    }
//                                    
//                                    HStack(alignment: .bottom) {
//                                        Group {
//                                            VStack {
//                                                HStack {
//                                                    Button(action: {
//                                                        
//                                                    }) {
//                                                        Text("Edit")
//                                                            .foregroundColor(Color("theme_text"))
//                                                            .font(.system(size: 11))
//                                                    }
//                                                    Button(action: {
////                                                        self.viewModel.deleteItem(at: cat)
////                                                        self.deleteCart(at: IndexSet(integer: self.index))
//                                                    }) {
//                                                        Text("Delete " + String(cat))
//                                                            .foregroundColor(Color.red)
//                                                            .font(.system(size: 11))
//                                                    }
//                                                }.padding(.top, 2)
//                                                
//                                                
//                                                Text("Rp. " + String(cartItems[cat].menuPric))
//                                                    .font(.system(size: 12))
//                                                    .foregroundColor(Color("btn_primary"))
//                                                    .padding(.top, 1)
//                                            
//                                                
//                                                Spacer()
//                                            }
//                                        }
//                                        
//                                        Spacer()
//                                        
//                                        Group {
//                                            HStack {
//                                                Button(action: {
//    //                                                self.viewModel.addDonut(groupIndex: indexGroup, donutIndex: index)
//                                                }) {
//                                                    Image(systemName: "minus.square")
//                                                        .foregroundColor(Color("btn_primary"))
//                                                }
//    
//                                                Text("1")
//                                                    .foregroundColor(Color("theme_text"))
//    
//                                                Button(action: {
//    //                                                self.viewModel.addDonut(groupIndex: indexGroup, donutIndex: index)
//                                                }) {
//                                                    Image(systemName: "plus.square.fill")
//                                                        .foregroundColor(Color("btn_primary"))
//                                                }
//                                            }
//                                        }
//                                    }
//                                }.padding(.leading, 10)
//                                .padding(.top, 10)
//                            }.frame(
//                                minWidth: 0,
//                                maxWidth: .infinity,
//                                alignment: .topLeading
//                            ).padding(.horizontal, 10)
//                        }
//    //                    .onDelete(perform: deleteItem)
//                    }
//                    
////                    CartNotes()
//                }
//                // List cart items
//                
//                Spacer()
//                
//                // Btn submit order
//                VStack() {
//                    HStack() {
//                        VStack(alignment: .leading) {
//                            Text("Total Price")
//                                .foregroundColor(Color.black)
//                            Text("Rp. 100.000")
//                                .foregroundColor(Color("btn_primary"))
//                        }.padding(.leading, 10)
//                        
//                        Spacer()
//                        
//                        Button(action: {
//                            
//                        }) {
//                            HStack {
//                                Text("Check Out")
//                            }
//                        }.buttonStyle(PrimaryButtonStyle())
//                        
//        //                .alert(isPresented: $viewModel.showAlert) {
//        //                    Alert(title: Text("Error"), message: Text("Please complete the form"), dismissButton: .default(Text("Okay")))
//        //                }
//                    }
//                }.padding(.vertical, 5)
//                .padding(.horizontal, 10)
//                .frame(
//                    minWidth: 0,
//                    maxWidth: .infinity,
//                    alignment: .topLeading
//                )
//                // Btn submit order
//            }
//        }.onAppear {
////            self.viewModel.getCartItems()
//        }.navigationBarHidden(true)
//        .background(Color("c_fff"))
//    }
//    
//    
//}
//
//struct CartNotesOld: View {
//    var body: some View {
//        VStack(alignment: .leading) {
//            Text("Notes")
//            
//        }.padding(.top, 20)
//        .frame(maxWidth: .infinity)
//    }
//}

//struct CartViewOld_Previews: PreviewProvider {
//    static var previews: some View {
//        CartView()
//    }
//}
