//
//  Cart2ListViewModel.swift
//  Jco
//
//  Created by Ed on 18/05/21.
//

import Foundation
import CoreData

class Cart2ListViewModel: ObservableObject {
    @Published var cartz: [Cart2ViewModel] = []
    @Published var cartTotal = 0
    
    func getAllcarts() {
        cartz = CoreDataManager.shared.getAllCarts().map(Cart2ViewModel.init)
        
        cartTotal = 0
        cartz.forEach { (c) in
            let mPrice = Int(c.menuPrices ?? "0") ?? 0
            let mQty = Int(c.menuQty) ?? 0
            cartTotal += (mPrice * mQty)
        }
    }
    
    func delete(_ cart: Cart2ViewModel) {
        let existingCart = CoreDataManager.shared.getCartById(id: cart.id)
        if let existingCart = existingCart {
            CoreDataManager.shared.deleteCart(cart: existingCart)
        }
    }
    
    func save() {
        let newCart = Cart(context: CoreDataManager.shared.viewContext)
        newCart.menuCode = "12312312"
        newCart.menuName = "nama menu"
        newCart.menuImg = "menuimage"
        CoreDataManager.shared.save()
    }
    
    func subtractTotal(mPrice: String) {
        self.cartTotal -= Int(mPrice) ?? 0
    }

    func updateQty(_ cart: Cart2ViewModel, action: String) {
        let existingCart = CoreDataManager.shared.getCartById(id: cart.id)
        if let existingCart = existingCart {
            CoreDataManager.shared.updateCart(cart: existingCart, action: action)
        }
    }
}

struct Cart2ViewModel_Old {
    let cart: Cart
    var id: NSManagedObjectID {
        return cart.objectID
    }
    
    var menuCode: String { return cart.menuCode ?? "code" }
    var menuName: String { return cart.menuName ?? "name" }
    var menuPrices: String { return cart.menuPrices ?? "0" }
    var menuImg: String { return cart.menuImg ?? "img" }
    var menuQty: Int16 { return cart.menuQty }
    var menuVariant: String { return cart.menuVariant ?? "" }
}
