//
//  CoreDataManager.swift
//  Jco
//
//  Created by Ed on 18/05/21.
//

import Foundation
import CoreData

class CoreDataManager {
    let persistentContainer: NSPersistentContainer
    static let shared = CoreDataManager()
    
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    func getAllCarts() -> [Cart] {
        let request: NSFetchRequest<Cart> = Cart.fetchRequest()
        do {
            return try viewContext.fetch(request)
        } catch {
            return []
        }
    }
    
    func getCartById(id: NSManagedObjectID) -> Cart? {
        do {
            return try viewContext.existingObject(with: id) as? Cart
        } catch {
            return nil
        }
    }
    
    func getCartByIdJcoR(id: NSManagedObjectID) -> CartJcoR? {
        do {
            return try viewContext.existingObject(with: id) as? CartJcoR
        } catch {
            return nil
        }
    }
    
    func save() {
        do {
            try viewContext.save()
        } catch {
            viewContext.rollback()
            print(error.localizedDescription)
        }
    }
    
    func deleteCart(cart: Cart) {
        viewContext.delete(cart)
        save()
    }
    
    func deleteCartJcoR(cart: CartJcoR) {
        viewContext.delete(cart)
        save()
    }
    
    func updateCart(cart: Cart, action: String) {
        viewContext.performAndWait {
            if action == "add" {
                cart.menuQty += 1
            } else {
                if cart.menuQty > 0 {
                    cart.menuQty -= 1
                }
            }
            save()
        }
    }
    
    private init() {
        persistentContainer = NSPersistentContainer(name: "Jco")
        persistentContainer.loadPersistentStores { (description, error) in
            if let error = error {
                fatalError("Unable to initialize core data stack \(error)")
            }
        }
    }
}
