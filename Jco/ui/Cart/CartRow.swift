//
//  CartRow.swift
//  Jco
//
//  Created by Ed on 17/04/21.
//

import SwiftUI

struct CartRow: View {
    let menu: Cart
    var body: some View {
        VStack(alignment: .leading) {
          menu.menuName.map(Text.init)
        }
    }
}

//struct CartRow_Previews: PreviewProvider {
//    static var previews: some View {
//        CartRow()
//    }
//}
