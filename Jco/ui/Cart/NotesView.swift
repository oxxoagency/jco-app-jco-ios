//
//  NotesView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 06/10/21.
//

import SwiftUI
import PartialSheet

struct NotesView : View {
    @Binding var message: String
    @Binding var isPresented: Bool
    @ObservedObject var viewModel: CartViewModel
    @EnvironmentObject var partialSheetManager: PartialSheetManager
    
    var body: some View {
        VStack {
            HStack{
            Text("label-notes")
                .padding(.horizontal, 20)
                .font(.custom("Poppins-Regular", size: 20))
                .foregroundColor(Color.black)
                Spacer()
                Image(systemName: "multiply")
                    .foregroundColor(Color.black)
                    .font(.system(size: 25))
                    .padding(.leading, 15)
                    .onTapGesture {
                        self.partialSheetManager.closePartialSheet()
                    }
            } .padding(.vertical, 10)
            
            Text("Anda dapat membuat catatan apa pun untuk outlet tersebut (maks 200 karakter)")
                .padding(.horizontal, 20)
                .font(.custom("Poppins-Regular", size: 16))
                .foregroundColor(Color.gray)
            
            MultilineTextField(text: self.$message)
                .frame(width: UIScreen.main.bounds.width * 0.9, height: 100)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.gray, lineWidth: 1))
                .font(.custom("Poppins-Regular", size: 14))
                .padding(.leading, 15)
            
            Button(action: {
                self.isPresented = false
                viewModel.notes = self.message
            }) {
                HStack {
                    Spacer()
                    Text("Simpan")
                    Spacer()
                }
            }.buttonStyle(PrimaryButtonStyle())
            .font(.custom("Poppins-Medium", size: 16))
            .padding(.horizontal, 16)
            .padding(.vertical, 16)
            
        }
    }
}

struct MultilineTextField: UIViewRepresentable {
    @Binding var text: String

    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.delegate = context.coordinator
        view.isScrollEnabled = true
        view.isEditable = true
        view.isUserInteractionEnabled = true
        view.font = UIFont.systemFont(ofSize: 18)
        return view
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        if uiView.text != text {
            uiView.text = text
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator : NSObject, UITextViewDelegate {

        var parent: MultilineTextField

        init(_ textView: MultilineTextField) {
            self.parent = textView
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            return true
        }

        func textViewDidChange(_ textView: UITextView) {
            self.parent.text = textView.text
        }
    }
}
