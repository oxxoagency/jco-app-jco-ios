//
//  CartView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import URLImage
import MidtransKit
import PartialSheet

struct MidTransWrapper: UIViewControllerRepresentable {
    typealias UIViewControllerType = MidtransUIPaymentViewController
    
    @ObservedObject var viewModel: CartViewModel
//    @State var mtToken: MidtransTransactionTokenResponse?
    
    var vc: MidtransUIPaymentViewController?
//    var foo: (MidtransTransactionResult) -> Void
    @Binding var resp: String?
    
//    public init(viewModel: CartViewModel, foos: @escaping (String) -> Void) {
    public init(viewModel: CartViewModel, mtResult: Binding<String?>) {
        self.viewModel = viewModel
        self._resp = mtResult
//        self.foo = foos
//        MidtransCreditCardConfig.shared()?.authenticationType = .type3DS
        self.vc = MidtransUIPaymentViewController.init(token: self.viewModel.mtToken, andPaymentFeature: self.viewModel.mtPaymentFeature ?? MidtransPaymentFeature.bankTransferBCAVA)
    }

    func makeUIViewController(context: Context) -> MidtransUIPaymentViewController {
//        vc = MidtransUIPaymentViewController.init(token: self.viewModel.mtToken)
//        vc?.popViewController(animated: true)
        vc?.delegate = context.coordinator
        vc?.paymentDelegate = context.coordinator
        return vc!
    }

    func updateUIViewController(_ uiViewController: MidtransUIPaymentViewController, context: Context) {
//        code
    }
    
    func makeCoordinator() -> Coordinator {
//        Coordinator(vc: vc!, foo: foo, vm: viewModel)
        Coordinator(self)
    }

    class Coordinator: NSObject, MidtransUIPaymentViewControllerDelegate, UINavigationControllerDelegate {
        var parent: MidTransWrapper
        
        init(_ parent: MidTransWrapper) {
            self.parent = parent
        }
        
//        var foo: (String) -> Void
//        var vm: CartViewModel
//
//        init(vc: MidtransUIPaymentViewController, foo: @escaping (String) -> Void, vm: CartViewModel) {
//            self.foo = foo
//            self.vm = vm
//            super.init()
//            vc.delegate = self
//            print("INIT DELEGATE")
//        }
        func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentFailed error: Error!) {
            parent.resp = "fail"
            parent.viewModel.paymentFailed()
//            foo("FAILED")
            print("FAILED")
        }
            
        func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentPending result: MidtransTransactionResult!) {
//            foo("PENDING")
            parent.viewModel.paymentSuccess()
            parent.resp = "pending"
            print("PENDING")
//            self.vm.clearCart()
        }
            
        func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentSuccess result: MidtransTransactionResult!) {
            parent.viewModel.paymentSuccess()
            parent.resp = "success"
//            foo("SUCCESS")
            print("SUCCESS")
//            self.vm.clearCart()
        }

        func paymentViewController_paymentCanceled(_ viewController: MidtransUIPaymentViewController!) {
//            foo("CANCEL")
            parent.resp = "cancel"
            print("CANCEL")
        }

        //This delegate methods is added on ios sdk v1.16.4 to handle the new3ds flow
        func paymentViewController(_ viewController: MidtransUIPaymentViewController!, paymentDeny result: MidtransTransactionResult!) {
        }
    }
}

struct CartView: View {
    @EnvironmentObject var viewModel: CartViewModel
    @EnvironmentObject var brandSettings: BrandSettings
//    @ObservedObject var cartListVM = Cart2ListViewModel()
    @ObservedObject var hvm: HomeViewModel
    @State private var delivery = true
    @State private var mtToken: MidtransTransactionTokenResponse?
    @State private var mtResult: String?
    
    @State private var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    
    var body: some View {
        VStack {
            Header(title: "tab-cart", back: false)
            
            ZStack(alignment: .topLeading) {
                if userLogged {
                    VStack(alignment: .leading) {
                        ScrollView {
                            // Tab delivery / pick up
                            HStack {
                                Button(action: {
                                    withAnimation {
                                        self.delivery = true
                                        self.viewModel.changeDeliveryType(brand: self.brandSettings.curBrand, type: 1, orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR")
                                    }
                                }) {
                                    VStack {
                                        if brandSettings.curBrand == 1 {
                                            Text("Delivery")
                                                .font(.custom("Poppins-Medium", size: 15))
                                                .foregroundColor(self.delivery ? Color("btn_primary") : Color("c_666666"))
                                                .padding(.top, 15)
                                                .padding(.bottom, 10)
                                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 0)
                                        } else {
                                            Text("Delivery")
                                                .font(.custom("Poppins-Medium", size: 15))
                                                .foregroundColor(self.delivery ? Color("jcor_gold") : Color.white)
                                                .padding(.top, 15)
                                                .padding(.bottom, 10)
                                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 0)
                                        }
                                        if self.delivery {
                                            Rectangle().frame(height: 2.5).foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                        }
                                    }.frame(minWidth: 0, maxWidth: .infinity)
                                    .padding(.horizontal, 3)
                                }

                                Button(action: {
                                    withAnimation {
                                        self.delivery = false
                                        self.viewModel.changeDeliveryType(brand: self.brandSettings.curBrand, type: 2, orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR")
                                    }
                                }) {
                                    VStack {
                                        if brandSettings.curBrand == 1 {
                                            Text("Pick Up")
                                                .font(.custom("Poppins-Medium", size: 15))
                                                .foregroundColor(!self.delivery ? Color("btn_primary") : Color("c_666666"))
                                                .padding(.top, 15)
                                                .padding(.bottom, 10)
                                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 6)
                                        } else {
                                            Text("Pick Up")
                                                .font(.custom("Poppins-Medium", size: 15))
                                                .foregroundColor(!self.delivery ? Color("jcor_gold") : Color.white)
                                                .padding(.top, 15)
                                                .padding(.bottom, 10)
                                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 6)
                                        }
                                        if !self.delivery {
                                            Rectangle().frame(height: 2.5).foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                        }
                                    }.frame(minWidth: 0, maxWidth: .infinity)
                                    .padding(.horizontal, 3)
                                }

                            }
                            .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
                            .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.2), radius: 5, x: 0, y: 5)
                            // Rectangle().frame(height: 1).foregroundColor(Color.gray)
                            // Tab delivery / pick up

                            // Choose address
                            Group {
                                if delivery {
                                    NavigationLink(destination: UserAddressListView(cartViewModel: viewModel)) {
                                        VStack(alignment: .leading) {
                                            HStack {
                                                Image(uiImage: self.brandSettings.curBrand == 1 ? UIImage(named: "icon_location")! : UIImage(named: "icon_location_jcor")!)
                                                .resizable()
                                                .scaledToFit()
                                                .frame(width: 22, height: 22)
                                            Text("delivery-to")
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                .font(.custom("Poppins-Regular", size: 14))
                                                .padding(.horizontal, 5)
                                                .padding(.bottom, 2)
                                                .padding(.top, 2)
                                            }
                                            if UserDefaults.standard.object(forKey: "AddressLabel") as? String ?? "" == "" {
                                                Text("choose-delivery-address")
                                                    .font(.custom("Poppins-Regular", size: 14))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                            }
                                            Text(UserDefaults.standard.object(forKey: "AddressLabel") as? String ?? "")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                                .font(.system(size: 13))
                                            if UserDefaults.standard.object(forKey: "AddressName") as? String ?? "" != "" {
                                                Text(self.viewModel.addressChosenName + " (" + self.viewModel.addressChosenPhone + ")")
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                                    .font(.system(size: 13))
                                            }
                                            Text(UserDefaults.standard.object(forKey: "AddressUser") as? String ?? "")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                .multilineTextAlignment(.leading)
                                                .font(.system(size: 11))
                                        }.padding(.horizontal, 20)
                                        .padding(.top, 5)
                                        .padding(.bottom, 10)
                                        .frame(
                                              minWidth: 0,
                                              maxWidth: .infinity,
                                              alignment: .topLeading
                                        )
                                    }
                                } else {
                                    NavigationLink(
                                        destination: OutletCityView(cartViewModel: viewModel),
                                        isActive: $viewModel.isDismiss
                                    ) {
                                        VStack(alignment: .leading) {
                                            HStack {
                                                Image(uiImage: self.brandSettings.curBrand == 1 ? UIImage(named: "icon_location")! : UIImage(named: "icon_location_jcor")!)
                                                    .resizable()
                                                    .scaledToFit()
                                                    .frame(width: 22, height: 22)
                                                Text("Pick Up At")
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                    .font(.custom("Poppins-Regular", size: 14))
                                                    .padding(.horizontal, 5)
                                                    .padding(.bottom, 2)
                                                    .padding(.top, 2)
                                                
                                            }
                                            if self.viewModel.outletChosenName == "" {
                                                Text("Choose Pick Up Outlet")
                                                    .font(.custom("Poppins-Regular", size: 14))
                                            }

                                            if self.viewModel.outletChosenName != "" {
                                                Text(self.viewModel.outletChosenName)
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                                    .font(.system(size: 13))
                                            }
                                            Text(self.viewModel.outletChosenCity)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                                .font(.system(size: 11))

                                        }.padding(.horizontal, 20)
                                        .padding(.top, 5)
                                        .padding(.bottom, 10)
                                        .frame(
                                              minWidth: 0,
                                              maxWidth: .infinity,
                                              alignment: .topLeading
                                        )
                                    }.isDetailLink(false)
                                }
                                
                                Group {
                                    Rectangle().frame(height: 1).foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                }.padding(.horizontal, 10)
                                
                                Group {
                                    Text("delivery-note")
                                        .italic()
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                        .font(.custom("Poppins-Regular", size: 12))
                                        .padding(.horizontal, 10)
                                        .padding(.bottom, 10)
                                    
                                    HStack {
                                        Button(action: {
                                            self.viewModel.clearCart(brand: self.brandSettings.curBrand)
                                        }) {
                                            Text("clear-cart")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                .font(.custom("Poppins-Medium", size: 12))
                                        }
                                        
                                        Spacer()
                                        
                                        Button(action: {
                                            self.hvm.tabHome()
                                        }) {
                                            Text("add-items")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                .font(.custom("Poppins-Medium", size: 12))
                                        }
                                    }.padding(.horizontal, 20)
                                    .padding(.bottom, 5)
                                }
                                
                            }
                            // Choose address

                            if self.brandSettings.curBrand == 1 {
                                if viewModel.cartz.count == 0 {
                                    Group {
                                        Text("no-cart-items").multilineTextAlignment(.center)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .font(.custom("Poppins-Medium", size: 15))

                                        Spacer()
                                    }
                                }

                            } else {
                                if viewModel.cartzJcoR.count == 0 {
                                    Group {
                                        Text("no-cart-items").multilineTextAlignment(.center)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .font(.custom("Poppins-Medium", size: 15))

                                        Spacer()
                                    }
                                }

                            }

                            // Cart Items
                            if brandSettings.curBrand == 1 {
                            Group {
                                VStack(alignment: .leading) {
                                    ForEach(viewModel.cartz.indices, id: \.self) { c in
                                        HStack(alignment: .center) {
                                            HStack {
                                                URLImage(url: URL(string: viewModel.cartz[c].menuImg ?? "".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")!,
                                                content: { image in
                                                     image
                                                        .resizable()
                                                        .aspectRatio(contentMode: .fit)
                                                        .clipped()
                                                        .frame(maxWidth: 102, maxHeight: 102)
                                                })
                                            }.frame(minWidth: 100, minHeight: 100)
                                            .background(Color("c_e6e6e6"))
                                            .cornerRadius(10)

                                            VStack(alignment: .leading) {
                //                                Text(String(id))
                                                HStack {
                                                    Text(viewModel.cartz[c].menuName ?? "")
                                                        .font(.custom("Poppins-Regular", size: 18))
//                                                            .font(.system(size: 14))
                                                        .padding(.top, 5)
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                }

                                                Group {
                                                    Text(viewModel.cartz[c].menuVariant ?? "")
                                                        .font(.custom("Poppins-Regular", size: 12))
//                                                            .font(.system(size: 12))
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                                        .lineLimit(10)
                                                        .multilineTextAlignment(.leading)
                                                        .fixedSize(horizontal: false, vertical: true)
                                                }

                                                HStack(alignment: .bottom) {
                                                    Group {
                                                        VStack {
                                                            Text("Rp. " + Util().addTS(str: viewModel.cartz[c].menuPrices ?? "0"))
                                                                .font(.custom("Poppins-Medium", size: 20))
//                                                                    .font(.system(size: 13))
                                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                                .padding(.top, 1)

                                                            Spacer()
                                                        }
                                                    }

                                                    Spacer()

                                                    Group {
                                                        HStack {
                                                            if viewModel.cartz[c].menuQty == 1 {
                                                                Button(action: {
                                                                    deleteCart(cartIx: c, cartPrice: viewModel.cartz[c].menuPrices ?? "0")
                                                                }) {
//                                                                        Text("Delete")
//                                                                            .foregroundColor(Color.red)
//                                                                            .font(.system(size: 11))
                                                                    Image(uiImage: UIImage(named: "icon_trash")!)
                                                                        .resizable()
                                                                        .scaledToFit()
                                                                        .frame(width: 32, height: 32)
                                                                }
                                                            } else {
                                                                Button(action: {
                                                                    editQty(cartIx: c, action: "sub")
                                                                }) {
                                                                    Image(uiImage: UIImage(named: "icon_minus")!)
                                                                        .resizable()
                                                                        .scaledToFit()
                                                                        .frame(width: 32, height: 32)
                                                                }
                                                            }

                                                            Text(String(viewModel.cartz[c].menuQty))
                                                                .font(.custom("Poppins-Regular", size: 24))
                                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)

                                                            Button(action: {
                                                                editQty(cartIx: c, action: "add")
                                                            }) {
                                                                Image(uiImage: UIImage(named: "icon_plus")!)
                                                                    .resizable()
                                                                    .scaledToFit()
                                                                .frame(width: 32, height: 32)                                                            }
                                                        }
                                                    }
                                                }
                                            }.padding(.leading, 10)
                                            .padding(.top, 10)
                                        }.frame(
                                            minWidth: 0,
                                            maxWidth: .infinity,
                                            alignment: .topLeading
                                        ).padding(.horizontal, 10)
                                    }
                //                    .onDelete(perform: deleteItem)
                                }
                                
                                Group {
                                    VStack(alignment: .leading) {
                                        Text("price-note")
                                            .italic()
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                            .font(.custom("Poppins-Regular", size: 13))
                                            .padding(.horizontal, 10)
                                            .padding(.top, 10)

                                        Group {
                                            Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        }.padding()
                                        
                                        CartNotes(viewModel: self.viewModel)
//                                        Spacer()
                                        
                                        Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
//                                        Rectangle()
//                                            .fill(Color.gray)
//                                            .frame(width: .infinity, height: 1)
//                                            .padding()
                                        
                                        CartPayment(viewModel: self.viewModel)
                                        
                                        Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
//                                        Rectangle()
//                                            .fill(Color.gray)
//                                            .frame(width: .infinity, height: 1)
//                                            .padding()
                                        
                                        
                                        CartVoucher(viewModel: self.viewModel)
                                        
                                        Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        
//                                        Rectangle()
//                                            .fill(Color.gray)
//                                            .frame(width: .infinity, height: 1)
//                                            .padding()
                                        
                                        CartJpoint(vm: viewModel)
                                        if viewModel.isAnyRecommendation {
                                            CartRecommendation(vm: viewModel, hvm: hvm)
                                        }

                                    }
                                }
                            }
                            } else {
                               Group {
                                    VStack(alignment: .leading) {
                                        ForEach(viewModel.cartzJcoR.indices, id: \.self) { c in
                                            HStack(alignment: .center) {
                                                HStack {
                                                    URLImage(url: URL(string: viewModel.cartzJcoR[c].menuImg ?? "".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")!,
                                                    content: { image in
                                                         image
                                                            .resizable()
                                                            .aspectRatio(contentMode: .fit)
                                                            .clipped()
                                                            .frame(maxWidth: 102, maxHeight: 102)
                                                    })
                                                }.frame(minWidth: 100, minHeight: 100)
                                                .background(Color("c_e6e6e6"))
                                                .cornerRadius(10)

                                                VStack(alignment: .leading) {
                    //                                Text(String(id))
                                                    HStack {
                                                        Text(viewModel.cartzJcoR[c].menuName ?? "" )
                                                            .font(.custom("Poppins-Regular", size: 18))
//                                                            .font(.system(size: 14))
                                                            .padding(.top, 5)
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                                    }

                                                    Group {
                                                        Text(viewModel.cartzJcoR[c].menuVariant ?? "")
                                                            .font(.custom("Poppins-Regular", size: 12))
//                                                            .font(.system(size: 12))
                                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                                            .lineLimit(10)
                                                            .multilineTextAlignment(.leading)
                                                            .fixedSize(horizontal: false, vertical: true)
                                                    }

                                                    HStack(alignment: .bottom) {
                                                        Group {
                                                            VStack {
                                                                Text("Rp. " + Util().addTS(str: viewModel.cartzJcoR[c].menuPrices ?? "0"))
                                                                    .font(.custom("Poppins-Medium", size: 20))
//                                                                    .font(.system(size: 13))
                                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                                    .padding(.top, 1)

                                                                Spacer()
                                                            }
                                                        }

                                                        Spacer()

                                                        Group {
                                                            HStack {
                                                                if viewModel.cartzJcoR[c].menuQty == 1 {
                                                                    Button(action: {
                                                                        deleteCart(cartIx: c, cartPrice: viewModel.cartzJcoR[c].menuPrices ?? "0")
                                                                    }) {
//                                                                        Text("Delete")
//                                                                            .foregroundColor(Color.red)
//                                                                            .font(.system(size: 11))
                                                                        Image(uiImage: UIImage(named: "icon_trash_jcor")!)
                                                                            .resizable()
                                                                            .scaledToFit()
                                                                            .frame(width: 32, height: 32)
                                                                    }
                                                                } else {
                                                                    Button(action: {
                                                                        editQty(cartIx: c, action: "sub")
                                                                    }) {
                                                                        Image(uiImage: UIImage(named: "icon_minus_jcor")!)
                                                                            .resizable()
                                                                            .scaledToFit()
                                                                            .frame(width: 32, height: 32)
                                                                    }
                                                                }

                                                                Text(String(viewModel.cartzJcoR[c].menuQty ?? 0))
                                                                    .font(.custom("Poppins-Regular", size: 24))
                                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)

                                                                Button(action: {
                                                                    editQty(cartIx: c, action: "add")
                                                                }) {
                                                                    Image(uiImage: UIImage(named: "icon_plus_jcor")!)
                                                                        .resizable()
                                                                        .scaledToFit()
                                                                    .frame(width: 32, height: 32)                                                            }
                                                            }
                                                        }
                                                    }
                                                }.padding(.leading, 10)
                                                .padding(.top, 10)
                                            }.frame(
                                                minWidth: 0,
                                                maxWidth: .infinity,
                                                alignment: .topLeading
                                            ).padding(.horizontal, 10)
                                        }
                    //                    .onDelete(perform: deleteItem)
                                    }
                                    
                                    Group {
                                        VStack(alignment: .leading) {
                                            Text("price-note")
                                                .italic()
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("c_666666") : Color.white)
                                                .font(.custom("Poppins-Regular", size: 13))
                                                .padding(.horizontal, 10)
                                                .padding(.top, 10)

                                            Group {
                                                Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            }.padding()
                                            
                                            CartNotes(viewModel: self.viewModel)
    //                                        Spacer()
                                            
                                            Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            
    //                                        Rectangle()
    //                                            .fill(Color.gray)
    //                                            .frame(width: .infinity, height: 1)
    //                                            .padding()
                                            
                                            CartPayment(viewModel: self.viewModel)
                                            
                                            Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            
    //                                        Rectangle()
    //                                            .fill(Color.gray)
    //                                            .frame(width: .infinity, height: 1)
    //                                            .padding()
                                            
                                            
                                            CartVoucher(viewModel: self.viewModel)
                                            
                                            Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            
    //                                        Rectangle()
    //                                            .fill(Color.gray)
    //                                            .frame(width: .infinity, height: 1)
    //                                            .padding()
                                            
                                            CartJpoint(vm: viewModel)
                                            
                                            Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        }
                                    }
                                }
                            }
                            // Cart Items

                            // Other Cost
                            if brandSettings.curBrand == 1 {
                                if viewModel.cartz.count > 0 {
                                    Group {
                                        HStack {
                                            VStack(alignment: .leading) {
                                                Text("Harga")
                                                Spacer().frame(height: 5)
                                                
                                                if self.viewModel.ecobagQuantity > 0 {
                                                    Text(String(self.viewModel.ecobagQuantity) + "X ")
                                                    + Text(self.viewModel.ecobagName)
                                                    Spacer().frame(height: 5)
                                                } else {
                                                    Text(self.viewModel.ecobagName)
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                if self.viewModel.deliveryType == 0 {
                                                    Text("delivery-service")
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                if viewModel.isUsedVoucher {
                                                    Text("Promo")
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                
                                                if self.viewModel.useJpoint {
                                                    Text("jpoint-used")
                                                    Spacer().frame(height: 5)
                                                } else {
                                                  if self.viewModel.jPointReceived > 0 {
                                                    Text("jpoint-received")
                                                      Spacer().frame(height: 5)
                                                  }
                                                 
                                                }
                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                            .font(.custom("Poppins-Medium", size: 15))
                                            Spacer()
                                            VStack(alignment: .trailing) {
                                                Text("Rp\(Util().addTS(str: String(self.viewModel.cartSubtotal)))")
                                                Spacer().frame(height: 5)
                                                
                                                Text("Rp\(Util().addTS(str: String(self.viewModel.ecobagSubtotal)))")
                                                Spacer().frame(height: 5)
                                                
                                                if self.viewModel.deliveryType == 0 {
                                                    HStack {
                                                        if self.viewModel.freeDelivery > 0 {
                                                            Text("Rp\(Util().addTS(str: String(self.viewModel.freeDelivery)))")
                                                                .strikethrough()
                                                        }
                                                        Text("Rp\(Util().addTS(str: String(self.viewModel.deliveryFee)))")
                                                        
                                                    }
                                                }

                                                if self.viewModel.isUsedVoucher {
                                                    Text(Util().addTS(str: String(self.viewModel.promoValue)))
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                                                                
                                                if self.viewModel.useJpoint {
                                                    Text(Util().addTS(str: String(self.viewModel.jPointUsed)) + " JPoint")
                                                    Spacer().frame(height: 5)
                                                } else {
                                                  if self.viewModel.jPointReceived > 0 {
                                                    Text(Util().addTS(str: String(self.viewModel.jPointReceived)) + " JPoint")
                                                      Spacer().frame(height: 5)
                                                  }
                                                 
                                                }
                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                            .font(.custom("Poppins-Medium", size: 15))
                                        }.padding()
                                        Spacer().frame(height: 35)
                                    }
                                }
                            } else {
                                if viewModel.cartzJcoR.count > 0 {
                                    Group {
                                        HStack {
                                            VStack(alignment: .leading) {
                                                Text("Harga")
                                                Spacer().frame(height: 5)
                                                
                                                if self.viewModel.ecobagQuantity > 0 {
                                                    Text(String(self.viewModel.ecobagQuantity) + "X ")
                                                    + Text(self.viewModel.ecobagName)
                                                    Spacer().frame(height: 5)
                                                } else {
                                                    Text(self.viewModel.ecobagName)
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                if self.viewModel.deliveryType == 0 {
                                                    Text("delivery-service")
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                if viewModel.isUsedVoucher {
                                                    Text("Promo")
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                
                                                if self.viewModel.useJpoint {
                                                    Text("jpoint-used")
                                                    Spacer().frame(height: 5)
                                                } else {
                                                  if self.viewModel.jPointReceived > 0 {
                                                    Text("jpoint-received")
                                                      Spacer().frame(height: 5)
                                                  }
                                                 
                                                }
                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                            .font(.custom("Poppins-Medium", size: 15))
                                            Spacer()
                                            VStack(alignment: .trailing) {
                                                Text("Rp\(Util().addTS(str: String(self.viewModel.cartSubtotal)))")
                                                Spacer().frame(height: 5)
                                                
                                                Text("Rp\(Util().addTS(str: String(self.viewModel.ecobagSubtotal)))")
                                                Spacer().frame(height: 5)
                                                
                                                if self.viewModel.deliveryType == 0 {
                                                    HStack {
                                                        if self.viewModel.freeDelivery > 0 {
                                                            Text("Rp\(Util().addTS(str: String(self.viewModel.freeDelivery)))")
                                                                .strikethrough()
                                                        }
                                                        Text("Rp\(Util().addTS(str: String(self.viewModel.deliveryFee)))")
                                                        
                                                    }
                                                }

                                                if self.viewModel.isUsedVoucher {
                                                    Text(Util().addTS(str: String(self.viewModel.promoValue)))
                                                    Spacer().frame(height: 5)
                                                }
                                                
                                                                                                
                                                if self.viewModel.useJpoint {
                                                    Text(Util().addTS(str: String(self.viewModel.jPointUsed)) + " JPoint")
                                                    Spacer().frame(height: 5)
                                                } else {
                                                  if self.viewModel.jPointReceived > 0 {
                                                    Text(Util().addTS(str: String(self.viewModel.jPointReceived)) + " JPoint")
                                                      Spacer().frame(height: 5)
                                                  }
                                                 
                                                }                                            }.foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text_primary") : Color("jcor_gold"))
                                            .font(.custom("Poppins-Medium", size: 15))
                                        }.padding()
                                        Spacer().frame(height: 35)
                                    }
                                }
                            }
                        }
                        
                        // Btn submit order
                        
                        if brandSettings.curBrand == 1 {
                            if viewModel.cartz.count > 0 {
                                VStack() {
                                    HStack() {
                                        VStack(alignment: .leading) {
                                            Text("total-price")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                                .font(.custom("Poppins-Medium", size: 17))
                                            Text("Rp. " + Util().addTS(str: String(self.viewModel.cartTotal)))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                .font(.custom("Poppins-Medium", size: 17))
                                        }.padding(.leading, 10)
                                        
                                        Spacer()
                                        
                                        NavigationLink(destination: PaymentView(vm: viewModel), isActive: $viewModel.navigatePayment) {
                                            EmptyView()
                                        }
                                      
                                        if brandSettings.curBrand == 1 {
                                            Button(action: {
            //                                    self.viewModel.checkOut()
                                                self.viewModel.cartOrder(brand: self.brandSettings.curBrand)
        //                                        midtransPay()
                                            }) {
                                                HStack {
                                                    if viewModel.isDisableButtonOrder {
                                                        ProgressView()
                                                    }
                                                    Text(viewModel.isDisableButtonOrder ? "LOADING" : "ORDER")
                                                        .font(.custom("Poppins-Bold", size: 17))
                                                }
                                            }.buttonStyle(PrimaryButtonStyle())
                                            .disabled(viewModel.isDisableButtonOrder)
                                            .alert(isPresented: $viewModel.showAlert) {
                                                Alert(title: Text("Error"), message: Text(self.viewModel.alertMessage), dismissButton: .default(Text("Okay")))
                                            }
                                        } else {
                                            Button(action: {
            //                                    self.viewModel.checkOut()
                                                self.viewModel.cartOrder(brand: self.brandSettings.curBrand)
        //                                        midtransPay()
                                            }) {
                                                HStack {
                                                    Text("ORDER")
                                                        .font(.custom("Poppins-Bold", size: 17))
                                                }
                                            }.buttonStyle(PrimaryButtonStyleJcoR())
                                            .alert(isPresented: $viewModel.showAlert) {
                                                Alert(title: Text("Error"), message: Text(self.viewModel.alertMessage), dismissButton: .default(Text("Okay")))
                                            }
                                        }
                                    }
                                }.padding(.vertical, 5)
                                .padding(.horizontal, 10)
                                .frame(
                                    minWidth: 0,
                                    maxWidth: .infinity,
                                    alignment: .topLeading
                                )
                            }
                        } else {
                            if viewModel.cartzJcoR.count > 0 {
                                VStack() {
                                    HStack() {
                                        VStack(alignment: .leading) {
                                            Text("total-price")
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                                                .font(.custom("Poppins-Medium", size: 17))
                                            Text("Rp. " + Util().addTS(str: String(self.viewModel.cartTotal)))
                                                .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                .font(.custom("Poppins-Medium", size: 17))
                                        }.padding(.leading, 10)
                                        
                                        Spacer()
                                        
                                        NavigationLink(destination: PaymentView(vm: viewModel), isActive: $viewModel.navigatePayment) {
                                            EmptyView()
                                        }
                                      
                                        if brandSettings.curBrand == 1 {
                                            Button(action: {
            //                                    self.viewModel.checkOut()
                                                self.viewModel.cartOrder(brand: self.brandSettings.curBrand)
        //                                        midtransPay()
                                            }) {
                                                HStack {
                                                    Text("ORDER")
                                                        .font(.custom("Poppins-Bold", size: 17))
                                                }
                                            }.buttonStyle(PrimaryButtonStyle())
                                            .alert(isPresented: $viewModel.showAlert) {
                                                Alert(title: Text("Error"), message: Text(self.viewModel.alertMessage), dismissButton: .default(Text("Okay")))
                                            }
                                        } else {
                                            Button(action: {
            //                                    self.viewModel.checkOut()
                                                self.viewModel.cartOrder(brand: self.brandSettings.curBrand)
        //                                        midtransPay()
                                            }) {
                                                HStack {
                                                    Text("ORDER")
                                                        .font(.custom("Poppins-Bold", size: 17))
                                                }
                                            }.buttonStyle(PrimaryButtonStyleJcoR())
                                            .alert(isPresented: $viewModel.showAlert) {
                                                Alert(title: Text("Error"), message: Text(self.viewModel.alertMessage), dismissButton: .default(Text("Okay")))
                                            }
                                        }
                                    }
                                }.padding(.vertical, 5)
                                .padding(.horizontal, 10)
                                .frame(
                                    minWidth: 0,
                                    maxWidth: .infinity,
                                    alignment: .topLeading
                                )
                            }
                        }
                        // Btn submit order
                        
                    }
                } else {
                    VStack(alignment: .center) {
                        Spacer()
                        Text("Please Log In first").multilineTextAlignment(.center)
                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                            .font(.custom("Poppins-Medium", size: 15))
                        
                        Spacer()
                    }.frame(
                        maxWidth: .infinity,
                        maxHeight: .infinity
                    )
                }
                
                
                Group {
                    Loader(isLoading: $viewModel.isLoading).zIndex(2)
                }
                
                // Navigate to order detail
                NavigationLink(destination: OrderDetailView(orderId: String(self.viewModel.orderFinishedID), from: "cart"), isActive: self.$viewModel.navigateOrderDetail) {
                    EmptyView()
                }
            }
        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .navigationBarTitleDisplayMode(.inline)
        .background(self.brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.top) : Color("theme_background_jcor").edgesIgnoringSafeArea(.top))
        .sheet(isPresented: $viewModel.showMT, onDismiss: loadResult) {
            MidTransWrapper(viewModel: self.viewModel, mtResult: self.$mtResult)
//            MidTransWrapper(viewModel: self.viewModel) { result in
////                print(data)
////                self.mtResult = result
//                print(result)
//                print("DONE")
//            }
        }.addPartialSheet()
        .onAppear {
            self.viewModel.getAllcarts(brand: self.brandSettings.curBrand)
            self.viewModel.filterCart(brand: self.brandSettings.curBrand, city: "", orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR")
            self.viewModel.getJPoint()
            self.viewModel.getRecommendation()
        
        }
    }
    // View
    
    func loadResult() {
        print("LOAD")
        
//        guard let mtResult = mtResult else { return }
//        print(mtResult)
//        image = Image(uiImage: inputImage)
    }
    
    func deleteCart(cartIx: Int, cartPrice: String) {
        if brandSettings.curBrand == 1 {
            let cart = viewModel.cartz[cartIx]
            viewModel.delete(cart)
        } else {
            let cartJcoR = viewModel.cartzJcoR[cartIx]
            viewModel.deleteJcoR(cartJcoR)
        }
        
        viewModel.getAllcarts(brand: self.brandSettings.curBrand)
        var filterCity = self.viewModel.addressChosenCity
        if self.viewModel.deliveryType == 1 {
            filterCity = self.viewModel.outletChosenCity
        }
        viewModel.filterCart(brand: self.brandSettings.curBrand, city: filterCity, orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR")
    }
    
    func editQty(cartIx: Int, action: String) {
        let cart = viewModel.cartz[cartIx]
        viewModel.updateQty(cart, action: action)
        viewModel.getAllcarts(brand: self.brandSettings.curBrand)
        viewModel.filterCart(brand: self.brandSettings.curBrand, city: "Jakarta Barat", orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR")
    }
    
    
}

struct CartNotes: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var viewModel: CartViewModel
    @State private var isPresented = false
    @State var message: String = ""
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("label-notes")
                .padding(.horizontal, 20)
                .font(.custom("Poppins-Regular", size: 14))
                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
            
            HStack {
                Image(uiImage: self.brandSettings.curBrand == 1 ? UIImage(named: "icon_notes")! : UIImage(named: "icon_notes_jcor")!)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 30, height: 30)
                    .padding(.leading, 15)
                
                Text(LocalizedStringKey(addNotes()))
                    .padding(.horizontal, 10)
                    .font(.custom("Poppins-Regular", size: 16))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                
                Spacer()
                
                Button(action: {
                    isPresented = true
                }) {
                    HStack {
                        Text("btn-change")
                            .font(.custom("Poppins-Regular", size: 14))
                            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                            .cornerRadius(10)
                            .padding(.horizontal, 24)
                            .padding(.vertical, 8)
                    }
                }.background(brandSettings.curBrand == 1 ? Color("btn_primary").opacity(0.1) : Color("jcor_gold").opacity(0.1))
                    .padding(.horizontal, 20)
                  
                
//                CustomTextField(placeHolder: "Add Notes", value: $viewModel.notes, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
            }
        }.padding(.top, 5)
        .frame(
            minWidth: 0,
            maxWidth: .infinity,
            alignment: .topLeading
        )
        .partialSheet(isPresented: $isPresented) {
            // view notes
            NotesView(message: self.$message, isPresented: self.$isPresented, viewModel: viewModel)
        }
    }
    
    func addNotes() -> String {
        if self.message.isEmpty {
            return "add-notes"
        } else {
            return self.message
        }
    }
}

struct CartVoucher: View {
    @ObservedObject var viewModel: CartViewModel
    @EnvironmentObject var brandSettings: BrandSettings
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Promo")
                .padding(.horizontal, 20)
                .font(.custom("Poppins-Regular", size: 14))
                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
            
            NavigationLink(destination: VoucherView(cartViewModel: viewModel), isActive: $viewModel.isDismissVoucher) {
                HStack {
                    Image(uiImage: self.brandSettings.curBrand == 1 ? UIImage(named: "Coupon")! : UIImage(named: "Coupon_jcor")!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30)
                        .padding(.leading, 15)
                    
                    if !viewModel.isUsedVoucher {
                        Text("use-voucher")
                            .padding(.horizontal, 10)
                            .font(.custom("Poppins-Regular", size: 16))
                            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                    } else {
                        Text(viewModel.voucherName)
                            .padding(.horizontal, 10)
                            .font(.custom("Poppins-Regular", size: 16))
                            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                    }
                }
            }
        }.padding(.top, 5)
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
    }
}

struct CartPayment: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var viewModel: CartViewModel
    @State private var isPresented = false
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("label-payment")
                .padding(.horizontal, 20)
                .font(.custom("Poppins-Regular", size: 14))
                .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
            
//            NavigationLink(destination: VoucherView()) {
            HStack {
                if self.viewModel.paymentMethodIcon != "" {
                    URLImage(url: URL(string: self.viewModel.paymentMethodIcon)!,
                    content: { image in
                         image
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 40, height: 40)
                            .clipped()
                    })
                }
                
                Text(self.viewModel.paymentMethodName != "" ? self.viewModel.paymentMethodName : "Belum Pilih Pembayaran")
                    .padding(.horizontal, 5)
                    .font(.custom("Poppins-Regular", size: 16))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
                
                Spacer()
                
                Button(action: {
                    isPresented.toggle()
                }) {
                    HStack {
                        Text("btn-choose")
                            .font(.custom("Poppins-Regular", size: 14))
                            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                            .cornerRadius(10)
                            .padding(.horizontal, 24)
                            .padding(.vertical, 8)
                    }
                }.fullScreenCover(isPresented: $isPresented) {
                    ChoosePaymentModalView(viewModel: self.viewModel)
                }
                .background(brandSettings.curBrand == 1 ? Color("btn_primary").opacity(0.1) : Color("jcor_gold").opacity(0.1))
                        .padding(.horizontal, 20)
                
//                Button("Present!") {
//                    isPresented.toggle()
//                }
//                .fullScreenCover(isPresented: $isPresented, content: ChoosePaymentModalView.init)
                
            }.padding(.leading, 15)
            
//            }
        }.padding(.top, 5)
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                alignment: .topLeading
            )
    }
}

struct CartJpoint: View {
    @ObservedObject var vm: CartViewModel
    @EnvironmentObject var brandSettings: BrandSettings
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Gunakan JPoint Kamu")
                    .font(.custom("Poppins-Regular", size: 15))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                Group {
                    Text("you-have")
                    + Text(" " + Util().addTS(str: String(self.vm.jPoint)) + " JPoint")
                }.font(.custom("Poppins-Regular", size: 13))
                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
            }
            
            Spacer()
            
            Toggle("", isOn: self.$vm.useJpoint)
                .labelsHidden()
                .onChange(of: self.vm.useJpoint) { _useJpoint in
                    self.vm.toggleJpoint(brand: brandSettings.curBrand, city: vm.addressChosenCity, isOn: _useJpoint, orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR")
                }.disabled(self.vm.jPoint <= 0)
        }
        .padding(.horizontal, 15)
        .padding(.vertical, 10)
    }
}


//struct CartView_Previews: PreviewProvider {
//    static var previews: some View {
//        CartView()
//    }
//}

struct Line: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width, y: 0))
        return path
    }
}

struct ChoosePaymentModalView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel: CartViewModel

    var body: some View {
        VStack {
//            Color.white.edgesIgnoringSafeArea(.all)
            
            HStack {
                Group {
                    Button(action: {presentationMode.wrappedValue.dismiss()} ) {
                        Image(systemName: "arrow.left")
                            .foregroundColor(Color.white)
                            .font(.system(size: 20))
                            .offset(x:0, y:0)
                    }.padding(.horizontal, 10)
                }
                .frame(width: 50)
                
                Spacer()
                Text("Choose Payment")
                    .foregroundColor(Color.white)
                    .font(.custom("Poppins-Bold", size: 20))
                Spacer()
                Rectangle()
                    .frame(width: 50, height: 10)
            }.padding(.vertical, 20)
                .background(brandSettings.curBrand == 1 ? Image("header").resizable().edgesIgnoringSafeArea(.top) : Image("header_jcor").resizable().edgesIgnoringSafeArea(.top))
            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
            .frame(
                minWidth: 0,
                maxWidth: .infinity
            )
            
            VStack(alignment: .leading) {
                if self.viewModel.paymentList.count > 0 {
                    ForEach(self.viewModel.paymentList, id: \.self) { p in
                        Button(action: {
                            self.viewModel.choosePaymentMethod(paymentName: p.paymentName, paymentMethod: p.paymentMethod, paymentIcon: p.paymentIcon)
                            presentationMode.wrappedValue.dismiss()
                        }) {
                            HStack {
                                URLImage(url: URL(string: p.paymentIcon)!,
                                content: { image in
                                     image
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 40, height: 40)
                                        .clipped()
                                })
                                
                                Text(p.paymentName)
                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                    .font(.custom("Poppins-Regular", size: 18))
                                    .padding(.leading, 10)
                            }.padding(.horizontal, 15)
                        }.padding(.vertical, 10)
                        Rectangle().frame(height: 1).foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                    }
                }
            }
            
            Spacer()
            
            Line().stroke(style: StrokeStyle(lineWidth: 1, dash: [5])).frame(height: 1)
                .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
            
//            Button("Dismiss Modal") {
//                presentationMode.wrappedValue.dismiss()
//            }
        }.onAppear() {
            self.viewModel.getPaymentList()
        }
        .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

struct CartRecommendation: View {
    @ObservedObject var vm: CartViewModel
    @ObservedObject var hvm: HomeViewModel
    @EnvironmentObject var brandSettings: BrandSettings
    var body: some View {
        VStack(alignment: .leading) {
            Text("recommendation-menu")
                .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white).font(.system(size: 16))
                .padding(.horizontal, 8)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ForEach(vm.cartRecommendation, id: \.self) { recommendation in
                        NavigationLink(destination: MenuDetailView(hvmm: hvm, menuCode: recommendation.menuCode, isFromPromo: false
                                                                  )) {
                            VStack(alignment: .leading) {
                                VStack(alignment: .leading) {
                                    ZStack(alignment: .topTrailing) {

                                        VStack(alignment: .leading) {
                                            URLImage(url: URL(string: recommendation.menuImg)!,
                                                     content: { image in
                                                image
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fit)
                                                    .clipped()
                                                    .background(Color.white)
                                            }).background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))

                                            VStack(alignment: .leading) {
                                                Text("\(recommendation.menuName)")
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white).font(.system(size: 12))
                                                    .padding(.horizontal, 5)
                                                    .font(.custom("Poppins-Regular", size: 13))
                                                    .multilineTextAlignment(.leading)

                                                HStack(alignment: .top) {
                                                    Text("\(Util().addTS(str: String(recommendation.menuPrice)))")
                                                        .font(.custom("Poppins-Regular", size: 14))
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                                    //                                        .font(.system(size: 12))
                                                        .padding(.horizontal, 5)
                                                        .padding(.bottom, 5)
                                                        .lineLimit(1)

                                                    Spacer()

                                                }.padding(.trailing, 6)
                                            }.frame(minHeight: 70, alignment: .topLeading)
                                        }.frame(maxWidth: .infinity, alignment: .topLeading)
                                            .background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))
                                            .cornerRadius(10)
                                    }
                                }.frame(width: UIScreen.main.bounds.width - 320)
                                    .cornerRadius(10)
                            }.cornerRadius(10)
                                .background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))
                                .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.15), radius: 5, x: 0, y: 0)
                        }.padding(.bottom, 5)
                            .padding(.horizontal, 2)

                    }
                }
            }
        }
    }
}

