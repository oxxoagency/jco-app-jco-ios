//
//  Header2.swift
//  Jco
//
//  Created by Ed on 23/04/21.
//

import SwiftUI

struct Header2: View {
    @Environment(\.presentationMode) var presentationMode

    var title: String
    var body: some View {
        HStack {
            Spacer()
            Text(title)
                .foregroundColor(Color.white)
                .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
            Spacer()
        }
        .padding(.vertical, 20)
        .background(Image("header").resizable())
        .foregroundColor(Color("btn_primary"))
        .frame(
            minWidth: 0,
            maxWidth: .infinity
        )
    }
}

struct Header2_Previews: PreviewProvider {
    static var previews: some View {
        Header2(title: "Title")
    }
}
