//
//  Loader.swift
//  Jco
//
//  Created by Ed on 06/05/21.
//

import SwiftUI

struct ActivityIndicator: UIViewRepresentable {
    @Binding var animate: Bool
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: .large)
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        uiView.startAnimating()
    }
}

//struct Loader_Previews: PreviewProvider {
//    static var previews: some View {
//        Loader(animate: true)
//    }
//}
