//
//  Loader.swift
//  Jco
//
//  Created by Ed on 06/05/21.
//

import SwiftUI

struct Loader: View {
    @Binding var isLoading: Bool
    let style = StrokeStyle(lineWidth: 10, lineCap: .round)
    @State var animate = false
    let color1 = Color.gray
    let color2 = Color.gray.opacity(0.5)
    
    var body: some View {
        if isLoading {
            VStack() {
                Spacer()
                HStack {
                    Spacer()
    //                ActivityIndicator(animate: $isLoading)
                    VStack {
                        Group {
                            HStack {
                                Spacer()
                                Circle()
                                    .trim(from: 0, to: 0.7)
                                    .stroke(
                                        AngularGradient(gradient: .init(colors: [color1,color2]), center: .center), style: style)
                                    .rotationEffect(Angle(degrees: animate ? 360 : 0))
                                    .animation(Animation.linear(duration: 0.7).repeatForever(autoreverses: false))
                                Spacer()
                            }.frame(maxWidth: .infinity, maxHeight: .infinity)
                        }.frame(width: 100, height: 100)
                        .padding(.all, 30)
                    }.background(Color("theme_background"))
                    .cornerRadius(15)
                    .frame(width: 500, height: 500)
                    .shadow(radius: 10)
                    Spacer()
                }.frame(minWidth: 0,
                        maxWidth: .infinity,
                        minHeight: 0,
                        maxHeight: .infinity)
                Spacer()
            }
            .background(Color(red: 0, green: 0, blue: 0, opacity: 0.1))
    //        .background(Color.red)
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity)
            .offset(x:0, y:0)
            .zIndex(5)
            .ignoresSafeArea()
            .onAppear() {
                self.animate.toggle()
            }
        } else {
            EmptyView()
        }
    }
}

//struct Loader_Previews: PreviewProvider {
//    static var previews: some View {
//        Loader()
//    }
//}
