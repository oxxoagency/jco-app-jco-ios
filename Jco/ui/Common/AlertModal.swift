//
//  AlertDialog.swift
//  Jco
//
//  Created by Ed on 07/05/21.
//

import SwiftUI

struct AlertModal: View {
//    @Binding var showAlert: Bool
    
    var body: some View {
//        if showAlert {
            VStack() {
                Spacer()
                HStack {
                    Spacer()
                    Group {
                        VStack(alignment: .center) {
                            Text("Item sucessfully added to cart")
                                .frame(alignment: .center)
                                .multilineTextAlignment(.center)
                                .padding(.bottom, 20)
                            
                            Button(action: {
                                
                            }) {
                                HStack {
//                                    Spacer()
                                    Text("View Menu")
//                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
                            
                            Button(action: {
                                
                            }) {
                                HStack {
//                                    Spacer()
                                    Text("View Cart")
//                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
                        }.frame(width: 200, height: 200)
                        .padding(.all, 30)
                    }.background(Color("theme_background"))
                    .cornerRadius(15)
                    .frame(width: 500, height: 500)
                    .shadow(radius: 10)
                    Spacer()
                }
                Spacer()
            }.background(Color(red: 0, green: 0, blue: 0, opacity: 0.1))
            .frame(minWidth: 0,
                   maxWidth: .infinity,
                   minHeight: 0,
                   maxHeight: .infinity)
            .offset(x:0, y:0)
            .zIndex(5)
            .ignoresSafeArea()
//        } else {
//            EmptyView()
//        }
    }
}

//struct AlertDialog_Previews: PreviewProvider {
//    static var previews: some View {
//        AlertDialog()
//    }
//}
