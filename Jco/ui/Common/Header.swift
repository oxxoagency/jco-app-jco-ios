//
//  Header.swift
//  Jco
//
//  Created by Ed on 17/04/21.
//

import SwiftUI

struct Header: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode

    var title: String
    var back: Bool
    
    var body: some View {
        HStack {
            if back {
                Group {
                    Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
                        Image(systemName: "arrow.left")
                            .foregroundColor(Color.white)
                            .font(.system(size: 20))
                            .offset(x:0, y:0)
    //
    //                    Image(uiImage: UIImage(named: "arrow_left")!)
    //                        .resizable()
    //                        .scaledToFit()
    //                        .frame(width: 30)
    //                        .offset(x:0, y:0)
                    }.padding(.horizontal, 10)
                }
                .frame(width: 50)
            }
            
            Spacer()
            Text(LocalizedStringKey(title))
                .foregroundColor(Color.white)
//                .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                .font(.custom("Poppins-Bold", size: 20))
            Spacer()
            
            if back {
                Rectangle()
                    .frame(width: 50, height: 10)
                    .foregroundColor(Color(red: 255, green: 255, blue: 255, opacity: 0))
            }
        }
        .padding(.vertical, 20)
        .background(Image(brandSettings.curBrand == 1 ? "header" : "header_jcor").resizable().edgesIgnoringSafeArea(.top))
//        .foregroundColor(Color("btn_primary"))
        .frame(
            minWidth: 0,
            maxWidth: .infinity
        )
        .navigationBarHidden(true)
    }
}

//struct Header_Previews: PreviewProvider {
//    static var previews: some View {
//        Header(title: "Title", back: true)
//    }
//}
