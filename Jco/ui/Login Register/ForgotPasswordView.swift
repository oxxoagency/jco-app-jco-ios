//
//  ForgotPasswordView.swift
//  Jco
//
//  Created by Ed on 10/01/21.
//

import SwiftUI
import Firebase

struct ForgotPasswordView: View {
    @ObservedObject var vm = NewPasswordViewModel()
    @State var inputEmail: String = ""
    @State var action: Bool = false
    @State var verificationID = ""
    @ObservedObject var homeViewModel: HomeViewModel
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            VStack(alignment: .leading) {
                LoginHeader()
                
                Group {
                    Text("Forgot Password")
                        .foregroundColor(Color("theme_text"))
                        .font(.custom("Poppins-Medium", size: 24))
                    Text("Enter your email associated with your account and we will send you an OTP code to reset your password.")
                        .foregroundColor(Color.gray)
                        .font(.custom("Poppins-Regular", size: 14))
                    
                    HStack {
                        CustomTextField(placeHolder: "Email", value: $inputEmail, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false, isEmail: true)
                    }
                    
                    NavigationLink(destination: RegisterOTPView(handphone: self.inputEmail, verificationID: self.verificationID, homeViewModel: homeViewModel), isActive: $action) {
                        EmptyView()
                    }
                    
                    Button(action: {
                        self.navigateOTP()
                    }) {
                        HStack {
                            Spacer()
                            Text("Send")
                            Spacer()
                        }
                    }.buttonStyle(PrimaryButtonStyle())
                        .alert(isPresented: $vm.showAlertOtpReq) {
                        Alert(title: Text("Failed"), message: Text("Data tidak ditemukan. Mohon cek kembali"), dismissButton: .default(Text("Okay")))
                    }
                }.padding()
                
                Spacer()
            }.navigationBarHidden(true)
            .background(Color("theme_background").edgesIgnoringSafeArea(.vertical))
            .onAppear {
                homeViewModel.isForgotPasswordActive = false
            }
            
            Group {
                Loader(isLoading: $vm.isLoading).zIndex(2)
            }
        }
        
//        ScrollView {
//            ZStack(alignment: .topLeading) {
//                VStack(alignment: .leading) {
//                    HStack(alignment: .center) {
//                        Image(uiImage: UIImage(named: "logo_jco")!)
//                            .resizable()
//                            .scaledToFit()
//                            .frame(width: 80, height: 80)
//                    }.frame(maxWidth: .infinity)
//                    Group {
//                        Text("Forgot Password")
//                        Text("Please input your e-mail to reset password")
//                        CustomTextField(placeHolder: "E-mail", value: $inputEmail, lineColor: Color("border_primary"), width: 2, secure: false)
//                        Button(action: {
//
//                        }) {
//                            HStack {
//                                Spacer()
//                                Text("Reset Password")
//                                Spacer()
//                            }
//                        }.buttonStyle(PrimaryButtonStyle())
//                    }
//                }.padding()
//                .navigationBarHidden(true)
//
//                Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
//                    Image(uiImage: UIImage(named: "arrow_left")!)
//                        .resizable()
//                        .scaledToFit()
//                        .frame(width: 30)
//                        .offset(x:20, y:0)
//                }
//            }.frame(maxWidth: .infinity)
//        }.background(Color("theme_background").edgesIgnoringSafeArea(.all))
    }
    
    func navigateOTP() {
        if self.inputEmail != "" {
            self.vm.setLoading(loading: true)
            self.vm.otpRequestVM(email: self.inputEmail, { (isAction) in
                self.vm.setLoading(loading: false)
                action = isAction
            })

        }
    }
}

//struct ForgotPasswordView_Previews: PreviewProvider {
//    static var previews: some View {
//        ForgotPasswordView()
//    }
//}
