//
//  RegisterViewModel.swift
//  Jco
//
//  Created by Ed on 25/02/21.
//

import Foundation
import Combine
import SwiftUI
import Firebase

class RegisterViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    
    @Published var name = ""
    @Published var hp = ""
    @Published var email = ""
    @Published var pass = ""
    @Published var repass = ""
    @Published var inputUserPhone = ""
    
    @Published var otpCode = ""
    
    @Published var accessToken: String?
    @Published var refreshToken: String?
    @Published var showingAlert = false
    @Published var errorMsg = ""
    @Published var isLoading = false
    @Published var navigateRegisterProfile = false
    @Published var navigateRoot = false
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func register(_ completion: @escaping ((Bool)->Void)) {
        if self.name != "" && self.pass != "" && self.repass != "" && self.hp != "" && self.email != "" {
            
            if self.pass.count >= 5 {
                if self.pass == self.repass {
                    self.isLoading = true
                    
                    let cancellable = self.registerUser(name: self.name, pass: self.pass, repass: self.repass, hp: self.hp, email: self.email)
                        .sink(receiveCompletion: { result in
                            switch result {
                            case .failure(let error):
                                print("Handle error reg: \(error)")
    //                            print("name: " + self.name + " pass:" + self.pass + " repass:" + self.repass + " hp:" + self.hp + " email:" + self.email)
                                completion(false)
                                self.errorMsg = "Unknown Error"
                                self.isLoading = false
                            case .finished:
                                break
                            }
                            
                        }) { (result) in
                            if result.status == 200 {
                                if let regToken = result.token {
                                    self.accessToken = regToken.accessToken
                                    self.refreshToken = regToken.refreshToken
                                    UserDefaults.standard.set(self.accessToken, forKey: "AccessToken")
                                    UserDefaults.standard.set(self.refreshToken, forKey: "RefreshToken")
                                }
                                
                                print("SUCCESS")
                                UserDefaults.standard.set(true, forKey: "userIsLogged")
                                self.navigateRoot = true
                                completion(true)
                            } else if result.status == 409 {
                                self.showingAlert = true
                                self.errorMsg = "Email / Handphone sudah terdaftar"
                                self.isLoading = false
                                completion(false)
                            }
                            
                            
            //                print(self.accessToken)
            //                self.detaildata = detail.data
            //                self.variantGroup = detail.data.variantGroup
            //                self.arrVariantChoosen2 = self.variantGroup.map{$0.variantGroupName}
            //                print(detail.data)
                    }
                    cancellables.insert(cancellable)
                } else {
                    completion(false)
                    self.showingAlert = true
                    self.errorMsg = "Password and Confirm Password does not match"
                }
            } else {
                completion(false)
                self.showingAlert = true
                self.errorMsg = "Password length must be more than 5 characters"
            }
        } else {
            completion(false)
            self.showingAlert = true
            self.errorMsg = "Please complete the form"
            self.isLoading = false
        }
    }
    
    func setLoading(loading: Bool) {
        self.isLoading = loading
    }
    
    func setOTPCode(codeOtp: String, verificationID: String) {
        otpCode = codeOtp
        print("SET " + otpCode + " ID: " + verificationID)
        
    }
    
    func goToRegisterProfile() {
        if self.inputUserPhone != "" {
            self.isLoading = true
            
            // Check if phone is registered
            let cancellable = self.getUserCheckPhone(hp: self.inputUserPhone)
                .sink(receiveCompletion: { result in
                    switch result {
                    case .failure(let error):
                        print("Handle error checkphone: \(error)")
//                        completion(false)
                        self.errorMsg = "Unknown Error"
                        self.navigateRegisterProfile = true
                        self.isLoading = false
                    case .finished:
                        break
                    }
                    
                }) { (result) in
//                completion(true)
                    if result.statusCode == 200 {
                        self.displayAlert(msg: "Phone Number is already Registered")
                    } else if result.statusCode == 404 {
                        self.navigateRegisterProfile = true
                    } else {
                        self.displayAlert(msg: "Invalid Phone Number")
                    }
                    self.isLoading = false
            }
            cancellables.insert(cancellable)
        } else {
            self.displayAlert(msg: "Please fill Phone Number")
        }
    }
    
    func verifyCode(email: String) {
        self.isLoading = true
        let cancellable = self.otpVerify(memberLoginKey: email, otp: Int(self.otpCode) ?? 0)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error otp verify: \(error)")
                    self.isLoading = false
                    self.displayAlert(msg: "OTP yang anda masukan salah")

                case .finished:
                    break
                }
                
            }) { (result) in
                self.isLoading = false
                if result.statusCode == 200 {
                    self.navigateRegisterProfile = true
                } else {
                    self.displayAlert(msg: "OTP yang anda masukan salah")
                }
        }
        cancellables.insert(cancellable)
    }
    
    func displayAlert(msg: String) {
        self.showingAlert = true
        self.errorMsg = msg
    }
}
