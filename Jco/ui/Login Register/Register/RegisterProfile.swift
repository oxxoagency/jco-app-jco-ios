//
//  RegisterProfile.swift
//  Jco
//
//  Created by Ed on 29/01/21.
//

import SwiftUI
import URLImage

struct RegisterProfile: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var viewModel = RegisterViewModel()
    @State var name: String = ""
    @State var email: String = ""
    @State var pass: String = ""
    @State var repass: String = ""
    @State private var showingAlert = false
    @State private var isSecured: Bool = true
    @State private var isSecuredConfirm: Bool = true
    @ObservedObject var user: User
    
//    let userHp: String
    init(user: User) {
//        self.userHp = userHp
        self.user = user
//        self.viewModel.hp = userHp
    }
    
//    @ObservedObject var user = User()
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            ScrollView {
                VStack(alignment: .leading) {
                    LoginHeader()
                    
                    Group {
                        Text("Akun ini dapat digunakan pada Apps berikut:")
                            .foregroundColor(Color("theme_text"))
                            .font(.custom("Poppins-Regular", size: 15))
                            .padding(.top, 10)
                            .padding(.horizontal, 20)
                            .lineLimit(2)
                            .multilineTextAlignment(.center)
                        
                        HStack {
                            URLImage(url: URL(string: "https://cdn.app.jcodelivery.com/img/jagroup.jpg")!,
                            content: { image in
                                 image
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
    //                                .frame(width: 100, height: 100)
                                    .clipped()
                            }).padding(.horizontal, 20)
                        }
                    }
                    
                    Group {
                        CustomTextField(placeHolder: "Full Name", value: self.$viewModel.name, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                        CustomTextField(placeHolder: "Email", value: self.$viewModel.email, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                        CustomTextField(placeHolder: "Phone Number", value: self.$viewModel.hp, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                        ZStack(alignment: .trailing) {
                        if isSecured {
                        CustomTextField(placeHolder: "Password", value: self.$viewModel.pass, lineColor: Color("border_primary"), width: 2, secure: true, is_disabled: false, is_number: false)
                        } else {
                            CustomTextField(placeHolder: "Password", value: self.$viewModel.pass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                        }
                            Button(action: {
                                isSecured.toggle()
                            }) {
                                Image(systemName: self.isSecured ? "eye.slash" : "eye")
                                    .accentColor(.gray)
                            }.padding(.horizontal,10)
                        }
                        ZStack(alignment: .trailing) {
                        if isSecuredConfirm {
                        CustomTextField(placeHolder: "Confirm Password", value: self.$viewModel.repass, lineColor: Color("border_primary"), width: 2, secure: true, is_disabled: false, is_number: false)
                        } else {
                            CustomTextField(placeHolder: "Confirm Password", value: self.$viewModel.repass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                        }
                            Button(action: {
                                isSecuredConfirm.toggle()
                            }) {
                                Image(systemName: self.isSecuredConfirm ? "eye.slash" : "eye")
                                    .accentColor(.gray)
                            }.padding(.horizontal,10)
                        }
                    }.padding(.horizontal, 10)
                        .padding(.vertical, 10)
                    
                    HStack {
                        NavigationLink(destination: ContentView(), isActive: self.$viewModel.navigateRoot) {
                            EmptyView()
                        }
                        
                        Button(action: {
                            self.viewModel.register() { (isSuccess) in
                                if isSuccess {
                                    self.user.tokenIsActive = true
                                    self.presentationMode.wrappedValue.dismiss()
                                } else {
                                    self.showingAlert = true
                                }
                            }
                        }) {
                            HStack {
                                Spacer()
                                Text("Create")
                                Spacer()
                            }
                        }.buttonStyle(PrimaryButtonStyle())
                        .alert(isPresented: $showingAlert) {
                            Alert(title: Text("Error"), message: Text(self.viewModel.errorMsg), dismissButton: .default(Text("Okay")))
                        }
                    }.padding(.vertical)
                    .padding(.horizontal)
                    
                    Spacer()
                }
            }
            
            Group {
                Loader(isLoading: $viewModel.isLoading).zIndex(2)
            }
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.vertical))
    }
    
    func userRegister() {
//        self.user.tokenIsActive = true
    }
}

//struct RegisterProfile_Previews: PreviewProvider {
//    static var previews: some View {
//        RegisterProfile()
//    }
//}
