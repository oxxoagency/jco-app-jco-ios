//
//  RegisterView.swift
//  Jco
//
//  Created by Ed on 10/01/21.
//

import SwiftUI
import FirebaseAuth
import URLImage

struct RegisterView: View {
    @ObservedObject var viewModel = RegisterViewModel()
    
    @State var inputEmail: String = ""
    @State var action: Bool = false
    @State var verificationID = ""
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var user: User
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            ScrollView {
                ZStack(alignment: .topLeading) {
                    VStack(alignment: .leading) {
                        HStack(alignment: .center) {
                            Image(uiImage: UIImage(named: "logo_jco")!)
                                .resizable()
                                .scaledToFit()
                                .frame(width: 80, height: 80)
                        }.frame(maxWidth: .infinity)
                        
                        Group {
                            VStack(alignment: .leading) {
                                Text("Register")
                                    .padding(.top, 15)
                                    .padding(.bottom, 10)
                                    .font(.system(size: 18, weight: .bold))
                                
                                Text("Akun ini dapat digunakan pada Apps berikut:")
                                    .foregroundColor(Color("theme_text"))
                                    .font(.custom("Poppins-Regular", size: 15))
                                    .padding(.top, 10)
                                    .padding(.horizontal, 15)
                                    .lineLimit(2)
                                    .multilineTextAlignment(.center)
                                
                                
                                HStack {
                                    URLImage(url: URL(string: "https://cdn.app.jcodelivery.com/img/jagroup.jpg")!,
                                    content: { image in
                                         image
                                            .resizable()
                                            .aspectRatio(contentMode: .fill)
            //                                .frame(width: 100, height: 100)
                                            .clipped()
                                    })
                                    .padding(.horizontal, 15)
                                }
                                
                                HStack {
//                                    Text("+62 ")
//                                        .padding(.bottom, 5)
                                    CustomTextField(placeHolder: "Handphone Number", value: $viewModel.inputUserPhone, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: true)
                                }
                                .padding(.top, 20)
                            }.frame(
                                minWidth: 0,
                                maxWidth: .infinity,
                                alignment: .topLeading
                            )
                        }

                        HStack {
//                            NavigationLink(destination: RegisterOTPView(handphone: self.inputEmail, verificationID: self.verificationID), isActive: $action) {
//                                EmptyView()
//                            }
                            
                            NavigationLink(destination: RegisterProfile(user: user), isActive: self.$viewModel.navigateRegisterProfile) {
                                EmptyView()
                            }
                            
                            Button(action: {
//                                self.navigateRegisterProfile()
                                self.viewModel.goToRegisterProfile()
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                HStack {
                                    Spacer()
                                    Text("Register")
                                    Spacer()
                                }
                            }.buttonStyle(PrimaryButtonStyle())
//                            .alert(isPresented: $viewModel.showingAlert) {
//                                Alert(title: Text("Error"), message: Text(self.viewModel.errorMsg), dismissButton: .default(Text("Okay")))
//                            }
                            .alert(isPresented: self.$viewModel.showingAlert) {
                                Alert(title: Text("Error"), message: Text(self.viewModel.errorMsg), dismissButton: .default(Text("Okay")))
                            }
                            
    //                        Text("Click me!")
    //                            .onTapGesture {
    //                                self.navigateOTP()
    //                        }
                        }.padding(.vertical)
                    }.padding()
                    .navigationBarHidden(true)
                    
                    Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
                        Image(uiImage: UIImage(named: "arrow_left")!)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 30)
                            .offset(x:15, y:20)
                    }
                }.frame(maxWidth: .infinity)
            }
            
            Group {
                Loader(isLoading: $viewModel.isLoading).zIndex(2)
            }
        }.background(Color("theme_background").edgesIgnoringSafeArea(.vertical))
    }

    func navigateOTP() {
        if self.inputEmail != "" {
            self.viewModel.setLoading(loading: true)
    //        action = true
            
            PhoneAuthProvider.provider().verifyPhoneNumber("+62" + self.inputEmail, uiDelegate: nil) { (verificationID, error) in
                if let error = error {
    //                self.showMessagePrompt(error.localizedDescription)
                    print(error.localizedDescription)
                    self.viewModel.setLoading(loading: false)
                    return
                }

                self.verificationID = verificationID ?? ""
                action = true

    //            UserDefaults.standard.set(verificationID, forKey: “authVerificationID”)
    //            verifyPhoneNumberAlert()
            }
        } else {
            self.viewModel.displayAlert(msg: "Please fill Phone Number")
        }
    }
    
}

//struct RegisterView_Previews: PreviewProvider {
//    static var previews: some View {
//        RegisterView()
//    }
//}
