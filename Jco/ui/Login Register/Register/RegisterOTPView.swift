//
//  RegisterOTPView.swift
//  Jco
//
//  Created by Ed on 30/01/21.
//

import SwiftUI
import VKPinCodeView
import Firebase

struct VKPinCodeViewWrapper : UIViewRepresentable {
    @ObservedObject var viewModel: RegisterViewModel
    @State var verificationID: String
    
    func makeUIView(context: Context) -> VKPinCodeView {
        let view = VKPinCodeView()
        view.length = 6
        view.translatesAutoresizingMaskIntoConstraints = false
        view.onSettingStyle = { UnderlineStyle() }
        view.becomeFirstResponder()
        view.onComplete = { code, pinView in
            viewModel.otpCode = code
            print("CODE" + viewModel.otpCode)
            UserDefaults.standard.set(code, forKey: "otpCode")

            self.viewModel.setOTPCode(codeOtp: code, verificationID: self.verificationID)
        }
        return view
    }
    
    func updateUIView(_ uiView: VKPinCodeView, context: Context) {
        //
    }
}

struct RegisterOTPView: View {
    @ObservedObject var viewModel = RegisterViewModel()
    
    @State var action: Bool = false
    @State var handphone: String
    @State var verificationID: String
    @ObservedObject var homeViewModel: HomeViewModel
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            VStack {
                LoginHeader()
                
                Text("A verification OTP has been sent to " + self.handphone)
                    .padding()
                    .foregroundColor(Color("theme_text"))
                    .font(.custom("Poppins-Regular", size: 15))
                
                Text("Please enter the OTP sent to your email below")
                    .padding()
                    .foregroundColor(Color("theme_text"))
                    .font(.custom("Poppins-Regular", size: 15))
                
                VKPinCodeViewWrapper(viewModel: self.viewModel, verificationID: self.verificationID)
                    .frame(height: 50)
                    .padding()
                
                HStack {
//                    NavigationLink(destination: RegisterProfile(userHp: handphone), isActive: self.$viewModel.navigateRegisterProfile) {
//                        EmptyView()
//                    }
                    
                    NavigationLink(destination: NewPasswordView(userHp: handphone, homeViewModel: homeViewModel), isActive: self.$viewModel.navigateRegisterProfile) {
                        EmptyView()
                    }
                    
                    Button(action: {
    //                    self.navigateProfile()
                        self.viewModel.verifyCode(email: self.handphone)
                        
                    }) {
                        HStack {
                            Spacer()
                            Text("Verify")
                            Spacer()
                        }
                    }.buttonStyle(PrimaryButtonStyle())
                }.padding(.vertical)
                .padding(.horizontal)
                .alert(isPresented: self.$viewModel.showingAlert) {
                    Alert(title: Text("Error"), message: Text(self.viewModel.errorMsg), dismissButton: .default(Text("Okay")))
                }

                Spacer()
            }
            
            Group {
                Loader(isLoading: $viewModel.isLoading).zIndex(2)
            }
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.vertical))
    }
    
    func navigateProfile() {
        action = true
    }
    
}

//struct RegisterOTPView_Previews: PreviewProvider {
//    static var previews: some View {
//        RegisterOTPView()
//    }
//}
