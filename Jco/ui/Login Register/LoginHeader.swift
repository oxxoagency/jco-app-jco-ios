//
//  LoginHeader.swift
//  Jco
//
//  Created by Ed on 28/01/21.
//

import SwiftUI

struct LoginHeader: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            VStack(alignment: .leading) {
                HStack(alignment: .center) {
                    Image(uiImage: UIImage(named: "logo_jco")!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 120, height: 120)
                }.frame(maxWidth: .infinity)
            }.padding(.top, 20)
            Button(action: {self.presentationMode.wrappedValue.dismiss()} ) {
                Image(uiImage: UIImage(named: "arrow_left")!)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 30)
                    .offset(x:20, y:20)
            }
        }
    }
}

struct LoginHeader_Previews: PreviewProvider {
    static var previews: some View {
        LoginHeader()
    }
}
