//
//  NewPasswordViewModel.swift
//  Jco
//
//  Created by Ed on 28/10/21.
//

import Foundation
import Combine

class NewPasswordViewModel: ObservableObject, ModelService {
    var apiSession: APIService

    @Published var isLoading = false
    @Published var user = ""
    @Published var newPass = ""
    @Published var rePass = ""
    @Published var showAlert = false
    @Published var showAlertOtpReq = false
    @Published var alertMessage = ""
    @Published var titleAlertMessage = ""

    @Published var isSecuredOldPassword = true
    @Published var isSecuredNewPassword = true
    @Published var isSecuredRepeatPassword = true
    @Published var navigateToLogin = false

    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }

    func changePassword(phoneNumber: String, _ completion: @escaping ((Bool)->Void)) {
        if self.newPass != "" && self.rePass != "" {
            if self.newPass.count >= 5 {
                if self.newPass == self.rePass {
                    self.isLoading = true
                    
                    let cancellable = self.resetPassword(user: phoneNumber, newpass: self.newPass, confpass: self.rePass)
                        .sink(receiveCompletion: { result in
                            switch result {
                            case .failure(let error):
                                print("Handle error update: \(error)")
                                self.showAlert = true
                                self.alertMessage = "Update Failed. Wrong Password"
                                self.isLoading = false
                                self.titleAlertMessage = "Error"
                                completion(false)
                                self.navigateToLogin = false
                            case .finished:
                                break
                            }

                        }) { (result) in
                            print(result)
                            if result.statusCode == 200 {
                                self.showAlert = true
                                self.alertMessage = "Password Successfuly Updated"
                                self.titleAlertMessage = "Success"
                                self.newPass = ""
                                self.rePass  = ""
                                self.navigateToLogin = true
                                completion(true)
                            } else {
                                completion(false)
                            }
                            self.isLoading = false
                    }
                    cancellables.insert(cancellable)
                } else {
                    completion(false)
                    self.showAlert = true
                    self.alertMessage = "Password and Confirm Password does not match"
                }
            } else {
                completion(false)
                self.showAlert = true
                self.alertMessage = "Password length must be more than 5 characters"
            }
        } else {
            completion(false)
            self.showAlert = true
            self.alertMessage = "Please complete the form"
        }
    }
    
    func otpRequestVM(email: String, _ completion: @escaping ((Bool)->Void)) {
        let cancellable = self.otpRequest(memberLoginKey: email)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error otp request: \(error.localizedDescription)")
                    self.showAlertOtpReq = true
                    completion(false)

                case .finished:
                    break
                }
                
            }) { (result) in
                if result.statusCode == 200 {
                    self.showAlertOtpReq = false
                    completion(true)
                } else {
                    self.showAlertOtpReq = true
                    completion(false)
                }
        }
        cancellables.insert(cancellable)

    }
    
    func setLoading(loading: Bool) {
        self.isLoading = loading
    }
}
