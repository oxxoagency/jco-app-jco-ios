//
//  NewPasswordView.swift
//  Jco
//
//  Created by Ed on 28/10/21.
//

import Foundation

import SwiftUI

struct NewPasswordView: View {
    @ObservedObject var vm = NewPasswordViewModel()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var showLogin: Bool = false
    @ObservedObject var homeViewModel: HomeViewModel
    
    let userHp: String
    init(userHp: String, homeViewModel: HomeViewModel) {
        self.userHp = userHp
        self.homeViewModel = homeViewModel
    }
    
    var body: some View {
        VStack {
            LoginHeader()
            
            ZStack(alignment: .topLeading) {
                VStack {
                    Spacer().frame(height: 40)
                    
                    ZStack(alignment: .trailing) {
//                        if isSecuredNewPassword {
                        CustomTextField(placeHolder: "New Password", value: $vm.newPass, lineColor: Color("border_primary"), width: 2, secure: self.vm.isSecuredNewPassword, is_disabled: false, is_number: false)
                                .foregroundColor(Color("theme_text"))
//                        } else {
//                            CustomTextField(placeHolder: "New Password", value: $vm.newPass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
//                                .foregroundColor(Color("theme_text"))
//                        }
                        Button(action: {
                            self.vm.isSecuredNewPassword.toggle()
                        }) {
                            Image(systemName: self.vm.isSecuredNewPassword ? "eye.slash" : "eye")
                                .accentColor(.gray)
                        }.padding(.horizontal,10)
                    }
                    Spacer().frame(height: 40)
                    
                    ZStack(alignment: .trailing) {
//                        if isSecuredRepeatPassword {
                        CustomTextField(placeHolder: "Repeat Password", value: $vm.rePass, lineColor: Color("border_primary"), width: 2, secure: self.vm.isSecuredRepeatPassword, is_disabled: false, is_number: false)
                                .foregroundColor(Color("theme_text"))
//                        } else {
//                            CustomTextField(placeHolder: "Repeat Password", value: $vm.rePass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false)
//                                .foregroundColor(Color("theme_text"))
//                        }
                        Button(action: {
                            self.vm.isSecuredRepeatPassword.toggle()
                        }) {
                            Image(systemName: self.vm.isSecuredRepeatPassword ? "eye.slash" : "eye")
                                .accentColor(.gray)
                        }.padding(.horizontal,10)
                    }
                    Spacer()
                    
                    VStack {
                        Button(action: {
                            self.vm.changePassword(phoneNumber: "\(userHp)") { (isSuccess) in
                                if isSuccess {
                                    self.presentationMode.wrappedValue.dismiss()
                                    self.homeViewModel.viewLogin()
                                    self.homeViewModel.isForgotPasswordActive = false

                                }
                            }
                        }) {
                            HStack {
                                Spacer()
                                Text("Save")
                                    .font(.custom("Poppins-Medium", size: 18))
                                Spacer()
                            }
                        }.buttonStyle(PrimaryButtonStyle())
                        .font(.custom("Poppins-Medium", size: 16))
                        .alert(isPresented: $vm.showAlert) {
                            Alert(title: Text(self.vm.titleAlertMessage), message: Text(self.vm.alertMessage), dismissButton: .default(Text("Okay"), action: {
                                self.showLogin = true
                            }))
                        }
                        
//                        Button(action: {
//                            self.presentationMode.wrappedValue.dismiss()
//                        }) {
//                            HStack {
//                                Spacer()
//                                Text("Cancel")
//                                    .font(.custom("Poppins-Medium", size: 18))
//                                Spacer()
//                            }
//                        }.buttonStyle(SecondaryButtonStyle())
//                        .font(.custom("Poppins-Medium", size: 16))
                    }.padding(.horizontal, 15)
                }
                
                Group {
                    Loader(isLoading: $vm.isLoading).zIndex(2)
                }
            }
            NavigationLink(destination: ContentView(), isActive: $vm.navigateToLogin) { EmptyView() }
            Spacer()
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}

//struct UserChangePassword_Previews: PreviewProvider {
//    static var previews: some View {
//        UserChangePassword()
//    }
//}
