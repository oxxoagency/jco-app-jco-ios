//
//  LoginViewModel.swift
//  Jco
//
//  Created by Ed on 23/01/21.
//

import Foundation
import Combine
import SwiftUI
import Firebase

class LoginViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    
    @Published var user = ""
    @Published var pass = ""
    
    @Published var otpCode = ""
    
    @Published var accessToken: String?
    @Published var refreshToken: String?
    @Published var showingAlert = true
    @Published var isLoading = false
    @Published var navigateRegisterProfile = false
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func login(_ completion: @escaping ((Bool)->Void)) {
        let cancellable = self.loginUser(user: user, pass: pass)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error llogin: \(error)")
                    completion(false)
                case .finished:
                    break
                }
                
            }) { (result) in
                self.accessToken = result.accessToken
                self.refreshToken = result.refreshToken
                UserDefaults.standard.set(self.accessToken, forKey: "AccessToken")
                UserDefaults.standard.set(self.refreshToken, forKey: "RefreshToken")
                UserDefaults.standard.set(self.user, forKey: "MemberHandphone")
                UserDefaults.standard.set(true, forKey: "userIsLogged")
                completion(true)
                
//                print(self.accessToken)
//                self.detaildata = detail.data
//                self.variantGroup = detail.data.variantGroup
//                self.arrVariantChoosen2 = self.variantGroup.map{$0.variantGroupName}
//                print(detail.data)
                
                self.getUserProfile()
        }
        cancellables.insert(cancellable)
    }
    
    func getUserProfile(brand: Int) {
        let cancellable = self.getUserProfile()
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error loginprofile: \(error)")
                    self.getMenuList(brand: brand)
                case .finished:
                    break
                }

            }) { (result) in
                print(result)
                if result.statusCode == 200 {
                    UserDefaults.standard.set(result.data.memberEmail, forKey: "MemberEmail")
                    UserDefaults.standard.set(result.data.memberName, forKey: "MemberName")
                    UserDefaults.standard.set(result.data.memberPhone, forKey: "MemberPhone")
                    UserDefaults.standard.set(result.data.memberPhone2, forKey: "MemberPhone2")
                }
        }
        cancellables.insert(cancellable)
    }
    
    func setLoading() {
        self.isLoading = true
    }
}
