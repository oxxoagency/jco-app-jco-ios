//
//  HelpCenter.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 07/10/21.
//

import SwiftUI

struct HelpCenterView: View {
    
    var body: some View {
        VStack {
            Header(title: "Help Center", back: true)
            
            WebView(request: URLRequest(url: URL(string: "https://order.jcodelivery.com/help/ios")!))
            
            Spacer()
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}

struct ContactUsWebView: View {
    
    var body: some View {
        VStack {
            Header(title: "contact-us", back: true)
            
            WebView(request: URLRequest(url: URL(string: "https://order.jcodelivery.com/contact/ios")!))
            
            Spacer()
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}
