//
//  LoginView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import URLImage

struct LoginView: View {
    @ObservedObject var user: User
    @ObservedObject var homeViewModel: HomeViewModel
    @ObservedObject var viewModel = LoginViewModel()
    @State var inputUser: String = ""
    @State var inputPass: String = ""
    @State private var showingAlert = false
    @State private var isSecured: Bool = true
    @ObservedObject var viewModelNew = UserAddressViewModel()
    @ObservedObject var vm: MenuListViewModel
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    let appBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
    
    //@State private var checkAmount = UserDefaults.standard.object(forKey: "AccessToken")
    @State private var checkAmount = UserDefaults.standard.object(forKey: "AccessToken") as? String ?? "false"
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            ScrollView {
                VStack(alignment: .leading) {
                    HStack(alignment: .center) {
                        Image(uiImage: UIImage(named: "logo_jco")!)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 100)
                    }.frame(maxWidth: .infinity)
                    Group {
                        Text("Login")
                            .font(.custom("Poppins-Bold", size: 22))
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                        Text("Silahkan gunakan akun yang terdaftar pada Apps berikut:")
                            .foregroundColor(Color("theme_text"))
                            .font(.custom("Poppins-Regular", size: 15))
                            .padding(.top, 10)
                            .lineLimit(nil)
                            .fixedSize(horizontal: false, vertical: true)
                            .multilineTextAlignment(.center)
                            .frame(maxWidth: .infinity, alignment: .center)
                      
                      Text("Part of Johnny Andrean Group")
                          .foregroundColor(Color("theme_text"))
                          .font(.custom("Poppins-Regular", size: 15))
                          .frame(maxWidth: .infinity, alignment: .center)
                        
                        HStack {
                            Spacer()
                            URLImage(url: URL(string: "https://cdn.app.jcodelivery.com/img/jagroup.jpg")!,
                            content: { image in
                                 image
                                    .resizable()
                                    .scaledToFit()
                                    .frame(height: 30)
                            })
                            Spacer()
                        }
                    }
                    
                    Group {
                        CustomTextField(placeHolder: "Handphone", value: $vm.user, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: true)
                            .foregroundColor(Color.red)
                        //                    Spacer().frame(height: 10)
                        ZStack(alignment: .trailing) {
                            if isSecured {
                                CustomTextField(placeHolder: "Password", value: $vm.pass, lineColor: Color("border_primary"), width: 2, secure: true, is_disabled: false, is_number: false)
                                    .foregroundColor(Color("theme_text"))
                            }else {
                                CustomTextField(placeHolder: "Password", value: $vm.pass, lineColor: Color("border_primary"), width: 2, secure: false, is_disabled: false, is_number: false)
                                    .foregroundColor(Color("theme_text"))
                            }
                            Button(action: {
                                isSecured.toggle()
                            }) {
                                Image(systemName: self.isSecured ? "eye.slash" : "eye")
                                    .accentColor(.gray)
                            }.padding(.horizontal,10)
                        }
    //                    Spacer().frame(height: 10)
                        Button(action: {
                            self.vm.login { (isSuccess) in
                                if isSuccess {
                                    self.user.tokenIsActive = true
                                    self.homeViewModel.tabHome()
                                } else {
                                    self.showingAlert = true
                                }
                            }
                        }) {
                            HStack {
                                Spacer()
                                Text("Sign In")
                                Spacer()
                            }
                        }.buttonStyle(PrimaryButtonStyle())
                        .alert(isPresented: $showingAlert) {
                            Alert(title: Text("Error"), message: Text(vm.errorMessage), dismissButton: .default(Text("Okay")))
                        }
                    }.padding(.top, 20)
                    
                    Spacer().frame(height: 30)
                    
                    // Register & Version code
                    Group {
                        VStack(spacing: 10) {
                            Button(action: {
                                homeViewModel.isForgotPasswordActive = true
                            }) {
                                Text("Forgot Password?")
                                    .font(.system(size: 14))
                                    .foregroundColor(Color("btn_primary"))
                            }.frame(maxWidth: .infinity)
                            NavigationLink(destination: ForgotPasswordView(homeViewModel: homeViewModel),isActive: $homeViewModel.isForgotPasswordActive) {
                                EmptyView()
                            }
                            NavigationLink(destination: RegisterProfile(user: user)) {
                                Text("Don't Have Account Yet?")
                                    .font(.custom("Poppins-Regular", size: 13))
                                    .foregroundColor(Color.gray)
                                Text("Let's Create")
                                    .font(.custom("Poppins-Regular", size: 13))
                                    .foregroundColor(Color("btn_primary"))
                            
                            }.frame(maxWidth: .infinity)
                            
                            NavigationLink(destination: HelpCenterView()) {
                                Text("Can't Login?")
                                    .font(.custom("Poppins-Regular", size: 13))
                                    .foregroundColor(Color.gray)
                                Text("Help Center")
                                    .font(.custom("Poppins-Regular", size: 13))
                                    .foregroundColor(Color("btn_primary"))
                            
                            }.frame(maxWidth: .infinity)
                            
                            Text("\(appVersion ?? "") (\(appBuild ?? ""))")
                                .font(.custom("Poppins-Regular", size: 14))
                                .foregroundColor(Color.gray)
                        }
                    }
                    // Register & Version code
                    Spacer()
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .padding()
                
                Spacer()
            }
            
            Group {
                Loader(isLoading: $vm.isLoading).zIndex(2)
            }
        }.onChange(of: self.viewModel.accessToken) {
            self.user.tokenIsActive = $0 != nil
        }
        .navigationBarTitle("") //this must be empty
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.top))
        
    }
    
    func Login() {
        
        let body: [String: Any] = ["member_loginkey": inputUser, "member_password": inputPass]
        let rb = try! JSONSerialization.data(withJSONObject: body)
        let returnData = Util().HttpRequest(url: "https://jcousergolangdev.oxxo.co.id/member/login", token: Constants.API_TOKEN, body: rb)
        
        print("ASD" + returnData)
       
//        self.user.tokenIsActive = true
    }
}

//struct LoginView_Previews: PreviewProvider {
//    static var previews: some View {
//        LoginView(user: User())
//    }
//}

struct CustomTextField: View {
    @EnvironmentObject var brandSettings: BrandSettings
    var placeHolder: String
    @Binding var value: String
    
    var lineColor: Color
    var width: CGFloat
    var secure: Bool
    var is_disabled: Bool
    var is_number: Bool
    var isEmail: Bool?
    
    var body: some View {
        VStack {
            ZStack(alignment: .leading) {
                if value.isEmpty { Text(self.placeHolder).foregroundColor(Color.gray).padding(.horizontal) }
                
                if secure {
                    SecureField(self.placeHolder, text: $value)
                        .padding(.horizontal)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                } else {
                    TextField(self.placeHolder, text: $value)
                        .padding(.horizontal)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .disabled(is_disabled)
                        .keyboardType(is_number ? .numberPad : (isEmail ?? false ? .emailAddress : .default))
                }
            }
            
            Rectangle().frame(height: self.width)
                .padding(.horizontal, 20).foregroundColor(self.lineColor)
        }
    }
}
