//
//  TestMenuDetailView.swift
//  Jco
//
//  Created by Ed on 18/06/21.
//

import SwiftUI

struct TestMenuDetailView: View {
    @ObservedObject var viewModel = MenuDetailViewModel()

    let menuCode: String
    init(menuCode: String) {
        self.menuCode = menuCode
        self.viewModel.menuCode = menuCode
    }
    
    var body: some View {
        VStack {
            Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            
            if(self.viewModel.detaildata?.categoryName == "package") {
                Group {
                    ForEach(Array(self.viewModel.menuDetails.enumerated()), id: \.1) { indexGroup, vg in
                        
                        Text(String(self.viewModel.arrMenuVariantSelected[indexGroup].packageName))
                        Text(String(self.viewModel.arrMenuVariantSelected[indexGroup].packageQty))
                    }
                }
            }
            
            Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        }.onAppear {
            self.viewModel.getMenuDetail(brand: 1)
        }
    }
}
