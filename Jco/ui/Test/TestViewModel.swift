//
//  TestViewModel.swift
//  Jco
//
//  Created by Ed on 13/06/21.
//

import Foundation

class TestViewModel: ObservableObject {
    @Published var isDismiss: Bool
    
    init(){
        isDismiss = false
    }
    
    func setDismiss() {
        self.isDismiss = false
    }
}
