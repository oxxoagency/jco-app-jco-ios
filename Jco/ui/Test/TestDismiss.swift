//
//  TestDismiss.swift
//  Jco
//
//  Created by Ed on 13/06/21.
//

import SwiftUI

struct TestDismiss: View {
//    @State var isActive : Bool = false
    @ObservedObject var vm = TestViewModel()

    var body: some View {
        NavigationView {
            NavigationLink(
                destination: ScreenB(vm: vm),
//                isActive: self.$isActive
                isActive: $vm.isDismiss
            ) {
                Text("ScreenA")
            }
            .isDetailLink(false)
            .navigationBarTitle("Screen A")
        }.navigationBarHidden(true)
    }
}

struct ScreenB: View {
//    @Binding var rootIsActive : Bool
    @ObservedObject var vm: TestViewModel

    var body: some View {
        VStack {
            NavigationLink(destination: ScreenD(vm: vm)) {
                Text("Next screen")
            }
            .isDetailLink(false)
            
            Button (action: {
                self.vm.setDismiss()
            }){
                Text("Pop to root")
            }
        }
        .navigationBarTitle("Screen B")
        .navigationBarHidden(true)
    }
}

struct ScreenD: View {
//    @Binding var shouldPopToRootView : Bool
    @ObservedObject var vm: TestViewModel

    var body: some View {
        VStack {
            Text("Last Screen")
            
            Button (action: {
                self.vm.setDismiss()
            }){
                Text("Pop to root")
            }
//            Button (action: { self.shouldPopToRootView = false } ){
//                Text("Pop to root")
//            }
        }.navigationBarTitle("Screen D")
        .navigationBarHidden(true)
    }
}
