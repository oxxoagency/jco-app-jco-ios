//
//  PaymentView.swift
//  Jco
//
//  Created by Ed on 15/06/21.
//

import SwiftUI
import WebKit
import Combine

struct PaymentView: View {
//    @ObservedObject var model = WebViewModel(link: "https://www.wikipedia.org/")
//    @ObservedObject var vm: CartViewModel
    
    let vm: CartViewModel
    var body: some View {
        VStack {
            Header(title: "Check Out", back: true)
            
//            Text("Payment Successful")
//                .font(.custom("Poppins-Bold", size: 20))
//                .padding(25)
            
//            Text(self.vm.paymentUrl2)
            WebView(request: URLRequest(url: URL(string: self.vm.paymentUrl)!))
//            WebView(vm: vm)
//            SwiftUIWebView(viewModel: model)
//            if model.didFinishLoading {
//                //do your stuff
//            }
        }.background(Color("theme_background").edgesIgnoringSafeArea(.vertical))
        .navigationBarHidden(true)
    }
}

//struct PaymentView_Previews: PreviewProvider {
//    static var previews: some View {
//        PaymentView()
//    }
//}


struct WebView : UIViewRepresentable {
//    @ObservedObject var vm: CartViewModel
    let request: URLRequest
    
    
    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
//        let request = URLRequest(url: URL(string: self.vm.paymentUrl2)!)
        uiView.load(request)
    }
    
}

struct SwiftUIWebView: UIViewRepresentable {
    @ObservedObject var viewModel: WebViewModel

    let webView = WKWebView()

    func makeUIView(context: UIViewRepresentableContext<SwiftUIWebView>) -> WKWebView {
        self.webView.navigationDelegate = context.coordinator
        if let url = URL(string: viewModel.link) {
            self.webView.load(URLRequest(url: url))
        }
        return self.webView
    }

    func updateUIView(_ uiView: WKWebView, context: UIViewRepresentableContext<SwiftUIWebView>) {
        return
    }

    class Coordinator: NSObject, WKNavigationDelegate {
        private var viewModel: WebViewModel

        init(_ viewModel: WebViewModel) {
            self.viewModel = viewModel
        }

        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            //print("WebView: navigation finished")
            self.viewModel.didFinishLoading = true
        }
    }

    func makeCoordinator() -> SwiftUIWebView.Coordinator {
        Coordinator(viewModel)
    }
}
