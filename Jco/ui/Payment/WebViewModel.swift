//
//  WebViewModel.swift
//  Jco
//
//  Created by Ed on 15/06/21.
//

import Foundation
import Combine

class WebViewModel: ObservableObject {
    @Published var link: String
    @Published var didFinishLoading: Bool = false

    init (link: String) {
        self.link = link
    }
} 
