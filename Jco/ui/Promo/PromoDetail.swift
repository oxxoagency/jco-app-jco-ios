//
//  PromoDetail.swift
//  Jco
//
//  Created by Ed on 30/04/21.
//

import SwiftUI
import URLImage

struct PromoDetail: View {
    @ObservedObject var vm = PromoDetailViewModel()
    @ObservedObject var hvm: HomeViewModel
    @ObservedObject var viewModel = MenuDetailViewModel()
    @EnvironmentObject var brandSettings: BrandSettings
    
    let bannerID: String
    
    init(hvm: HomeViewModel, bannerID: String) {
        self.hvm = hvm
        self.bannerID = bannerID
        self.vm.bannerID = bannerID
    }
    
    var body: some View {
        VStack {
            Header(title: "Promo Detail", back: true)
            
            VStack(alignment: .leading) {
                ZStack(alignment: .topLeading) {
                    VStack {
                        ScrollView {
                            VStack(alignment: .leading) {
                                // Promo Img
                                Group {
                                    VStack {
                                        if let dataImg = self.vm.data?.bannerImg {
                                            URLImage(url: URL(string: dataImg)!,
                                            content: { image in
                                                 image
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: .infinity)
                                                    .clipped()
                                            })
                                        }
                                    }.frame(alignment: .center)
                                }
                                // Promo Img
                                
                                // Promo Period
                                Group {
                                    HStack(alignment: .center) {
                                        Image(systemName: "calendar")
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                                            .font(.system(size: 35))
                                            .padding(.leading, 15)
                                        
                                        VStack(alignment: .leading) {
                                            Text("Promo Period")
                                                .font(.custom("Poppins-Regular", size: 13))
                                                .foregroundColor(Color("c_666666"))
                                            
                                            if let startDate = self.vm.data?.startDate, let endDate = self.vm.data?.endDate {
                                                Text(startDate + " - " + endDate)
                                                    .font(.custom("Poppins-Medium", size: 13))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color.white)
                                            }
                                        }.padding(.leading, 6)
                                    }
                                }.padding(.top, 10)
                                // Promo Period
                                
                                // Promo Desc
                                Group {
                                    if let promoDesc = self.vm.data?.bannerDescription {
                                        Text(promoDesc)
                                            .font(.custom("Poppins-Regular", size: 15))
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("c_333333") : Color.white)
                                            .padding()
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                // Promo Desc
                            }
                            
                            Spacer()
                        }
                    }
                    
                    // Add to cart btn
                    Group {
                        VStack {
                            Spacer()
                            if !(self.vm.data?.relatedMenu.isEmpty ?? true) {
                                if self.vm.data?.relatedMenu.count ?? 0 > 1 {
                                    Button(action: {
                                        self.vm.isPresented.toggle()
                                    }) {
                                        HStack {
                                            Spacer()
                                            Text("choose-promo-menu")
                                            Spacer()
                                        }
                                    }.buttonStyle(PrimaryButtonStyle())
                                } else {
                                    Button(action: {
                                        self.vm.addToCart()
                                    }) {
                                        HStack {
                                            Spacer()
                                            Text("add-to-cart")
                                            Spacer()
                                        }
                                    }.buttonStyle(PrimaryButtonStyle())
                                }
                            }
                        }.sheet(isPresented: $vm.isPresented) {
                            PromoMenuModal(vm: self.vm)
                        }
                    }.padding()
                    
                    // Alert
                    Group {
                        if self.vm.showAlertSuccess {
                            AlertSuccess(viewModel: self.viewModel, homeVm: hvm, isFromoPromo: false)
                        } else {
                            EmptyView()
                        }
                    }
                }
            }
        }.onAppear {
            self.vm.getPromoDetail(brand: self.brandSettings.curBrand)
        }.navigationBarHidden(true)
        .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
    }
}

struct PromoMenuModal: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var vm: PromoDetailViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("choose-promo-menu")
                .font(.custom("Poppins-Bold", size: 20))
                .padding()
            
            Group {
                ForEach(self.vm.data!.relatedMenu, id: \.self) { m in
                    Button(action: {
                        self.vm.chooseMenu(
                            menuCode: m.menuCode, menuName: m.menuName, menuImg: m.menuImage, menuPrice: m.menuPrice
                        )
                    }) {
                        VStack(alignment: .leading) {
                            HStack {
                                HStack {
                                    URLImage(url: URL(string: m.menuImage)!,
                                    content: { image in
                                         image
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 100, height: 100)
                                            .clipped()
                                    })
                                }.frame(minWidth: 100, minHeight: 100)
                                .background(Color("c_e6e6e6"))
                                .cornerRadius(10)
                                .padding()
                                
                                VStack(alignment: .leading) {
                                    Text(m.menuName)
                                        .font(.custom("Poppins-Medium", size: 20))
                                        .foregroundColor(Color("theme_text"))
                                        .lineLimit(nil)
                                        .multilineTextAlignment(.leading)
                                        .fixedSize(horizontal: false, vertical: true)
                                        .padding(.bottom, 2)
                                                         
                                    Text("Rp. " + Util().addTS(str: String(m.menuPrice)))
                                        .font(.custom("Poppins-Regular", size: 15))
                                        .foregroundColor(Color("theme_text"))
                                }
                            }
                            
                            Rectangle().frame(height: 1).foregroundColor(Color.gray)
                        }.padding()
                    }
                }
            }
            
            Spacer()
        }.navigationBarTitle("")
        .navigationBarHidden(true)
        .frame(
            alignment: .topLeading
        )
    }
}

//struct PromoDetail_Previews: PreviewProvider {
//    static var previews: some View {
//        PromoDetail()
//    }
//}
