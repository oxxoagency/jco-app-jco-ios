//
//  PromoDetailViewModel.swift
//  Jco
//
//  Created by Ed on 19/09/21.
//

import Foundation
import Combine

class PromoDetailViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    var managedObjectContext = PersistenceController.shared.container.viewContext
    
    @Published var bannerID = ""
    @Published var isLoading = true
    @Published var showAlertSuccess = false
    @Published var isPresented = false
    @Published var data: BannerDetailResData?
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getPromoDetail(brand: Int) {
        let cancellable = self.getBannerDetail(bannerID: bannerID, brand: brand)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error promod: \(error)")
                case .finished:
                    break
                }
                
            }) { (result) in
                if result.statusCode == 200 {
                    self.data = result.data
                }
                self.isLoading = false
            }
        cancellables.insert(cancellable)
    }
    
    func addToCart() {
        let newCart = Cart(context: managedObjectContext)
        newCart.menuCode = self.data?.relatedMenu[0].menuCode
        newCart.menuName = self.data?.relatedMenu[0].menuName
        newCart.menuImg = self.data?.relatedMenu[0].menuImage
        newCart.menuQty = 1
        newCart.menuPrices = String(self.data?.relatedMenu[0].menuPrice ?? 0)
        newCart.menuVariant = self.data?.relatedMenu[0].menuDetail
        
        PersistenceController.shared.save()
        print(newCart)
        
//        if self.data?.relatedMenu.count ?? 0 > 0 {
//            self.data?.relatedMenu.map { (v) -> BannerDetailResMenu in
//
//                let newCart = Cart(context: managedObjectContext)
//                newCart.menuCode = v.menuCode
//                newCart.menuName = v.menuName
//                newCart.menuImg = v.menuImage
//                newCart.menuQty = 1
//                newCart.menuPrices = String(v.menuPrice)
//                newCart.menuVariant = ""
//
//                PersistenceController.shared.save()
//                print(newCart)
//
//                return v
//            }
//        }
        
        self.showAlertSuccess = true
    }
    
    func chooseMenu(menuCode: String, menuName: String, menuImg: String, menuPrice: Int) {
        
        let newCart = Cart(context: managedObjectContext)
        newCart.menuCode = menuCode
        newCart.menuName = menuName
        newCart.menuImg = menuImg
        newCart.menuQty = 1
        newCart.menuPrices = String(menuPrice)
        newCart.menuVariant = ""
        
        PersistenceController.shared.save()
        print(newCart)
        
        self.isPresented = false
        self.showAlertSuccess = true
    }
}
