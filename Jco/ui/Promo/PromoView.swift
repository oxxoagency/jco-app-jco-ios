//
//  PromoView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 14/10/21.
//

import SwiftUI
import URLImage

struct PromoView: View {
    @ObservedObject var vm = PromoViewModel()
    @ObservedObject var hvm: HomeViewModel
    @EnvironmentObject var brandSettings: BrandSettings
    
    var body: some View {
        VStack {
            ZStack(alignment: .topLeading) {
                Header(title: "Hot Promo", back: true)
            }
            content
        }.onAppear {
            vm.getPromos(brand: self.brandSettings.curBrand)
        }.navigationBarHidden(true)
        .background(self.brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

extension PromoView {
    var content: some View {
        ScrollView(.vertical, showsIndicators: false) {
            ForEach(self.vm.data, id: \.self) { promo in
                NavigationLink(destination: MenuDetailView(hvmm: hvm, menuCode: promo.menuCode, isFromPromo: true)) {
                    PromoRow(promo: promo)
                }
            }
        }
    }
}

struct PromoRow: View {
    @EnvironmentObject var brandSettings: BrandSettings
    var promo: Promo
    var body: some View {
        VStack {
            imagePromo
            nameProduct
            period
            line
        }.frame(width: UIScreen.main.bounds.width - 24)
    }
}

extension PromoRow {
    var imagePromo: some View {
        URLImage(url: URL(string: promo.menuImage)!, content: { image in
            image
                .resizable()
                .aspectRatio(contentMode: .fill)
                .scaledToFit()
                .cornerRadius(16)
        })
    }
    
    var nameProduct: some View {
        Text(promo.menuName)
            .font(.custom("Poppins-Medium", size: 16))
            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top)
    }
    
    var period: some View {
        HStack {
            Image(systemName: "calendar")
                .foregroundColor(Color("btn_primary"))
                .font(.system(size: 20))
                .padding(.top, 2)
            VStack {
                Text("Promo Period")
                    .font(.custom("Poppins-Regular", size: 10))
                    .foregroundColor(Color.gray)
                    .frame(maxWidth: .infinity, alignment: .leading)
                if let startDate = promo.startDate, let endDate = promo.endDate {
                    Text(startDate + " - " + endDate)
                        .font(.custom("Poppins-Medium", size: 13))
                        .foregroundColor(Color("btn_primary"))
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
            }
        }
    }
    
    var line: some View {
        Rectangle()
            .fill(Color.gray)
            .frame(width: .infinity, height: 1)
            .padding(.vertical)
    }
}
