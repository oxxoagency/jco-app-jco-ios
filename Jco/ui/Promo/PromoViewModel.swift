//
//  PromoViewModel.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 14/10/21.
//

import Foundation
import Combine

class PromoViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    
    @Published var isLoading = true
    @Published var data = [Promo]()
    @Published var message = ""
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
  func getPromos(brand: Int){
        let cancellable = self.getPromo(brand: brand).sink(receiveCompletion: { result in
            switch result {
            case .failure(let error):
                print("Handle error getpromo: \(error)")
            case .finished:
                break
            }
        }){ (result) in
            if let promos = result.data {
                self.data = promos
            }
            
            if result.data?.isEmpty ?? false {
              self.message = "Saat ini promo tidak tersedia"
              print(self.message)
            }
            print(self.data)
            self.isLoading = false
        }
        cancellables.insert(cancellable)
    }
}
