//
//  OutletView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI

struct OutletView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @Environment(\.presentationMode) var presentationMode
    var city: String
    @ObservedObject var viewModel = OutletViewModel()
    @ObservedObject var cartViewModel: CartViewModel
    
    var body: some View {
        VStack {
            Header(title: "Outlet", back: true)
            
            Group {
                ScrollView {
                    ForEach(self.viewModel.dataOutlet, id: \.self) { l in
                        Button(action: {
                            self.cartViewModel.chooseOutlet(
                                brand: self.brandSettings.curBrand,
                                outletId    : l.outletId,
                                outletName  : l.outletName,
                                outletCity  : l.outletCity,
                                outletCode  : l.outletCode,
                                outletLatitude: l.outletLatitude,
                                outletLongitude: l.outletLongitude,
                                outletAddress: l.outletAddress,
                                outletPostcode: l.outletPostcode,
                                orderBrand: self.brandSettings.curBrand == 1 ? "JCO" : "JCOR"
                            )
//                            self.presentationMode.wrappedValue.dismiss()
                            self.cartViewModel.setDismiss()
                        }) {
                            VStack(alignment: .leading) {
                                VStack(alignment: .leading) {
                                    Text(l.outletName)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                        .padding(.bottom, 3)
                                }.padding()
//                                .background(Color.red)
                                .frame(
                                    minWidth: 0,
                                    maxWidth: .infinity,
                                    alignment: .topLeading
                                )
                                
                                Rectangle().frame(height: 1)
                                    .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                            }.frame(maxWidth: .infinity)
                        }
                    }
                    Spacer()
                }
            }
            
            Spacer()
        }.onAppear {
            self.viewModel.getOutlet(city: city, brand: brandSettings.curBrand)
        }.navigationBarHidden(true)
            .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
    }
}

//struct OutletView_Previews: PreviewProvider {
//    static var previews: some View {
//        OutletView()
//    }
//}
