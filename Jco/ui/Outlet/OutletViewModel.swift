//
//  OutletViewModel.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import Foundation
import Combine

class OutletViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    @Published var detaildata = [City]()
    @Published var dataOutlet = [Outlet]()
    @Published var search = ""
    @Published var outletNearest = [OutletNearest]()
    var userLatitude = UserDefaults.standard.object(forKey: "userLatitude") as? String ?? ""
    var userLongitude = UserDefaults.standard.object(forKey: "userLongitude") as? String ?? ""

    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getOutletNearest(brand: Int) {
        let cancellable = self.getOutletNearest(brand: brand, lat: userLatitude, lng: userLongitude)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error outlet nearest \(error)")
                case .finished:
                    break
                }
            }) { (outlet) in
                self.outletNearest = outlet.data
            }
        cancellables.insert(cancellable)
    }

    
    func getCity(search: String) {
        let cancellable = self.getCity(search: search)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    break
                }

            }) { (detail) in
                self.detaildata = detail.data
        }
        cancellables.insert(cancellable)
    }
    
    func getOutlet(city: String, brand: Int) {
        let cancellable = self.getOutlet(city: city, brand: brand)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error: \(error)")
                case .finished:
                    break
                }

            }) { (detail) in
                self.dataOutlet = detail.data
        }
        cancellables.insert(cancellable)
    }
}
