//
//  OutletCityView.swift
//  Jco
//
//  Created by Ed on 02/07/21.
//

import SwiftUI

struct OutletCityView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    @ObservedObject var cartViewModel: CartViewModel
    @ObservedObject var viewModel = OutletViewModel()
    
    var body: some View {
        VStack {
            Header(title: "Choose Outlet", back: true)
            Group {
                HStack {
                    Image(uiImage: brandSettings.curBrand == 1 ? UIImage(named: "icon_location")! : UIImage(named: "icon_location_jcor")!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20)
                    TextField("Find City", text: $viewModel.search)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                        .background(brandSettings.curBrand == 1 ? Color("theme_background") : Color("theme_background_jcor"))
                        .onChange(of: self.viewModel.search) { newValue in
                            print(newValue)
                            self.viewModel.getCity(search: newValue)
                        }
                }.padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color("border_primary"), lineWidth: 2)
                )
            }.padding()
            
            if viewModel.search.isEmpty {
                Group {
                    ScrollView {
                        ForEach(self.viewModel.outletNearest, id: \.self) { l in
                            Button(action: {
                                self.cartViewModel.chooseOutlet(
                                    brand       : brandSettings.curBrand,
                                    outletId    : l.outletId,
                                    outletName  : l.outletName,
                                    outletCity  : l.outletCity,
                                    outletCode  : l.outletCode,
                                    outletLatitude: l.outletLatitude,
                                    outletLongitude: l.outletLongitude,
                                    outletAddress: l.outletAddress,
                                    outletPostcode: l.outletPostcode,
                                    orderBrand: brandSettings.curBrand == 1 ? "JCO" : "JCOR"
                                )
                                //                            self.presentationMode.wrappedValue.dismiss()
                                self.cartViewModel.setDismiss()
                            }) {
                                VStack(alignment: .leading) {
                                    VStack(alignment: .leading) {
                                        Text(l.outletName)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .padding(.bottom, 3)
                                    }.padding()
                                    //                                .background(Color.red)
                                        .frame(
                                            minWidth: 0,
                                            maxWidth: .infinity,
                                            alignment: .topLeading
                                        )
                                    
                                    Rectangle().frame(height: 1)
                                        .foregroundColor(Color.gray)
                                }.frame(maxWidth: .infinity)
                            }
                        }
                        Spacer()
                    }
                }
            } else {
                Group {
                    ScrollView {
                        ForEach(self.viewModel.detaildata, id: \.self) { l in
                            NavigationLink(destination: OutletView(
                                city: l.city,
                                cartViewModel: cartViewModel
                            )) {
                                VStack(alignment: .leading) {
                                    VStack(alignment: .leading) {
                                        Text(l.city)
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color("theme_text") : Color.white)
                                            .font(.custom("Poppins-Regular", size: 14))
                                            .padding(.bottom, 3)
                                    }.padding()
                                    //                                .background(Color.red)
                                        .frame(
                                            minWidth: 0,
                                            maxWidth: .infinity,
                                            alignment: .topLeading
                                        )
                                    
                                    Rectangle().frame(height: 1)
                                        .foregroundColor(brandSettings.curBrand == 1 ? Color.gray : Color.white)
                                }.frame(maxWidth: .infinity)
                            }.isDetailLink(false)
                        }
                        Spacer()
                    }
                }
                
            }

            Spacer()
        }.navigationBarHidden(true)
            .background(brandSettings.curBrand == 1 ? Color("theme_background").edgesIgnoringSafeArea(.bottom) : Color("theme_background_jcor").edgesIgnoringSafeArea(.bottom))
            .onAppear {
                self.viewModel.getOutletNearest(brand: brandSettings.curBrand)
            }
    }
}

//struct OutletCityView_Previews: PreviewProvider {
//    static var previews: some View {
//        OutletCityView()
//    }
//}
