//
//  HomeView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import URLImage

struct HomeView1: View {
    @State var search: String = ""
    @State var isLoading = true
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                ZStack(alignment: .top) {
                    Image(uiImage: UIImage(named: "home_banner")!)
                        .resizable()
                        .scaledToFit()
                        .frame(width: .infinity)
                    
                    VStack(alignment: .leading) {
                        Text("Good Afternoon")
                            .foregroundColor(Color.white)
                            .font(.system(size: 22, weight: .bold))
                        
                        Spacer().frame(height: 25)
                        
                        Button(action: {
                            
                        }) {
                            HStack {
                                Text("J.Co Reserve")
                                    .font(.system(size: 14))
                                    .padding(.horizontal, 20)
                                    .padding(.vertical, 5)
                                    .foregroundColor(Color("jcor_gold"))
                            }.background(Color.black)
                            .cornerRadius(25)
                        }
                    }.offset(y: -10)
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                    .padding(.horizontal, 20)
                    
                    HStack() {
                        HStack() {
                            HStack {
                                Image(uiImage: UIImage(named: "icon_search")!)
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 25)
                                TextField("Find donuts, drinks or yogurt", text: $search)
                                    .foregroundColor(Color("theme_text"))
                            }.padding()
                            .foregroundColor(Color("c_666666"))
                        }.frame(maxWidth: .infinity, maxHeight: .infinity)
                        .background(Color.white)
                        .cornerRadius(10)
                        .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.2), radius: 5, x: 0, y: 10)
                    }.frame(maxWidth: .infinity, maxHeight: 60)
                    .offset(y:100)
                    .padding()
                }
                Group {
                    VStack(alignment: .leading, spacing: 10) {
                        Text("Order From")
                            .foregroundColor(Color("c_666666"))
//                        NavigationLink(destination: OutletView()) {
//                            Text("J.Co Cihampelas Walk")
//                                .foregroundColor(Color.black)
//                        }
//                        HStack {
//                            Image(uiImage: UIImage(named: "icon_search")!)
//                                .resizable()
//                                .scaledToFit()
//                                .frame(width: 20)
//                            TextField("Find donuts, drinks or yogurt", text: $search)
//                        }.padding()
//                        .overlay(
//                            RoundedRectangle(cornerRadius: 10)
//                                .stroke(Color("border_primary"), lineWidth: 2)
//                        )
                    }.padding(.top, 30)
                    .padding()
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                }
                Group {
//                    MenuListView()
                }
                Spacer()
            }
        }.navigationBarHidden(true)
        .background(Color("c_fff"))
        .ignoresSafeArea()
    }
}

struct HomeView1_Previews: PreviewProvider {
    static var previews: some View {
        HomeView1()
    }
}
