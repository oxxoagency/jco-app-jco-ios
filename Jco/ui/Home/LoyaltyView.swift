//
//  LoyaltyView.swift
//  Jco
//
//  Created by Lingga Kusuma Sakti on 14/01/22.
//

import SwiftUI

struct LoyaltyView: View {
  var body: some View {
      VStack {
          Header(title: "Reward", back: true)
          
          WebView(request: URLRequest(url: URL(string: "\(Constants.API_LOYALTY)loyalty/detail?point_status=1&token=\(UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")&comp_id=JID")!))
          
          Spacer()
      }.navigationBarHidden(true)
      .background(Color("theme_background").edgesIgnoringSafeArea(.bottom))
  }
}
