//
//  MenuListViewModel.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import Foundation
import Combine
import SwiftUI

class MenuListViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    
    @Published var user = ""
    @Published var pass = ""
    @Published var search = ""
    
    @Published var accessToken: String?
    @Published var refreshTokens: String?
    
    @Published var menu = [MenuListItem]()
    @Published var category = [CategoryListItem]()
    @Published var promo = [HomePromo]()
    @Published var appVersion: HomeVersion?
    @Published var jPoint = 0
    @Published var jPointTier = ""
    @Published var categorySelected = ""
    @Published var greetings = ""
    @Published var isLoading = false
    @Published var userFullname = ""
    @Published var favArr: [Int] = []
    @Published var isAlert = false
    @Published var errorMessage = ""
    @Published var password = ""
    @Published var isAlertProfile = false
    @Published var errorMessageProfile = ""
    @Published var titleErrorMessageProfile = ""
    @Published var isDelete = false
    
    var tokenRefresh = UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? ""
    var memberPhone = UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? ""
    
    var cancellables = Set<AnyCancellable>()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func refreshToken(brand: Int) {
        if memberPhone != "" {
            let cancellable = self.getRefreshToken(token: tokenRefresh)
                .sink(receiveCompletion: { result in
                    switch result {
                    case .failure(let error):
                        print("Handle error refresh: \(error)")
                        self.getMenuList(brand: brand)
                        self.isLoading = false
                    case .finished:
                        break
                    }

                }) { (result) in
                    print(result)
                    if result.statusCode == 200 {
                        UserDefaults.standard.set(result.accessToken, forKey: "AccessToken")
                        self.getMenuList(brand: brand)
                        self.getUserProfile()
                    } else {
                        self.getMenuList(brand: brand)
                    }
            }
            cancellables.insert(cancellable)
        } else {
            self.getMenuList(brand: brand)
        }
    }
    
    func login(_ completion: @escaping ((Bool)->Void)) {
        self.isLoading = true
        
        let cancellable = self.loginUser(user: user, pass: pass)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error login: \(error)")
                    self.isLoading = false
                    completion(false)
                case .finished:
                    break
                }
                
            }) { (result) in
                if result.statusCode == 200 {
                    self.accessToken = result.accessToken
                    self.refreshTokens = result.refreshToken
                    UserDefaults.standard.set(self.accessToken, forKey: "AccessToken")
                    UserDefaults.standard.set(self.refreshTokens, forKey: "RefreshToken")
                    UserDefaults.standard.set(self.user, forKey: "MemberHandphone")
                    UserDefaults.standard.set(self.user, forKey: "MemberPhone")
                    UserDefaults.standard.set(true, forKey: "userIsLogged")
                    self.getUserProfile(brand: 1)
                    self.isLoading = false
                    completion(true)
                    
                } else {
                    self.isAlert = true
                    self.isLoading = false
                    self.errorMessage = result.error ?? ""
                    completion(false)
                }
               
//                print(self.accessToken)
//                self.detaildata = detail.data
//                self.variantGroup = detail.data.variantGroup
//                self.arrVariantChoosen2 = self.variantGroup.map{$0.variantGroupName}
//                print(detail.data)
        }
        cancellables.insert(cancellable)
    }
    
    func logout() {
        self.userFullname = ""
    }
    
    func getUserProfile(brand: Int) {
        print("GETUSERPROFILE")
        let cancellable = self.getUserProfile()
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error refresh: \(error)")
                    self.getMenuList(brand: brand)
                case .finished:
                    break
                }

            }) { (result) in
                print(result)
                print("PROFILE SUCCESS")
                if result.statusCode == 200 {
                    UserDefaults.standard.set(result.data.memberEmail, forKey: "MemberEmail")
                    UserDefaults.standard.set(result.data.memberName, forKey: "MemberName")
                    UserDefaults.standard.set(result.data.memberPhone, forKey: "MemberPhone")
                    UserDefaults.standard.set(result.data.memberPhone, forKey: "MemberHandphone")
                    UserDefaults.standard.set(result.data.memberPhone2, forKey: "MemberPhone2")
                    self.userFullname = result.data.memberName
                }
        }
        cancellables.insert(cancellable)
//        self.getMenuList()
    }
    
    func getMenuList(brand: Int) {
        self.isLoading = true
        let cancellable = self.getMenuList(brand: brand)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error menu: \(error)")
                    self.isLoading = false
                    self.errorMessage = "Gagal mengakses menu, sedang terjadi gangguan. Harap Coba lagi"
                    self.isAlert = true
                case .finished:
                    break
                }
                
            }) { (menu) in
                
                if !self.categorySelected.isEmpty {
                    self.setCategory(categorySelectedz: self.categorySelected, brand: brand)
                }
                
                self.menu = menu.products
                self.category = menu.category
                self.promo = menu.promos
                self.userFullname = menu.user.memberName ?? ""
                self.appVersion = menu.version
                self.greetings = self.userFullname
                self.jPoint = menu.jpoint.point ?? 0
                self.jPointTier = menu.jpoint.pointTier?.name ?? ""
                
                // Create favorite array
                menu.products.map{ (m) -> MenuListItem in
                    self.favArr.append(m.isFavorite ?? 0)
                    return m
                }
                
                // Save user information
                UserDefaults.standard.set(menu.user.memberEmail, forKey: "MemberEmail")
                UserDefaults.standard.set(menu.user.memberName, forKey: "MemberName")
                UserDefaults.standard.set(menu.user.memberPhone, forKey: "MemberPhone")
                UserDefaults.standard.set(menu.user.memberPhone, forKey: "MemberHandphone")
                UserDefaults.standard.set(menu.user.memberPhone2, forKey: "MemberPhone2")
                
                self.isLoading = false
                
                //                let location = CLLocation(latitude: -22.963451, longitude: -43.198242)
                //                location.fetchCityAndCountry { city, country, error in
                //                    guard let city = city, let country = country, error == nil else { return }
                //                    print(city + ", " + country)  // Rio de Janeiro, Brazil
                //                }
                
        }
        cancellables.insert(cancellable)
//        self.getGreetings()
    }
    
    func setCategory(categorySelectedz: String, brand: Int) {
        categorySelected = categorySelectedz
        let cancellable = self.getMenuListByCategory(category: categorySelectedz, brand: brand)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error category: \(error)")
                case .finished:
                    break
                }
                
            }) { (menu) in
                self.menu = menu.data
//                self.category = menu.category
        }
        cancellables.insert(cancellable)
    }
    
    func searchProduct(brand: Int) {
        let cancellable = self.getMenuSearch(search: self.search, brand: brand)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error search: \(error)")
                case .finished:
                    break
                }
                
            }) { (menu) in
                self.menu = menu.data
//                self.category = menu.category
        }
        cancellables.insert(cancellable)
    }
    
    func getGreetings() {
        let time = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH"
        let timeHours = timeFormatter.string(from: time)
        let timeHoursInt = Int(timeHours) ?? 0
//        strTime = String(timeHoursInt)
        
        if timeHoursInt > 16 {
            self.greetings = "Good Evening"
        } else if timeHoursInt > 11 {
            self.greetings = "Good Afternoon"
        } else if timeHoursInt > 4 {
            self.greetings = "Good Morning"
        } else {
            self.greetings = "Good Evening"
        }
    }
    
    func productFavorite(menuCode: String, isFavorite: Int, pos: Int) {
        var favStatus = 0
        if isFavorite == 0 {
            favStatus = 1
        }
        
        let cancellable = self.setMenuFavorite(menuCode: menuCode, isFavorite: favStatus).sink(receiveCompletion: { result in
            switch result {
            case .failure(let error):
                print("Handle error fav: \(error)")
            case .finished:
                break
            }
        }){ (result) in
            
        }
        cancellables.insert(cancellable)
        
        self.favArr[pos] = favStatus
        
//        if self.categorySelected != "" {
//            self.getMenuListByCategory(category: self.categorySelected)
//        } else {
//            self.getMenuList()
//        }
    }
    
    func setFavorite(isFavorite: Int) -> Int{
        var favorite = 0
        if isFavorite == 1 {
            favorite = 0
        } else if isFavorite == 0 {
            favorite = 1
        }
        print("\(favorite)")
        return favorite
    }
    
    func getHomeJPoint() {
        let cancellable = self.getJpoint()
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Jpoint: \(error)")
                        self.jPoint = 0
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                self.jPoint = result.data.point
            }
        cancellables.insert(cancellable)
    }
    
    func deleteAccountVM(_ completion: @escaping ((Bool)->Void)) {
        let cancellable = self.deleteAccount(password: password)
            .sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    print("Handle error delete account: \(error)")
                    completion(false)

                case .finished:
                    break
                }
                
            }) { (result) in
                if result.message?.isEmpty ?? false || result.message == nil {
                    self.isAlertProfile = true
                    self.errorMessageProfile = result.error ?? ""
                    self.titleErrorMessageProfile = "Error"
                    completion(false)
                } else {
                    self.isAlertProfile = true
                    self.errorMessageProfile = result.message ?? ""
                    self.titleErrorMessageProfile = "Success"
                    completion(true)
                }
                
               
//                print(self.accessToken)
//                self.detaildata = detail.data
//                self.variantGroup = detail.data.variantGroup
//                self.arrVariantChoosen2 = self.variantGroup.map{$0.variantGroupName}
//                print(detail.data)
        }
        cancellables.insert(cancellable)

    }
}

//extension CLLocation {
//    func fetchCityAndCountry(completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
//        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality, $0?.first?.country, $1) }
//    }
//}
