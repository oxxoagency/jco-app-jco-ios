//
//  MenuListView.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import SwiftUI
import URLImage

struct MenuListView: View {
//    @ObservedObject var viewModel = MenuListViewModel()
      @ObservedObject var viewModel: MenuListViewModel
      @EnvironmentObject var brandSettings: BrandSettings
//    @Binding var tab: Int
    @ObservedObject var hvm: HomeViewModel
//    @ObservedObject var tabData = HomeTabData()
    
    @State private var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    
    var body: some View {
        VStack {
            // Promo
            Group {
                VStack(alignment: .leading) {
                    HStack {
                    Text("Hot Promo")
                        .font(.custom("Poppins-Bold", size: 20))
                        .padding(.horizontal, 10)
                        .padding(.top, 30)
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("c_333333") : Color.white)
                        
                        Spacer()
                        
                        Button(action: {
                            hvm.isActivePromo = true
                        }) {
                            Text("see-all")
                                .font(.custom("Poppins-Regular", size: 14))
                                .padding(.horizontal, 10)
                                .padding(.top, 30)
                                .foregroundColor(Color("btn_primary"))
                        }
                        
                        NavigationLink(destination:
                                        PromoView(hvm: hvm), isActive: $hvm.isActivePromo) {
                            EmptyView()
                        }
                        
                    }
                    .padding(.top, 8)
                    
                    Group {
                        GeometryReader { geometry in
                            ImageCarouselView(numberOfImages: self.viewModel.promo.count) {
                                ForEach(Array(self.viewModel.promo.enumerated()), id: \.1) { index, img in
                                    NavigationLink(destination: PromoDetail(hvm: hvm, bannerID: img.bannerId)) {
                                        URLImage(url: URL(string: img.bannerImg)!,
                                        content: { image in
                                             image
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: geometry.size.width-4, height: geometry.size.height)
                                                .clipped()
                                        }).cornerRadius(15)
                                    }.padding(.horizontal, 2)
                                }
                            }
                        }.frame(height: 120, alignment: .center)
                        .padding(.horizontal, 10)
                    }
                }
            }
            // Promo
            
            // Category
            Group {
                VStack(alignment: .leading) {
                    Text("What are you looking for?")
                        .font(.custom("Poppins-Regular", size: 17))
                        .padding()
                        .foregroundColor(brandSettings.curBrand == 1 ? Color("c_333333") : Color.white)
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                          
                          if brandSettings.curBrand == 1 {
                            ForEach(self.viewModel.category, id: \.self) { cat in
                                Button(action: {
                                  self.viewModel.setCategory(categorySelectedz: cat.categoryName, brand: self.brandSettings.curBrand)
                                }) {
                                    Text(cat.categoryTitle.capitalized)
                                        .font(.custom("Poppins-Medium", size: 15))
                                        .padding(.horizontal, 15)
                                        .padding(.vertical, 8)
                                        .foregroundColor(self.viewModel.categorySelected == cat.categoryName ? Color.white : Color.black)
                                }.overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(self.viewModel.categorySelected == cat.categoryName ? Color("btn_primary") : Color("c_666666"))
                                ).background(self.viewModel.categorySelected == cat.categoryName ? Color("btn_primary") : Color.white)
                                .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                            }

                          } else {
                            ForEach(self.viewModel.category, id: \.self) { cat in
                                Button(action: {
                                  self.viewModel.setCategory(categorySelectedz: cat.categoryName, brand: self.brandSettings.curBrand)
                                }) {
                                    Text(cat.categoryTitle.capitalized)
                                        .font(.custom("Poppins-Medium", size: 15))
                                        .padding(.horizontal, 15)
                                        .padding(.vertical, 8)
                                        .foregroundColor(self.viewModel.categorySelected == cat.categoryName ? Color.white : Color.black)
                                }.overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(self.viewModel.categorySelected == cat.categoryName ? Color("jcor_gold") : Color("c_666666"))
                                ).background(self.viewModel.categorySelected == cat.categoryName ? Color("jcor_gold") : Color.white)
                                .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                            }

                          }
                          
                        }.padding(.horizontal, 10)
                    }
                }
            }.padding(.bottom, 10)
            // Category
            
            GridStack(rows: self.viewModel.menu.count, columns: 2) { row, col in
                if self.viewModel.menu.indices.contains(row * 2 + col) {
//                    NavigationLink(destination: ProductDetailView(menuCode: self.viewModel.menu[row * 2 + col].menuCode, menuId: Util().md5Hash(str: self.viewModel.menu[row * 2 + col].menuCode), tab: $tab)) {

                    NavigationLink(destination: MenuDetailView(hvmm: hvm, menuCode: self.viewModel.menu[row * 2 + col].menuCode, isFromPromo: false
                    )) {
                        VStack(alignment: .leading) {
                            VStack(alignment: .leading) {
    //                                Text(self.cards[row * 2 + col])
//                                ZStack {
                                    
//                                        .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.1), radius: 5, x: 0, y: 0)
                                    
//                                    Image(systemName: "heart.fill")
//                                        .foregroundColor(Color(red: 0, green: 0, blue: 0, opacity: 0.4))
//                                        .font(.system(size: 30))
//                                        .padding(.leading, 15)
//                                        .offset(x: -5, y: 5)
//                                }
                                
                                ZStack(alignment: .topTrailing) {
                                    if self.viewModel.menu[row * 2 + col].isPromo == 1 {
                                    Text("Promo")
                                        .foregroundColor(Color.white)
                                        .font(.custom("Poppins-Medium", size: 12))
                                        .padding(.vertical, 4)
                                        .padding(.horizontal, 16)
                                        .lineLimit(1)
                                        .background(Color("red_promo"))
                                        .cornerRadius(16, corners:[.bottomRight, .bottomLeft, .topRight])
                                        .rotationEffect(Angle(degrees: 90))
                                        .offset(x:-5, y:16)
                                        .zIndex(5)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .padding(.horizontal, -18)
                                        .padding(.vertical, 4)
                                    }
                                    
                                    Button(action: {
                                        self.viewModel.productFavorite(menuCode: self.viewModel.menu[row * 2 + col].menuCode,
                                            isFavorite: self.viewModel.favArr[row * 2 + col],
//                                            isFavorite: self.viewModel.setFavorite(isFavorite: self.viewModel.menu[row * 2 + col].isFavorite ?? 0),
                                            pos: row * 2 + col
                                        )
                                    }) {
                                        
                                        if  self.viewModel.favArr[row * 2 + col] == 1 {
                                            Image(systemName: "heart.fill")
                                                .foregroundColor(Color(.red))
                                                .font(.system(size: 30))
                                        } else {
                                            Image(systemName: "heart")
                                                .foregroundColor(Color(red: 0, green: 0, blue: 0, opacity: 0.4))
                                                .font(.system(size: 30))
                                        }
                                        
                                       
                                    }.offset(x:-5, y:7)
                                    .zIndex(5)
                                    
                                    VStack(alignment: .leading) {
                                        URLImage(url: URL(string: self.viewModel.menu[row * 2 + col].menuImage)!,
                                            content: { image in
                                                image
                                                 .resizable()
                                                 .aspectRatio(contentMode: .fit)
                                                    .clipped()
                                                    .background(Color.white)
    //                                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.1), radius: 5, x: 0, y: 0)
                                        }).background(Color.white)
                                        
                                        if self.viewModel.menu[row * 2 + col].isFreedelivery == 1 {
                                            
                                            HStack {
                                                Image(uiImage: UIImage(named: "icon_free_delivery")!)
                                                    .padding(.leading, 4)
                                                
                                                Text("Free Delivery")
                                                    .foregroundColor(Color.white)
                                                    .font(.custom("Poppins-MediumItalic", size: 12))
                                                    .padding(.vertical, 4)
                                                    .padding(.trailing, 8)
                                                    .lineLimit(1)
                                            }.background(Color("green_free_delivery"))
                                                .cornerRadius(16, corners:[.bottomRight, .topRight])
                                                .padding(.top, -8)
                                        }
                                        
                                        VStack(alignment: .leading) {
                                            Text("\(self.viewModel.menu[row * 2 + col].menuName)")
                                            .foregroundColor(brandSettings.curBrand == 1 ? Color.black : Color.white)
        //                                        .fontWeight(.bold)
        //                                        .font(.system(size: 12))
                                                .padding(.horizontal, 5)
                                                .font(.custom("Poppins-Regular", size: 13))
                                                .multilineTextAlignment(.leading)
                //                            Text(Util().md5Hash(str: self.viewModel.menu[row * 2 + col].menuCode))
                                            
                                            HStack(alignment: .top) {
                                                if self.self.viewModel.menu[row * 2 + col].menuNormalprice > 0 {
                                                    Text("\(Util().addTS(str: String(self.viewModel.menu[row * 2 + col].menuNormalprice)))")
                                                        .strikethrough()
                                                        .font(.custom("Poppins-Regular", size: 12))
                                                        .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
                //                                        .font(.system(size: 12))
                                                        .padding(.horizontal, 5)
                                                        .lineLimit(1)
                                                    
                                                    Spacer()
                                                }
                                                
                                            }.padding(.trailing, 6)
                                            
                                            HStack(alignment: .top) {
                                                Text("\(Util().addTS(str: String(self.viewModel.menu[row * 2 + col].menuPrice)))")
                                                    .font(.custom("Poppins-Regular", size: 14))
                                                    .foregroundColor(brandSettings.curBrand == 1 ? Color("btn_primary") : Color("jcor_gold"))
            //                                        .font(.system(size: 12))
                                                    .padding(.horizontal, 5)
                                                    .padding(.bottom, 5)
                                                    .lineLimit(1)
                                                
                                                Spacer()
                                                
                                            }.padding(.trailing, 6)
                                        }.frame(minHeight: 70, alignment: .topLeading)
                                    }.frame(maxWidth: .infinity, alignment: .topLeading)
                                    .background(brandSettings.curBrand == 1 ? Color.white : Color("theme_background_jcor_80"))
                                    .cornerRadius(10)
                                    }
                            }.frame(maxWidth: .infinity)
                            .cornerRadius(10)
                        }.cornerRadius(10)
                        .background(Color.white)
                        .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                        .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.15), radius: 5, x: 0, y: 0)
                    }.padding(.bottom, 5)
                    .padding(.horizontal, 5)
                } else {
                    VStack {
                        EmptyView()
                    }.frame(maxWidth: .infinity)
                    .background(Color.blue)
                }
            }
        }
        .alert(isPresented: $viewModel.isAlert) {
            Alert(title: Text("Error"), message: Text(viewModel.errorMessage), dismissButton: .default(Text("Okay")))
        }
//        .background(self.hvm.currentBrand == 1 ? Color.white : Color.black)
//        .onAppear {
//            if userLogged {
////                self.viewModel.refreshToken()
//                self.viewModel.getUserProfile()
//            }
//        }
//            } else {
//                self.viewModel.getMenuList()
//            }
//        }
    }
}

struct GridStack<Content: View>: View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content

    var body: some View {
        VStack {
            ForEach(0 ..< rows, id: \.self) { row in
                HStack(alignment: .center, spacing: 0) {
                    ForEach(0 ..< self.columns, id: \.self) { column in
                        self.content(row, column)
                    }
                }
            }
        }
    }

    init(rows: Int, columns: Int, @ViewBuilder content: @escaping (Int, Int) -> Content) {
        self.rows = rows
        self.columns = columns
        self.content = content
    }
}

//struct MenuListView_Previews: PreviewProvider {
//    static var previews: some View {
//        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
//    }
//}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

