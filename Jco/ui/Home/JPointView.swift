//
//  JPointView.swift
//  Jco
//
//  Created by Ed on 08/12/21.
//

import SwiftUI

struct JPointView: View {
    @ObservedObject var vm = JPointViewModel()
    
    @State private var tabState = 1
    
    var body: some View {
        VStack {
            Header(title: "JPoint History", back: true)
            
            // Tab
            HStack {
                Button(action: {
                    withAnimation {
                        self.tabState = 1
                    }
                }) {
                    VStack {
                        Text("JPoint Received")
                            .font(.custom("Poppins-Medium", size: 15))
                            .foregroundColor(self.tabState == 1 ? Color("btn_primary") : Color("c_666666"))
                            .padding(.top, 10)
                            .padding(.bottom, 5)
                            .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 0)
                        if self.tabState == 1 {
                            Rectangle().frame(height: 2.5).foregroundColor(Color("btn_primary"))
                        }
                    }.frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.horizontal, 3)
                }

                Button(action: {
                    withAnimation {
                        self.tabState = 2
                    }
                }) {
                    VStack {
                        Text("JPoint Used")
                            .font(.custom("Poppins-Medium", size: 15))
                            .foregroundColor(self.tabState == 2 ? Color("btn_primary") : Color("c_666666"))
                            .padding(.top, 10)
                            .padding(.bottom, 5)
                            .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0), radius: 5, x: 0, y: 6)
                        if self.tabState == 2 {
                            Rectangle().frame(height: 2.5).foregroundColor(Color("btn_primary"))
                        }
                    }.frame(minWidth: 0, maxWidth: .infinity)
                    .padding(.horizontal, 3)
                }

            }
            .background(Color("theme_background"))
            .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.2), radius: 5, x: 0, y: 5)
            // Tab
            
            // Content
            ScrollView {
                if self.tabState == 1 {
                    // Received
                    if self.vm.dataEarn.count > 0 {
                        Group {
                            VStack(alignment: .leading) {
                                ForEach(self.vm.dataEarn, id: \.self) { d in
                                    VStack(alignment: .leading) {
                                        Text(d.createdAt)
                                            .font(.custom("Poppins-Regular", size: 14))
                                            .foregroundColor(Color.gray)
                                        
                                        Text(d.description)
                                            .font(.custom("Poppins-Regular", size: 14))
                                        
                                        Text("+" + Util().addTS(str: String(d.pointEarn)))
                                            .font(.custom("Poppins-Medium", size: 16))
                                            .foregroundColor(Color("btn_primary"))
                                        
                                    }.padding()
                                    
                                    Rectangle().frame(height: 1).foregroundColor(Color.gray)
                                }
                            }.frame(
                                maxWidth: .infinity,
                                alignment: .leading
                            ).padding()
                        }
                    } else {
                        Text("No Data")
                            .font(.custom("Poppins-Medium", size: 16))
                            .padding()
                    }
                } else {
                    // Used
                    if self.vm.dataUsed.count > 0 {
                        Group {
                            VStack {
                                
                            }
                        }
                    } else {
                        Text("No Data")
                            .font(.custom("Poppins-Medium", size: 16))
                            .padding()
                    }
                }
            }
            // Content
        }.onAppear() {
            self.vm.getData()
        }
    }
}

struct JPointView_Previews: PreviewProvider {
    static var previews: some View {
        JPointView()
    }
}
