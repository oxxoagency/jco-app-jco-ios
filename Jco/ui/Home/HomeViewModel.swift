//
//  HomeViewModel.swift
//  Jco
//
//  Created by Ed on 10/05/21.
//

import Foundation

class HomeViewModel: ObservableObject {
    @Published var homeTabSelected = 0
//    @Published var homeTabSelected = UserDefaults.standard.object(forKey: "HomeTabSelected") as? Int ?? 0
    @Published var greetings = ""
    @Published var currentBrand = 1
    @Published var totalQtyCart = UserDefaults.standard.object(forKey: "CartQtyCount") as? Int ?? 1
    @Published var appLanguage = UserDefaults.standard.object(forKey: "AppLanguage") as? String ?? "en"
    @Published var isForgotPasswordActive = false
    @Published var isActivePromo = false
    
    func viewCart() {
        self.homeTabSelected = 2
    }
    
    func viewLogin() {
        self.homeTabSelected = 4
    }
    
    func tabHome() {
        self.homeTabSelected = 0
    }
    
    func changeBrand(brand: Int) {
        self.currentBrand = brand
    }
  
    func getGreetings() {
        let time = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH"
        let timeHours = timeFormatter.string(from: time)
        let timeHoursInt = Int(timeHours) ?? 0
//        strTime = String(timeHoursInt)
        
        if timeHoursInt > 16 {
            self.greetings = "Good Evening"
        } else if timeHoursInt > 11 {
            self.greetings = "Good Afternoon"
        } else if timeHoursInt > 4 {
            self.greetings = "Good Morning"
        } else {
            self.greetings = "Good Evening"
        }
    }
}
