//
//  JPointViewModel.swift
//  Jco
//
//  Created by Ed on 08/12/21.
//

import Foundation
import Combine

class JPointViewModel: ObservableObject, ModelService {
    var apiSession: APIService
    var cancellables = Set<AnyCancellable>()
    
    @Published var dataEarn = [JpointMutationResData]()
    @Published var dataUsed = [JpointMutationResData]()
    
    init(apiSession: APIService = APISession()) {
        self.apiSession = apiSession
    }
    
    func getData() {
        let cancellable = self.getJpointMutation(
            status: 1,
            page: 1
        )
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Jpoint Mutation Earn: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                if result.statusCode == 200 {
                    self.dataEarn = result.data
                }
            }
        cancellables.insert(cancellable)
        
        let cancellableUsed = self.getJpointMutation(
            status: 2,
            page: 1
        )
            .sink(receiveCompletion: { result in
                switch result {
                    case .failure(let error):
                        print("Handle Error Jpoint Mutation Used: \(error)")
                    case .finished:
                        break
                }
            }) { (result) in
                print(result)
                if result.statusCode == 200 {
                    self.dataUsed = result.data
                }
            }
        cancellables.insert(cancellableUsed)
        
        print("TOKEN: " + String(UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? ""))
    }
}
