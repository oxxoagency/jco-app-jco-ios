//
//  HomeView.swift
//  Jco
//
//  Created by Ed on 06/05/21.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject var brandSettings: BrandSettings
    let appearance: UITabBarAppearance = UITabBarAppearance()
    
    @State var search: String = ""
    @State var isLoading = true
//    @Binding var strTime: String
//    @Binding var tab: Int
    @ObservedObject var hvm: HomeViewModel
//    @ObservedObject var tabData = HomeTabData()
    
//    @ObservedObject var vm = MenuListViewModel()
    @StateObject var vm: MenuListViewModel
    @StateObject var locationManager = LocationManager()
    
    @State private var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
    
    var body: some View {
        ZStack {
            VStack {
                ScrollView {
                    VStack(alignment: .leading) {
                        ZStack(alignment: .top) {
                            Image(uiImage: UIImage(named: brandSettings.curBrand == 1 ? "home_banner" : "home_banner_jcor")!)
                                .resizable()
                                .scaledToFit()
                                .frame(maxWidth: .infinity)
                            
                            // Banner
                            VStack(alignment: .leading) {
                              HStack(alignment: .center) {
                                Spacer()
                                
                                HStack {
                                  Button(action: {
                                      self.hvm.changeBrand(brand: 1)
                                      self.brandSettings.curBrand = 1
                                      self.vm.getMenuList(brand: 1)
                                      UserDefaults.standard.set(1, forKey: "CurrentBrand")
                                    
                                  }) {
                                    Image(uiImage: UIImage(named: brandSettings.curBrand == 1 ? "switch_jco_enable" : "switch_jco_disable")!)
                                      .resizable()
                                      .scaledToFit()
                                      .frame(width: 80)
                                      .background(brandSettings.curBrand == 1 ? Color.white : Color("jcor_gold_dark"))
                                      .clipShape(RoundedRectangle(cornerRadius: 16))
                                      
                                  }

                                  Button(action: {
                                      self.hvm.changeBrand(brand: 2)
                                      self.brandSettings.curBrand = 2
                                      self.vm.getMenuList(brand: 2)
                                      UserDefaults.standard.set(2, forKey: "CurrentBrand")
                                    

                                  }) {
                                    Image(uiImage: UIImage(named: brandSettings.curBrand == 1 ? "switch_jco_r_disable" : "switch_jco_r_enable")!)
                                      .resizable()
                                      .scaledToFit()
                                      .frame(width: 80)
                                      .padding(.trailing, 8)

                                  }
                                } .background(brandSettings.curBrand == 1 ? Color("dark_orange") : Color.white)
                                  .clipShape(RoundedRectangle(cornerRadius: 16))
                                
                                Spacer()
                                
                                }.zIndex(3)
                               
                                HStack {
                                    // Greeting
                                    VStack(alignment: .leading) {
                                        HStack {
        //                                    Image(uiImage: UIImage(named: "logo_jco_symbol")!)
        //                                        .resizable()
        //                                        .scaledToFit()
        //                                        .frame(width: 35)
                                            Text("Hello")
                                                .foregroundColor(Color.white)
                                                .font(.custom("Poppins-Bold", size: 16))
                                        }
                                        .padding(.top, 5)
                                        
                                        Text(self.vm.userFullname)
                                            .foregroundColor(Color.white)
                                            .font(.custom("Poppins-Bold", size: 24))
                                        
                                        Spacer().frame(height: 25)
                                    }
                                    // Greeting
                                    
                                    Spacer()
                                    
                                    // JPoint
                                    VStack(alignment: .trailing) {
                                        Text("JPoint")
                                            .foregroundColor(Color.white)
                                            .font(.custom("Poppins-Regular", size: 16))
                                        
                                        NavigationLink(destination: {
                                            LoyaltyView()
                                        }()) {
                                            VStack(alignment: .trailing) {
                                                Text(String(self.vm.jPoint))
                                                    .foregroundColor(Color.white)
                                                    .font(.custom("Poppins-Bold", size: 20))
                                                
                                                Text("\(self.vm.jPointTier) >")
                                                    .font(.custom("Poppins-Regular", size: 18))
                                                    .foregroundColor(Color.white)
                                            }
                                        }
                                    }
                                }
                            }.offset(y: 50)
                            .frame(maxWidth: .infinity, alignment: .topLeading)
                            .padding(.horizontal, 20)
                            // Banner
                            
                            HStack() {
                                HStack() {
                                    HStack {
                                        Image(uiImage: UIImage(named: "icon_search")!)
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 25)
                                        
                                        ZStack(alignment: .leading) {
                                            if self.vm.search.isEmpty {
                                                Text("find-donuts")
                                                    .foregroundColor(Color.gray)
                                            }
                                        
                                            TextField("", text: $vm.search)
                                                .foregroundColor(Color("theme_text"))
                                                .onChange(of: self.vm.search) { newValue in
                                                    self.vm.searchProduct(brand: self.brandSettings.curBrand)
//                                                    self.viewModel.setIsLoadingResult(status: true)
                                                }
                                        }.padding(.horizontal, 10)
                                    }.padding()
                                    .foregroundColor(Color("c_666666"))
                                }.frame(maxWidth: .infinity, maxHeight: .infinity)
                                .background(Color.white)
                                .cornerRadius(10)
                                .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.2), radius: 5, x: 0, y: 10)
                                .zIndex(3)
                            }.frame(maxWidth: .infinity, maxHeight: 30)
                            .offset(y:180)
                            .padding()
                            .zIndex(10)
                        }
                        
                        // Outlet
//                        Group {
//                            VStack(alignment: .leading, spacing: 10) {
//                                Text("Order From")
//                                    .foregroundColor(Color("c_666666"))
//                                NavigationLink(destination: OutletView()) {
//                                    Text("J.Co Cihampelas Walk")
//                                        .foregroundColor(Color("btn_primary"))
//                                }
//                            }.padding(.top, 30)
//                            .padding()
//                            .frame(maxWidth: .infinity, alignment: .topLeading)
//                        }
                        // Outlet
                        
                        // Menu
                        Group {
                            MenuListView(viewModel: vm, hvm: hvm)
                        }
                        // Menu
                        Spacer()
                    }
                }.edgesIgnoringSafeArea(.top)
            }.background(brandSettings.curBrand == 1 ? Color.white.edgesIgnoringSafeArea(.top) : Color("theme_background_jcor").edgesIgnoringSafeArea(.top))
//            .edgesIgnoringSafeArea(.top)
            
            Group {
                Loader(isLoading: $vm.isLoading).zIndex(2)
            }
//            Loader(isLoading:s $isLoading).zIndex(2)
//            Loader3().zIndex(2)
            
        }.onAppear() {
            self.vm.getHomeJPoint()
            self.vm.getMenuList(brand: self.brandSettings.curBrand)
        }.navigationBarHidden(true)
        .background(Color("c_fff"))
    }
}

//struct HomeView_Previews: PreviewProvider {
//    static var previews: some View {
//        HomeView()
//    }
//}
