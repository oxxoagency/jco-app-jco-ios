//
//  ContentView.swift
//  Jco3
//
//  Created by Ed on 17/04/21.
//

import SwiftUI
import CoreData
import URLImage

struct ContentView: View {
    @ObservedObject var user = User()
    @Environment(\.managedObjectContext) private var viewContext
    @EnvironmentObject var brandSettings: BrandSettings
//    @State private var tabSelected: Int = 0
    @StateObject var viewModel = HomeViewModel()
//    @ObservedObject var tabData = HomeTabData()
    @StateObject var vmMenu = MenuListViewModel()
    @EnvironmentObject var cartViewModel: CartViewModel
    @State private var userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false

//    @FetchRequest(
//        sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
//        animation: .default)
//    private var items: FetchedResults<Item>

//    @available(iOS 15.0, *)
//    @NSCopying open var scrollEdgeAppearance: UITabBarAppearance?
    let appearance: UITabBarAppearance = UITabBarAppearance()
    let appearanceNav = UINavigationBarAppearance()
    init() {
        if #available(iOS 15.0, *) {
            appearanceNav.configureWithOpaqueBackground()
            
            if self.viewModel.currentBrand == 1 {
                appearanceNav.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0)
//                appearance.backgroundColor = .white
//                UITabBar.appearance().standardAppearance = appearance
//                UITabBar.appearance().scrollEdgeAppearance = appearance
            } else {
                appearanceNav.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
//                appearance.backgroundColor = .black
//                UITabBar.appearance().standardAppearance = appearance
//                UITabBar.appearance().scrollEdgeAppearance = appearance
            }
            appearanceNav.shadowColor = .clear
            
            
            UINavigationBar.appearance().scrollEdgeAppearance = appearanceNav
            UINavigationBar.appearance().standardAppearance = appearanceNav
            UINavigationBar.appearance().compactAppearance = appearanceNav
            
        
//            UINavigationBar.appearance().isTranslucent = true
//            appearance.backgroundColor = <your tint color>
//            UINavigationBar.standardAppearance = appearance;
//            UINavigationBar.scrollEdgeAppearance = navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
            UITabBar.appearance().barTintColor = UIColor.white
        }
    }

    var body: some View {
        ZStack(alignment: .bottomLeading) {
            NavigationView {
                GeometryReader { geometry in
                    ZStack(alignment: .bottomLeading) {
                        if (user.tokenIsActive) {
                            TabView(selection: self.$viewModel.homeTabSelected) {
                                
                                HomeView(hvm: viewModel, vm: vmMenu)
                                .tabItem {
                                    VStack {
                                        Image(uiImage: UIImage(named: "icon_home")!)
                                        Text("tab-home")
                                    }
                                }.tag(0)
                                
                                
                                OrderView(back: false, hvm: viewModel)
                                .tabItem {
                                    VStack {
                                        Image(uiImage: UIImage(named: "icon_recent_order")!)
                                        Text("tab-order")
                                    }
                                }.tag(1)
                                
                                
                                CartView(hvm: viewModel)
                                .tabItem {
                                    VStack {
                                        Image(uiImage: UIImage(named: "icon_cart")!)
                                        Text("tab-cart")
                                    }
                                }.tag(2)
                                
                                FavoriteView(hvm: viewModel)
                                .tabItem {
                                    VStack {
                                        Image(uiImage: UIImage(named: "icon_love")!)
                                        Text("tab-favorite")
                                    }
                                }.tag(3)
                                
                                
                                ProfileView(user: self.user, vm: self.vmMenu, hvm: viewModel)
                                .tabItem {
                                    VStack {
                                        Image(uiImage: UIImage(named: "icon_profile")!)
                                        Text("User")
                                    }
                                }.tag(4)
                            }.accentColor(Color("btn_primary"))
                                .navigationBarHidden(true)
                                .background(Color.white.edgesIgnoringSafeArea(.top))
                            
                        } else {
                            TabView(selection: self.$viewModel.homeTabSelected) {
                                
                                HomeView(hvm: viewModel, vm: vmMenu)
                                .tabItem {
                                        VStack {
                                            Image(uiImage: UIImage(named: "icon_home")!)
                                            Text("tab-home")
                                        }
                                }.tag(0)
                                
                                
                                OrderView(back: false, hvm: viewModel)
                                .tabItem {
                                    VStack {
                                        Image(uiImage: UIImage(named: "icon_recent_order")!)
                                        Text("tab-order")
                                    }
                                }.tag(1)
                                
                                
                                CartView(hvm: viewModel)
                                .tabItem {
                                        VStack {
                                            Image(uiImage: UIImage(named: "icon_cart")!)
                                            Text("tab-cart")
                                        }
                                }.tag(2)
                                
                                
                                FavoriteView(hvm: viewModel)
                                .tabItem {
                                        VStack {
                                            Image(uiImage: UIImage(named: "icon_love")!)
                                            Text("tab-favorite")
                                        }
                                }.tag(3)

                                LoginView(user: self.user, homeViewModel: self.viewModel, vm: vmMenu)
                                .tabItem {
                                        VStack {
                                            Image(uiImage: UIImage(named: "icon_profile")!)
                                            Text("Login")
                                        }
                                }.tag(4)

                            }.accentColor(Color("btn_primary"))
                                .navigationBarHidden(true)
                                .background(Color.white.edgesIgnoringSafeArea(.top))
                        }
                        
                        // Badge View
                        if (user.tokenIsActive) {
                            ZStack {
                              Circle()
                                .foregroundColor(.red)

                                Text(String(cartViewModel.totalQtyCart(brand: viewModel.currentBrand)))
                                .foregroundColor(.white)
                                .font(Font.system(size: 12))
                            }
                            .frame(width: 15, height: 15)
                            .offset(x: ( ( 2 * 3) - 0.95 ) * ( geometry.size.width / ( 2 * 5 ) ) + 2, y: -25)
    //                        .opacity(self.viewModel.totalQtyCart == 0 ? 0 : 1.0)
                            .opacity(1)
                            .zIndex(5)
                        }
                    }
                }.ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .navigationBarHidden(true)
//            .navigationBarTitle("", displayMode: .inline)
//            .edgesIgnoringSafeArea([.top, .bottom])

            // Update
            if self.vmMenu.appVersion?.versionBuildLatest ?? 1 > Int(Constants.APP_BUILD ?? "") ?? 0 {
                if self.vmMenu.appVersion?.mustUpdate ?? false {
                    Group {
                    VStack() {
                        Spacer()
                        HStack(alignment: .top) {
                            Group {
                                VStack(alignment: .leading) {
                                    URLImage(url: URL(string: self.vmMenu.appVersion?.updateImg ?? "")!,
                                    content: { image in
                                         image
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .clipped()
                                    })
                                    
                                    Text(self.vmMenu.appVersion?.updateText ?? "Please Update App")
                                        .font(.custom("Poppins-Medium", size: 14))
                                        .foregroundColor(Color("theme_text"))
                                        .padding()
                                    
                                    Button(action: {
                                        
                                    }) {
                                        HStack {
                                            Spacer()
                                            Text("Update App")
                                            Spacer()
                                        }
                                    }.buttonStyle(PrimaryButtonStyle())
                                    .padding()
                                }.frame(width: 300, height: 375)
                            }.background(Color("theme_background"))
                            .cornerRadius(15)
                            .frame(width: 500, height: 500)
                            .shadow(radius: 10)
                            Spacer()
                        }
                        Spacer()
                    }.background(Color(red: 0, green: 0, blue: 0, opacity: 0.1))
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           minHeight: 0,
                           maxHeight: .infinity)
                    .offset(x:0, y:0)
                    .zIndex(5)
                    .ignoresSafeArea()
                    }
                }
            } else {
                EmptyView()
            }
            
        }
//        .navigationBarTitle("") //this must be empty
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
//        .navigationBarTitleDisplayMode(.inline)
        .onAppear() {
//            self.checkLoginValidity()
//            self.vmMenu.getMenuList()
            self.checkLoginValidity()
            self.vmMenu.refreshToken(brand: self.brandSettings.curBrand)
        }
        .onDisappear {
            print("ContentView disappeared!")
        }
}
    
    func checkLoginValidity() {

        let userLogged = UserDefaults.standard.object(forKey: "userIsLogged") as? Bool ?? false
        let accessToken = UserDefaults.standard.object(forKey: "AccessToken") as? String ?? "notoken"
        print(accessToken)

        if (userLogged) {
            self.user.tokenIsActive = true
//            let existinLogin = UserDefaults.standard.object(forKey: "loginExpiry") as! Date

//            if (existinLogin > Date().addingTimeInterval(86400 * 2)){
//                self.user.tokenIsActive = true
//            } else {
//                self.user.tokenIsActive = false
//            }
        } else {
            self.user.tokenIsActive = false
        }
    }

//    private func addItem() {
//        withAnimation {
//            let newItem = Item(context: viewContext)
//            newItem.timestamp = Date()
//
//            do {
//                try viewContext.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nsError = error as NSError
//                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
//            }
//        }
//    }
//
//    private func deleteItems(offsets: IndexSet) {
//        withAnimation {
//            offsets.map { items[$0] }.forEach(viewContext.delete)
//
//            do {
//                try viewContext.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nsError = error as NSError
//                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
//            }
//        }
//    }
    
//
//    lazy var persistentContainer: NSPersistentContainer = {
//      let container = NSPersistentContainer(name: "FaveFlicks")
//      container.loadPersistentStores { _, error in
//        if let error = error as NSError? {
//          fatalError("Unresolved Error \(error), \(error.userInfo)")
//        }
//      }
//      return container
//    }()
//
//    mutating func saveContext() {
//      let context = persistentContainer.viewContext
//      if context.hasChanges {
//        do {
//          try context.save()
//        } catch {
//          let nserror = error as NSError
//          fatalError("Error \(nserror), \(nserror.userInfo)")
//        }
//      }
//    }
}

//class HomeTabData: ObservableObject {
//    @Published var tabIndex = 0
//}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
