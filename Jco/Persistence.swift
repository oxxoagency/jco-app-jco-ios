//
//  Persistence.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import CoreData

struct PersistenceController {
    let container: NSPersistentContainer
    static let shared = PersistenceController()
    
    var viewContext: NSManagedObjectContext {
        return container.viewContext
    }

    static var preview: PersistenceController = {
        let result = PersistenceController(inMemory: true)
        let viewContext = result.container.viewContext
        for _ in 0..<10 {
            let newItem = Item(context: viewContext)
            newItem.timestamp = Date()
        }
        for _ in 0..<10 {
            let newCart = Cart(context: viewContext)
            newCart.menuName = "name"
            newCart.menuImg = "img"
            newCart.menuCode = "code"
        }
        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return result
    }()

    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "Jco")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    func getAllCarts() -> [Cart] {
        let request: NSFetchRequest<Cart> = Cart.fetchRequest()
        do {
            return try viewContext.fetch(request)
        } catch {
            return []
        }
    }
    
    func getAllCartsJcoR() -> [CartJcoR] {
        let request: NSFetchRequest<CartJcoR> = CartJcoR.fetchRequest()
        do {
            return try viewContext.fetch(request)
        } catch {
            return []
        }
    }
    
    func getCartById(id: NSManagedObjectID) -> Cart? {
        do {
            return try viewContext.existingObject(with: id) as? Cart
        } catch {
            return nil
        }
    }
    
    func save() {
        do {
            try viewContext.save()
        } catch {
            viewContext.rollback()
            print(error.localizedDescription)
        }
    }
    
    func deleteCart(cart: Cart) {
        viewContext.delete(cart)
        save()
    }
    
    func updateCart(cart: Cart, action: String) {
        viewContext.performAndWait {
            if action == "add" {
                cart.menuQty += 1
            } else {
                if cart.menuQty > 0 {
                    cart.menuQty -= 1
                }
            }
            save()
        }
    }
    
//    func save() {
//        let context = container.viewContext
//
//        if context.hasChanges {
//            print("CHANGE")
//            do {
//                try context.save()
//            } catch {
//                // Show some error here
//                print("ERROR")
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        } else {
//            print("NOZ")
//        }
//    }
}
