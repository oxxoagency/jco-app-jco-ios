//
//  MenuEndpoint.swift
//  Jco
//
//  Created by Ed on 09/01/21.
//

import Foundation


enum MenuEndpoint {
    case menuList
}

extension MenuEndpoint: RequestBuilder {
    var urlRequest: URLRequest {
        switch self {
        case .menuList:
            guard let url = URL(string: "https://jcouserapidev.oxxo.co.id/home/app")
                else {preconditionFailure("Invalid URL format")}
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let body: [String: Any] = ["member_phone": "0900010000", "city": "Jakarta Barat", "latitude": "-6.20073720", "longitude": "106.74090380", "brand": 1]
            let rb = try! JSONSerialization.data(withJSONObject: body)
            request.httpBody = rb
            
            return request
        }
        
    }
}
