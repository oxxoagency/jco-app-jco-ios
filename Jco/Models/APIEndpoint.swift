//
//  APIEndpoint.swift
//  Jco
//
//  Created by Ed on 23/01/21.
//

import Foundation
import WebKit

enum APIEndpoint {
    case menuList(brand: Int)
    case menuListByCategory(category: String, brand: Int)
    case menuSearch(search: String, brand: Int)
    case menuFavorite(menuCode: String, isFavorite: Int)
    case menuListFavorite(brand: Int)
    case menuDetail(menuCode: String, brand: Int)
    case bannerDetail(bannerID: String, brand: Int)
    case promo(brand: Int)
    
    case userTokenRefresh(token: String)
    case userLogin(user: String, pass: String)
    case userCheckPhone(hp: String)
    case userRegister(name: String, pass: String, repass: String, hp: String, email: String)
    case userProfile
    case userJpoint
    case userJpointMutation(status: Int, page: Int)
    case userUpdateProfile(phone: String, name: String, gender: Int)
    case userChangePassword(pass: String, newpass: String, confpass: String)
    case userResetPassword(user: String, newpass: String, confpass: String)
    
    case userAddressList(hp: String)
    case userAddressNew(memberHandphone: String, addressLabel: String, recipientName: String, recipientPhone: String, recipientPostcode: String, recipientCity: String, recipientAddress: String, recipientAddressDetail: String, latitude: String, longitude: String)
    case userAddressDelete(addressID: Int)
    case userLocationQuery(location: String)
    case citySearch(search: String)
    case getUserCity(lat: String, lng: String)
    case outletSearch(city: String, brand: Int)
    case outletNearest(brand: Int,lat: String, lng: String)
    
    case cartFilter(brand: Int, city: String, deliveryType: Int, cartOrderDetail: [[String: Any]], useJpoint: Int, orderAddress: String, orderAddressInfo: String, orderLatitude: String, orderLongitude: String, couponCode: String, orderBrand: String)
    case cartAddOrder(brand: Int, name: String, email: String, phone: String, trxTime: String, city: String, address: String, detailAddress: String, postcode: String, latitude: String, longitude: String, notes: String, outletID: String, outletCode: String, orderDetail: [[String: Any]], deliveryType: Int, orderPayment: String, jpoint: Int, couponCode: String)
    case cartCheckOut(orderId: String)
    case cartPayment(orderId: String)
    case cartPaymentList
    case cartRecommendation(city: String, cartInfo: [[String: Any]])
    case orderList(brand: Int)
    case orderDetail(brand: Int, orderId: String)
    case couponList(deliveryType: String, orderBrand: String)
    case deleteAccount(password: String)
    case otpRequest(member_loginkey: String)
    case otpVerify(member_loginkey: String, otp: Int)
}

extension APIEndpoint: RequestBuilder {
    var urlRequest: URLRequest {
        var http_method: String = ""
        var url_str: String
        var api_token: String = ""
        var rb: Data = Data()
        
        switch self {
            case .menuList(let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "city": UserDefaults.standard.object(forKey: "AddressCity") as? String ?? "Jakarta Barat",
                    "member_phone": UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? "0",
//                    "city": "Jakarta Barat",
//                    "member_phone": "0",
                    "app_os": "ios",
                    "app_version": Int(Constants.APP_BUILD ?? "1") ?? 1
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "home/app"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .menuListByCategory(let category, let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "city": UserDefaults.standard.object(forKey: "AddressCity") as? String ?? "Jakarta Barat",
                    "member_phone": UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? "0",
                    "category": category,
                    "app_os": "ios",
                    "app_version": Int(Constants.APP_BUILD ?? "1") ?? 1
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/category"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .menuSearch(let search, let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "city": UserDefaults.standard.object(forKey: "AddressCity") as? String ?? "Jakarta Barat",
                    "menu_name": search
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/search"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .menuFavorite(let menuCode, let isFavorite):
                let body: [String: Any] = [
                    "member_phone": UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? "0",
                    "menu_code": menuCode,
                    "favorited": isFavorite
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/favorite"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .menuListFavorite(let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "member_phone": UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? "0",
                    "city": UserDefaults.standard.object(forKey: "AddressCity") as? String ?? "Jakarta Barat"
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/show/favorite"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .menuDetail(let menuCode, let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "city": brand == 2 ? "Jakarta Barat" : UserDefaults.standard.object(forKey: "AddressCity") as? String ?? "Jakarta Barat",
                    "member_phone": UserDefaults.standard.object(forKey: "AddressPhone") as? String ?? "0",
                    "menu_code": menuCode
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/detail"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .promo(let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "city": "Jakarta Barat",
                    "member_phone": UserDefaults.standard.object(forKey: "AddressPhone") as? String ?? "0",
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/promo"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .bannerDetail(let bannerID, let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "banner_id": bannerID
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "product/promo/detail"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userTokenRefresh(let token):
                let body: [String: Any] = [:]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "user/refreshtoken"
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userLogin(let user, let pass):
                let body: [String: Any] = [
                    "member_loginkey": user,
                    "member_password": pass,
                    "device_token": UserDefaults.standard.object(forKey: "FCMToken") as? String ?? "",
                    "brand": "JCO",
                    "company_id": "JID"
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "user/login"
                api_token   = Constants.API_TOKEN
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userCheckPhone(let hp):
                let body: [String: Any] = ["member_phone": hp]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "user/get"
                api_token   = "jco"
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userRegister(let name, let pass, let repass, let hp, let email):
                let body: [String: Any] = ["member_name": name, "member_email": email, "member_phone": hp, "member_phone2": "", "member_password": pass, "member_confpassword": repass, "member_ip": "1", "member_useragent": "1"]
                http_method = "POST"
                url_str     = Constants.API_URL_GO + "member/register"
                api_token   = Constants.API_TOKEN
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userProfile:
                http_method = "GET"
                url_str     = Constants.API_URL_GO + "member/me"
//                api_token   = Constants.API_TOKEN
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
            
            case .userJpoint:
                http_method = "GET"
                url_str     = Constants.API_URL_JCO + "jpoint/balance"
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
            
            case .userJpointMutation(let status, let page):
                http_method = "GET"
                url_str     = Constants.API_URL_JCO + "jpoint/mutation?point_status=" + String(status) + "&page=" + String(page)
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
                
            case .userUpdateProfile(let phone, let name, let gender):
                let body: [String: Any] = [
                    "member_phone": phone,
                    "member_phone2": "",
                    "member_name": name,
                    "member_gender": gender,
                    "member_photo": "no-photo.jpeg",
                    "member_dob": ""
                ]
                http_method = "PUT"
                url_str     = Constants.API_URL_GO + "member/update"
//                api_token   = Constants.API_TOKEN
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken") as! String)
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .userChangePassword(let pass, let newpass, let confpass):
                let body: [String: Any] = [
                    "member_password": pass,
                    "member_newpassword": newpass,
                    "member_confpassword": confpass
                ]
                http_method = "PUT"
                url_str     = Constants.API_URL_JCO + "user/changepassword"
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken") as! String)
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .userResetPassword(let user, let newpass, let confpass):
                let body: [String: Any] = [
                    "member_loginkey": user,
                    "member_newpassword": newpass,
                    "member_confpassword": confpass
                ]
                http_method = "PUT"
                url_str     = Constants.API_URL_JCO + "user/resetpassword"
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userAddressList(let hp):
                let body: [String: Any] = [
                    "member_phone": UserDefaults.standard.object(forKey: "MemberPhone") as? String ?? "0"
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "member/address/showall"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .userAddressNew(let memberHandphone, let addressLabel, let recipientName, let recipientPhone, let recipientPostcode, let recipientCity, let recipientAddress, let recipientAddressDetail, let latitude, let longitude):
                let body: [String: Any] = [
                   "member_phone": memberHandphone,
                   "address_label": addressLabel,
                   "recipient_name": recipientName,
                   "recipient_phone": recipientPhone,
                   "recipient_postcode": recipientPostcode,
                   "cityordistrict": recipientCity,
                   "address": recipientAddress,
                   "address_details": recipientAddressDetail,
                   "latitude": latitude,
                   "longitude": longitude
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "member/address/store"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .userAddressDelete(let addressID):
                let body: [String: Any] = [
                    "member_address_id": addressID
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "member/address/destroy"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
//            case .userLocationQuery(let location):
//                let locationUrl = location.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
//                http_method = "GET"
//                url_str     = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + (locationUrl ?? "") + "&fields=formatted_address,name,geometry&inputtype=textquery&key=" + Constants.MAP_API
        
            case .userLocationQuery(let location):
                let locationUrl = location.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                let body: [String: Any] = ["location": location]
                http_method = "POST"
                url_str     = "https://oxo.co.id/jco_controller/show_gmap_places"
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .citySearch(let search):
                let body: [String: Any] = ["brand": 1, "country": "indonesia", "city": search]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "city/search"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .getUserCity(let lat, let lng):
                let body: [String: Any] = ["brand": 1, "latitude": lat, "longitude": lng]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "outlet/location"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .outletSearch(let city, let brand):
                let body: [String: Any] = ["brand": brand, "city": city]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "outlet/city"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .outletNearest(let brand, let lat, let lng):
                let body: [String: Any] = ["brand": brand, "latitude": lat, "longitude": lng]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "outlet/location"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
        case .cartFilter(let brand, let city, let deliveryType, let cartOrderDetail, let useJpoint, let orderAddress, let orderAddressInfo, let orderLatitude, let orderLongitude, let couponCode, let orderBrand):
                let body: [String: Any] = [
                  "brand": brand,
                  "order_city": city,
                  "coupon_code": couponCode,
                  "order_payment": "credit_cards",
                  "delivery_type": deliveryType,
                  "order_brand": orderBrand,
                  "cart_info": cartOrderDetail,
                  "use_jpoint": useJpoint,
                  "order_address": orderAddress,
                  "order_address_info": orderAddressInfo,
                  "order_latitude": orderLatitude,
                  "order_longitude": orderLongitude
//                  "cart_info": [
//                    [
//                      "menu_code": "5111003",
//                      "menu_quantity": "10",
//                      "menu_detail": "asd asdsadasd"
//                    ],
//                    [
//                      "menu_code": "5113002",
//                      "menu_quantity": "5",
//                      "menu_detail": "asd asdsadasd"
//                    ]
//                  ]
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_ADM + "order/delivery"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                print("BODY CART FILTER \(body)")        
        case .cartAddOrder(let brand, let name, let email, let phone, let trxTime, let city, let address, let detailAddress, let postcode, let latitude, let longitude, let notes, let outletID, let outletCode, let orderDetail, let deliveryType, let orderPayment, let jpoint, let couponCode):
                let body: [String: Any] = [
//                    "brand"             : brand,
                    "order_type"        : "8",
                    "order_subtype"     : "2",
                    "payment_type"      : "3",
                    "delivery_type"     : deliveryType,
                    "member_name"       : name,
                    "member_email"      : UserDefaults.standard.object(forKey: "MemberEmail") ?? "",
                    "member_phone"      : UserDefaults.standard.object(forKey: "MemberPhone") ?? "",
                    "member_phone2"     : "",
                    "order_trx_time"    : trxTime,
                    "order_city"        : city,
                    "order_address"     : address,
                    "order_address_info": detailAddress,
                    "order_postcode"    : postcode,
                    "order_latitude"    : latitude,
                    "order_longitude"   : longitude,
                    "order_note"        : notes,
                    "order_company"     : "JID",
                    "order_brand"       : brand == 1 ? "JCO" : "JCOR",
                    "order_outlet_ID"   : outletID,
                    "order_outlet_code" : outletCode,
                    "order_ip"          : "::1",
                    "order_useragent"   : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36",
                    "order_detail"      : orderDetail,
                    "coupon_code": couponCode,
                    "order_payment": orderPayment,
                    "use_jpoint": jpoint,
                    "app_version": Int(Constants.APP_BUILD ?? "1") ?? 1,
                    "order_recipient_name": name,
                    "order_recipient_phone": phone
//                    "order_detail"      : [
//                      [
//                        "menu_code": "5111001",
//                        "menu_name": "Donuts ½ Lusin",
//                        "menu_quantity": "2",
//                        "material_unit_price": "48000",
//                        "material_sub_total": "96000",
//                        "menu_detail": "contoh detail pesan"
//                      ],
//                      [
//                        "menu_code": "5111002",
//                        "menu_name": "Donuts 1 Lusin",
//                        "menu_quantity": "3",
//                        "material_unit_price": "86000",
//                        "material_sub_total": "258000",
//                        "menu_detail": "API PESAN"
//                      ]
//                    ],
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "checkout/submit"
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken") as! String)
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .cartPayment(let orderId):
                let body: [String: Any] = ["order_id": orderId]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "checkout/payment"
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken") as! String)
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .cartCheckOut(let orderId):
                let body: [String: Any] = ["transaction_details": ["order_id": orderId, "gross_amount": 150000]]
                http_method = "POST"
                url_str     = Constants.API_URL_PAY + "transaction/jco/token/charge"
                api_token   = Constants.API_TOKEN
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            case .cartPaymentList:
                http_method = "GET"
                url_str     = Constants.API_URL_JCO + "payment/list/JID"
                api_token   = Constants.API_TOKEN_NEW
                
            case .cartRecommendation(let city, let cartInfo):
                let body: [String: Any] = [
                    "city": city,
                    "order_brand": "JCO",
                    "cart_info": cartInfo,
                    "app_os": "ios",
                    "app_version": Constants.APP_BUILD
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "menu/recommendation"
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .orderList(let brand):
                let body: [String: Any] = [
                    "brand": brand,
                    "member_email": UserDefaults.standard.object(forKey: "MemberEmail") as? String ?? "",
                    "city": "Jakarta Barat",
                    "lang": UserDefaults.standard.object(forKey: "AppLanguage") as? String ?? "id"
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_USR + "home/order/recent"
                api_token   = Constants.API_TOKEN_NEW
                rb          = try! JSONSerialization.data(withJSONObject: body)
                
            case .orderDetail(let brand, let orderId):
                let body: [String: Any] = [
                    "brand": brand,
                    "order_id": orderId,
                    "lang": UserDefaults.standard.object(forKey: "AppLanguage") as? String ?? "id"
                ]
                http_method = "POST"
                url_str     = Constants.API_URL_JCO + "order/detail"
                api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
                rb          = try! JSONSerialization.data(withJSONObject: body)
            
            
        case .couponList(let deliveryType, let orderBrand):
            let body: [String: Any] = [
                "platform": "2",
                "coupon_type": "2",
                "order_brand": orderBrand,
                "order_company": "JID",
                "delivery_type": deliveryType
            ]
            http_method = "POST"
            url_str     = Constants.API_URL_JCO + "coupon/coupon_list"
            api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
            rb          = try! JSONSerialization.data(withJSONObject: body)
            print("url \(url_str)")
            print("body \(body)")
            
        case .deleteAccount(let password):
            let body: [String: Any] = [
                "member_password": password,
                "member_ip": getIPAddress(),
                "member_useragent": WKWebView().value(forKey: "userAgent") ?? "",
                "lang": UserDefaults.standard.object(forKey: "AppLanguage") as? String ?? "id"
            ]
            
            http_method = "POST"
            url_str     = Constants.API_URL_JCO + "user/deleteaccount"
            api_token   = "Bearer " + (UserDefaults.standard.object(forKey: "AccessToken")  as? String ?? "")
            rb          = try! JSONSerialization.data(withJSONObject: body)
        case .otpRequest(let member_loginkey):
            let body: [String: Any] = [
                "member_loginkey": member_loginkey,
                "brand": "JCO",
                "lang": "id"
            ]
            
            http_method = "POST"
            url_str     = Constants.API_URL_JCO + "user/otprequest"
            rb          = try! JSONSerialization.data(withJSONObject: body)
        case .otpVerify(let member_loginkey, let otp):
            let body: [String: Any] = [
                "member_loginkey": member_loginkey,
                "otp": otp,
                "brand": "JCO",
                "lang": "id"
            ]
            
            http_method = "POST"
            url_str     = Constants.API_URL_JCO + "user/otpverify"
            rb          = try! JSONSerialization.data(withJSONObject: body)
        }
        
        guard let url = URL(string: url_str)
            else {preconditionFailure("Invalid URL format")}
        var req = URLRequest(url: url)
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        req.setValue(api_token, forHTTPHeaderField: "Authorization")
        req.setValue("Bearer " + String(UserDefaults.standard.object(forKey: "AccessToken") as? String ?? ""), forHTTPHeaderField: "accessToken")
        req.setValue(UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? "", forHTTPHeaderField: "x-refreshToken")
        req.httpMethod = http_method
        req.httpBody = rb
        return req
        
    }
    
    func getIPAddress() -> String {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }

                guard let interface = ptr?.pointee else { return "" }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) {

                    // wifi = ["en0"]
                    // wired = ["en2", "en3", "en4"]
                    // cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]

                    let name: String = String(cString: (interface.ifa_name))
                    if  name == "en0" || name == "en2" || name == "en3" || name == "en4" || name == "pdp_ip0" || name == "pdp_ip1" || name == "pdp_ip2" || name == "pdp_ip3" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address ?? ""
    }
}
