//
//  UserAddressListService.swift
//  Jco
//
//  Created by Ed on 03/04/21.
//

import Foundation
import Combine

protocol ModelService {
    var apiSession: APIService {get}
   
    func getUserCheckPhone(hp: String) -> AnyPublisher<UserCheckRes, APIError>
    func registerUser(name: String, pass: String, repass: String, hp: String, email: String) -> AnyPublisher<RegisterAPIResponse, APIError>
    func loginUser(user: String, pass: String) -> AnyPublisher<LoginAPIResponse, APIError>
    func getRefreshToken(token: String) -> AnyPublisher<RefreshTokenRes, APIError>
    func getJpoint() -> AnyPublisher<JpointRes, APIError>
    func getJpointMutation(status: Int, page: Int) -> AnyPublisher<JpointMutationRes, APIError>
    
    func getMenuList(brand: Int) -> AnyPublisher<MenuListAPIResponse, APIError>
    func getMenuListByCategory(category: String, brand: Int) -> AnyPublisher<MenuListByCategoryRes, APIError>
    func getMenuSearch(search: String, brand: Int) -> AnyPublisher<MenuListByCategoryRes, APIError>
    func getBannerDetail(bannerID: String, brand: Int) -> AnyPublisher<BannerDetailRes, APIError>
    func getUserAddressList(hp: String) -> AnyPublisher<UserAddressListAPIResponse, APIError>
    func getLocationQuery(location: String) -> AnyPublisher<LocationQueryRes, APIError>
    
    func getMenuFavorite(brand: Int) -> AnyPublisher<FavoriteGetRes, APIError>
    func setMenuFavorite(menuCode: String, isFavorite: Int) -> AnyPublisher<FavoriteSetRes, APIError>
    
    func getCity(search: String) -> AnyPublisher<CityRes, APIError>
    func getUserCity(lat: String, lng: String) -> AnyPublisher<UserCityRes, APIError>
    func getOutlet(city: String, brand: Int) -> AnyPublisher<OutletRes, APIError>
    func getOutletNearest(brand: Int, lat: String, lng: String) -> AnyPublisher<OutletNearestRes, APIError>
    
    func getOrder(brand: Int) -> AnyPublisher<OrderRes, APIError>
    func getPaymentList() -> AnyPublisher<PaymentListRes, APIError>
    func getOrderDetail(brand: Int, orderId: String) -> AnyPublisher<OrderDetailRes, APIError>
    
    func addUserAddress(memberHandphone: String,
                        addressLabel: String,
                        recipientName: String,
                        recipientPhone: String,
                        recipientPostcode: String,
                        recipientCity: String,
                        recipientAddress: String,
                        recipientAddressDetail: String,
                        latitude: String,
                        longitude: String
    ) -> AnyPublisher<UserAddressNewRes, APIError>
    
    func getCartFilter(brand: Int, city: String, deliveryType: Int, cartOrderDetail: [[String: Any]], useJpoint: Int, orderAddress: String, orderAddressInfo: String, orderLatitude: String, orderLongitude: String, couponCode: String, orderBrand: String) -> AnyPublisher<CartFilterRes, APIError>
    func getCartRecommendation(city: String, cartInfo: [[String: Any]]) -> AnyPublisher<CartRecRes, APIError>
    
    func addCartOrder(brand: Int, name: String, email: String, phone: String, trxTime: String, city: String, address: String, detailAddress: String, postcode: String, latitude: String, longitude: String, notes: String, outletID: String, outletCode: String, orderDetail: [[String: Any]], deliveryType: Int, orderPayment: String, jpoint: Int, couponCode: String) -> AnyPublisher<CartOrderRes, APIError>
    func addCartCheckOut(orderId: String) -> AnyPublisher<CartCheckOutRes, APIError>
    func addCartPayment(orderId: String) -> AnyPublisher<CartCheckOutRes, APIError>
    
    func updateProfile(phone: String, name: String, gender: Int) -> AnyPublisher<ProfileUpdateRes, APIError>
    func updatePassword(pass: String, newpass: String, confpass: String) -> AnyPublisher<PasswordUpdateRes, APIError>
    func resetPassword(user: String, newpass: String, confpass: String) -> AnyPublisher<PasswordUpdateRes, APIError>
    
    func getUserProfile() -> AnyPublisher<UserProfileRes, APIError>
    func getPromo(brand: Int) -> AnyPublisher<PromoRes, APIError>
    
    func deleteUserAddress(addressID: Int) -> AnyPublisher<DeleteAddressRes, APIError>
    func couponList(deliveryType: String, orderBrand: String) -> AnyPublisher<CouponRes, APIError>
    func deleteAccount(password: String) -> AnyPublisher<Response, APIError>
    func otpRequest(memberLoginKey: String) -> AnyPublisher<Response, APIError>
    func otpVerify(memberLoginKey: String, otp: Int) -> AnyPublisher<Response, APIError>
}

extension ModelService {
    // Read
    func getUserCheckPhone(hp: String) -> AnyPublisher<UserCheckRes, APIError> {
        return apiSession.request(with: APIEndpoint.userCheckPhone(hp: hp))
            .eraseToAnyPublisher()
    }
    func registerUser(name: String, pass: String, repass: String, hp: String, email: String) -> AnyPublisher<RegisterAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.userRegister(name: name, pass: pass, repass: repass, hp: hp, email: email))
            .eraseToAnyPublisher()
    }
    func loginUser(user: String, pass: String) -> AnyPublisher<LoginAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.userLogin(user: user, pass: pass))
            .eraseToAnyPublisher()
    }
    func getRefreshToken(token: String) -> AnyPublisher<RefreshTokenRes, APIError> {
        return apiSession.request(with: APIEndpoint.userTokenRefresh(token: token))
            .eraseToAnyPublisher()
    }
    func getJpoint() -> AnyPublisher<JpointRes, APIError> {
        return apiSession.request(with: APIEndpoint.userJpoint)
            .eraseToAnyPublisher()
    }
    func getJpointMutation(status: Int, page: Int) -> AnyPublisher<JpointMutationRes, APIError> {
        return apiSession.request(with: APIEndpoint.userJpointMutation(status: status, page: page))
            .eraseToAnyPublisher()
    }
    
    func getMenuList(brand: Int) -> AnyPublisher<MenuListAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.menuList(brand: brand))
            .eraseToAnyPublisher()
    }
    func getMenuListByCategory(category: String, brand: Int) -> AnyPublisher<MenuListByCategoryRes, APIError> {
        return apiSession.request(with: APIEndpoint.menuListByCategory(category: category, brand: brand))
            .eraseToAnyPublisher()
    }
    func getMenuSearch(search: String, brand: Int) -> AnyPublisher<MenuListByCategoryRes, APIError> {
        return apiSession.request(with: APIEndpoint.menuSearch(search: search, brand: brand))
            .eraseToAnyPublisher()
    }
    func getBannerDetail(bannerID: String, brand: Int) -> AnyPublisher<BannerDetailRes, APIError> {
        return apiSession.request(with: APIEndpoint.bannerDetail(bannerID: bannerID, brand: brand))
            .eraseToAnyPublisher()
    }
    
    func getMenuFavorite(brand: Int) -> AnyPublisher<FavoriteGetRes, APIError> {
        return apiSession.request(with: APIEndpoint.menuListFavorite(brand: brand))
            .eraseToAnyPublisher()
    }
    func setMenuFavorite(menuCode: String, isFavorite: Int) -> AnyPublisher<FavoriteSetRes, APIError> {
        return apiSession.request(with: APIEndpoint.menuFavorite(menuCode: menuCode, isFavorite: isFavorite))
            .eraseToAnyPublisher()
    }
    
    func getUserAddressList(hp: String) -> AnyPublisher<UserAddressListAPIResponse, APIError> {
        return apiSession.request(with: APIEndpoint.userAddressList(hp: hp))
            .eraseToAnyPublisher()
    }
    func getLocationQuery(location: String) -> AnyPublisher<LocationQueryRes, APIError> {
        return apiSession.request(with: APIEndpoint.userLocationQuery(location: location))
            .eraseToAnyPublisher()
    }
    func getCity(search: String) -> AnyPublisher<CityRes, APIError> {
        return apiSession.request(with: APIEndpoint.citySearch(search: search))
            .eraseToAnyPublisher()
    }
    func getUserCity(lat: String, lng: String) -> AnyPublisher<UserCityRes, APIError> {
        return apiSession.request(with: APIEndpoint.getUserCity(lat: lat, lng: lng))
            .eraseToAnyPublisher()
    }
    func getOutlet(city: String, brand: Int) -> AnyPublisher<OutletRes, APIError> {
        return apiSession.request(with: APIEndpoint.outletSearch(city: city, brand: brand))
            .eraseToAnyPublisher()
    }
    func getPaymentList() -> AnyPublisher<PaymentListRes, APIError> {
        return apiSession.request(with: APIEndpoint.cartPaymentList)
            .eraseToAnyPublisher()
    }
    func getOrder(brand: Int) -> AnyPublisher<OrderRes, APIError> {
        return apiSession.request(with: APIEndpoint.orderList(brand: brand))
            .eraseToAnyPublisher()
    }
    func getOrderDetail(brand: Int, orderId: String) -> AnyPublisher<OrderDetailRes, APIError> {
        return apiSession.request(with: APIEndpoint.orderDetail(brand: brand, orderId: orderId))
            .eraseToAnyPublisher()
    }
    func getOutletNearest(brand: Int, lat: String, lng: String) -> AnyPublisher<OutletNearestRes, APIError> {
        return apiSession.request(with: APIEndpoint.outletNearest(brand: brand, lat: lat, lng: lng))
            .eraseToAnyPublisher()
    }
    
    // Create
    func addUserAddress(memberHandphone: String,
                        addressLabel: String,
                        recipientName: String,
                        recipientPhone: String,
                        recipientPostcode: String,
                        recipientCity: String,
                        recipientAddress: String,
                        recipientAddressDetail: String,
                        latitude: String,
                        longitude: String) -> AnyPublisher<UserAddressNewRes, APIError> {
        return apiSession.request(with: APIEndpoint.userAddressNew(memberHandphone: memberHandphone,
                                                                    addressLabel: addressLabel,
                                                                    recipientName: recipientName,
                                                                    recipientPhone: recipientPhone,
                                                                    recipientPostcode: recipientPostcode,
                                                                    recipientCity: recipientCity,
                                                                    recipientAddress: recipientAddress,
                                                                    recipientAddressDetail: recipientAddressDetail,
                                                                    latitude: latitude,
                                                                    longitude: longitude))
            .eraseToAnyPublisher()
    }
    
    func getCartFilter(brand: Int, city: String, deliveryType: Int, cartOrderDetail: [[String: Any]], useJpoint: Int, orderAddress: String, orderAddressInfo: String, orderLatitude: String, orderLongitude: String, couponCode: String, orderBrand: String) -> AnyPublisher<CartFilterRes, APIError> {
        return apiSession.request(with: APIEndpoint.cartFilter(brand: brand, city: city, deliveryType: deliveryType, cartOrderDetail: cartOrderDetail, useJpoint: useJpoint, orderAddress: orderAddress, orderAddressInfo: orderAddressInfo, orderLatitude: orderLatitude, orderLongitude: orderLongitude, couponCode: couponCode, orderBrand: orderBrand))
            .eraseToAnyPublisher()
    }
    func getCartRecommendation(city: String, cartInfo: [[String: Any]]) -> AnyPublisher<CartRecRes, APIError> {
        return apiSession.request(with: APIEndpoint.cartRecommendation(city: city, cartInfo: cartInfo))
            .eraseToAnyPublisher()
    }
    func addCartOrder(brand: Int, name: String, email: String, phone: String, trxTime: String, city: String, address: String, detailAddress: String, postcode: String, latitude: String, longitude: String, notes: String, outletID: String, outletCode: String, orderDetail: [[String: Any]], deliveryType: Int, orderPayment: String, jpoint: Int, couponCode: String) -> AnyPublisher<CartOrderRes, APIError> {
        return apiSession.request(with: APIEndpoint.cartAddOrder(
            brand: brand,
            name: name,
            email: email,
            phone: phone,
            trxTime: trxTime,
            city: city,
            address: address,
            detailAddress: detailAddress,
            postcode: postcode,
            latitude: latitude,
            longitude: longitude,
            notes: notes,
            outletID: outletID,
            outletCode: outletCode,
            orderDetail: orderDetail,
            deliveryType: deliveryType,
            orderPayment: orderPayment,
            jpoint: jpoint,
            couponCode: couponCode
        )).eraseToAnyPublisher()
    }
    func addCartCheckOut(orderId: String) -> AnyPublisher<CartCheckOutRes, APIError> {
        return apiSession.request(with: APIEndpoint.cartCheckOut(orderId: orderId))
            .eraseToAnyPublisher()
    }
    func addCartPayment(orderId: String) -> AnyPublisher<CartCheckOutRes, APIError> {
        return apiSession.request(with: APIEndpoint.cartPayment(orderId: orderId))
            .eraseToAnyPublisher()
    }
    func updateProfile(phone: String, name: String, gender: Int) -> AnyPublisher<ProfileUpdateRes, APIError> {
        return apiSession.request(with: APIEndpoint.userUpdateProfile(phone: phone, name: name, gender: gender))
            .eraseToAnyPublisher()
    }
    func updatePassword(pass: String, newpass: String, confpass: String) -> AnyPublisher<PasswordUpdateRes, APIError> {
        return apiSession.request(with: APIEndpoint.userChangePassword(pass: pass, newpass: newpass, confpass: confpass))
            .eraseToAnyPublisher()
    }
    func resetPassword(user: String, newpass: String, confpass: String) -> AnyPublisher<PasswordUpdateRes, APIError> {
        return apiSession.request(with: APIEndpoint.userResetPassword(user: user, newpass: newpass, confpass: confpass))
            .eraseToAnyPublisher()
    }
    
    func getUserProfile() -> AnyPublisher<UserProfileRes, APIError> {
        return apiSession.request(with: APIEndpoint.userProfile)
            .eraseToAnyPublisher()
    }
    
    func getPromo(brand: Int) -> AnyPublisher<PromoRes, APIError> {
        return apiSession.request(with: APIEndpoint.promo(brand: brand)).eraseToAnyPublisher()
    }
    
    func deleteUserAddress(addressID: Int) -> AnyPublisher<DeleteAddressRes, APIError> {
        return apiSession.request(with: APIEndpoint.userAddressDelete(addressID: addressID))
            .eraseToAnyPublisher()
    }
    
    func couponList(deliveryType: String, orderBrand: String) -> AnyPublisher<CouponRes, APIError> {
        return apiSession.request(with: APIEndpoint.couponList(deliveryType: deliveryType, orderBrand: orderBrand))
            .eraseToAnyPublisher()
    }
    
    func deleteAccount(password: String) -> AnyPublisher<Response, APIError> {
        return apiSession.request(with: APIEndpoint.deleteAccount(password: password))
            .eraseToAnyPublisher()
    }
    
    func otpRequest(memberLoginKey: String) -> AnyPublisher<Response, APIError> {
        return apiSession.request(with: APIEndpoint.otpRequest(member_loginkey: memberLoginKey))
            .eraseToAnyPublisher()
    }
    
    func otpVerify(memberLoginKey: String, otp: Int) -> AnyPublisher<Response, APIError> {
        return apiSession.request(with: APIEndpoint.otpVerify(member_loginkey: memberLoginKey, otp: otp))
            .eraseToAnyPublisher()
    }
}
