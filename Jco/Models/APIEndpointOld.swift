//
//  APIEndpoint.swift
//  Jco
//
//  Created by Ed on 23/01/21.
//

import Foundation

enum APIEndpointOld {
    case menuList
    case menuListByCategory(category: String)
    case menuDetail(menuCode: String)
    
    case userTokenRefresh(token: String)
    case userLogin(user: String, pass: String)
    case userRegister(name: String, pass: String, repass: String, hp: String, email: String)
    case userAddressList(hp: String)
    case userAddressNew(memberHandphone: String, addressLabel: String, recipientName: String, recipientPhone: String, recipientPostcode: String, recipientCity: String, recipientAddress: String, recipientAddressDetail: String, latitude: String, longitude: String)
    case userLocationQuery(location: String)
    case citySearch(search: String)
    case getUserCity(lat: String, lng: String)
    case outletSearch(city: String)
    
    case cartAddOrder(email: String, phone: String)
    case cartCheckOut(orderId: String)
    case orderList
    case orderDetail(orderId: String)
}

extension APIEndpointOld: RequestBuilder {
    var urlRequest: URLRequest {
        switch self {
            case .menuList:
                guard let url = URL(string: Constants.API_URL_USR + "home/app")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN_NEW, forHTTPHeaderField: "Authorization")
                request.setValue("Bearer " + String(UserDefaults.standard.object(forKey: "AccessToken") as? String ?? ""), forHTTPHeaderField: "accessToken")
                request.setValue(UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? "", forHTTPHeaderField: "x-refreshToken")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["member_phone": "0", "city": "Jakarta Barat", "brand": 1]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .menuListByCategory(let category):
                guard let url = URL(string: "https://jcouserapidev.oxxo.co.id/product/category")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["member_phone": "0900010000", "city": "Jakarta Barat", "brand": 1, "category": category]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .menuDetail(let menuCode):
                guard let url = URL(string: "https://jcouserapidev.oxxo.co.id/product/detail")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["brand": 1, "city": "Jakarta Barat", "menu_code": menuCode, "member_phone": "0900010000"]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
                
            case .userTokenRefresh(let token):
                guard let url = URL(string: "https://api.test.jcodelivery.com/v1/user/refreshtoken")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue(token, forHTTPHeaderField: "x-refreshToken")
                request.httpMethod = "POST"

                let rb = try! JSONSerialization.data(withJSONObject: [])
                request.httpBody = rb
                
                return request
            case .userLogin(let user, let pass):
                guard let url = URL(string: "https://jcousergolangdev.oxxo.co.id/member/login")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["member_loginkey": user, "member_password": pass]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .userRegister(let name, let pass, let repass, let hp, let email):
                guard let url = URL(string: "https://jcousergolangdev.oxxo.co.id/member/register")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["member_name": name, "member_email": email, "member_phone": hp, "member_phone2": "", "member_password": pass, "member_confpassword": repass, "member_ip": "1", "member_useragent": "1"]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .userAddressList(let hp):
                guard let url = URL(string: "https://jcouserapidev.oxxo.co.id/member/address/showall")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["member_phone": hp]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .userAddressNew(let memberHandphone, let addressLabel, let recipientName, let recipientPhone, let recipientPostcode, let recipientCity, let recipientAddress, let recipientAddressDetail, let latitude, let longitude):
                guard let url = URL(string: Constants.API_URL_USR + "member/address/store")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["member_phone": memberHandphone,
                                           "address_label": addressLabel,
                                           "recipient_name": recipientName,
                                           "recipient_phone": recipientPhone,
                                           "recipient_postcode": recipientPostcode,
                                           "cityordistrict": recipientCity,
                                           "address": recipientAddress,
                                           "address_details": recipientAddressDetail,
                                           "latitude": latitude,
                                           "longitude": longitude]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .userLocationQuery(let location):
                let locationUrl = location.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                guard let url = URL(string: "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + (locationUrl ?? "") + "&fields=formatted_address,name,geometry&inputtype=textquery&key=" + Constants.MAP_API)
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                return request
            case .citySearch(let search):
                guard let url = URL(string: "https://jcouserapidev.oxxo.co.id/city/search")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["brand": 1, "country": "indonesia", "city": search]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                return request
            case .getUserCity(let lat, let lng):
                guard let url = URL(string: Constants.API_URL_USR +  "outlet/location")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["brand": 1, "latitude": lat, "longitude": lng]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                return request
            case .outletSearch(let city):
                guard let url = URL(string: "https://jcouserapidev.oxxo.co.id/outlet/city")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["brand": 1, "city": city]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                return request
            case .cartAddOrder(let email, let phone):
                guard let url = URL(string: "https://jcoadminapidev.oxxo.co.id/order/store")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.setValue(UserDefaults.standard.object(forKey: "AccessToken") as? String ?? "", forHTTPHeaderField: "accessToken")
                request.setValue(UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? "", forHTTPHeaderField: "x-refreshToken")
                request.httpMethod = "POST"
                
                let body: [String: Any] = [
                    "brand": 1,
                    "order_type": "8",
                    "payment_type": "3",
                    "delivery_type": 0,
                    "member_name": "Member JCO",
                    "member_email": "ronals@mail.com",
                    "member_phone": "9000100002",
                    "member_phone2": "",
                    "order_trx_time": "2021-07-15 12:01:00",
                    "order_city": "Jakarta Barat",
                    "order_address": "Jl. Meruya Selatan No.68, RT.9/RW.7, Meruya Sel., Kec. Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11620",
                    "order_address_info": "Detail pengiriman API",
                    "order_postcode": "11620",
                    "order_latitude": "-6.20073720",
                    "order_longitude": "106.74090380",
                    "order_note": "Lorem Ipsum dolor sit amet. Testing for order note order",
                    "order_company": "JID",
                    "order_brand": "JCO",
                    "order_outlet_ID": "J265",
                    "order_outlet_code": "JCO PUR2",
                    "order_ip": "::1",
                    "order_useragent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36",
                    "order_detail": [
                      [
                        "menu_code": "5111001",
                        "menu_name": "Donuts ½ Lusin",
                        "menu_quantity": "2",
                        "material_unit_price": "48000",
                        "material_sub_total": "96000",
                        "menu_detail": "contoh detail pesan"
                      ],
                      [
                        "menu_code": "5111002",
                        "menu_name": "Donuts 1 Lusin",
                        "menu_quantity": "3",
                        "material_unit_price": "86000",
                        "material_sub_total": "258000",
                        "menu_detail": "API PESAN"
                      ]
                    ],
                    "coupon_code": "",
                    "order_payment": "bca"
                ]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .cartCheckOut(let orderId):
                guard let url = URL(string: "https://jcopaymentapidev.oxxo.co.id/transaction/jco/token/charge")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
                request.httpMethod = "POST"
                
                let body: [String: Any] = ["transaction_details": ["order_id": orderId, "gross_amount": 150000]]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
            case .orderList:
                guard let url = URL(string: Constants.API_URL_USR + "home/order/recent")
                    else {preconditionFailure("Invalid URL format")}
                var request = URLRequest(url: url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue(Constants.API_TOKEN_NEW, forHTTPHeaderField: "Authorization")
                request.setValue(UserDefaults.standard.object(forKey: "AccessToken") as? String ?? "", forHTTPHeaderField: "accessToken")
                request.setValue(UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? "", forHTTPHeaderField: "x-refreshToken")
                request.httpMethod = "POST"
                
                let body: [String: Any] = [
                    "brand": 1,
                    "member_email": UserDefaults.standard.object(forKey: "MemberEmail") as? String ?? "",
                    "city": "Jakarta Barat"
                ]
                let rb = try! JSONSerialization.data(withJSONObject: body)
                request.httpBody = rb
                
                return request
        case .orderDetail(let orderId):
            guard let url = URL(string: Constants.API_URL_ADM + "order/detail")
                else {preconditionFailure("Invalid URL format")}
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(Constants.API_TOKEN, forHTTPHeaderField: "Authorization")
            request.setValue(UserDefaults.standard.object(forKey: "AccessToken") as? String ?? "", forHTTPHeaderField: "accessToken")
            request.setValue(UserDefaults.standard.object(forKey: "RefreshToken") as? String ?? "", forHTTPHeaderField: "x-refreshToken")
            request.httpMethod = "POST"
            
            let body: [String: Any] = [
                "brand": 1,
                "order_id": orderId
            ]
            let rb = try! JSONSerialization.data(withJSONObject: body)
            request.httpBody = rb
            
            return request
        }
        
    }
}
